﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Photon.SocketServer.Security.OakleyGroups::.cctor()
extern void OakleyGroups__cctor_mEE487C19564510344780DB28FCF08FAB66BD1349 (void);
// 0x00000002 System.Byte[] Photon.SocketServer.Security.ICryptoProvider::get_PublicKey()
// 0x00000003 System.Void Photon.SocketServer.Security.ICryptoProvider::DeriveSharedKey(System.Byte[])
// 0x00000004 System.Byte[] Photon.SocketServer.Security.ICryptoProvider::Encrypt(System.Byte[],System.Int32,System.Int32)
// 0x00000005 System.Byte[] Photon.SocketServer.Security.ICryptoProvider::Decrypt(System.Byte[],System.Int32,System.Int32)
// 0x00000006 System.Void Photon.SocketServer.Security.DiffieHellmanCryptoProvider::.ctor()
extern void DiffieHellmanCryptoProvider__ctor_mEED5CF4E94D4A37E43788595543E564166BBF03B (void);
// 0x00000007 System.Void Photon.SocketServer.Security.DiffieHellmanCryptoProvider::.ctor(System.Byte[])
extern void DiffieHellmanCryptoProvider__ctor_mA45E9BF1BECADA59F6EE5F2985F647C0A8D01798 (void);
// 0x00000008 System.Byte[] Photon.SocketServer.Security.DiffieHellmanCryptoProvider::get_PublicKey()
extern void DiffieHellmanCryptoProvider_get_PublicKey_mAF51FCC008349D1349720A851804320733B1EC70 (void);
// 0x00000009 System.Void Photon.SocketServer.Security.DiffieHellmanCryptoProvider::DeriveSharedKey(System.Byte[])
extern void DiffieHellmanCryptoProvider_DeriveSharedKey_mCEA2A1A47E14CC5783308E54AAEEADC062D43A07 (void);
// 0x0000000A System.Byte[] Photon.SocketServer.Security.DiffieHellmanCryptoProvider::Encrypt(System.Byte[],System.Int32,System.Int32)
extern void DiffieHellmanCryptoProvider_Encrypt_mC705B8A8A95291E25AAE17E63C33296034436643 (void);
// 0x0000000B System.Byte[] Photon.SocketServer.Security.DiffieHellmanCryptoProvider::Decrypt(System.Byte[],System.Int32,System.Int32)
extern void DiffieHellmanCryptoProvider_Decrypt_mD2AA88976027E9643AE10FFCA1B21872EEF69595 (void);
// 0x0000000C System.Void Photon.SocketServer.Security.DiffieHellmanCryptoProvider::Dispose()
extern void DiffieHellmanCryptoProvider_Dispose_m898DBFC6BB6B3CBB1CF867D78BBC230F02329183 (void);
// 0x0000000D System.Void Photon.SocketServer.Security.DiffieHellmanCryptoProvider::Dispose(System.Boolean)
extern void DiffieHellmanCryptoProvider_Dispose_mFA5FAD398D484DA18926BE3D2F740660CD804082 (void);
// 0x0000000E Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Security.DiffieHellmanCryptoProvider::CalculatePublicKey()
extern void DiffieHellmanCryptoProvider_CalculatePublicKey_m418F07D4B6CD32A81BC097A96D2F6AFFA588F322 (void);
// 0x0000000F Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Security.DiffieHellmanCryptoProvider::CalculateSharedKey(Photon.SocketServer.Numeric.BigInteger)
extern void DiffieHellmanCryptoProvider_CalculateSharedKey_mBD7C289D60270BF2020FC6960997D9861F37725D (void);
// 0x00000010 Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Security.DiffieHellmanCryptoProvider::GenerateRandomSecret(System.Int32)
extern void DiffieHellmanCryptoProvider_GenerateRandomSecret_mE238E890E4BB6E476FB84637BB949FE61AAE0502 (void);
// 0x00000011 System.Void Photon.SocketServer.Security.DiffieHellmanCryptoProvider::.cctor()
extern void DiffieHellmanCryptoProvider__cctor_m34DA563DA4E57F1210EC7B62DD164A9B57EAFC88 (void);
// 0x00000012 System.Void Photon.SocketServer.Numeric.BigInteger::.ctor()
extern void BigInteger__ctor_m7654A4E955F4DBF085DB6D6D7CA2110303BC43AC (void);
// 0x00000013 System.Void Photon.SocketServer.Numeric.BigInteger::.ctor(System.Int64)
extern void BigInteger__ctor_m20A30083A85B7F65F08565E68BCA041C63CA9260 (void);
// 0x00000014 System.Void Photon.SocketServer.Numeric.BigInteger::.ctor(Photon.SocketServer.Numeric.BigInteger)
extern void BigInteger__ctor_m9D56B47AAEB7FBF25260773E6663A0EB85842597 (void);
// 0x00000015 System.Void Photon.SocketServer.Numeric.BigInteger::.ctor(System.Byte[])
extern void BigInteger__ctor_mFB53F6C2CD5B373EC0B478D58156642DA9FBA61E (void);
// 0x00000016 System.Void Photon.SocketServer.Numeric.BigInteger::.ctor(System.UInt32[])
extern void BigInteger__ctor_mDD9AE78E88E60EC3FF7C4C0178B5E2748BE00A99 (void);
// 0x00000017 Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Numeric.BigInteger::op_Implicit(System.Int64)
extern void BigInteger_op_Implicit_m038D734EA9CFEEFC334278161B8182005BFE2C4A (void);
// 0x00000018 Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Numeric.BigInteger::op_Implicit(System.Int32)
extern void BigInteger_op_Implicit_mE7777641390753B556FF1FD504B3A7F2FEE43240 (void);
// 0x00000019 Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Numeric.BigInteger::op_Addition(Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger)
extern void BigInteger_op_Addition_m959824F8E3F87297FB175D21FC404CD403798C4B (void);
// 0x0000001A Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Numeric.BigInteger::op_Subtraction(Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger)
extern void BigInteger_op_Subtraction_m871FBEDE8026A6E50B21157D741CF729DFDB6F8E (void);
// 0x0000001B Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Numeric.BigInteger::op_Multiply(Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger)
extern void BigInteger_op_Multiply_m0717827BA7EB4732DCD2C82ADE1FCCDCE723C8F6 (void);
// 0x0000001C Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Numeric.BigInteger::op_LeftShift(Photon.SocketServer.Numeric.BigInteger,System.Int32)
extern void BigInteger_op_LeftShift_m36C249FFD4E33FB167D14EA1040B2A51A57136FC (void);
// 0x0000001D System.Int32 Photon.SocketServer.Numeric.BigInteger::shiftLeft(System.UInt32[],System.Int32)
extern void BigInteger_shiftLeft_mC81AD034750BF1A10B3B73A5951A8F6602061BAE (void);
// 0x0000001E System.Int32 Photon.SocketServer.Numeric.BigInteger::shiftRight(System.UInt32[],System.Int32)
extern void BigInteger_shiftRight_mC0A7A08C847F07B5576F66F4AE693E8903D7C00F (void);
// 0x0000001F Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Numeric.BigInteger::op_UnaryNegation(Photon.SocketServer.Numeric.BigInteger)
extern void BigInteger_op_UnaryNegation_m1C37D388C5FABA9880A3CCF9ACDAEA377A045882 (void);
// 0x00000020 System.Boolean Photon.SocketServer.Numeric.BigInteger::op_Equality(Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger)
extern void BigInteger_op_Equality_m6474ADCED8364C13284E89AAC2DCD9A5DDBB1418 (void);
// 0x00000021 System.Boolean Photon.SocketServer.Numeric.BigInteger::Equals(System.Object)
extern void BigInteger_Equals_mFC4BD77E14958DC59F038CA4B57DDD936668D199 (void);
// 0x00000022 System.Int32 Photon.SocketServer.Numeric.BigInteger::GetHashCode()
extern void BigInteger_GetHashCode_m26DA903494055B79A9B4C3E2F1FC3D9C06E15C91 (void);
// 0x00000023 System.Boolean Photon.SocketServer.Numeric.BigInteger::op_GreaterThan(Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger)
extern void BigInteger_op_GreaterThan_m12DE07EBCD6C50D7463DCD2DCE516B0805CB6796 (void);
// 0x00000024 System.Boolean Photon.SocketServer.Numeric.BigInteger::op_LessThan(Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger)
extern void BigInteger_op_LessThan_m9A2B88AB5E5F6E5C8BB5F8192C5945357B9DEAF3 (void);
// 0x00000025 System.Boolean Photon.SocketServer.Numeric.BigInteger::op_GreaterThanOrEqual(Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger)
extern void BigInteger_op_GreaterThanOrEqual_mD7CEA0FF6F33C684954E9ADDD0A6EC8CEFF0A033 (void);
// 0x00000026 System.Void Photon.SocketServer.Numeric.BigInteger::multiByteDivide(Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger)
extern void BigInteger_multiByteDivide_m6B16471D0E3E03A6E1AFCB8D7A883E0606F9FAC1 (void);
// 0x00000027 System.Void Photon.SocketServer.Numeric.BigInteger::singleByteDivide(Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger)
extern void BigInteger_singleByteDivide_mCC1B96C9A65800761B36F0C92BB5A8F1E925DF49 (void);
// 0x00000028 Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Numeric.BigInteger::op_Division(Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger)
extern void BigInteger_op_Division_m53B0EFEBFEF15410DCA583D08BE8315AF3BFBF51 (void);
// 0x00000029 Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Numeric.BigInteger::op_Modulus(Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger)
extern void BigInteger_op_Modulus_mDF8ADED75CF0AD36F0F27D817E8D55D8D966F3B1 (void);
// 0x0000002A System.String Photon.SocketServer.Numeric.BigInteger::ToString()
extern void BigInteger_ToString_mAA9CCDF4D2E91A19083BA449C7DE7E3F2B4A5F0C (void);
// 0x0000002B System.String Photon.SocketServer.Numeric.BigInteger::ToString(System.Int32)
extern void BigInteger_ToString_m40EC68B17522B4790FE2A7A9444011C8864CCC31 (void);
// 0x0000002C Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Numeric.BigInteger::ModPow(Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger)
extern void BigInteger_ModPow_m1BCAF6A2734663F10C5D5C419014EB8272571990 (void);
// 0x0000002D Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Numeric.BigInteger::BarrettReduction(Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger)
extern void BigInteger_BarrettReduction_mEFDCB07965CC0195F8F54F9E7396019F2C7F2693 (void);
// 0x0000002E Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Numeric.BigInteger::GenerateRandom(System.Int32)
extern void BigInteger_GenerateRandom_mBA6450672822064BA5A03ABD43C88D479FBF3A0D (void);
// 0x0000002F System.Void Photon.SocketServer.Numeric.BigInteger::genRandomBits(System.Int32,System.Random)
extern void BigInteger_genRandomBits_mEC777F07AF82405BD5288EB485C22419C25270EC (void);
// 0x00000030 System.Int32 Photon.SocketServer.Numeric.BigInteger::bitCount()
extern void BigInteger_bitCount_m4F5EBDC11660A1A189B57152873565AC776C822B (void);
// 0x00000031 System.Byte[] Photon.SocketServer.Numeric.BigInteger::GetBytes()
extern void BigInteger_GetBytes_m992F27AD584544DE44102032C68E6FBC2905A079 (void);
// 0x00000032 System.Void Photon.SocketServer.Numeric.BigInteger::.cctor()
extern void BigInteger__cctor_m99653F6665C22C6B042412EA0220BA757D80A6BB (void);
// 0x00000033 System.Void ExitGames.Client.Photon.Version::.cctor()
extern void Version__cctor_m14E6C97EAB505D37977A7CD87D8C91204E5F8E26 (void);
// 0x00000034 System.Void ExitGames.Client.Photon.Hashtable::.ctor()
extern void Hashtable__ctor_mF9D32F7643B5F4BC034869DF7FED7FD9B856007F (void);
// 0x00000035 System.Void ExitGames.Client.Photon.Hashtable::.ctor(System.Int32)
extern void Hashtable__ctor_m21C3D1A4D13C0E47F103016CE15919602D9B8CC2 (void);
// 0x00000036 System.Object ExitGames.Client.Photon.Hashtable::get_Item(System.Object)
extern void Hashtable_get_Item_m9D94443F944580E7D54903CE54335A089D9F7122 (void);
// 0x00000037 System.Void ExitGames.Client.Photon.Hashtable::set_Item(System.Object,System.Object)
extern void Hashtable_set_Item_m814AA0E55EEF6ED7A59D56BDE4ACB784BBA20331 (void);
// 0x00000038 System.Collections.Generic.IEnumerator`1<System.Collections.DictionaryEntry> ExitGames.Client.Photon.Hashtable::GetEnumerator()
extern void Hashtable_GetEnumerator_m32CC484CB5A95C9FD49C8A307FAB2915D296F6D0 (void);
// 0x00000039 System.String ExitGames.Client.Photon.Hashtable::ToString()
extern void Hashtable_ToString_mC77A49741434C7C3EC6BAE41C487E4E411D53BB0 (void);
// 0x0000003A System.Void ExitGames.Client.Photon.DictionaryEntryEnumerator::.ctor(System.Collections.IDictionaryEnumerator)
extern void DictionaryEntryEnumerator__ctor_mDE026E9865C77B8C210DDB5E5079C931D129E6BD (void);
// 0x0000003B System.Boolean ExitGames.Client.Photon.DictionaryEntryEnumerator::MoveNext()
extern void DictionaryEntryEnumerator_MoveNext_m6ED263E5E8A9F2D244F07DE5F7A8E51A0472D6FC (void);
// 0x0000003C System.Void ExitGames.Client.Photon.DictionaryEntryEnumerator::Reset()
extern void DictionaryEntryEnumerator_Reset_m54702804DD8E087499C67EF82BB126F9A52CAE40 (void);
// 0x0000003D System.Object ExitGames.Client.Photon.DictionaryEntryEnumerator::System.Collections.IEnumerator.get_Current()
extern void DictionaryEntryEnumerator_System_Collections_IEnumerator_get_Current_mECCDD932F3824EF512FF7799A5D357C92073C458 (void);
// 0x0000003E System.Collections.DictionaryEntry ExitGames.Client.Photon.DictionaryEntryEnumerator::get_Current()
extern void DictionaryEntryEnumerator_get_Current_mE9A5C61240977483C92C60B017B018D755A3C127 (void);
// 0x0000003F System.Void ExitGames.Client.Photon.DictionaryEntryEnumerator::Dispose()
extern void DictionaryEntryEnumerator_Dispose_mF7375EAA50473DABBEE6DD2B65402298431E31BB (void);
// 0x00000040 System.Collections.Generic.List`1<System.Reflection.MethodInfo> ExitGames.Client.Photon.SupportClass::GetMethods(System.Type,System.Type)
extern void SupportClass_GetMethods_mA1DAB55A43B24B1DA454AFBB244B4D16241100C2 (void);
// 0x00000041 System.Int32 ExitGames.Client.Photon.SupportClass::GetTickCount()
extern void SupportClass_GetTickCount_m508AB440648DF77ECB5D775076706830756D0A6E (void);
// 0x00000042 System.Byte ExitGames.Client.Photon.SupportClass::CallInBackground(System.Func`1<System.Boolean>,System.Int32,System.String)
extern void SupportClass_CallInBackground_mFC6750DF3A3DC7C9F679C697D47FF7E43C9898D0 (void);
// 0x00000043 System.Byte ExitGames.Client.Photon.SupportClass::StartBackgroundCalls(System.Func`1<System.Boolean>,System.Int32,System.String)
extern void SupportClass_StartBackgroundCalls_m83CE203DE0AC119B61A5120825CB3BEE7BBF4FC6 (void);
// 0x00000044 System.Boolean ExitGames.Client.Photon.SupportClass::StopAllBackgroundCalls()
extern void SupportClass_StopAllBackgroundCalls_m1A720D56F587D06B4A07EA23C85E9160776F67D2 (void);
// 0x00000045 System.Void ExitGames.Client.Photon.SupportClass::WriteStackTrace(System.Exception,System.IO.TextWriter)
extern void SupportClass_WriteStackTrace_m9D1ED09B80B784F3AD3E84DC6087AC4AE3AB1C78 (void);
// 0x00000046 System.Void ExitGames.Client.Photon.SupportClass::WriteStackTrace(System.Exception)
extern void SupportClass_WriteStackTrace_m28141655ED044AE33F34B6CD49B2F85E17AA00ED (void);
// 0x00000047 System.String ExitGames.Client.Photon.SupportClass::DictionaryToString(System.Collections.IDictionary)
extern void SupportClass_DictionaryToString_mE8C789DBC4CF65223825BDF90C948E1D9D992965 (void);
// 0x00000048 System.String ExitGames.Client.Photon.SupportClass::DictionaryToString(System.Collections.IDictionary,System.Boolean)
extern void SupportClass_DictionaryToString_m055DA340910A84B9DAD8C7096617989787BADA7D (void);
// 0x00000049 System.String ExitGames.Client.Photon.SupportClass::ByteArrayToString(System.Byte[])
extern void SupportClass_ByteArrayToString_mF5A56EBB889C26AE344E3FB35A2A290CD43F1793 (void);
// 0x0000004A System.UInt32[] ExitGames.Client.Photon.SupportClass::InitializeTable(System.UInt32)
extern void SupportClass_InitializeTable_m0447919B22EAEE012D30BA17E7660343B1DA02C2 (void);
// 0x0000004B System.UInt32 ExitGames.Client.Photon.SupportClass::CalculateCrc(System.Byte[],System.Int32)
extern void SupportClass_CalculateCrc_mEB9B95CB432299982740C4C12ABB5BAB6BD6D529 (void);
// 0x0000004C System.Void ExitGames.Client.Photon.SupportClass::.cctor()
extern void SupportClass__cctor_m42E427DE501E9D016179E517DEB09493B06ECA50 (void);
// 0x0000004D System.Void ExitGames.Client.Photon.SupportClass/IntegerMillisecondsDelegate::.ctor(System.Object,System.IntPtr)
extern void IntegerMillisecondsDelegate__ctor_m11666C49068D352A52644CEB6909BF64D69730D5 (void);
// 0x0000004E System.Int32 ExitGames.Client.Photon.SupportClass/IntegerMillisecondsDelegate::Invoke()
extern void IntegerMillisecondsDelegate_Invoke_mF5B9C2456C857608EA138B56F337902DB635A4D6 (void);
// 0x0000004F System.IAsyncResult ExitGames.Client.Photon.SupportClass/IntegerMillisecondsDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void IntegerMillisecondsDelegate_BeginInvoke_mF887B99A4EADA24B2E56D4FE9C43221C750F1E7C (void);
// 0x00000050 System.Int32 ExitGames.Client.Photon.SupportClass/IntegerMillisecondsDelegate::EndInvoke(System.IAsyncResult)
extern void IntegerMillisecondsDelegate_EndInvoke_m99985ADAC82D1A51135240ECA0A9A124CFB58B83 (void);
// 0x00000051 System.Int32 ExitGames.Client.Photon.SupportClass/ThreadSafeRandom::Next()
extern void ThreadSafeRandom_Next_mE8EBD798219A6B348CF41939ACE7FC9A569930F5 (void);
// 0x00000052 System.Void ExitGames.Client.Photon.SupportClass/ThreadSafeRandom::.cctor()
extern void ThreadSafeRandom__cctor_mD24A912B6C400AD675AA96B1466FDB046223FABD (void);
// 0x00000053 System.Void ExitGames.Client.Photon.SupportClass/<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_mA23F9BC40FC9C6D7A865E40733C979785B60A535 (void);
// 0x00000054 System.Void ExitGames.Client.Photon.SupportClass/<>c__DisplayClass6_0::<StartBackgroundCalls>b__0()
extern void U3CU3Ec__DisplayClass6_0_U3CStartBackgroundCallsU3Eb__0_m983126A5A16C7AA2F1B52F9F4260990DEB7B7911 (void);
// 0x00000055 System.Void ExitGames.Client.Photon.SupportClass/<>c::.cctor()
extern void U3CU3Ec__cctor_m4D2300D9699E93CFAD0BD56AD065C1CF8AA00E7A (void);
// 0x00000056 System.Void ExitGames.Client.Photon.SupportClass/<>c::.ctor()
extern void U3CU3Ec__ctor_m16CA5C3CEE7C1F73426D5A4B5BE6CF4F13BC59B2 (void);
// 0x00000057 System.Int32 ExitGames.Client.Photon.SupportClass/<>c::<.cctor>b__20_0()
extern void U3CU3Ec_U3C_cctorU3Eb__20_0_m048C6973C52F6559EBFD6048D45EFCB5C01DB161 (void);
// 0x00000058 System.Void ExitGames.Client.Photon.IPhotonPeerListener::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String)
// 0x00000059 System.Void ExitGames.Client.Photon.IPhotonPeerListener::OnOperationResponse(ExitGames.Client.Photon.OperationResponse)
// 0x0000005A System.Void ExitGames.Client.Photon.IPhotonPeerListener::OnStatusChanged(ExitGames.Client.Photon.StatusCode)
// 0x0000005B System.Void ExitGames.Client.Photon.IPhotonPeerListener::OnEvent(ExitGames.Client.Photon.EventData)
// 0x0000005C System.Void ExitGames.Client.Photon.StreamBuffer::.ctor(System.Int32)
extern void StreamBuffer__ctor_m7F0ED256385E47AF69692C5BD1028E0662D16965 (void);
// 0x0000005D System.Void ExitGames.Client.Photon.StreamBuffer::.ctor(System.Byte[])
extern void StreamBuffer__ctor_m14F2D045ACCDD0608E0C846A5F2A18BFB5D8CF07 (void);
// 0x0000005E System.Byte[] ExitGames.Client.Photon.StreamBuffer::ToArray()
extern void StreamBuffer_ToArray_m9C37D61D978A55F2DEC32E65D3D20964D8CF79AC (void);
// 0x0000005F System.Byte[] ExitGames.Client.Photon.StreamBuffer::ToArrayFromPos()
extern void StreamBuffer_ToArrayFromPos_mB0B98B80F89BD8D884D5F79345A130265C8779FF (void);
// 0x00000060 System.Void ExitGames.Client.Photon.StreamBuffer::Compact()
extern void StreamBuffer_Compact_m584C429EAC58F98444FFADC47A469750D836398B (void);
// 0x00000061 System.Byte[] ExitGames.Client.Photon.StreamBuffer::GetBuffer()
extern void StreamBuffer_GetBuffer_mB2EB1A6F8252B7E3083CD445B73551131F651499 (void);
// 0x00000062 System.Byte[] ExitGames.Client.Photon.StreamBuffer::GetBufferAndAdvance(System.Int32,System.Int32&)
extern void StreamBuffer_GetBufferAndAdvance_m440B5F2AD727185E0412AA60986BDE5DC5998ED4 (void);
// 0x00000063 System.Int32 ExitGames.Client.Photon.StreamBuffer::get_Length()
extern void StreamBuffer_get_Length_mF8F62F1856C0FB2E00A788695B56E7780948C34F (void);
// 0x00000064 System.Int32 ExitGames.Client.Photon.StreamBuffer::get_Position()
extern void StreamBuffer_get_Position_mBDBC8E75BBD06EEB73505239E520BFF86794DDE9 (void);
// 0x00000065 System.Void ExitGames.Client.Photon.StreamBuffer::set_Position(System.Int32)
extern void StreamBuffer_set_Position_m6FF1D39C7F5C25CB5FEFB43169712739BCAF383F (void);
// 0x00000066 System.Int64 ExitGames.Client.Photon.StreamBuffer::Seek(System.Int64,System.IO.SeekOrigin)
extern void StreamBuffer_Seek_m00D9849A38807DA28F4B6A5261F01871F650FE83 (void);
// 0x00000067 System.Void ExitGames.Client.Photon.StreamBuffer::SetLength(System.Int64)
extern void StreamBuffer_SetLength_m80BDB2D33060CE967AE7A1F541CE1FA750662488 (void);
// 0x00000068 System.Void ExitGames.Client.Photon.StreamBuffer::SetCapacityMinimum(System.Int32)
extern void StreamBuffer_SetCapacityMinimum_mBC8D7C80985E0C879CACEDEAF6CFEDBDA91F8186 (void);
// 0x00000069 System.Int32 ExitGames.Client.Photon.StreamBuffer::Read(System.Byte[],System.Int32,System.Int32)
extern void StreamBuffer_Read_m2DCA3EFBD2350B51F4D19463C2D350188CAB750B (void);
// 0x0000006A System.Void ExitGames.Client.Photon.StreamBuffer::Write(System.Byte[],System.Int32,System.Int32)
extern void StreamBuffer_Write_m46BB33B27E747E8A6560F0C531511EB0315CFDFC (void);
// 0x0000006B System.Byte ExitGames.Client.Photon.StreamBuffer::ReadByte()
extern void StreamBuffer_ReadByte_mD6CA8D84EA717C29B9E8EC480DFB6D1F395D2E52 (void);
// 0x0000006C System.Void ExitGames.Client.Photon.StreamBuffer::WriteByte(System.Byte)
extern void StreamBuffer_WriteByte_mA31883A13FA3BCFB89EB6F8B151AD5CD90EE1E4C (void);
// 0x0000006D System.Void ExitGames.Client.Photon.StreamBuffer::WriteBytes(System.Byte,System.Byte)
extern void StreamBuffer_WriteBytes_mCBFB947875BE288D2B3AD3C2D88B025140637B3F (void);
// 0x0000006E System.Boolean ExitGames.Client.Photon.StreamBuffer::CheckSize(System.Int32)
extern void StreamBuffer_CheckSize_m069E4A12B1100AC180202901241A27E4B304A51C (void);
// 0x0000006F System.Byte ExitGames.Client.Photon.PhotonPeer::get_ClientSdkIdShifted()
extern void PhotonPeer_get_ClientSdkIdShifted_mBA20D7BCDEC3FC349FAB8CB03A101834D67A9E64 (void);
// 0x00000070 System.String ExitGames.Client.Photon.PhotonPeer::get_ClientVersion()
extern void PhotonPeer_get_ClientVersion_mCE1217D603C03298DC56F405C5626D131F545C21 (void);
// 0x00000071 ExitGames.Client.Photon.SerializationProtocol ExitGames.Client.Photon.PhotonPeer::get_SerializationProtocolType()
extern void PhotonPeer_get_SerializationProtocolType_m241B6FD506A2ADFD73E919D7800C79E5FE0DEBB5 (void);
// 0x00000072 System.Void ExitGames.Client.Photon.PhotonPeer::set_SerializationProtocolType(ExitGames.Client.Photon.SerializationProtocol)
extern void PhotonPeer_set_SerializationProtocolType_m3F1A6890FDCAB6240FE1617BFF7CCF4EAB80B4E3 (void);
// 0x00000073 System.Type ExitGames.Client.Photon.PhotonPeer::get_SocketImplementation()
extern void PhotonPeer_get_SocketImplementation_m6820F1409E0067DE4B3B99676AAC7D10B39FBEAB (void);
// 0x00000074 System.Void ExitGames.Client.Photon.PhotonPeer::set_SocketImplementation(System.Type)
extern void PhotonPeer_set_SocketImplementation_mB7D4C9883EFEA396B3DA9B1D45467D97B80EDB51 (void);
// 0x00000075 ExitGames.Client.Photon.IPhotonPeerListener ExitGames.Client.Photon.PhotonPeer::get_Listener()
extern void PhotonPeer_get_Listener_mE4DE6E89632E53F3DFC74FC70FEF50E8DBE72CBA (void);
// 0x00000076 System.Void ExitGames.Client.Photon.PhotonPeer::set_Listener(ExitGames.Client.Photon.IPhotonPeerListener)
extern void PhotonPeer_set_Listener_m6AE3033E47CDC53C687383BA8302ED398135CE03 (void);
// 0x00000077 System.Boolean ExitGames.Client.Photon.PhotonPeer::get_ReuseEventInstance()
extern void PhotonPeer_get_ReuseEventInstance_m11BB0E904457B18F66E1A61997B2DE083832A8DD (void);
// 0x00000078 System.Boolean ExitGames.Client.Photon.PhotonPeer::get_EnableServerTracing()
extern void PhotonPeer_get_EnableServerTracing_m79A85DCE8AB979C0C8029A1F58071AFDDE8B106B (void);
// 0x00000079 System.Byte ExitGames.Client.Photon.PhotonPeer::get_QuickResendAttempts()
extern void PhotonPeer_get_QuickResendAttempts_m2BCB8DCCD0F8A4542AAD0C6EDFBBDCF2354F5941 (void);
// 0x0000007A System.Void ExitGames.Client.Photon.PhotonPeer::set_QuickResendAttempts(System.Byte)
extern void PhotonPeer_set_QuickResendAttempts_m6DB0CD7664806B86AD9A4605D33260D84B428D9E (void);
// 0x0000007B ExitGames.Client.Photon.PeerStateValue ExitGames.Client.Photon.PhotonPeer::get_PeerState()
extern void PhotonPeer_get_PeerState_m744647FF47CD8C6E8D82A0C272AD42CA8EAE70C8 (void);
// 0x0000007C System.String ExitGames.Client.Photon.PhotonPeer::get_PeerID()
extern void PhotonPeer_get_PeerID_mF9E0C4EC264895D0C0AA28C84C075EF4DE7B0EAF (void);
// 0x0000007D System.Boolean ExitGames.Client.Photon.PhotonPeer::get_CrcEnabled()
extern void PhotonPeer_get_CrcEnabled_mE56C894F8F801F306E69015FBC682368DF9B4602 (void);
// 0x0000007E System.Void ExitGames.Client.Photon.PhotonPeer::set_CrcEnabled(System.Boolean)
extern void PhotonPeer_set_CrcEnabled_m5EFE4B945190C571F55C72290F39B6148898758A (void);
// 0x0000007F System.Int32 ExitGames.Client.Photon.PhotonPeer::get_PacketLossByCrc()
extern void PhotonPeer_get_PacketLossByCrc_m44CF3DB2B594358BCB73EC785CF117B6EAA57DC9 (void);
// 0x00000080 System.Int32 ExitGames.Client.Photon.PhotonPeer::get_ResentReliableCommands()
extern void PhotonPeer_get_ResentReliableCommands_m593BA98ED969E97ADF337923C6D6D9407A961EA7 (void);
// 0x00000081 System.Int32 ExitGames.Client.Photon.PhotonPeer::get_ServerTimeInMilliSeconds()
extern void PhotonPeer_get_ServerTimeInMilliSeconds_m073DB9E850E0A15CFF62168FB14719B5332AA8E9 (void);
// 0x00000082 System.Void ExitGames.Client.Photon.PhotonPeer::set_LocalMsTimestampDelegate(ExitGames.Client.Photon.SupportClass/IntegerMillisecondsDelegate)
extern void PhotonPeer_set_LocalMsTimestampDelegate_m976350E12892A2C87DEBC3BA436F556D010B37DE (void);
// 0x00000083 System.Int32 ExitGames.Client.Photon.PhotonPeer::get_ConnectionTime()
extern void PhotonPeer_get_ConnectionTime_mFCBBFD1B4B6E8A165F187BE45297591A9D3812AF (void);
// 0x00000084 System.Int32 ExitGames.Client.Photon.PhotonPeer::get_LastSendOutgoingTime()
extern void PhotonPeer_get_LastSendOutgoingTime_m1EF6C16FBE9C64E3831833D92482051DCA01B9C7 (void);
// 0x00000085 System.Int32 ExitGames.Client.Photon.PhotonPeer::get_LongestSentCall()
extern void PhotonPeer_get_LongestSentCall_mD99C118B30F791AE0ED3D8A3E96A619C45EC7C59 (void);
// 0x00000086 System.Int32 ExitGames.Client.Photon.PhotonPeer::get_RoundTripTime()
extern void PhotonPeer_get_RoundTripTime_m62272FBDA0559234256B59318B17958B356CC470 (void);
// 0x00000087 System.Int32 ExitGames.Client.Photon.PhotonPeer::get_RoundTripTimeVariance()
extern void PhotonPeer_get_RoundTripTimeVariance_mDAA6158DACAB662BD837D8FA5C43C188DA3AC944 (void);
// 0x00000088 System.Int32 ExitGames.Client.Photon.PhotonPeer::get_TimestampOfLastSocketReceive()
extern void PhotonPeer_get_TimestampOfLastSocketReceive_m2636CC04F03168D4523E6ED6B54D23846CF2DE38 (void);
// 0x00000089 System.String ExitGames.Client.Photon.PhotonPeer::get_ServerAddress()
extern void PhotonPeer_get_ServerAddress_m7FA216808061F2113B7A902B921B6B3BE3C49A52 (void);
// 0x0000008A System.String ExitGames.Client.Photon.PhotonPeer::get_ServerIpAddress()
extern void PhotonPeer_get_ServerIpAddress_mCCDB432F5B35677DD1F2610F0BBEC6FCE140ADDF (void);
// 0x0000008B ExitGames.Client.Photon.ConnectionProtocol ExitGames.Client.Photon.PhotonPeer::get_UsedProtocol()
extern void PhotonPeer_get_UsedProtocol_m941EB6A3A8BD5D059352ACA7E6EF13845CCE68F2 (void);
// 0x0000008C ExitGames.Client.Photon.ConnectionProtocol ExitGames.Client.Photon.PhotonPeer::get_TransportProtocol()
extern void PhotonPeer_get_TransportProtocol_mFEED042A2E83B87CBE556BA1252CBEFAC0207623 (void);
// 0x0000008D System.Void ExitGames.Client.Photon.PhotonPeer::set_TransportProtocol(ExitGames.Client.Photon.ConnectionProtocol)
extern void PhotonPeer_set_TransportProtocol_m9101DD605A90289E142A201B00A8A504AB83C119 (void);
// 0x0000008E System.Boolean ExitGames.Client.Photon.PhotonPeer::get_IsSimulationEnabled()
extern void PhotonPeer_get_IsSimulationEnabled_m43EF1F251D098CC443D78CED9EBE5263F432ED5A (void);
// 0x0000008F System.Void ExitGames.Client.Photon.PhotonPeer::set_IsSimulationEnabled(System.Boolean)
extern void PhotonPeer_set_IsSimulationEnabled_m9141CD17E738B46600AD4ED3E8926A1B55BF40E1 (void);
// 0x00000090 ExitGames.Client.Photon.NetworkSimulationSet ExitGames.Client.Photon.PhotonPeer::get_NetworkSimulationSettings()
extern void PhotonPeer_get_NetworkSimulationSettings_m97D23008F2713E2E3A62AF889CAD9AD2D00C2BAD (void);
// 0x00000091 System.Int32 ExitGames.Client.Photon.PhotonPeer::get_MaximumTransferUnit()
extern void PhotonPeer_get_MaximumTransferUnit_mFF03F9C31DD8A110D705F322181623B8E4DC6BC7 (void);
// 0x00000092 System.Boolean ExitGames.Client.Photon.PhotonPeer::get_IsEncryptionAvailable()
extern void PhotonPeer_get_IsEncryptionAvailable_mE014D64EEFD5D20BFC57367BC420CCFC68716439 (void);
// 0x00000093 System.Boolean ExitGames.Client.Photon.PhotonPeer::get_IsSendingOnlyAcks()
extern void PhotonPeer_get_IsSendingOnlyAcks_m33A8FFD5F0EC8D5C60AF70F1C3AB48C12844643F (void);
// 0x00000094 System.Void ExitGames.Client.Photon.PhotonPeer::set_IsSendingOnlyAcks(System.Boolean)
extern void PhotonPeer_set_IsSendingOnlyAcks_mB45BBA417D520682220A98BD088D161D062A6156 (void);
// 0x00000095 ExitGames.Client.Photon.TrafficStats ExitGames.Client.Photon.PhotonPeer::get_TrafficStatsIncoming()
extern void PhotonPeer_get_TrafficStatsIncoming_m2DE8612AD9C2442910F6470C783E8F3926C5DFCD (void);
// 0x00000096 System.Void ExitGames.Client.Photon.PhotonPeer::set_TrafficStatsIncoming(ExitGames.Client.Photon.TrafficStats)
extern void PhotonPeer_set_TrafficStatsIncoming_m3527030FD2D27CE57094A84ADED781871F63B751 (void);
// 0x00000097 ExitGames.Client.Photon.TrafficStats ExitGames.Client.Photon.PhotonPeer::get_TrafficStatsOutgoing()
extern void PhotonPeer_get_TrafficStatsOutgoing_mD5AAF890C29FEED4293897A3EE59BF40FA24327D (void);
// 0x00000098 System.Void ExitGames.Client.Photon.PhotonPeer::set_TrafficStatsOutgoing(ExitGames.Client.Photon.TrafficStats)
extern void PhotonPeer_set_TrafficStatsOutgoing_m4549C6D901C01786E112188A617D851011D03BA6 (void);
// 0x00000099 ExitGames.Client.Photon.TrafficStatsGameLevel ExitGames.Client.Photon.PhotonPeer::get_TrafficStatsGameLevel()
extern void PhotonPeer_get_TrafficStatsGameLevel_m303B9889C33154A66D106DF084FE4307F41B7796 (void);
// 0x0000009A System.Void ExitGames.Client.Photon.PhotonPeer::set_TrafficStatsGameLevel(ExitGames.Client.Photon.TrafficStatsGameLevel)
extern void PhotonPeer_set_TrafficStatsGameLevel_m5ACC9C88363789E2F54E597663C243608B9D8104 (void);
// 0x0000009B System.Int64 ExitGames.Client.Photon.PhotonPeer::get_TrafficStatsElapsedMs()
extern void PhotonPeer_get_TrafficStatsElapsedMs_mC5EC6DF9440E00F14867964EB87430C3C73F8651 (void);
// 0x0000009C System.Boolean ExitGames.Client.Photon.PhotonPeer::get_TrafficStatsEnabled()
extern void PhotonPeer_get_TrafficStatsEnabled_mBF1DB3B0597DA43209A70CEC016184E6A79003B7 (void);
// 0x0000009D System.Void ExitGames.Client.Photon.PhotonPeer::set_TrafficStatsEnabled(System.Boolean)
extern void PhotonPeer_set_TrafficStatsEnabled_mD1BC18264BC8E32FF3BF002198066BC8717961B8 (void);
// 0x0000009E System.Void ExitGames.Client.Photon.PhotonPeer::TrafficStatsReset()
extern void PhotonPeer_TrafficStatsReset_m02CC52F2C9BF5C3E1F4B9ADC07425401931178A0 (void);
// 0x0000009F System.Void ExitGames.Client.Photon.PhotonPeer::InitializeTrafficStats()
extern void PhotonPeer_InitializeTrafficStats_mCE9ECBFA897962B11CCB2E8130FADE5B71DAB9A6 (void);
// 0x000000A0 System.String ExitGames.Client.Photon.PhotonPeer::VitalStatsToString(System.Boolean)
extern void PhotonPeer_VitalStatsToString_mBE4DB09E7BFDAC0C6C2143A5E4F1676C1638685F (void);
// 0x000000A1 System.Void ExitGames.Client.Photon.PhotonPeer::.ctor(ExitGames.Client.Photon.ConnectionProtocol)
extern void PhotonPeer__ctor_mA24D4FC9F9550C68800F4D1CE652EDBD341B525A (void);
// 0x000000A2 System.Void ExitGames.Client.Photon.PhotonPeer::.ctor(ExitGames.Client.Photon.IPhotonPeerListener,ExitGames.Client.Photon.ConnectionProtocol)
extern void PhotonPeer__ctor_m8662F89CCD4FF7AF4D98F68B638DBE818E0D543C (void);
// 0x000000A3 System.Boolean ExitGames.Client.Photon.PhotonPeer::Connect(System.String,System.String)
extern void PhotonPeer_Connect_m55EBBF5DCAC6ADB2787F0DDEAADC46A3B9859BAD (void);
// 0x000000A4 System.Boolean ExitGames.Client.Photon.PhotonPeer::Connect(System.String,System.String,System.Object)
extern void PhotonPeer_Connect_mB27D3BA2C51FB0832F6114BA26DF66FD91FA3E53 (void);
// 0x000000A5 System.Void ExitGames.Client.Photon.PhotonPeer::CreatePeerBase()
extern void PhotonPeer_CreatePeerBase_m5E7A92325DAC67A66AFF6EB4B20F3734B5814E93 (void);
// 0x000000A6 System.Void ExitGames.Client.Photon.PhotonPeer::Disconnect()
extern void PhotonPeer_Disconnect_m93DA50619C668C585E6FF76AC96243E71ADF3D55 (void);
// 0x000000A7 System.Void ExitGames.Client.Photon.PhotonPeer::StopThread()
extern void PhotonPeer_StopThread_mF30F314511A11097BEA7E84A8DAB82CEC3AB721A (void);
// 0x000000A8 System.Void ExitGames.Client.Photon.PhotonPeer::FetchServerTimestamp()
extern void PhotonPeer_FetchServerTimestamp_mFED8B6680D0DC87B643CBB4A151DA31FC1C26CBB (void);
// 0x000000A9 System.Boolean ExitGames.Client.Photon.PhotonPeer::EstablishEncryption()
extern void PhotonPeer_EstablishEncryption_mBFDFC1E1BD18BE4EB9DFF8093497F0DD481470D2 (void);
// 0x000000AA System.Boolean ExitGames.Client.Photon.PhotonPeer::InitDatagramEncryption(System.Byte[],System.Byte[],System.Boolean)
extern void PhotonPeer_InitDatagramEncryption_m957B1401835C4C14EE1B88D54105DC8ADFE95300 (void);
// 0x000000AB System.Void ExitGames.Client.Photon.PhotonPeer::InitPayloadEncryption(System.Byte[])
extern void PhotonPeer_InitPayloadEncryption_m5818EE76FF876DAC4D2F2D8758DC9A764C6D4525 (void);
// 0x000000AC System.Void ExitGames.Client.Photon.PhotonPeer::Service()
extern void PhotonPeer_Service_m86FA834146E54C53B7AEA6DB957ADAFB8514A52A (void);
// 0x000000AD System.Boolean ExitGames.Client.Photon.PhotonPeer::SendOutgoingCommands()
extern void PhotonPeer_SendOutgoingCommands_mC69CB3D8F7AB255651AF411377E2C4860F1D40B6 (void);
// 0x000000AE System.Boolean ExitGames.Client.Photon.PhotonPeer::SendAcksOnly()
extern void PhotonPeer_SendAcksOnly_m7AFFE361BB36F1D3E2A759029B1F7DE344DC92A5 (void);
// 0x000000AF System.Boolean ExitGames.Client.Photon.PhotonPeer::DispatchIncomingCommands()
extern void PhotonPeer_DispatchIncomingCommands_mFD406C9E792960EFFF5BEF4B7D04CECAB41DCE99 (void);
// 0x000000B0 System.Boolean ExitGames.Client.Photon.PhotonPeer::SendOperation(System.Byte,System.Collections.Generic.Dictionary`2<System.Byte,System.Object>,ExitGames.Client.Photon.SendOptions)
extern void PhotonPeer_SendOperation_m4D7D271BA8337D9006365956332B91ACA154A068 (void);
// 0x000000B1 System.Boolean ExitGames.Client.Photon.PhotonPeer::RegisterType(System.Type,System.Byte,ExitGames.Client.Photon.SerializeStreamMethod,ExitGames.Client.Photon.DeserializeStreamMethod)
extern void PhotonPeer_RegisterType_m1F8EDFA7E77F1D5F279103FA2D39C506364650EB (void);
// 0x000000B2 System.Void ExitGames.Client.Photon.PhotonPeer::.cctor()
extern void PhotonPeer__cctor_m19723A97D60766CD651A293E69962978B57BDD95 (void);
// 0x000000B3 System.Boolean ExitGames.Client.Photon.PhotonPeer::<EstablishEncryption>b__176_0()
extern void PhotonPeer_U3CEstablishEncryptionU3Eb__176_0_m87B77B44E4DE1FC6EE194C1E1C9117E51A89D1FC (void);
// 0x000000B4 System.Void ExitGames.Client.Photon.PhotonCodes::.cctor()
extern void PhotonCodes__cctor_m1D735D3E8ED81167A471CD98BA5592E979A1F0C8 (void);
// 0x000000B5 System.Type ExitGames.Client.Photon.PeerBase::get_SocketImplementation()
extern void PeerBase_get_SocketImplementation_m79A3101465FD7FA6060FC55A12A3D58F154A8505 (void);
// 0x000000B6 System.String ExitGames.Client.Photon.PeerBase::get_ServerAddress()
extern void PeerBase_get_ServerAddress_m71CA11FE813913DAA4F1F9554964E367EC1E8419 (void);
// 0x000000B7 System.Void ExitGames.Client.Photon.PeerBase::set_ServerAddress(System.String)
extern void PeerBase_set_ServerAddress_m7C1A0AC51E2E110A24CB79B6C5CF061405B51877 (void);
// 0x000000B8 ExitGames.Client.Photon.IPhotonPeerListener ExitGames.Client.Photon.PeerBase::get_Listener()
extern void PeerBase_get_Listener_m8A72A92EFEF8395D98F2C0538E3420E8A42A3E45 (void);
// 0x000000B9 ExitGames.Client.Photon.DebugLevel ExitGames.Client.Photon.PeerBase::get_debugOut()
extern void PeerBase_get_debugOut_m95153EB49C30BC059E23104B0DA1E3EC77080138 (void);
// 0x000000BA System.Int32 ExitGames.Client.Photon.PeerBase::get_DisconnectTimeout()
extern void PeerBase_get_DisconnectTimeout_m141518E1E754C78C917DCCA42178DE749CC45C6C (void);
// 0x000000BB System.Int32 ExitGames.Client.Photon.PeerBase::get_timePingInterval()
extern void PeerBase_get_timePingInterval_mFB4C5685DC0A19D076B2A1E7717B0262169623D1 (void);
// 0x000000BC System.Byte ExitGames.Client.Photon.PeerBase::get_ChannelCount()
extern void PeerBase_get_ChannelCount_mACF054DC726A37FC6E0E39F62A6F75447920D8C4 (void);
// 0x000000BD System.String ExitGames.Client.Photon.PeerBase::get_PeerID()
extern void PeerBase_get_PeerID_m754EF3CD8E17F2C497833AC6A2FEF5A30E9D09B0 (void);
// 0x000000BE System.Int32 ExitGames.Client.Photon.PeerBase::get_timeInt()
extern void PeerBase_get_timeInt_m94580582ABA72B8851C01EC4E598DCFD4D6A5024 (void);
// 0x000000BF System.Boolean ExitGames.Client.Photon.PeerBase::get_IsSendingOnlyAcks()
extern void PeerBase_get_IsSendingOnlyAcks_mC759A8A5D23A2327C5E1EED67A0C53BF07A50DC2 (void);
// 0x000000C0 System.Int32 ExitGames.Client.Photon.PeerBase::get_mtu()
extern void PeerBase_get_mtu_mEC86F0DD4AF2C40E596839235FE151C781F614DA (void);
// 0x000000C1 System.Boolean ExitGames.Client.Photon.PeerBase::get_IsIpv6()
extern void PeerBase_get_IsIpv6_m3EA33C3E36F90D24C2497113BAB6B0DDB4FA0F4F (void);
// 0x000000C2 System.Void ExitGames.Client.Photon.PeerBase::.ctor()
extern void PeerBase__ctor_mE31EA56DDBFCBE25590BC491DA8C70364F8EF2C7 (void);
// 0x000000C3 ExitGames.Client.Photon.StreamBuffer ExitGames.Client.Photon.PeerBase::MessageBufferPoolGet()
extern void PeerBase_MessageBufferPoolGet_mF43ADD7AADFD540877793EABA1489AC5B3B444A1 (void);
// 0x000000C4 System.Void ExitGames.Client.Photon.PeerBase::MessageBufferPoolPut(ExitGames.Client.Photon.StreamBuffer)
extern void PeerBase_MessageBufferPoolPut_m7513BC7EAD750A9AC82B84BEF2BC2F409D26A903 (void);
// 0x000000C5 System.Void ExitGames.Client.Photon.PeerBase::InitPeerBase()
extern void PeerBase_InitPeerBase_m0E945EDBED31E3B81474FBB434DAD23695954ED1 (void);
// 0x000000C6 System.Boolean ExitGames.Client.Photon.PeerBase::Connect(System.String,System.String,System.Object)
// 0x000000C7 System.String ExitGames.Client.Photon.PeerBase::GetHttpKeyValueString(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void PeerBase_GetHttpKeyValueString_mBD50A4BDADEC199C285F9BE694A6FA7402EA3FEC (void);
// 0x000000C8 System.Byte[] ExitGames.Client.Photon.PeerBase::PrepareConnectData(System.String,System.String,System.Object)
extern void PeerBase_PrepareConnectData_m1BF42E8D598315385C35B722328E2F65C11E493B (void);
// 0x000000C9 System.String ExitGames.Client.Photon.PeerBase::PepareWebSocketUrl(System.String,System.String,System.Object)
extern void PeerBase_PepareWebSocketUrl_mE3F995C56A7F40515E3C8B264BB188531AA065E9 (void);
// 0x000000CA System.Void ExitGames.Client.Photon.PeerBase::OnConnect()
// 0x000000CB System.Void ExitGames.Client.Photon.PeerBase::InitCallback()
extern void PeerBase_InitCallback_m384CDAE1DFE2844D384A9A043A97DD305CCB5622 (void);
// 0x000000CC System.Void ExitGames.Client.Photon.PeerBase::Disconnect()
// 0x000000CD System.Void ExitGames.Client.Photon.PeerBase::StopConnection()
// 0x000000CE System.Void ExitGames.Client.Photon.PeerBase::FetchServerTimestamp()
// 0x000000CF System.Boolean ExitGames.Client.Photon.PeerBase::EnqueueOperation(System.Collections.Generic.Dictionary`2<System.Byte,System.Object>,System.Byte,ExitGames.Client.Photon.SendOptions,ExitGames.Client.Photon.EgMessageType)
// 0x000000D0 ExitGames.Client.Photon.StreamBuffer ExitGames.Client.Photon.PeerBase::SerializeOperationToMessage(System.Byte,System.Collections.Generic.Dictionary`2<System.Byte,System.Object>,ExitGames.Client.Photon.EgMessageType,System.Boolean)
// 0x000000D1 System.Boolean ExitGames.Client.Photon.PeerBase::SendOutgoingCommands()
// 0x000000D2 System.Boolean ExitGames.Client.Photon.PeerBase::SendAcksOnly()
extern void PeerBase_SendAcksOnly_m1305296D0CAEFCEF356E3555754A3B68F95AFB45 (void);
// 0x000000D3 System.Void ExitGames.Client.Photon.PeerBase::ReceiveIncomingCommands(System.Byte[],System.Int32)
// 0x000000D4 System.Boolean ExitGames.Client.Photon.PeerBase::DispatchIncomingCommands()
// 0x000000D5 System.Boolean ExitGames.Client.Photon.PeerBase::DeserializeMessageAndCallback(ExitGames.Client.Photon.StreamBuffer)
extern void PeerBase_DeserializeMessageAndCallback_m956E8E078C23BE6F8AFAA4E80C3630AE25B97A80 (void);
// 0x000000D6 System.Void ExitGames.Client.Photon.PeerBase::UpdateRoundTripTimeAndVariance(System.Int32)
extern void PeerBase_UpdateRoundTripTimeAndVariance_mE35461EE543B563ED93948696D371D511E4D2A75 (void);
// 0x000000D7 System.Boolean ExitGames.Client.Photon.PeerBase::ExchangeKeysForEncryption(System.Object)
extern void PeerBase_ExchangeKeysForEncryption_m79F8BDC9F5A8B79589EE8B09658AD813B705688A (void);
// 0x000000D8 System.Void ExitGames.Client.Photon.PeerBase::DeriveSharedKey(ExitGames.Client.Photon.OperationResponse)
extern void PeerBase_DeriveSharedKey_mF302F8533114D8F3B5724562B246BBA55233AD5B (void);
// 0x000000D9 System.Void ExitGames.Client.Photon.PeerBase::InitEncryption(System.Byte[])
extern void PeerBase_InitEncryption_m7E66B1132094454CF9A7D584D7D015D1DC2B5B7F (void);
// 0x000000DA System.Void ExitGames.Client.Photon.PeerBase::EnqueueActionForDispatch(ExitGames.Client.Photon.PeerBase/MyAction)
extern void PeerBase_EnqueueActionForDispatch_m1BBE511B812834985E304E6D39520D6F82831F50 (void);
// 0x000000DB System.Void ExitGames.Client.Photon.PeerBase::EnqueueDebugReturn(ExitGames.Client.Photon.DebugLevel,System.String)
extern void PeerBase_EnqueueDebugReturn_m43A0CAA0916A7FB988AB52C51E339FEAFEDB48F7 (void);
// 0x000000DC System.Void ExitGames.Client.Photon.PeerBase::EnqueueStatusCallback(ExitGames.Client.Photon.StatusCode)
extern void PeerBase_EnqueueStatusCallback_mD104ECB17930B820145CD8B870AE1B4DCCDF9D9D (void);
// 0x000000DD ExitGames.Client.Photon.NetworkSimulationSet ExitGames.Client.Photon.PeerBase::get_NetworkSimulationSettings()
extern void PeerBase_get_NetworkSimulationSettings_m3A06EA5987C4E3AB2C28B6D3226CEF7993718227 (void);
// 0x000000DE System.Void ExitGames.Client.Photon.PeerBase::SendNetworkSimulated(System.Byte[])
extern void PeerBase_SendNetworkSimulated_m7F1DB81F6EDFF65F01BF8E5DE6325CB4F2636684 (void);
// 0x000000DF System.Void ExitGames.Client.Photon.PeerBase::ReceiveNetworkSimulated(System.Byte[])
extern void PeerBase_ReceiveNetworkSimulated_mC3EA2A65918D87FFFC1D5BFD622B0F7FF06182AC (void);
// 0x000000E0 System.Void ExitGames.Client.Photon.PeerBase::NetworkSimRun()
extern void PeerBase_NetworkSimRun_mBB05EDFBE1B39E744CBF4907D5ED7C3F54B5A2FE (void);
// 0x000000E1 System.Boolean ExitGames.Client.Photon.PeerBase::get_TrafficStatsEnabled()
extern void PeerBase_get_TrafficStatsEnabled_mC073D175183A6D1E9DF3444465DDA6053EA0DF2E (void);
// 0x000000E2 ExitGames.Client.Photon.TrafficStats ExitGames.Client.Photon.PeerBase::get_TrafficStatsIncoming()
extern void PeerBase_get_TrafficStatsIncoming_mEFDE4E31EB4C7D63F90EEE1722C7110F40F86369 (void);
// 0x000000E3 ExitGames.Client.Photon.TrafficStats ExitGames.Client.Photon.PeerBase::get_TrafficStatsOutgoing()
extern void PeerBase_get_TrafficStatsOutgoing_m39246E29BF90DF7DEECA31244F5C7B1B00C26114 (void);
// 0x000000E4 ExitGames.Client.Photon.TrafficStatsGameLevel ExitGames.Client.Photon.PeerBase::get_TrafficStatsGameLevel()
extern void PeerBase_get_TrafficStatsGameLevel_mAA3C1ADC90CBF4FADDDF5A7FAAAD3D7720A2FFC8 (void);
// 0x000000E5 System.Int32 ExitGames.Client.Photon.PeerBase::get_CommandLogSize()
extern void PeerBase_get_CommandLogSize_m0272BA0E68346EBED1381BC2E8F864B27C5F309C (void);
// 0x000000E6 System.Void ExitGames.Client.Photon.PeerBase::CommandLogResize()
extern void PeerBase_CommandLogResize_m12247256EE0054174B7CA7004CB7730E9D6E7174 (void);
// 0x000000E7 System.Void ExitGames.Client.Photon.PeerBase::CommandLogInit()
extern void PeerBase_CommandLogInit_m86007D73282305CA5A55328FF9FCCE0CA3F544CC (void);
// 0x000000E8 System.Void ExitGames.Client.Photon.PeerBase::.cctor()
extern void PeerBase__cctor_mB75A698BBAEB86982256B91C22ED16BC78EEE186 (void);
// 0x000000E9 System.Void ExitGames.Client.Photon.PeerBase/MyAction::.ctor(System.Object,System.IntPtr)
extern void MyAction__ctor_m2063CA7F96349400661145A2E272CB00F9325729 (void);
// 0x000000EA System.Void ExitGames.Client.Photon.PeerBase/MyAction::Invoke()
extern void MyAction_Invoke_m146AB584524C6E17179A1E1A714C958BE9F11755 (void);
// 0x000000EB System.IAsyncResult ExitGames.Client.Photon.PeerBase/MyAction::BeginInvoke(System.AsyncCallback,System.Object)
extern void MyAction_BeginInvoke_m84D1393AB90A6A84616F44D815DCAB9808BE465F (void);
// 0x000000EC System.Void ExitGames.Client.Photon.PeerBase/MyAction::EndInvoke(System.IAsyncResult)
extern void MyAction_EndInvoke_mA444DF66DED1780E583646376BCFEDDC331958E1 (void);
// 0x000000ED System.Void ExitGames.Client.Photon.PeerBase/<>c__DisplayClass104_0::.ctor()
extern void U3CU3Ec__DisplayClass104_0__ctor_mC25CA6D2D449CF07A13AE17AC374CB0115584133 (void);
// 0x000000EE System.Void ExitGames.Client.Photon.PeerBase/<>c__DisplayClass104_0::<EnqueueDebugReturn>b__0()
extern void U3CU3Ec__DisplayClass104_0_U3CEnqueueDebugReturnU3Eb__0_mA924D04338C87495348EF2F28793191B8EF0683C (void);
// 0x000000EF System.Void ExitGames.Client.Photon.PeerBase/<>c__DisplayClass105_0::.ctor()
extern void U3CU3Ec__DisplayClass105_0__ctor_m8933D54880906067B2193D0DCBAFFA41AB190A40 (void);
// 0x000000F0 System.Void ExitGames.Client.Photon.PeerBase/<>c__DisplayClass105_0::<EnqueueStatusCallback>b__0()
extern void U3CU3Ec__DisplayClass105_0_U3CEnqueueStatusCallbackU3Eb__0_m97CC8A71DA1BEE3A9827F1601299F6D700B3AC85 (void);
// 0x000000F1 System.Void ExitGames.Client.Photon.CmdLogItem::.ctor()
extern void CmdLogItem__ctor_m32026F769F8D0688E36B1270F3F37197CFE569B2 (void);
// 0x000000F2 System.Void ExitGames.Client.Photon.CmdLogItem::.ctor(ExitGames.Client.Photon.NCommand,System.Int32,System.Int32,System.Int32)
extern void CmdLogItem__ctor_mEDA72F344CEE187C40DEA539F3BF34EAAB65AFDA (void);
// 0x000000F3 System.String ExitGames.Client.Photon.CmdLogItem::ToString()
extern void CmdLogItem_ToString_m945408FFC25E43A643E427C0DF401747160CAB0B (void);
// 0x000000F4 System.Void ExitGames.Client.Photon.CmdLogReceivedReliable::.ctor(ExitGames.Client.Photon.NCommand,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void CmdLogReceivedReliable__ctor_m93B8CF226CC177A245237DDBDAAB9234CF1E67B2 (void);
// 0x000000F5 System.String ExitGames.Client.Photon.CmdLogReceivedReliable::ToString()
extern void CmdLogReceivedReliable_ToString_m0200DD37EFFC8759129B7381FD0085E855E767B1 (void);
// 0x000000F6 System.Void ExitGames.Client.Photon.CmdLogReceivedAck::.ctor(ExitGames.Client.Photon.NCommand,System.Int32,System.Int32,System.Int32)
extern void CmdLogReceivedAck__ctor_m883C9D7509A995B25249C9BA442D41FB912B607F (void);
// 0x000000F7 System.String ExitGames.Client.Photon.CmdLogReceivedAck::ToString()
extern void CmdLogReceivedAck_ToString_mAF6FAF10603BD993308D43B45693AA817FEFE026 (void);
// 0x000000F8 System.Void ExitGames.Client.Photon.CmdLogSentReliable::.ctor(ExitGames.Client.Photon.NCommand,System.Int32,System.Int32,System.Int32,System.Boolean)
extern void CmdLogSentReliable__ctor_m63E7C8618186D3988E4CCD0004C2712FF5DB5B3D (void);
// 0x000000F9 System.String ExitGames.Client.Photon.CmdLogSentReliable::ToString()
extern void CmdLogSentReliable_ToString_m3D32AAD8165EBC71A527D7EDC6733FAB68AAB3C4 (void);
// 0x000000FA System.Void ExitGames.Client.Photon.SendOptions::set_Reliability(System.Boolean)
extern void SendOptions_set_Reliability_mD444223906FA26A1589B9EECB772CA98CA66E164 (void);
// 0x000000FB System.Void ExitGames.Client.Photon.SendOptions::.cctor()
extern void SendOptions__cctor_mD73641D14A950B790656EC669F4DF1E198B7C527 (void);
// 0x000000FC System.Void ExitGames.Client.Photon.EnetPeer::.ctor()
extern void EnetPeer__ctor_mC16BA5BE856937FD5013C2E858C9AEC81FD68396 (void);
// 0x000000FD System.Void ExitGames.Client.Photon.EnetPeer::InitPeerBase()
extern void EnetPeer_InitPeerBase_m6F4958EC3E0EF7AD6752151219B3E87DD77F4DA6 (void);
// 0x000000FE System.Void ExitGames.Client.Photon.EnetPeer::ApplyRandomizedSequenceNumbers()
extern void EnetPeer_ApplyRandomizedSequenceNumbers_m260809622D75FFB81C2D83374F624CBB9425A0D9 (void);
// 0x000000FF System.Boolean ExitGames.Client.Photon.EnetPeer::Connect(System.String,System.String,System.Object)
extern void EnetPeer_Connect_mD4F2DD6412A5378DE8AF795BCCB4E272E5EBF5A4 (void);
// 0x00000100 System.Void ExitGames.Client.Photon.EnetPeer::OnConnect()
extern void EnetPeer_OnConnect_mD31CB14E4ADE8ACC99F12CB2C1BFD3D292B2D634 (void);
// 0x00000101 System.Void ExitGames.Client.Photon.EnetPeer::Disconnect()
extern void EnetPeer_Disconnect_mA93FC6CED1708D1CCFC653EF9C2E8D45AF4AD7BB (void);
// 0x00000102 System.Void ExitGames.Client.Photon.EnetPeer::StopConnection()
extern void EnetPeer_StopConnection_mEEEC4D532A831B54D129D983D6B1C893F0C9113D (void);
// 0x00000103 System.Void ExitGames.Client.Photon.EnetPeer::FetchServerTimestamp()
extern void EnetPeer_FetchServerTimestamp_mB0A96A370674E7C082DC4598CBA69F0E8C69D2B4 (void);
// 0x00000104 System.Boolean ExitGames.Client.Photon.EnetPeer::DispatchIncomingCommands()
extern void EnetPeer_DispatchIncomingCommands_m6E81ED91B3A7920805A832B1F45E203CD86A52EA (void);
// 0x00000105 System.Int32 ExitGames.Client.Photon.EnetPeer::GetFragmentLength()
extern void EnetPeer_GetFragmentLength_m4DCA08CA85A208184FFD77EA01FEDAA38F17BB61 (void);
// 0x00000106 System.Int32 ExitGames.Client.Photon.EnetPeer::CalculateBufferLen()
extern void EnetPeer_CalculateBufferLen_m41DCC0ECCE15F5AFD2C775B24542D532D2ABFD5E (void);
// 0x00000107 System.Int32 ExitGames.Client.Photon.EnetPeer::CalculateInitialOffset()
extern void EnetPeer_CalculateInitialOffset_m4CB5BCA2852247CB0E1A7620D9E687DE3C146A02 (void);
// 0x00000108 System.Boolean ExitGames.Client.Photon.EnetPeer::SendAcksOnly()
extern void EnetPeer_SendAcksOnly_m248A3B4E348AC6D0019E9528D5654A547FBC2C5F (void);
// 0x00000109 System.Boolean ExitGames.Client.Photon.EnetPeer::SendOutgoingCommands()
extern void EnetPeer_SendOutgoingCommands_mDA37A5DBB0AF3E816A33D7780CAA2AA34EA99D16 (void);
// 0x0000010A System.Boolean ExitGames.Client.Photon.EnetPeer::AreReliableCommandsInTransit()
extern void EnetPeer_AreReliableCommandsInTransit_mB81E257DF032D48AF5796A4E4B18185B30387139 (void);
// 0x0000010B System.Boolean ExitGames.Client.Photon.EnetPeer::EnqueueOperation(System.Collections.Generic.Dictionary`2<System.Byte,System.Object>,System.Byte,ExitGames.Client.Photon.SendOptions,ExitGames.Client.Photon.EgMessageType)
extern void EnetPeer_EnqueueOperation_m13141EB4BAE223899210464B0D58E433D10DE181 (void);
// 0x0000010C ExitGames.Client.Photon.EnetChannel ExitGames.Client.Photon.EnetPeer::GetChannel(System.Byte)
extern void EnetPeer_GetChannel_m66F0E0B762B4FDF4A479D2F58D90A82FD21AD242 (void);
// 0x0000010D System.Boolean ExitGames.Client.Photon.EnetPeer::CreateAndEnqueueCommand(System.Byte,ExitGames.Client.Photon.StreamBuffer,System.Byte)
extern void EnetPeer_CreateAndEnqueueCommand_m7B9E0742E16B337CB97766C61A484558BFBBE4D7 (void);
// 0x0000010E ExitGames.Client.Photon.StreamBuffer ExitGames.Client.Photon.EnetPeer::SerializeOperationToMessage(System.Byte,System.Collections.Generic.Dictionary`2<System.Byte,System.Object>,ExitGames.Client.Photon.EgMessageType,System.Boolean)
extern void EnetPeer_SerializeOperationToMessage_m372A833A19081AAA150841CC7F1C758C2EAE4E37 (void);
// 0x0000010F System.Int32 ExitGames.Client.Photon.EnetPeer::SerializeAckToBuffer()
extern void EnetPeer_SerializeAckToBuffer_m8485E0F374E2A135DCDFE10EEB06327B7961DBD5 (void);
// 0x00000110 System.Int32 ExitGames.Client.Photon.EnetPeer::SerializeToBuffer(System.Collections.Generic.Queue`1<ExitGames.Client.Photon.NCommand>)
extern void EnetPeer_SerializeToBuffer_m97F0967FE3F12E6F8D2D929A76A96E6C07433F4C (void);
// 0x00000111 System.Void ExitGames.Client.Photon.EnetPeer::SendData(System.Byte[],System.Int32)
extern void EnetPeer_SendData_m3BCF88DE2DD6A00B5E56352BF0866A59CF5A2403 (void);
// 0x00000112 System.Void ExitGames.Client.Photon.EnetPeer::SendToSocket(System.Byte[],System.Int32)
extern void EnetPeer_SendToSocket_m22FDBBE8E82DF6F00EFC7E0F536E9B05B718EC96 (void);
// 0x00000113 System.Void ExitGames.Client.Photon.EnetPeer::SendDataEncrypted(System.Byte[],System.Int32)
extern void EnetPeer_SendDataEncrypted_m9C6D6FDFD58916CC6F9B670156D3A7DBF22AA730 (void);
// 0x00000114 System.Void ExitGames.Client.Photon.EnetPeer::QueueSentCommand(ExitGames.Client.Photon.NCommand)
extern void EnetPeer_QueueSentCommand_m25E8E48FB3436E4498E0BF1208611C34FE9BC023 (void);
// 0x00000115 System.Void ExitGames.Client.Photon.EnetPeer::QueueOutgoingReliableCommand(ExitGames.Client.Photon.NCommand)
extern void EnetPeer_QueueOutgoingReliableCommand_m18F7367F1F665391BB9DDD2EE00FF0FB47A5BEC7 (void);
// 0x00000116 System.Void ExitGames.Client.Photon.EnetPeer::QueueOutgoingUnreliableCommand(ExitGames.Client.Photon.NCommand)
extern void EnetPeer_QueueOutgoingUnreliableCommand_mD46505878D1A89EE761F00ED96B45CDD11651F2E (void);
// 0x00000117 System.Void ExitGames.Client.Photon.EnetPeer::QueueOutgoingAcknowledgement(ExitGames.Client.Photon.NCommand,System.Int32)
extern void EnetPeer_QueueOutgoingAcknowledgement_m3142B32F001108364EF6FAECC3E71306A1026196 (void);
// 0x00000118 System.Void ExitGames.Client.Photon.EnetPeer::ReceiveIncomingCommands(System.Byte[],System.Int32)
extern void EnetPeer_ReceiveIncomingCommands_m6934BA2C5ABA3A01676D29A655720410F1EED19E (void);
// 0x00000119 System.Boolean ExitGames.Client.Photon.EnetPeer::ExecuteCommand(ExitGames.Client.Photon.NCommand)
extern void EnetPeer_ExecuteCommand_mC3FCC0808F6BFFB23812DEE44D73EB035123CD00 (void);
// 0x0000011A System.Boolean ExitGames.Client.Photon.EnetPeer::QueueIncomingCommand(ExitGames.Client.Photon.NCommand)
extern void EnetPeer_QueueIncomingCommand_mCCFB2017D73DED9676DD5D6F0682003A7587000C (void);
// 0x0000011B ExitGames.Client.Photon.NCommand ExitGames.Client.Photon.EnetPeer::RemoveSentReliableCommand(System.Int32,System.Int32,System.Boolean)
extern void EnetPeer_RemoveSentReliableCommand_mB3DD19635F42CD363CA3BCB27986E015A4DD7D84 (void);
// 0x0000011C System.Void ExitGames.Client.Photon.EnetPeer::.cctor()
extern void EnetPeer__cctor_mADAB19D86CD5EBC58A2C38D9490E506EE8FFCC59 (void);
// 0x0000011D System.Void ExitGames.Client.Photon.EnetPeer::<ExecuteCommand>b__71_0()
extern void EnetPeer_U3CExecuteCommandU3Eb__71_0_m8541C1486162470F57850A932CFC89128B7EA8D8 (void);
// 0x0000011E System.Void ExitGames.Client.Photon.EnetChannel::.ctor(System.Byte,System.Int32)
extern void EnetChannel__ctor_m27DE491AB4D607CD74B4B4C797AB8ECC71F55FFD (void);
// 0x0000011F System.Boolean ExitGames.Client.Photon.EnetChannel::ContainsUnreliableSequenceNumber(System.Int32)
extern void EnetChannel_ContainsUnreliableSequenceNumber_mC39737900F3B65BF0B8DA66EBCCF9981935110F0 (void);
// 0x00000120 System.Boolean ExitGames.Client.Photon.EnetChannel::ContainsReliableSequenceNumber(System.Int32)
extern void EnetChannel_ContainsReliableSequenceNumber_mE6F01508E0A1861256CD459E5F9B952968AE9D84 (void);
// 0x00000121 ExitGames.Client.Photon.NCommand ExitGames.Client.Photon.EnetChannel::FetchReliableSequenceNumber(System.Int32)
extern void EnetChannel_FetchReliableSequenceNumber_m6F24FB6BCFD362289856B49C4117E897E0D59B70 (void);
// 0x00000122 System.Boolean ExitGames.Client.Photon.EnetChannel::TryGetFragment(System.Int32,System.Boolean,ExitGames.Client.Photon.NCommand&)
extern void EnetChannel_TryGetFragment_mB8F6D45911EE25780D54E6DD1A13F712583E8EC5 (void);
// 0x00000123 System.Void ExitGames.Client.Photon.EnetChannel::RemoveFragment(System.Int32,System.Boolean)
extern void EnetChannel_RemoveFragment_m35877E62DAF48FC2ED3C5710FFB77822CC4C579B (void);
// 0x00000124 System.Void ExitGames.Client.Photon.EnetChannel::clearAll()
extern void EnetChannel_clearAll_m42BE5930070E0C05832C200C59DB401050CE894D (void);
// 0x00000125 System.Boolean ExitGames.Client.Photon.EnetChannel::QueueIncomingReliableUnsequenced(ExitGames.Client.Photon.NCommand)
extern void EnetChannel_QueueIncomingReliableUnsequenced_mED56B16EAD0A232F440B0CFB33FC861893AEEB81 (void);
// 0x00000126 System.Int32 ExitGames.Client.Photon.NCommand::get_SizeOfPayload()
extern void NCommand_get_SizeOfPayload_m3315BB34B3E705E0168E9F82A3A49A7433C84E36 (void);
// 0x00000127 System.Boolean ExitGames.Client.Photon.NCommand::get_IsFlaggedUnsequenced()
extern void NCommand_get_IsFlaggedUnsequenced_m6B9E6E8F62B45AF7D06AD99DEF402470EA83024E (void);
// 0x00000128 System.Boolean ExitGames.Client.Photon.NCommand::get_IsFlaggedReliable()
extern void NCommand_get_IsFlaggedReliable_mD64E895E47FAA56D11372DC725F27BFEC15D9618 (void);
// 0x00000129 System.Void ExitGames.Client.Photon.NCommand::.ctor(ExitGames.Client.Photon.EnetPeer,System.Byte,ExitGames.Client.Photon.StreamBuffer,System.Byte)
extern void NCommand__ctor_m75847131A07A0A65D2CBA53FBC0D3E011A282B5D (void);
// 0x0000012A System.Void ExitGames.Client.Photon.NCommand::CreateAck(System.Byte[],System.Int32,ExitGames.Client.Photon.NCommand,System.Int32)
extern void NCommand_CreateAck_m53FDB2FEA5BD52168164317092BC8CE652E50DA2 (void);
// 0x0000012B System.Void ExitGames.Client.Photon.NCommand::.ctor(ExitGames.Client.Photon.EnetPeer,System.Byte[],System.Int32&)
extern void NCommand__ctor_mD368544E4ACE32DB42A8F0AC51A18DF89C2C9211 (void);
// 0x0000012C System.Void ExitGames.Client.Photon.NCommand::SerializeHeader(System.Byte[],System.Int32&)
extern void NCommand_SerializeHeader_mA2578944C419E5E9397E316BD7F9A4E10700E65B (void);
// 0x0000012D System.Byte[] ExitGames.Client.Photon.NCommand::Serialize()
extern void NCommand_Serialize_mB1BE9852725338715B906B668BF2BF7F3DCCFB01 (void);
// 0x0000012E System.Void ExitGames.Client.Photon.NCommand::FreePayload()
extern void NCommand_FreePayload_m66C17A68D5E64FFB6D007463AB3DFD708AB36AFB (void);
// 0x0000012F System.Int32 ExitGames.Client.Photon.NCommand::CompareTo(ExitGames.Client.Photon.NCommand)
extern void NCommand_CompareTo_m2BC91BD2FA385F8A622B6D83E1D0682C82C8B889 (void);
// 0x00000130 System.String ExitGames.Client.Photon.NCommand::ToString()
extern void NCommand_ToString_mB42A0C647E0D464792B68E9753D1102C07D389EC (void);
// 0x00000131 System.Void ExitGames.Client.Photon.TPeer::.ctor()
extern void TPeer__ctor_m0AAD038F46524E7C33C194CBCB60C2EF4FE2B4A0 (void);
// 0x00000132 System.Void ExitGames.Client.Photon.TPeer::InitPeerBase()
extern void TPeer_InitPeerBase_m933F91DDF8D5C6E5D6453CE2021BD83081767D19 (void);
// 0x00000133 System.Boolean ExitGames.Client.Photon.TPeer::Connect(System.String,System.String,System.Object)
extern void TPeer_Connect_mB5E19BE438AD5C6C98FAE73802202DB9607B7C60 (void);
// 0x00000134 System.Void ExitGames.Client.Photon.TPeer::OnConnect()
extern void TPeer_OnConnect_mD088FF892E5D3C59E690FDE0717D1970ABF2A65E (void);
// 0x00000135 System.Void ExitGames.Client.Photon.TPeer::Disconnect()
extern void TPeer_Disconnect_mD15D80443A8401D03E9210AA1FC78F787F6D3446 (void);
// 0x00000136 System.Void ExitGames.Client.Photon.TPeer::StopConnection()
extern void TPeer_StopConnection_mBA5FC93C53B3BA1C22C49D66145E3F3AA53A77F7 (void);
// 0x00000137 System.Void ExitGames.Client.Photon.TPeer::FetchServerTimestamp()
extern void TPeer_FetchServerTimestamp_mF05A52D2AF86BEB276123975E70C65C7D6A7F62E (void);
// 0x00000138 System.Void ExitGames.Client.Photon.TPeer::EnqueueInit(System.Byte[])
extern void TPeer_EnqueueInit_mDA521CF73AECDBD1E33EA52E891895CD5B0709C1 (void);
// 0x00000139 System.Boolean ExitGames.Client.Photon.TPeer::DispatchIncomingCommands()
extern void TPeer_DispatchIncomingCommands_m553C5D561EF7BFD1D6C0E0AEE9685BA5C1CF2E68 (void);
// 0x0000013A System.Boolean ExitGames.Client.Photon.TPeer::SendOutgoingCommands()
extern void TPeer_SendOutgoingCommands_mAD8142D5186B9FE11694DD522B4CE5EF7995D904 (void);
// 0x0000013B System.Boolean ExitGames.Client.Photon.TPeer::SendAcksOnly()
extern void TPeer_SendAcksOnly_m0F51C9848996F89C697407E0330A1497B9257AAF (void);
// 0x0000013C System.Boolean ExitGames.Client.Photon.TPeer::EnqueueOperation(System.Collections.Generic.Dictionary`2<System.Byte,System.Object>,System.Byte,ExitGames.Client.Photon.SendOptions,ExitGames.Client.Photon.EgMessageType)
extern void TPeer_EnqueueOperation_m0FC01EF92EBB29BB78967379AD74F93E50C6E375 (void);
// 0x0000013D ExitGames.Client.Photon.StreamBuffer ExitGames.Client.Photon.TPeer::SerializeOperationToMessage(System.Byte,System.Collections.Generic.Dictionary`2<System.Byte,System.Object>,ExitGames.Client.Photon.EgMessageType,System.Boolean)
extern void TPeer_SerializeOperationToMessage_m40F15E7EEABE1E7F44D95FB13E0D711991EBD754 (void);
// 0x0000013E System.Boolean ExitGames.Client.Photon.TPeer::EnqueueMessageAsPayload(ExitGames.Client.Photon.DeliveryMode,ExitGames.Client.Photon.StreamBuffer,System.Byte)
extern void TPeer_EnqueueMessageAsPayload_m7A0A4E1AAAED9F89038E8F2C20900AAE17144484 (void);
// 0x0000013F System.Void ExitGames.Client.Photon.TPeer::SendPing()
extern void TPeer_SendPing_m700E8BEE15A1825E0C84DD69D25EAE8799461B11 (void);
// 0x00000140 System.Void ExitGames.Client.Photon.TPeer::SendData(System.Byte[],System.Int32)
extern void TPeer_SendData_mE46B18DAFDC4FF4190E07EF8F160C58D41358638 (void);
// 0x00000141 System.Void ExitGames.Client.Photon.TPeer::ReceiveIncomingCommands(System.Byte[],System.Int32)
extern void TPeer_ReceiveIncomingCommands_m0260CF906CEB6D7A6F571E3F147CA4717AEDAA10 (void);
// 0x00000142 System.Void ExitGames.Client.Photon.TPeer::ReadPingResult(System.Byte[])
extern void TPeer_ReadPingResult_m27549D3A36D87047F8BBE45202ADF109F3F929EE (void);
// 0x00000143 System.Void ExitGames.Client.Photon.TPeer::ReadPingResult(ExitGames.Client.Photon.OperationResponse)
extern void TPeer_ReadPingResult_mE1983BB482F9F5B5628E6D88F89D3C37CC7C88FB (void);
// 0x00000144 System.Void ExitGames.Client.Photon.TPeer::.cctor()
extern void TPeer__cctor_mFBCEDEDC2A674F76E7F358B1D6BCE6EEB7E54F78 (void);
// 0x00000145 ExitGames.Client.Photon.IProtocol ExitGames.Client.Photon.SerializationProtocolFactory::Create(ExitGames.Client.Photon.SerializationProtocol)
extern void SerializationProtocolFactory_Create_m5F16DBF123F6C86DE1F7A88A829261A317B195E0 (void);
// 0x00000146 System.String ExitGames.Client.Photon.IProtocol::get_ProtocolType()
// 0x00000147 System.Byte[] ExitGames.Client.Photon.IProtocol::get_VersionBytes()
// 0x00000148 System.Void ExitGames.Client.Photon.IProtocol::Serialize(ExitGames.Client.Photon.StreamBuffer,System.Object,System.Boolean)
// 0x00000149 System.Void ExitGames.Client.Photon.IProtocol::SerializeShort(ExitGames.Client.Photon.StreamBuffer,System.Int16,System.Boolean)
// 0x0000014A System.Void ExitGames.Client.Photon.IProtocol::SerializeString(ExitGames.Client.Photon.StreamBuffer,System.String,System.Boolean)
// 0x0000014B System.Void ExitGames.Client.Photon.IProtocol::SerializeEventData(ExitGames.Client.Photon.StreamBuffer,ExitGames.Client.Photon.EventData,System.Boolean)
// 0x0000014C System.Void ExitGames.Client.Photon.IProtocol::SerializeOperationRequest(ExitGames.Client.Photon.StreamBuffer,System.Byte,System.Collections.Generic.Dictionary`2<System.Byte,System.Object>,System.Boolean)
// 0x0000014D System.Void ExitGames.Client.Photon.IProtocol::SerializeOperationResponse(ExitGames.Client.Photon.StreamBuffer,ExitGames.Client.Photon.OperationResponse,System.Boolean)
// 0x0000014E System.Object ExitGames.Client.Photon.IProtocol::Deserialize(ExitGames.Client.Photon.StreamBuffer,System.Byte)
// 0x0000014F System.Int16 ExitGames.Client.Photon.IProtocol::DeserializeShort(ExitGames.Client.Photon.StreamBuffer)
// 0x00000150 System.Byte ExitGames.Client.Photon.IProtocol::DeserializeByte(ExitGames.Client.Photon.StreamBuffer)
// 0x00000151 ExitGames.Client.Photon.EventData ExitGames.Client.Photon.IProtocol::DeserializeEventData(ExitGames.Client.Photon.StreamBuffer,ExitGames.Client.Photon.EventData)
// 0x00000152 ExitGames.Client.Photon.OperationRequest ExitGames.Client.Photon.IProtocol::DeserializeOperationRequest(ExitGames.Client.Photon.StreamBuffer)
// 0x00000153 ExitGames.Client.Photon.OperationResponse ExitGames.Client.Photon.IProtocol::DeserializeOperationResponse(ExitGames.Client.Photon.StreamBuffer)
// 0x00000154 System.Byte[] ExitGames.Client.Photon.IProtocol::Serialize(System.Object)
extern void IProtocol_Serialize_mC230D51065735957460BFA29F0EC0BF6DD9A7278 (void);
// 0x00000155 System.Object ExitGames.Client.Photon.IProtocol::DeserializeMessage(ExitGames.Client.Photon.StreamBuffer)
extern void IProtocol_DeserializeMessage_mC02A0E20435EDB669F67D0E0F865A83B91C1E0E5 (void);
// 0x00000156 System.Void ExitGames.Client.Photon.IProtocol::.ctor()
extern void IProtocol__ctor_m210F07501179E0D1583384E853319379CE615D29 (void);
// 0x00000157 System.Void ExitGames.Client.Photon.OperationRequest::.ctor()
extern void OperationRequest__ctor_mAAE3BDF567044323DD790053485BA12285BB986A (void);
// 0x00000158 System.Object ExitGames.Client.Photon.OperationResponse::get_Item(System.Byte)
extern void OperationResponse_get_Item_mFB5F85A07DEEF51181E49C8F80C1FEC67BDFAAE9 (void);
// 0x00000159 System.String ExitGames.Client.Photon.OperationResponse::ToString()
extern void OperationResponse_ToString_m1E6A5A078575A5956918F1C076F8A59DFEAE0886 (void);
// 0x0000015A System.String ExitGames.Client.Photon.OperationResponse::ToStringFull()
extern void OperationResponse_ToStringFull_m44C8BD752CF85BA4EE8091B55CD2B567F4493271 (void);
// 0x0000015B System.Void ExitGames.Client.Photon.OperationResponse::.ctor()
extern void OperationResponse__ctor_m8DFAE336235BEDD612AAA6E756A132F8ECC1DF31 (void);
// 0x0000015C System.Object ExitGames.Client.Photon.EventData::get_Item(System.Byte)
extern void EventData_get_Item_m9F3B5A208B3320BE5703C900234045F531945D09 (void);
// 0x0000015D System.Int32 ExitGames.Client.Photon.EventData::get_Sender()
extern void EventData_get_Sender_m2C4FD0BD7FB932831A4FE09DA5684BF47A097C7E (void);
// 0x0000015E System.Object ExitGames.Client.Photon.EventData::get_CustomData()
extern void EventData_get_CustomData_mC2B19B362AD05F80FE3C0A3942298A274A7905B6 (void);
// 0x0000015F System.Void ExitGames.Client.Photon.EventData::Reset()
extern void EventData_Reset_mA6F9E66B16D35D5F05413A673AEC4C271AE1B237 (void);
// 0x00000160 System.String ExitGames.Client.Photon.EventData::ToString()
extern void EventData_ToString_m9ACDEF9B8BBB9B08E9753D0581A9E4C5C016D015 (void);
// 0x00000161 System.Void ExitGames.Client.Photon.EventData::.ctor()
extern void EventData__ctor_mFEC6E52A01DA96CF8744498E59912E2463FDCE39 (void);
// 0x00000162 System.Void ExitGames.Client.Photon.SerializeMethod::.ctor(System.Object,System.IntPtr)
extern void SerializeMethod__ctor_m53A0D155AEA1303B5C087D8AECCF5E2106BF01F8 (void);
// 0x00000163 System.Byte[] ExitGames.Client.Photon.SerializeMethod::Invoke(System.Object)
extern void SerializeMethod_Invoke_mF244EE2F86DE7D2BA965F242191800544C0C3200 (void);
// 0x00000164 System.IAsyncResult ExitGames.Client.Photon.SerializeMethod::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
extern void SerializeMethod_BeginInvoke_mE1905D51F9776AA4670E6592459036E3735367FB (void);
// 0x00000165 System.Byte[] ExitGames.Client.Photon.SerializeMethod::EndInvoke(System.IAsyncResult)
extern void SerializeMethod_EndInvoke_m7933046D7C2F615CF4E93131E90B162EE98576F5 (void);
// 0x00000166 System.Void ExitGames.Client.Photon.SerializeStreamMethod::.ctor(System.Object,System.IntPtr)
extern void SerializeStreamMethod__ctor_m18AEEE1ACFECE593803F5BACB876161A9B90BAEC (void);
// 0x00000167 System.Int16 ExitGames.Client.Photon.SerializeStreamMethod::Invoke(ExitGames.Client.Photon.StreamBuffer,System.Object)
extern void SerializeStreamMethod_Invoke_mCCD2BE0E82AE7C7D55830163421FBF91C8A0FCBC (void);
// 0x00000168 System.IAsyncResult ExitGames.Client.Photon.SerializeStreamMethod::BeginInvoke(ExitGames.Client.Photon.StreamBuffer,System.Object,System.AsyncCallback,System.Object)
extern void SerializeStreamMethod_BeginInvoke_m96AEBC54D3B30CF7BB8999A177CBCB22FF5AE907 (void);
// 0x00000169 System.Int16 ExitGames.Client.Photon.SerializeStreamMethod::EndInvoke(System.IAsyncResult)
extern void SerializeStreamMethod_EndInvoke_m5ED21923CC063F16EF3717A906A1CA18148EC29A (void);
// 0x0000016A System.Void ExitGames.Client.Photon.DeserializeMethod::.ctor(System.Object,System.IntPtr)
extern void DeserializeMethod__ctor_m3DF590D0362710894C97C458B50AF7C8E50C38FF (void);
// 0x0000016B System.Object ExitGames.Client.Photon.DeserializeMethod::Invoke(System.Byte[])
extern void DeserializeMethod_Invoke_m70CBA7C1B5A21F7C6356AB4D0563E35C3B1DFA5C (void);
// 0x0000016C System.IAsyncResult ExitGames.Client.Photon.DeserializeMethod::BeginInvoke(System.Byte[],System.AsyncCallback,System.Object)
extern void DeserializeMethod_BeginInvoke_m8DF87D6B7302FF6D61ED3F57918181C500C95039 (void);
// 0x0000016D System.Object ExitGames.Client.Photon.DeserializeMethod::EndInvoke(System.IAsyncResult)
extern void DeserializeMethod_EndInvoke_mEF00BEAF2019744BFD2F68C77F785038964EE120 (void);
// 0x0000016E System.Void ExitGames.Client.Photon.DeserializeStreamMethod::.ctor(System.Object,System.IntPtr)
extern void DeserializeStreamMethod__ctor_m262F59ADCC2C3231C1669392405B0EEC17D0BCBF (void);
// 0x0000016F System.Object ExitGames.Client.Photon.DeserializeStreamMethod::Invoke(ExitGames.Client.Photon.StreamBuffer,System.Int16)
extern void DeserializeStreamMethod_Invoke_m6FD40B3E411B8CD37D8992B4DDCF74BF539B8193 (void);
// 0x00000170 System.IAsyncResult ExitGames.Client.Photon.DeserializeStreamMethod::BeginInvoke(ExitGames.Client.Photon.StreamBuffer,System.Int16,System.AsyncCallback,System.Object)
extern void DeserializeStreamMethod_BeginInvoke_mBFB52E941674D727215F5FA3D6577831BA678203 (void);
// 0x00000171 System.Object ExitGames.Client.Photon.DeserializeStreamMethod::EndInvoke(System.IAsyncResult)
extern void DeserializeStreamMethod_EndInvoke_m83B7093FE124B2300B51B912915D6C6A1C682577 (void);
// 0x00000172 System.Void ExitGames.Client.Photon.CustomType::.ctor(System.Type,System.Byte,ExitGames.Client.Photon.SerializeStreamMethod,ExitGames.Client.Photon.DeserializeStreamMethod)
extern void CustomType__ctor_m9B5896E661D1A284E3F424D408F6C09AC9B132C9 (void);
// 0x00000173 System.Boolean ExitGames.Client.Photon.Protocol::TryRegisterType(System.Type,System.Byte,ExitGames.Client.Photon.SerializeStreamMethod,ExitGames.Client.Photon.DeserializeStreamMethod)
extern void Protocol_TryRegisterType_m1F42CEE04C1CB3AA95A4EFCF4E71BBEF1EF4941A (void);
// 0x00000174 System.Void ExitGames.Client.Photon.Protocol::Serialize(System.Int16,System.Byte[],System.Int32&)
extern void Protocol_Serialize_m51A1A0D9AA1D7ECF0451A85B1A2503BCBA159C44 (void);
// 0x00000175 System.Void ExitGames.Client.Photon.Protocol::Serialize(System.Int32,System.Byte[],System.Int32&)
extern void Protocol_Serialize_m68E82AB3C30D00432FF336B2DA2F728CA841AFF9 (void);
// 0x00000176 System.Void ExitGames.Client.Photon.Protocol::Serialize(System.Single,System.Byte[],System.Int32&)
extern void Protocol_Serialize_mB0113005516D9F65E01D660036CA261988B4AD77 (void);
// 0x00000177 System.Void ExitGames.Client.Photon.Protocol::Deserialize(System.Int32&,System.Byte[],System.Int32&)
extern void Protocol_Deserialize_mA419E340A605A24E5E58E167D7C8B491B1A97E5E (void);
// 0x00000178 System.Void ExitGames.Client.Photon.Protocol::Deserialize(System.Int16&,System.Byte[],System.Int32&)
extern void Protocol_Deserialize_mC51EDC50C443F02A9A10F662B9A6B90E33E35283 (void);
// 0x00000179 System.Void ExitGames.Client.Photon.Protocol::Deserialize(System.Single&,System.Byte[],System.Int32&)
extern void Protocol_Deserialize_m0C8F5B0748310559281378101B4D945823CD0406 (void);
// 0x0000017A System.Void ExitGames.Client.Photon.Protocol::.cctor()
extern void Protocol__cctor_m4464D147D032ECA451D409E29FFE31B23FF8DC20 (void);
// 0x0000017B System.String ExitGames.Client.Photon.Protocol16::get_ProtocolType()
extern void Protocol16_get_ProtocolType_m4117BFA067EA5B6D95CB543A7D3500B33D719A5F (void);
// 0x0000017C System.Byte[] ExitGames.Client.Photon.Protocol16::get_VersionBytes()
extern void Protocol16_get_VersionBytes_mFFC0E7D06CC68114C4824681AE461DE0DDFFFDCA (void);
// 0x0000017D System.Boolean ExitGames.Client.Photon.Protocol16::SerializeCustom(ExitGames.Client.Photon.StreamBuffer,System.Object)
extern void Protocol16_SerializeCustom_m51F02A854AB6D79AE3D2C31141A905F670C10D93 (void);
// 0x0000017E System.Object ExitGames.Client.Photon.Protocol16::DeserializeCustom(ExitGames.Client.Photon.StreamBuffer,System.Byte)
extern void Protocol16_DeserializeCustom_mA66301817515061C364B96A654724B7587078715 (void);
// 0x0000017F System.Type ExitGames.Client.Photon.Protocol16::GetTypeOfCode(System.Byte)
extern void Protocol16_GetTypeOfCode_mBB4406039CD2930C880AA7A344DFBEDA7A76A6A5 (void);
// 0x00000180 ExitGames.Client.Photon.Protocol16/GpType ExitGames.Client.Photon.Protocol16::GetCodeOfType(System.Type)
extern void Protocol16_GetCodeOfType_m404353148323F3B7BAABED19054E00B8250C2795 (void);
// 0x00000181 System.Array ExitGames.Client.Photon.Protocol16::CreateArrayByType(System.Byte,System.Int16)
extern void Protocol16_CreateArrayByType_m4B7180AE6ABFAF8D027A8E4B7F55650369FAF8F0 (void);
// 0x00000182 System.Void ExitGames.Client.Photon.Protocol16::SerializeOperationRequest(ExitGames.Client.Photon.StreamBuffer,ExitGames.Client.Photon.OperationRequest,System.Boolean)
extern void Protocol16_SerializeOperationRequest_m4BBA5011C31AD542F763E9F67475441AB12C2575 (void);
// 0x00000183 System.Void ExitGames.Client.Photon.Protocol16::SerializeOperationRequest(ExitGames.Client.Photon.StreamBuffer,System.Byte,System.Collections.Generic.Dictionary`2<System.Byte,System.Object>,System.Boolean)
extern void Protocol16_SerializeOperationRequest_m60BE0F1CC22EFE1B0BCB66DE0883AAF32D25B74A (void);
// 0x00000184 ExitGames.Client.Photon.OperationRequest ExitGames.Client.Photon.Protocol16::DeserializeOperationRequest(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol16_DeserializeOperationRequest_m7BF6E3B988F16DA080B43FABA4C9B1460014BD8D (void);
// 0x00000185 System.Void ExitGames.Client.Photon.Protocol16::SerializeOperationResponse(ExitGames.Client.Photon.StreamBuffer,ExitGames.Client.Photon.OperationResponse,System.Boolean)
extern void Protocol16_SerializeOperationResponse_mEE7DF98645155131422FE54F45DE1F284F420F64 (void);
// 0x00000186 ExitGames.Client.Photon.OperationResponse ExitGames.Client.Photon.Protocol16::DeserializeOperationResponse(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol16_DeserializeOperationResponse_mE4931FC9875501D583E4CFEF4451DBAC1D6D196F (void);
// 0x00000187 System.Void ExitGames.Client.Photon.Protocol16::SerializeEventData(ExitGames.Client.Photon.StreamBuffer,ExitGames.Client.Photon.EventData,System.Boolean)
extern void Protocol16_SerializeEventData_mFFDA044BAA2401B07AEFD64A6022169D2993E5D9 (void);
// 0x00000188 ExitGames.Client.Photon.EventData ExitGames.Client.Photon.Protocol16::DeserializeEventData(ExitGames.Client.Photon.StreamBuffer,ExitGames.Client.Photon.EventData)
extern void Protocol16_DeserializeEventData_m1360D20976EC991DA3B0846C887265E2A1692894 (void);
// 0x00000189 System.Void ExitGames.Client.Photon.Protocol16::SerializeParameterTable(ExitGames.Client.Photon.StreamBuffer,System.Collections.Generic.Dictionary`2<System.Byte,System.Object>)
extern void Protocol16_SerializeParameterTable_m477E86E43B9D749F3FC4F2CD44374EC4E37CCA5F (void);
// 0x0000018A System.Collections.Generic.Dictionary`2<System.Byte,System.Object> ExitGames.Client.Photon.Protocol16::DeserializeParameterTable(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol16_DeserializeParameterTable_m29ED70DB84DB714CB981AE64677B221F88212D7C (void);
// 0x0000018B System.Void ExitGames.Client.Photon.Protocol16::Serialize(ExitGames.Client.Photon.StreamBuffer,System.Object,System.Boolean)
extern void Protocol16_Serialize_mD67437DF9C379AB41269F636FF2A9B53F06B44D1 (void);
// 0x0000018C System.Void ExitGames.Client.Photon.Protocol16::SerializeByte(ExitGames.Client.Photon.StreamBuffer,System.Byte,System.Boolean)
extern void Protocol16_SerializeByte_mE7A2DE97766F460339C5AAAE8FD49EA71C7CCCB7 (void);
// 0x0000018D System.Void ExitGames.Client.Photon.Protocol16::SerializeBoolean(ExitGames.Client.Photon.StreamBuffer,System.Boolean,System.Boolean)
extern void Protocol16_SerializeBoolean_m961D93A4C48B2F261666BF4A4AE32E7816B561CE (void);
// 0x0000018E System.Void ExitGames.Client.Photon.Protocol16::SerializeShort(ExitGames.Client.Photon.StreamBuffer,System.Int16,System.Boolean)
extern void Protocol16_SerializeShort_mA017EE1ACF8E528E1F1110F0D02666319FF9AA0E (void);
// 0x0000018F System.Void ExitGames.Client.Photon.Protocol16::SerializeInteger(ExitGames.Client.Photon.StreamBuffer,System.Int32,System.Boolean)
extern void Protocol16_SerializeInteger_mAB82399E7917AF27CB112E4D7481E6671822CC8A (void);
// 0x00000190 System.Void ExitGames.Client.Photon.Protocol16::SerializeLong(ExitGames.Client.Photon.StreamBuffer,System.Int64,System.Boolean)
extern void Protocol16_SerializeLong_mCBB1B4DE3B55AA49FD22A26A1483F59B10863729 (void);
// 0x00000191 System.Void ExitGames.Client.Photon.Protocol16::SerializeFloat(ExitGames.Client.Photon.StreamBuffer,System.Single,System.Boolean)
extern void Protocol16_SerializeFloat_mE4A5403413074E677BAAB7B00A4544EEA42AE9E3 (void);
// 0x00000192 System.Void ExitGames.Client.Photon.Protocol16::SerializeDouble(ExitGames.Client.Photon.StreamBuffer,System.Double,System.Boolean)
extern void Protocol16_SerializeDouble_m5E8D87283ACF0BDFB1ACF55E684129398DD62B05 (void);
// 0x00000193 System.Void ExitGames.Client.Photon.Protocol16::SerializeString(ExitGames.Client.Photon.StreamBuffer,System.String,System.Boolean)
extern void Protocol16_SerializeString_m945B7BB907C8776CC66F14B51B403A037AADD527 (void);
// 0x00000194 System.Void ExitGames.Client.Photon.Protocol16::SerializeArray(ExitGames.Client.Photon.StreamBuffer,System.Array,System.Boolean)
extern void Protocol16_SerializeArray_mD9970EF06A769EF994AD8FA64F064093C17A3269 (void);
// 0x00000195 System.Void ExitGames.Client.Photon.Protocol16::SerializeByteArray(ExitGames.Client.Photon.StreamBuffer,System.Byte[],System.Boolean)
extern void Protocol16_SerializeByteArray_m345789FD0A865793B52636722E792A58835DDB44 (void);
// 0x00000196 System.Void ExitGames.Client.Photon.Protocol16::SerializeByteArraySegment(ExitGames.Client.Photon.StreamBuffer,System.Byte[],System.Int32,System.Int32,System.Boolean)
extern void Protocol16_SerializeByteArraySegment_mE994FE9493B4A87A319E876A7675718E4715D969 (void);
// 0x00000197 System.Void ExitGames.Client.Photon.Protocol16::SerializeIntArrayOptimized(ExitGames.Client.Photon.StreamBuffer,System.Int32[],System.Boolean)
extern void Protocol16_SerializeIntArrayOptimized_m2A965AC0B57EF717BEEAC2902E4652DA39610827 (void);
// 0x00000198 System.Void ExitGames.Client.Photon.Protocol16::SerializeObjectArray(ExitGames.Client.Photon.StreamBuffer,System.Collections.IList,System.Boolean)
extern void Protocol16_SerializeObjectArray_mE1543AF4B9C3DC00900F19BD8BDDCFFD769E15F2 (void);
// 0x00000199 System.Void ExitGames.Client.Photon.Protocol16::SerializeHashTable(ExitGames.Client.Photon.StreamBuffer,ExitGames.Client.Photon.Hashtable,System.Boolean)
extern void Protocol16_SerializeHashTable_mD9A2245C98278F9B0666D526ECDF93CC7E92B7BE (void);
// 0x0000019A System.Void ExitGames.Client.Photon.Protocol16::SerializeDictionary(ExitGames.Client.Photon.StreamBuffer,System.Collections.IDictionary,System.Boolean)
extern void Protocol16_SerializeDictionary_mAB6B3817D0F0DA74146E7C583D9BA80B5852A907 (void);
// 0x0000019B System.Void ExitGames.Client.Photon.Protocol16::SerializeDictionaryHeader(ExitGames.Client.Photon.StreamBuffer,System.Type)
extern void Protocol16_SerializeDictionaryHeader_mF01ABD689EB216F5439254691DF2BF6EE376081E (void);
// 0x0000019C System.Void ExitGames.Client.Photon.Protocol16::SerializeDictionaryHeader(ExitGames.Client.Photon.StreamBuffer,System.Object,System.Boolean&,System.Boolean&)
extern void Protocol16_SerializeDictionaryHeader_mE3176475FB07D641CAA268C89FE3D32382BFD10A (void);
// 0x0000019D System.Void ExitGames.Client.Photon.Protocol16::SerializeDictionaryElements(ExitGames.Client.Photon.StreamBuffer,System.Object,System.Boolean,System.Boolean)
extern void Protocol16_SerializeDictionaryElements_m029994D35AB62954E2DD4F17B387EBFA4FAACEC3 (void);
// 0x0000019E System.Object ExitGames.Client.Photon.Protocol16::Deserialize(ExitGames.Client.Photon.StreamBuffer,System.Byte)
extern void Protocol16_Deserialize_m2D4E73FE116ABC6469C0275C21411E337BDF4F15 (void);
// 0x0000019F System.Byte ExitGames.Client.Photon.Protocol16::DeserializeByte(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol16_DeserializeByte_mCC85BCCD41C1EAEF3FD34BBA7A40F51CC7ED40DF (void);
// 0x000001A0 System.Boolean ExitGames.Client.Photon.Protocol16::DeserializeBoolean(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol16_DeserializeBoolean_mC5E230DE51DA7262F631B11CB18431E3B9622405 (void);
// 0x000001A1 System.Int16 ExitGames.Client.Photon.Protocol16::DeserializeShort(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol16_DeserializeShort_m423E63D87D0E1AC87D581E1AD30FDAB4AA505969 (void);
// 0x000001A2 System.Int32 ExitGames.Client.Photon.Protocol16::DeserializeInteger(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol16_DeserializeInteger_m9894A4B1BF5B4095D57EA34F443D0ED796A7B423 (void);
// 0x000001A3 System.Int64 ExitGames.Client.Photon.Protocol16::DeserializeLong(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol16_DeserializeLong_m0076276DA3AD3FF724F8C4494D1A460F142157D7 (void);
// 0x000001A4 System.Single ExitGames.Client.Photon.Protocol16::DeserializeFloat(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol16_DeserializeFloat_mEBF6EBC72148213980984E7F1FFA154E4C649819 (void);
// 0x000001A5 System.Double ExitGames.Client.Photon.Protocol16::DeserializeDouble(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol16_DeserializeDouble_m05FC8B4B58B0E26AF8C12E5C531AD720EB042BEA (void);
// 0x000001A6 System.String ExitGames.Client.Photon.Protocol16::DeserializeString(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol16_DeserializeString_mE6BB13EBE18335BD2E38049C6733F50EA980B3B9 (void);
// 0x000001A7 System.Array ExitGames.Client.Photon.Protocol16::DeserializeArray(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol16_DeserializeArray_m79B5F660AAAD7A460AD3E8EF96088769FFAB5B78 (void);
// 0x000001A8 System.Byte[] ExitGames.Client.Photon.Protocol16::DeserializeByteArray(ExitGames.Client.Photon.StreamBuffer,System.Int32)
extern void Protocol16_DeserializeByteArray_mACB1269F8EA101451CC692E2379BCCFC077773E8 (void);
// 0x000001A9 System.Int32[] ExitGames.Client.Photon.Protocol16::DeserializeIntArray(ExitGames.Client.Photon.StreamBuffer,System.Int32)
extern void Protocol16_DeserializeIntArray_m3BFF6BBC4EEBE0F694949ABF0BF8F6E78EB0A6C9 (void);
// 0x000001AA System.String[] ExitGames.Client.Photon.Protocol16::DeserializeStringArray(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol16_DeserializeStringArray_mB4DF22E304CC4FFB1B06DAAACC3801E9BBFC4E80 (void);
// 0x000001AB System.Object[] ExitGames.Client.Photon.Protocol16::DeserializeObjectArray(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol16_DeserializeObjectArray_m27E7107257D3D0C4BB50581967336A9C30925A08 (void);
// 0x000001AC ExitGames.Client.Photon.Hashtable ExitGames.Client.Photon.Protocol16::DeserializeHashTable(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol16_DeserializeHashTable_mFDDA9CD7ABC1330FDC66AF3328A0C3BC2398F441 (void);
// 0x000001AD System.Collections.IDictionary ExitGames.Client.Photon.Protocol16::DeserializeDictionary(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol16_DeserializeDictionary_m176EC6C735BCC77731D7A994D12F4876E8DF6B59 (void);
// 0x000001AE System.Boolean ExitGames.Client.Photon.Protocol16::DeserializeDictionaryArray(ExitGames.Client.Photon.StreamBuffer,System.Int16,System.Array&)
extern void Protocol16_DeserializeDictionaryArray_m579130A4CA868EE9AF7DC6D9A8B59E43C326463E (void);
// 0x000001AF System.Type ExitGames.Client.Photon.Protocol16::DeserializeDictionaryType(ExitGames.Client.Photon.StreamBuffer,System.Byte&,System.Byte&)
extern void Protocol16_DeserializeDictionaryType_m7CFD67ECB349092C3017269D75C71C0F61648418 (void);
// 0x000001B0 System.Void ExitGames.Client.Photon.Protocol16::.ctor()
extern void Protocol16__ctor_m5DD0125F13E5A36EEA804792D8E22AE165EC7849 (void);
// 0x000001B1 System.Void ExitGames.Client.Photon.Protocol16::.cctor()
extern void Protocol16__cctor_m5BF7B1F43A20AD8AFDB1D561D99A4DDC41A37265 (void);
// 0x000001B2 System.Void ExitGames.Client.Photon.InvalidDataException::.ctor(System.String)
extern void InvalidDataException__ctor_m758F4FD47275F3EE043B409F144D4CA828F9134F (void);
// 0x000001B3 System.String ExitGames.Client.Photon.Protocol18::get_ProtocolType()
extern void Protocol18_get_ProtocolType_m08E01A92C24E912CD2FD903877C80D7D759D37C2 (void);
// 0x000001B4 System.Byte[] ExitGames.Client.Photon.Protocol18::get_VersionBytes()
extern void Protocol18_get_VersionBytes_mAF0EB5AEB64ABD146A3286295709213C4D1872C7 (void);
// 0x000001B5 System.Void ExitGames.Client.Photon.Protocol18::Serialize(ExitGames.Client.Photon.StreamBuffer,System.Object,System.Boolean)
extern void Protocol18_Serialize_m7C6FB14B76FF91615FE6FD3CA83D1841A52055E7 (void);
// 0x000001B6 System.Void ExitGames.Client.Photon.Protocol18::SerializeShort(ExitGames.Client.Photon.StreamBuffer,System.Int16,System.Boolean)
extern void Protocol18_SerializeShort_mBB0F0D7218C747B5907831D954C59BFE68F52389 (void);
// 0x000001B7 System.Void ExitGames.Client.Photon.Protocol18::SerializeString(ExitGames.Client.Photon.StreamBuffer,System.String,System.Boolean)
extern void Protocol18_SerializeString_mC4E096D4D5CE13B493E64B0C3D51441AE1AB7CBE (void);
// 0x000001B8 System.Object ExitGames.Client.Photon.Protocol18::Deserialize(ExitGames.Client.Photon.StreamBuffer,System.Byte)
extern void Protocol18_Deserialize_m1FE393106DF14380C910828D4D323CEB143461AB (void);
// 0x000001B9 System.Int16 ExitGames.Client.Photon.Protocol18::DeserializeShort(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_DeserializeShort_m2432676BFF74D3B16DA7DBE7FB8C24CA339D3235 (void);
// 0x000001BA System.Byte ExitGames.Client.Photon.Protocol18::DeserializeByte(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_DeserializeByte_mCD9F2A1CDDD1C5FD2B11E87DA7E48B4C3515F7E9 (void);
// 0x000001BB System.Type ExitGames.Client.Photon.Protocol18::GetAllowedDictionaryKeyTypes(ExitGames.Client.Photon.Protocol18/GpType)
extern void Protocol18_GetAllowedDictionaryKeyTypes_m2BC01A796D123A0C27B309F825816BC90293BF6B (void);
// 0x000001BC System.Type ExitGames.Client.Photon.Protocol18::GetClrArrayType(ExitGames.Client.Photon.Protocol18/GpType)
extern void Protocol18_GetClrArrayType_mDD8DB56376D2E59F33317387A599985DCC7EF929 (void);
// 0x000001BD ExitGames.Client.Photon.Protocol18/GpType ExitGames.Client.Photon.Protocol18::GetCodeOfType(System.Type)
extern void Protocol18_GetCodeOfType_m395B9C479ADF3DFB54D8497114680B4D77A7980A (void);
// 0x000001BE ExitGames.Client.Photon.Protocol18/GpType ExitGames.Client.Photon.Protocol18::GetCodeOfTypeCode(System.TypeCode)
extern void Protocol18_GetCodeOfTypeCode_mA91B7B6663DBC8919048BC671B5EF8A3DD31AAF8 (void);
// 0x000001BF System.Object ExitGames.Client.Photon.Protocol18::Read(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_Read_mFB5CCF85CBEAA840D46BF84D2D559B245AE4149B (void);
// 0x000001C0 System.Object ExitGames.Client.Photon.Protocol18::Read(ExitGames.Client.Photon.StreamBuffer,System.Byte)
extern void Protocol18_Read_m0C9C1B33AD94BE442B79C9E7F7847108AB21C5E5 (void);
// 0x000001C1 System.Boolean ExitGames.Client.Photon.Protocol18::ReadBoolean(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadBoolean_mA741304E3977EA7C673BAD06AD347CE0BA0E46E1 (void);
// 0x000001C2 System.Byte ExitGames.Client.Photon.Protocol18::ReadByte(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadByte_m0130C5C2D77AA5B51B9D200A970A28803B63DCF2 (void);
// 0x000001C3 System.Int16 ExitGames.Client.Photon.Protocol18::ReadInt16(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadInt16_mCFE1065DD31950DEE0DE7011767CECEA8AD08310 (void);
// 0x000001C4 System.UInt16 ExitGames.Client.Photon.Protocol18::ReadUShort(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadUShort_mC69C03B256F0671166941F2730109CC6077FCEE8 (void);
// 0x000001C5 System.Single ExitGames.Client.Photon.Protocol18::ReadSingle(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadSingle_m1A82A3E2FF37D73EB2D97F37825F94A91E6D4D35 (void);
// 0x000001C6 System.Double ExitGames.Client.Photon.Protocol18::ReadDouble(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadDouble_m72FD5E1AD19325D3CFC20192FFE13C81CC8E2988 (void);
// 0x000001C7 System.Byte[] ExitGames.Client.Photon.Protocol18::ReadByteArray(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadByteArray_m238D163A841FD8B36FDFA3902D5F1B1BC33F1822 (void);
// 0x000001C8 System.Object ExitGames.Client.Photon.Protocol18::ReadCustomType(ExitGames.Client.Photon.StreamBuffer,System.Byte)
extern void Protocol18_ReadCustomType_m3541BB96FCC6D4DA0B5DAB480F4666B311C1B35C (void);
// 0x000001C9 ExitGames.Client.Photon.EventData ExitGames.Client.Photon.Protocol18::DeserializeEventData(ExitGames.Client.Photon.StreamBuffer,ExitGames.Client.Photon.EventData)
extern void Protocol18_DeserializeEventData_m11BE750DC8B6CAF17D1DDF637BDDCAEB103797BB (void);
// 0x000001CA System.Collections.Generic.Dictionary`2<System.Byte,System.Object> ExitGames.Client.Photon.Protocol18::ReadParameterTable(ExitGames.Client.Photon.StreamBuffer,System.Collections.Generic.Dictionary`2<System.Byte,System.Object>)
extern void Protocol18_ReadParameterTable_mEF088E2A7264C72F23F6AA55FF927DEFD3022FD2 (void);
// 0x000001CB ExitGames.Client.Photon.Hashtable ExitGames.Client.Photon.Protocol18::ReadHashtable(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadHashtable_mC9C7A07179B4ED749E13618D8A18DB98991BA46B (void);
// 0x000001CC ExitGames.Client.Photon.OperationRequest ExitGames.Client.Photon.Protocol18::DeserializeOperationRequest(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_DeserializeOperationRequest_m35D7A5186DC4D10D979CA3B29D299A1FCE372EC0 (void);
// 0x000001CD ExitGames.Client.Photon.OperationResponse ExitGames.Client.Photon.Protocol18::DeserializeOperationResponse(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_DeserializeOperationResponse_m40299B281C32BD65340D38A84BEEA6D2CF6804B6 (void);
// 0x000001CE System.String ExitGames.Client.Photon.Protocol18::ReadString(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadString_m7F0C36011DD5A3998323F631D9B59371F123C6D5 (void);
// 0x000001CF System.Object ExitGames.Client.Photon.Protocol18::ReadCustomTypeArray(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadCustomTypeArray_mE488D22819AF87A2AF128B195814BC1FD2AFD08D (void);
// 0x000001D0 System.Type ExitGames.Client.Photon.Protocol18::ReadDictionaryType(ExitGames.Client.Photon.StreamBuffer,ExitGames.Client.Photon.Protocol18/GpType&,ExitGames.Client.Photon.Protocol18/GpType&)
extern void Protocol18_ReadDictionaryType_mEEB530A532207371C9D429D1888A549F1E18B816 (void);
// 0x000001D1 System.Type ExitGames.Client.Photon.Protocol18::ReadDictionaryType(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadDictionaryType_m825B743097E20ED52ED718F84600133AC3DA7A78 (void);
// 0x000001D2 System.Type ExitGames.Client.Photon.Protocol18::GetDictArrayType(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_GetDictArrayType_m2EC2C1AA54111F9AE92918DB55AAA57A73ABB3FB (void);
// 0x000001D3 System.Collections.IDictionary ExitGames.Client.Photon.Protocol18::ReadDictionary(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadDictionary_m7035D57890135D288F57A1EAA8B6778CF7D8366F (void);
// 0x000001D4 System.Boolean ExitGames.Client.Photon.Protocol18::ReadDictionaryElements(ExitGames.Client.Photon.StreamBuffer,ExitGames.Client.Photon.Protocol18/GpType,ExitGames.Client.Photon.Protocol18/GpType,System.Collections.IDictionary)
extern void Protocol18_ReadDictionaryElements_mA3AA7CA6E4975D0D4F5475F6729522F29D50EC44 (void);
// 0x000001D5 System.Object[] ExitGames.Client.Photon.Protocol18::ReadObjectArray(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadObjectArray_m0FCE00465BD7902FEE21055092D85F9C01154CA6 (void);
// 0x000001D6 System.Boolean[] ExitGames.Client.Photon.Protocol18::ReadBooleanArray(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadBooleanArray_m77C3BA7465BF53BE0EEC32001CD143D10E7DF214 (void);
// 0x000001D7 System.Int16[] ExitGames.Client.Photon.Protocol18::ReadInt16Array(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadInt16Array_m72E13B2F035ECF8F24E6120B74ADE3885F383283 (void);
// 0x000001D8 System.Single[] ExitGames.Client.Photon.Protocol18::ReadSingleArray(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadSingleArray_mE74F022589D56D6EB408F956ADCA857B380EBDEC (void);
// 0x000001D9 System.Double[] ExitGames.Client.Photon.Protocol18::ReadDoubleArray(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadDoubleArray_m7C9B2FF1033B2AAED0106F46C9B7F02A1D4F76EF (void);
// 0x000001DA System.String[] ExitGames.Client.Photon.Protocol18::ReadStringArray(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadStringArray_mFF9000AEF1C9CE9CCF30199D4BA049442B7BC3CF (void);
// 0x000001DB ExitGames.Client.Photon.Hashtable[] ExitGames.Client.Photon.Protocol18::ReadHashtableArray(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadHashtableArray_m72A20B1A359271EFBD9D908090CB37B251212B8F (void);
// 0x000001DC System.Collections.IDictionary[] ExitGames.Client.Photon.Protocol18::ReadDictionaryArray(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadDictionaryArray_m8E126F56E19BB8F278CF867680B5BF081C420E60 (void);
// 0x000001DD System.Array ExitGames.Client.Photon.Protocol18::ReadArrayInArray(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadArrayInArray_mCE6FFC3C75C6E3DAD3B9569F8803B546D0729711 (void);
// 0x000001DE System.Int32 ExitGames.Client.Photon.Protocol18::ReadInt1(ExitGames.Client.Photon.StreamBuffer,System.Boolean)
extern void Protocol18_ReadInt1_m13981CEAC26C36F639A35B11EDDF4220F38D7CF9 (void);
// 0x000001DF System.Int32 ExitGames.Client.Photon.Protocol18::ReadInt2(ExitGames.Client.Photon.StreamBuffer,System.Boolean)
extern void Protocol18_ReadInt2_m52273E733DFD2EAC7E6AE0DCEEA13FA2E065D547 (void);
// 0x000001E0 System.Int32 ExitGames.Client.Photon.Protocol18::ReadCompressedInt32(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadCompressedInt32_mA2A4E657653C85751A7CD46645F361C8DD1C3702 (void);
// 0x000001E1 System.UInt32 ExitGames.Client.Photon.Protocol18::ReadCompressedUInt32(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadCompressedUInt32_m7EB98434941C130BEFB21776EAD9DF3308231A1E (void);
// 0x000001E2 System.Int64 ExitGames.Client.Photon.Protocol18::ReadCompressedInt64(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadCompressedInt64_m6D036EAD500BB6565BF57A4163F15395BFFF64E9 (void);
// 0x000001E3 System.UInt64 ExitGames.Client.Photon.Protocol18::ReadCompressedUInt64(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadCompressedUInt64_mD033F7B9255D15C910954048D1B31134855FAAC3 (void);
// 0x000001E4 System.Int32[] ExitGames.Client.Photon.Protocol18::ReadCompressedInt32Array(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadCompressedInt32Array_m2889BEF8151DEE6031A93366886B861FB7BF99B7 (void);
// 0x000001E5 System.Int64[] ExitGames.Client.Photon.Protocol18::ReadCompressedInt64Array(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadCompressedInt64Array_m2BE6D697330767C0D15D548B77729535BB9A71E8 (void);
// 0x000001E6 System.Int32 ExitGames.Client.Photon.Protocol18::DecodeZigZag32(System.UInt32)
extern void Protocol18_DecodeZigZag32_m44762B11F5C71392DCE0EEAEFFA44EA9DCF1AD3F (void);
// 0x000001E7 System.Int64 ExitGames.Client.Photon.Protocol18::DecodeZigZag64(System.UInt64)
extern void Protocol18_DecodeZigZag64_m7E74E517D455D04168977EB4AF6C728B90E9829C (void);
// 0x000001E8 System.Void ExitGames.Client.Photon.Protocol18::Write(ExitGames.Client.Photon.StreamBuffer,System.Object,System.Boolean)
extern void Protocol18_Write_mAB8D7EDA5931D939D8BFC5BE64E7FB3FDF96D266 (void);
// 0x000001E9 System.Void ExitGames.Client.Photon.Protocol18::Write(ExitGames.Client.Photon.StreamBuffer,System.Object,ExitGames.Client.Photon.Protocol18/GpType,System.Boolean)
extern void Protocol18_Write_m270019BD722DD2618EC628D088C51452F8E9C464 (void);
// 0x000001EA System.Void ExitGames.Client.Photon.Protocol18::SerializeEventData(ExitGames.Client.Photon.StreamBuffer,ExitGames.Client.Photon.EventData,System.Boolean)
extern void Protocol18_SerializeEventData_m1FA5B22295AAE71F96F35D7248C10E955779C211 (void);
// 0x000001EB System.Void ExitGames.Client.Photon.Protocol18::WriteParameterTable(ExitGames.Client.Photon.StreamBuffer,System.Collections.Generic.Dictionary`2<System.Byte,System.Object>)
extern void Protocol18_WriteParameterTable_m0AD5BA217308208F76544843A720219B3AD2A094 (void);
// 0x000001EC System.Void ExitGames.Client.Photon.Protocol18::SerializeOperationRequest(ExitGames.Client.Photon.StreamBuffer,ExitGames.Client.Photon.OperationRequest,System.Boolean)
extern void Protocol18_SerializeOperationRequest_mB20D17F11A2E73BC747D5DD0D24D90CCD13B8D88 (void);
// 0x000001ED System.Void ExitGames.Client.Photon.Protocol18::SerializeOperationRequest(ExitGames.Client.Photon.StreamBuffer,System.Byte,System.Collections.Generic.Dictionary`2<System.Byte,System.Object>,System.Boolean)
extern void Protocol18_SerializeOperationRequest_mE5EB11A120A4ED311130D51FE9220EC9FAA07A46 (void);
// 0x000001EE System.Void ExitGames.Client.Photon.Protocol18::SerializeOperationResponse(ExitGames.Client.Photon.StreamBuffer,ExitGames.Client.Photon.OperationResponse,System.Boolean)
extern void Protocol18_SerializeOperationResponse_mE98959AF79C5261C772D1566958845247C1C2C6A (void);
// 0x000001EF System.Void ExitGames.Client.Photon.Protocol18::WriteByte(ExitGames.Client.Photon.StreamBuffer,System.Byte,System.Boolean)
extern void Protocol18_WriteByte_m6A9777EEA4FF2A02F2D2358663DD5EDDAF9384B7 (void);
// 0x000001F0 System.Void ExitGames.Client.Photon.Protocol18::WriteBoolean(ExitGames.Client.Photon.StreamBuffer,System.Boolean,System.Boolean)
extern void Protocol18_WriteBoolean_mDB0B42AE178EB6025F114B3842BF578389C7692C (void);
// 0x000001F1 System.Void ExitGames.Client.Photon.Protocol18::WriteUShort(ExitGames.Client.Photon.StreamBuffer,System.UInt16)
extern void Protocol18_WriteUShort_mACEC4768D9301159010DF062BFB9FACCB4B4FF22 (void);
// 0x000001F2 System.Void ExitGames.Client.Photon.Protocol18::WriteInt16(ExitGames.Client.Photon.StreamBuffer,System.Int16,System.Boolean)
extern void Protocol18_WriteInt16_m4DE8FA74E9395BCAA36AE229466CDB9D76AFCA2A (void);
// 0x000001F3 System.Void ExitGames.Client.Photon.Protocol18::WriteDouble(ExitGames.Client.Photon.StreamBuffer,System.Double,System.Boolean)
extern void Protocol18_WriteDouble_m167E263BAAC5586A2531A02123B17522CA313D92 (void);
// 0x000001F4 System.Void ExitGames.Client.Photon.Protocol18::WriteSingle(ExitGames.Client.Photon.StreamBuffer,System.Single,System.Boolean)
extern void Protocol18_WriteSingle_mD086CEB523C5B9058183842AF1C23568BA0ACCFD (void);
// 0x000001F5 System.Void ExitGames.Client.Photon.Protocol18::WriteString(ExitGames.Client.Photon.StreamBuffer,System.String,System.Boolean)
extern void Protocol18_WriteString_m63E833F200FAB7E7433CAB78252E8856D2D170E6 (void);
// 0x000001F6 System.Void ExitGames.Client.Photon.Protocol18::WriteHashtable(ExitGames.Client.Photon.StreamBuffer,System.Object,System.Boolean)
extern void Protocol18_WriteHashtable_mEC42D1BEB630C6322C08DA891D8415745AD7115E (void);
// 0x000001F7 System.Void ExitGames.Client.Photon.Protocol18::WriteByteArray(ExitGames.Client.Photon.StreamBuffer,System.Byte[],System.Boolean)
extern void Protocol18_WriteByteArray_mFFF2AE67AF76DF387CAD5386320CF91B72A1859E (void);
// 0x000001F8 System.Void ExitGames.Client.Photon.Protocol18::WriteByteArraySegment(ExitGames.Client.Photon.StreamBuffer,System.Byte[],System.Int32,System.Int32,System.Boolean)
extern void Protocol18_WriteByteArraySegment_m9B4D564E12703B70D0DC03BD639C90D222FA8ABD (void);
// 0x000001F9 System.Void ExitGames.Client.Photon.Protocol18::WriteInt32ArrayCompressed(ExitGames.Client.Photon.StreamBuffer,System.Int32[],System.Boolean)
extern void Protocol18_WriteInt32ArrayCompressed_mBB0FEE3782FC3E9EC7626D29CC108BF1E8583147 (void);
// 0x000001FA System.Void ExitGames.Client.Photon.Protocol18::WriteInt64ArrayCompressed(ExitGames.Client.Photon.StreamBuffer,System.Int64[],System.Boolean)
extern void Protocol18_WriteInt64ArrayCompressed_m8E930EB5AB006314458230E301A27E50CBBFA5C4 (void);
// 0x000001FB System.Void ExitGames.Client.Photon.Protocol18::WriteBoolArray(ExitGames.Client.Photon.StreamBuffer,System.Boolean[],System.Boolean)
extern void Protocol18_WriteBoolArray_m29018D85EEDCE7418874E1A660F262D394689E08 (void);
// 0x000001FC System.Void ExitGames.Client.Photon.Protocol18::WriteInt16Array(ExitGames.Client.Photon.StreamBuffer,System.Int16[],System.Boolean)
extern void Protocol18_WriteInt16Array_mB172D4EF1B33C7A56197658687A2259BA21E7D94 (void);
// 0x000001FD System.Void ExitGames.Client.Photon.Protocol18::WriteSingleArray(ExitGames.Client.Photon.StreamBuffer,System.Single[],System.Boolean)
extern void Protocol18_WriteSingleArray_m152F41A02966104DC368CA071E5D1F409212FF2A (void);
// 0x000001FE System.Void ExitGames.Client.Photon.Protocol18::WriteDoubleArray(ExitGames.Client.Photon.StreamBuffer,System.Double[],System.Boolean)
extern void Protocol18_WriteDoubleArray_mC31A8B9F35D97704AE34073A2CE773A977F37A46 (void);
// 0x000001FF System.Void ExitGames.Client.Photon.Protocol18::WriteStringArray(ExitGames.Client.Photon.StreamBuffer,System.Object,System.Boolean)
extern void Protocol18_WriteStringArray_mF667D9786577566F8CAD0C5A6761F94F6BF440E7 (void);
// 0x00000200 System.Void ExitGames.Client.Photon.Protocol18::WriteObjectArray(ExitGames.Client.Photon.StreamBuffer,System.Collections.IList,System.Boolean)
extern void Protocol18_WriteObjectArray_mC540DC60245B1E5E3A226BC69F3344C3817F3642 (void);
// 0x00000201 System.Void ExitGames.Client.Photon.Protocol18::WriteArrayInArray(ExitGames.Client.Photon.StreamBuffer,System.Object,System.Boolean)
extern void Protocol18_WriteArrayInArray_mE24870B9DF61820A9763B99CDD6BD280B916F513 (void);
// 0x00000202 System.Void ExitGames.Client.Photon.Protocol18::WriteCustomTypeBody(ExitGames.Client.Photon.CustomType,ExitGames.Client.Photon.StreamBuffer,System.Object)
extern void Protocol18_WriteCustomTypeBody_m02459C82D73BFC9737B3BE91C35D3A7E3970899A (void);
// 0x00000203 System.Void ExitGames.Client.Photon.Protocol18::WriteCustomType(ExitGames.Client.Photon.StreamBuffer,System.Object,System.Boolean)
extern void Protocol18_WriteCustomType_m9AD4C5D501DD2A51B7CD8C0493B6B3E08A998223 (void);
// 0x00000204 System.Void ExitGames.Client.Photon.Protocol18::WriteCustomTypeArray(ExitGames.Client.Photon.StreamBuffer,System.Object,System.Boolean)
extern void Protocol18_WriteCustomTypeArray_mD438CE38C2B2243ECA5B124D3C670D74EACD09DC (void);
// 0x00000205 System.Boolean ExitGames.Client.Photon.Protocol18::WriteArrayHeader(ExitGames.Client.Photon.StreamBuffer,System.Type)
extern void Protocol18_WriteArrayHeader_m165E8EDAF662B62143C0BEEBED9B939CE626A735 (void);
// 0x00000206 System.Void ExitGames.Client.Photon.Protocol18::WriteDictionaryElements(ExitGames.Client.Photon.StreamBuffer,System.Collections.IDictionary,ExitGames.Client.Photon.Protocol18/GpType,ExitGames.Client.Photon.Protocol18/GpType)
extern void Protocol18_WriteDictionaryElements_m756A188D80214363D40AABFEC623C9254981F406 (void);
// 0x00000207 System.Void ExitGames.Client.Photon.Protocol18::WriteDictionary(ExitGames.Client.Photon.StreamBuffer,System.Object,System.Boolean)
extern void Protocol18_WriteDictionary_m5D2C9A24E68406BC7CB202F6628D747E18C54376 (void);
// 0x00000208 System.Void ExitGames.Client.Photon.Protocol18::WriteDictionaryHeader(ExitGames.Client.Photon.StreamBuffer,System.Type,ExitGames.Client.Photon.Protocol18/GpType&,ExitGames.Client.Photon.Protocol18/GpType&)
extern void Protocol18_WriteDictionaryHeader_mF5510616F59EA19A56EAB3BC001FF864779B46CF (void);
// 0x00000209 System.Boolean ExitGames.Client.Photon.Protocol18::WriteArrayType(ExitGames.Client.Photon.StreamBuffer,System.Type,ExitGames.Client.Photon.Protocol18/GpType&)
extern void Protocol18_WriteArrayType_mDB9DADAA2395178F22492CA1AE1B4F026A2B8232 (void);
// 0x0000020A System.Void ExitGames.Client.Photon.Protocol18::WriteHashtableArray(ExitGames.Client.Photon.StreamBuffer,System.Object,System.Boolean)
extern void Protocol18_WriteHashtableArray_m503694D98DED09E7308777A668EF88298D7F5B3B (void);
// 0x0000020B System.Void ExitGames.Client.Photon.Protocol18::WriteDictionaryArray(ExitGames.Client.Photon.StreamBuffer,System.Collections.IDictionary[],System.Boolean)
extern void Protocol18_WriteDictionaryArray_mE328367A3028BC358CE5AE1A479E13F55B4E7159 (void);
// 0x0000020C System.Void ExitGames.Client.Photon.Protocol18::WriteIntLength(ExitGames.Client.Photon.StreamBuffer,System.Int32)
extern void Protocol18_WriteIntLength_mDD200FB74D5ECD5A5F9B84E8A72D7D3DD130F6A8 (void);
// 0x0000020D System.Void ExitGames.Client.Photon.Protocol18::WriteCompressedInt32(ExitGames.Client.Photon.StreamBuffer,System.Int32,System.Boolean)
extern void Protocol18_WriteCompressedInt32_m925C6263E481768696441FD66B5FBD38E205D0D5 (void);
// 0x0000020E System.Void ExitGames.Client.Photon.Protocol18::WriteCompressedInt64(ExitGames.Client.Photon.StreamBuffer,System.Int64,System.Boolean)
extern void Protocol18_WriteCompressedInt64_m615C6B98496D9DB780A46E02EFD7254E57DCC48A (void);
// 0x0000020F System.Void ExitGames.Client.Photon.Protocol18::WriteCompressedUInt32(ExitGames.Client.Photon.StreamBuffer,System.UInt32)
extern void Protocol18_WriteCompressedUInt32_m4C6E2C103DB47B3F858AF1E3F9794347CE428DC8 (void);
// 0x00000210 System.Int32 ExitGames.Client.Photon.Protocol18::WriteCompressedUInt32(System.Byte[],System.UInt32)
extern void Protocol18_WriteCompressedUInt32_m140A4F86F440339F65D60D63EE564606830832B8 (void);
// 0x00000211 System.Void ExitGames.Client.Photon.Protocol18::WriteCompressedUInt64(ExitGames.Client.Photon.StreamBuffer,System.UInt64)
extern void Protocol18_WriteCompressedUInt64_m683653B6E1B78041AACDDC385CF7D61463A834FF (void);
// 0x00000212 System.UInt32 ExitGames.Client.Photon.Protocol18::EncodeZigZag32(System.Int32)
extern void Protocol18_EncodeZigZag32_m367446287361EA0DEF7BF402A7A1B52DCA774002 (void);
// 0x00000213 System.UInt64 ExitGames.Client.Photon.Protocol18::EncodeZigZag64(System.Int64)
extern void Protocol18_EncodeZigZag64_m28BB0A4F75253DC03DC44737931AE4C9ED4462A2 (void);
// 0x00000214 System.Void ExitGames.Client.Photon.Protocol18::.ctor()
extern void Protocol18__ctor_m7B63A8E8A7BD6DB927516889B9B53E1283951D6E (void);
// 0x00000215 System.Void ExitGames.Client.Photon.Protocol18::.cctor()
extern void Protocol18__cctor_m521D7C7CF85014632615B99CEB7CAD64542DE541 (void);
// 0x00000216 ExitGames.Client.Photon.IPhotonPeerListener ExitGames.Client.Photon.IPhotonSocket::get_Listener()
extern void IPhotonSocket_get_Listener_m0551A21F4B0740E1F6AB78E323108FEF5EB64555 (void);
// 0x00000217 System.Int32 ExitGames.Client.Photon.IPhotonSocket::get_MTU()
extern void IPhotonSocket_get_MTU_mD1EE1B2381D26FAF4F77331FF7F0265CF849880D (void);
// 0x00000218 ExitGames.Client.Photon.PhotonSocketState ExitGames.Client.Photon.IPhotonSocket::get_State()
extern void IPhotonSocket_get_State_mE53E514522E11EB4EB724897E34BD331EC9AFC48 (void);
// 0x00000219 System.Void ExitGames.Client.Photon.IPhotonSocket::set_State(ExitGames.Client.Photon.PhotonSocketState)
extern void IPhotonSocket_set_State_mC8D290806513F5EA139B22D299985D8C3C472B0D (void);
// 0x0000021A System.Boolean ExitGames.Client.Photon.IPhotonSocket::get_Connected()
extern void IPhotonSocket_get_Connected_m4D00A7E518BBFB6FBE49A2F0A26819246E686BE7 (void);
// 0x0000021B System.String ExitGames.Client.Photon.IPhotonSocket::get_ServerAddress()
extern void IPhotonSocket_get_ServerAddress_m507F4853E9D31EF130D9A6FA92D6E08FD31C1468 (void);
// 0x0000021C System.Void ExitGames.Client.Photon.IPhotonSocket::set_ServerAddress(System.String)
extern void IPhotonSocket_set_ServerAddress_m54A239F3BCC438B9EA4D618C8EFCDD9E4D176A5B (void);
// 0x0000021D System.String ExitGames.Client.Photon.IPhotonSocket::get_ServerIpAddress()
extern void IPhotonSocket_get_ServerIpAddress_m4211E8F1F2397F4A6A115D1CEDB7AF16932AA77B (void);
// 0x0000021E System.Void ExitGames.Client.Photon.IPhotonSocket::set_ServerIpAddress(System.String)
extern void IPhotonSocket_set_ServerIpAddress_mC2CDC34DF19E06B745CB7580E873717E9E367550 (void);
// 0x0000021F System.Int32 ExitGames.Client.Photon.IPhotonSocket::get_ServerPort()
extern void IPhotonSocket_get_ServerPort_m288E05494A4C23632A5F0709E904B9400151C969 (void);
// 0x00000220 System.Void ExitGames.Client.Photon.IPhotonSocket::set_ServerPort(System.Int32)
extern void IPhotonSocket_set_ServerPort_m52E503F3F6E34A75BE04D06917CBE99ABAA6AED4 (void);
// 0x00000221 System.Boolean ExitGames.Client.Photon.IPhotonSocket::get_AddressResolvedAsIpv6()
extern void IPhotonSocket_get_AddressResolvedAsIpv6_m95FE13CA7CEC68DCB7C0070B5551A5D647B5556C (void);
// 0x00000222 System.Void ExitGames.Client.Photon.IPhotonSocket::set_AddressResolvedAsIpv6(System.Boolean)
extern void IPhotonSocket_set_AddressResolvedAsIpv6_mD221497B6749904CE2FFD2B9F2A5E95289BE569F (void);
// 0x00000223 System.Void ExitGames.Client.Photon.IPhotonSocket::set_UrlProtocol(System.String)
extern void IPhotonSocket_set_UrlProtocol_m0F7099863EB5593D791E901CE4C4B767708CC74C (void);
// 0x00000224 System.Void ExitGames.Client.Photon.IPhotonSocket::set_UrlPath(System.String)
extern void IPhotonSocket_set_UrlPath_m87B9DD2CA3ABC7AA56AA42303F4195219BA03CD0 (void);
// 0x00000225 System.Void ExitGames.Client.Photon.IPhotonSocket::.ctor(ExitGames.Client.Photon.PeerBase)
extern void IPhotonSocket__ctor_m41CB988C577239E38F8D9B92D133858599D197CA (void);
// 0x00000226 System.Boolean ExitGames.Client.Photon.IPhotonSocket::Connect()
extern void IPhotonSocket_Connect_m7A0B1483190F73A96078EB0CC21F4996B0DDE4A5 (void);
// 0x00000227 System.Boolean ExitGames.Client.Photon.IPhotonSocket::Disconnect()
// 0x00000228 ExitGames.Client.Photon.PhotonSocketError ExitGames.Client.Photon.IPhotonSocket::Send(System.Byte[],System.Int32)
// 0x00000229 ExitGames.Client.Photon.PhotonSocketError ExitGames.Client.Photon.IPhotonSocket::Receive(System.Byte[]&)
// 0x0000022A System.Void ExitGames.Client.Photon.IPhotonSocket::HandleReceivedDatagram(System.Byte[],System.Int32,System.Boolean)
extern void IPhotonSocket_HandleReceivedDatagram_m2E5FBFCD72BC93FF39AF51DB6BA5E47BBE37EA87 (void);
// 0x0000022B System.Boolean ExitGames.Client.Photon.IPhotonSocket::ReportDebugOfLevel(ExitGames.Client.Photon.DebugLevel)
extern void IPhotonSocket_ReportDebugOfLevel_m10DA4BA8075E690C233590B19905FB19840362DB (void);
// 0x0000022C System.Void ExitGames.Client.Photon.IPhotonSocket::EnqueueDebugReturn(ExitGames.Client.Photon.DebugLevel,System.String)
extern void IPhotonSocket_EnqueueDebugReturn_m8E7061C7152912F0D38AFFCF341576C6E13D2D94 (void);
// 0x0000022D System.Void ExitGames.Client.Photon.IPhotonSocket::HandleException(ExitGames.Client.Photon.StatusCode)
extern void IPhotonSocket_HandleException_m596BF9AC251A53883D136C3DEBD55036C35EB894 (void);
// 0x0000022E System.Boolean ExitGames.Client.Photon.IPhotonSocket::TryParseAddress(System.String,System.String&,System.UInt16&,System.String&,System.String&)
extern void IPhotonSocket_TryParseAddress_mE94333A1CD82A5303E69D4369639692FEE64ABA1 (void);
// 0x0000022F System.Boolean ExitGames.Client.Photon.IPhotonSocket::IsIpv6SimpleCheck(System.Net.IPAddress)
extern void IPhotonSocket_IsIpv6SimpleCheck_mDEB4F524082496210C6E222B42254B9270A775E5 (void);
// 0x00000230 System.Net.IPAddress ExitGames.Client.Photon.IPhotonSocket::GetIpAddress(System.String)
extern void IPhotonSocket_GetIpAddress_m7BC0DFF03AEBA416A3E63942988FF58A0025DE89 (void);
// 0x00000231 System.Void ExitGames.Client.Photon.IPhotonSocket::<HandleException>b__47_0()
extern void IPhotonSocket_U3CHandleExceptionU3Eb__47_0_mE715582B365C7183682882E38E4A9D7FB0E7A098 (void);
// 0x00000232 System.Void ExitGames.Client.Photon.SocketUdp::.ctor(ExitGames.Client.Photon.PeerBase)
extern void SocketUdp__ctor_mC6E7F786B2D9168A3033674FFDA5B53DB507E242 (void);
// 0x00000233 System.Void ExitGames.Client.Photon.SocketUdp::Dispose()
extern void SocketUdp_Dispose_mAB4C7332A466190BF0E52C7AD85156764097994E (void);
// 0x00000234 System.Boolean ExitGames.Client.Photon.SocketUdp::Connect()
extern void SocketUdp_Connect_m0E22325D01F5D96DE8F314AAC4061DD9BAA9AF83 (void);
// 0x00000235 System.Boolean ExitGames.Client.Photon.SocketUdp::Disconnect()
extern void SocketUdp_Disconnect_m70104096544AD84A502C033CC1F68780E2298B12 (void);
// 0x00000236 ExitGames.Client.Photon.PhotonSocketError ExitGames.Client.Photon.SocketUdp::Send(System.Byte[],System.Int32)
extern void SocketUdp_Send_mE44D3595EDFEFA6D5673C58A932820E0AAF8DD7F (void);
// 0x00000237 ExitGames.Client.Photon.PhotonSocketError ExitGames.Client.Photon.SocketUdp::Receive(System.Byte[]&)
extern void SocketUdp_Receive_mE4ABCEDABFE0C3B530DE0A09C7E2A85A7619E4C8 (void);
// 0x00000238 System.Void ExitGames.Client.Photon.SocketUdp::DnsAndConnect()
extern void SocketUdp_DnsAndConnect_m58AF55C58CDD073F97AC337FE9A52A219C397655 (void);
// 0x00000239 System.Void ExitGames.Client.Photon.SocketUdp::ReceiveLoop()
extern void SocketUdp_ReceiveLoop_mBA78CA0669E02422696BD04DAC3753E0ABB8B2BE (void);
// 0x0000023A System.Void ExitGames.Client.Photon.SocketTcp::.ctor(ExitGames.Client.Photon.PeerBase)
extern void SocketTcp__ctor_m072CB58C8C8A27CDEC64C9ADBC76A8EEB9D37B3C (void);
// 0x0000023B System.Void ExitGames.Client.Photon.SocketTcp::Dispose()
extern void SocketTcp_Dispose_m9F39A2738802D67590F659AED6DDE4BCE13ABB5A (void);
// 0x0000023C System.Boolean ExitGames.Client.Photon.SocketTcp::Connect()
extern void SocketTcp_Connect_mDBB2AE2B3D07D8EACD3BA124873355B61ECDF1A9 (void);
// 0x0000023D System.Boolean ExitGames.Client.Photon.SocketTcp::Disconnect()
extern void SocketTcp_Disconnect_mA79E879DD7A4F97F035B7DF163AA28DAF0DE9D4D (void);
// 0x0000023E ExitGames.Client.Photon.PhotonSocketError ExitGames.Client.Photon.SocketTcp::Send(System.Byte[],System.Int32)
extern void SocketTcp_Send_m1025822705450801D5902A99A1DDD1E1685285C0 (void);
// 0x0000023F ExitGames.Client.Photon.PhotonSocketError ExitGames.Client.Photon.SocketTcp::Receive(System.Byte[]&)
extern void SocketTcp_Receive_m31272566160E8EC550BC44B49A6A83D7FEF93A26 (void);
// 0x00000240 System.Void ExitGames.Client.Photon.SocketTcp::DnsAndConnect()
extern void SocketTcp_DnsAndConnect_m88FB14221EBA637B2CFD8FD7145DBD20BCD7DAAF (void);
// 0x00000241 System.Void ExitGames.Client.Photon.SocketTcp::ReceiveLoop()
extern void SocketTcp_ReceiveLoop_m4D6F93CD01DDEE04CC38FC4647C0E55827A09005 (void);
// 0x00000242 System.Void ExitGames.Client.Photon.SimulationItem::.ctor()
extern void SimulationItem__ctor_m58E35B4B03E7BFA3529806737DBB2ABCA4B11811 (void);
// 0x00000243 System.Int32 ExitGames.Client.Photon.SimulationItem::get_Delay()
extern void SimulationItem_get_Delay_m59BC993F4707DE4FEE34ABE28078ED6D7D3485D0 (void);
// 0x00000244 System.Void ExitGames.Client.Photon.SimulationItem::set_Delay(System.Int32)
extern void SimulationItem_set_Delay_m20B0D990E14FF816B48DF43B0027276B6B57C462 (void);
// 0x00000245 System.Boolean ExitGames.Client.Photon.NetworkSimulationSet::get_IsSimulationEnabled()
extern void NetworkSimulationSet_get_IsSimulationEnabled_m6C64C0079C4D249DA419DDDECC647DA1DCF58396 (void);
// 0x00000246 System.Void ExitGames.Client.Photon.NetworkSimulationSet::set_IsSimulationEnabled(System.Boolean)
extern void NetworkSimulationSet_set_IsSimulationEnabled_m0797432DE71AB180AD997EE6CF8837A3F8A97B75 (void);
// 0x00000247 System.Int32 ExitGames.Client.Photon.NetworkSimulationSet::get_OutgoingLag()
extern void NetworkSimulationSet_get_OutgoingLag_mEC03E58F5F36A5BD7E7EB97D96D7C9B8FB8B4C2B (void);
// 0x00000248 System.Void ExitGames.Client.Photon.NetworkSimulationSet::set_OutgoingLag(System.Int32)
extern void NetworkSimulationSet_set_OutgoingLag_m74D50D732A6A3327B08E0BD15585EFEC3526D19B (void);
// 0x00000249 System.Int32 ExitGames.Client.Photon.NetworkSimulationSet::get_OutgoingJitter()
extern void NetworkSimulationSet_get_OutgoingJitter_m911D4DF9D50DC91A3564ABF71114911A159661AF (void);
// 0x0000024A System.Void ExitGames.Client.Photon.NetworkSimulationSet::set_OutgoingJitter(System.Int32)
extern void NetworkSimulationSet_set_OutgoingJitter_m7134006F8500D0CD82AFC7A2E19FB6279B956566 (void);
// 0x0000024B System.Int32 ExitGames.Client.Photon.NetworkSimulationSet::get_OutgoingLossPercentage()
extern void NetworkSimulationSet_get_OutgoingLossPercentage_m92D5286B157CEE9523AC43B5B9F14530B8E042CC (void);
// 0x0000024C System.Void ExitGames.Client.Photon.NetworkSimulationSet::set_OutgoingLossPercentage(System.Int32)
extern void NetworkSimulationSet_set_OutgoingLossPercentage_m92B98F56F839CD7100586B8CCDF678FF48D3272D (void);
// 0x0000024D System.Int32 ExitGames.Client.Photon.NetworkSimulationSet::get_IncomingLag()
extern void NetworkSimulationSet_get_IncomingLag_m24A45732B49CC9274E5147B6D530C0FBE5DEAE08 (void);
// 0x0000024E System.Void ExitGames.Client.Photon.NetworkSimulationSet::set_IncomingLag(System.Int32)
extern void NetworkSimulationSet_set_IncomingLag_m9FEF65615A8643342F7D49F4A28D7A93F1D7521A (void);
// 0x0000024F System.Int32 ExitGames.Client.Photon.NetworkSimulationSet::get_IncomingJitter()
extern void NetworkSimulationSet_get_IncomingJitter_mFEBF3B0154008817357F79A0506298A47A83F3B5 (void);
// 0x00000250 System.Void ExitGames.Client.Photon.NetworkSimulationSet::set_IncomingJitter(System.Int32)
extern void NetworkSimulationSet_set_IncomingJitter_m83CFA57DE61E150697028FF81091578C4F678F72 (void);
// 0x00000251 System.Int32 ExitGames.Client.Photon.NetworkSimulationSet::get_IncomingLossPercentage()
extern void NetworkSimulationSet_get_IncomingLossPercentage_mD934819F1A37C9F7C0C058209260C061881CB3A8 (void);
// 0x00000252 System.Void ExitGames.Client.Photon.NetworkSimulationSet::set_IncomingLossPercentage(System.Int32)
extern void NetworkSimulationSet_set_IncomingLossPercentage_m14051A5B7A905F1950DF6634A8B4C17D88CFB0D8 (void);
// 0x00000253 System.Int32 ExitGames.Client.Photon.NetworkSimulationSet::get_LostPackagesOut()
extern void NetworkSimulationSet_get_LostPackagesOut_mE3DDEBFC7048D96AA247E606796D7F72260020DB (void);
// 0x00000254 System.Void ExitGames.Client.Photon.NetworkSimulationSet::set_LostPackagesOut(System.Int32)
extern void NetworkSimulationSet_set_LostPackagesOut_m71EE8A4CDA0C6124FE4FD7CBF9DE365AAB5D2B3A (void);
// 0x00000255 System.Int32 ExitGames.Client.Photon.NetworkSimulationSet::get_LostPackagesIn()
extern void NetworkSimulationSet_get_LostPackagesIn_m0FAF896285859C4CAB2375154FC84AC0F093656D (void);
// 0x00000256 System.Void ExitGames.Client.Photon.NetworkSimulationSet::set_LostPackagesIn(System.Int32)
extern void NetworkSimulationSet_set_LostPackagesIn_m99DC1E0A32C93D557C89F82041F488912B26E5BA (void);
// 0x00000257 System.String ExitGames.Client.Photon.NetworkSimulationSet::ToString()
extern void NetworkSimulationSet_ToString_mE4B93C12D944E31ECA7F5C3BD58A12EB893C9840 (void);
// 0x00000258 System.Void ExitGames.Client.Photon.NetworkSimulationSet::.ctor()
extern void NetworkSimulationSet__ctor_m7F0708A5CD215C458EB02B41FF06C1A279D4B31C (void);
// 0x00000259 System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_OperationByteCount()
extern void TrafficStatsGameLevel_get_OperationByteCount_m170EEE0C86D9F8D3077F345C3963B3B2D5463D9B (void);
// 0x0000025A System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_OperationByteCount(System.Int32)
extern void TrafficStatsGameLevel_set_OperationByteCount_mDCFE744B1963ECE8C13771187BDD0E59E8106577 (void);
// 0x0000025B System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_OperationCount()
extern void TrafficStatsGameLevel_get_OperationCount_m111B512B8F9D37E86229E49D054D226B311BC98E (void);
// 0x0000025C System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_OperationCount(System.Int32)
extern void TrafficStatsGameLevel_set_OperationCount_mE2D4B6DC1E125189C8F76E11C833120D347BF61A (void);
// 0x0000025D System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_ResultByteCount()
extern void TrafficStatsGameLevel_get_ResultByteCount_m80BFDD85B5B5734BF4D8573CF648F94B11AE71D8 (void);
// 0x0000025E System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_ResultByteCount(System.Int32)
extern void TrafficStatsGameLevel_set_ResultByteCount_m5446B07E0D0A379D95DCBE8B290372E0A463A5AA (void);
// 0x0000025F System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_ResultCount()
extern void TrafficStatsGameLevel_get_ResultCount_mD7E8060E13143225C362829967F71F8CC2174B61 (void);
// 0x00000260 System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_ResultCount(System.Int32)
extern void TrafficStatsGameLevel_set_ResultCount_m3CC9E5DD3CE24616FEDA6A30288A4134347A097D (void);
// 0x00000261 System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_EventByteCount()
extern void TrafficStatsGameLevel_get_EventByteCount_mA6C273E2399F50C2B4774C60603488AEAC8B3D2A (void);
// 0x00000262 System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_EventByteCount(System.Int32)
extern void TrafficStatsGameLevel_set_EventByteCount_m54ABC33D5E6485A3B70B04FF513DEB3817A84808 (void);
// 0x00000263 System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_EventCount()
extern void TrafficStatsGameLevel_get_EventCount_mB0CF01EF31EE5D769C581CA7DC48C03CEF0EF494 (void);
// 0x00000264 System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_EventCount(System.Int32)
extern void TrafficStatsGameLevel_set_EventCount_mCC6CDE66951D8A7B6AD979E3314563C16ADF3EA0 (void);
// 0x00000265 System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_LongestOpResponseCallback()
extern void TrafficStatsGameLevel_get_LongestOpResponseCallback_mFD7D9B3E7CCB375132612A07FC2FDE9CD30CFCE8 (void);
// 0x00000266 System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_LongestOpResponseCallback(System.Int32)
extern void TrafficStatsGameLevel_set_LongestOpResponseCallback_m135EAFC22AE1FA5C08082C511F2CD820E737B911 (void);
// 0x00000267 System.Byte ExitGames.Client.Photon.TrafficStatsGameLevel::get_LongestOpResponseCallbackOpCode()
extern void TrafficStatsGameLevel_get_LongestOpResponseCallbackOpCode_m6992DB8B32A62EF34E79A89F918E0B631A9C371C (void);
// 0x00000268 System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_LongestOpResponseCallbackOpCode(System.Byte)
extern void TrafficStatsGameLevel_set_LongestOpResponseCallbackOpCode_m3392D9077C18542A7957840E6D060A5DAF89DE81 (void);
// 0x00000269 System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_LongestEventCallback()
extern void TrafficStatsGameLevel_get_LongestEventCallback_m99722D0FC45EA8F948FF7ECAC0BA50B463B1649F (void);
// 0x0000026A System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_LongestEventCallback(System.Int32)
extern void TrafficStatsGameLevel_set_LongestEventCallback_mADA5C8191F23F0E0753FAAC39193AF09C1E95A4E (void);
// 0x0000026B System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_LongestMessageCallback()
extern void TrafficStatsGameLevel_get_LongestMessageCallback_m9BFFA5257FDE0AAFFD08258FE67B064F2B57AA31 (void);
// 0x0000026C System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_LongestMessageCallback(System.Int32)
extern void TrafficStatsGameLevel_set_LongestMessageCallback_mDCB4AE22FFF20BA7BF5FB33C8DF6EFC02DD03C3D (void);
// 0x0000026D System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_LongestRawMessageCallback()
extern void TrafficStatsGameLevel_get_LongestRawMessageCallback_m82F479078957DF54089D3A92AAC1E755F2267098 (void);
// 0x0000026E System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_LongestRawMessageCallback(System.Int32)
extern void TrafficStatsGameLevel_set_LongestRawMessageCallback_m6DD456707E367DEEB5D3435335DF7FEDEF860C2A (void);
// 0x0000026F System.Byte ExitGames.Client.Photon.TrafficStatsGameLevel::get_LongestEventCallbackCode()
extern void TrafficStatsGameLevel_get_LongestEventCallbackCode_mB640866FE8237D2FFA6AAE2A833299D6A84AF5AB (void);
// 0x00000270 System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_LongestEventCallbackCode(System.Byte)
extern void TrafficStatsGameLevel_set_LongestEventCallbackCode_mAB95B63CD0F46ABECACD2B7923FBEC51E5C7F6A5 (void);
// 0x00000271 System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_LongestDeltaBetweenDispatching()
extern void TrafficStatsGameLevel_get_LongestDeltaBetweenDispatching_m8E4CFD18DDF757847919E02CB719EE276D7F574B (void);
// 0x00000272 System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_LongestDeltaBetweenDispatching(System.Int32)
extern void TrafficStatsGameLevel_set_LongestDeltaBetweenDispatching_m25E2F5C2EE0664C0C27FC5FBB490F39DF7115CC6 (void);
// 0x00000273 System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_LongestDeltaBetweenSending()
extern void TrafficStatsGameLevel_get_LongestDeltaBetweenSending_m0D602DB49980F5D7781AC41C4649DD53EF19CB5D (void);
// 0x00000274 System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_LongestDeltaBetweenSending(System.Int32)
extern void TrafficStatsGameLevel_set_LongestDeltaBetweenSending_m6725D7C5370BE640674DA8C302E17E268C30CC69 (void);
// 0x00000275 System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_DispatchIncomingCommandsCalls()
extern void TrafficStatsGameLevel_get_DispatchIncomingCommandsCalls_m237506964895E1336089714DEE5525929A3DD193 (void);
// 0x00000276 System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_DispatchIncomingCommandsCalls(System.Int32)
extern void TrafficStatsGameLevel_set_DispatchIncomingCommandsCalls_mBB41143F95D0719346B7502DE3211EA49C64156F (void);
// 0x00000277 System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_SendOutgoingCommandsCalls()
extern void TrafficStatsGameLevel_get_SendOutgoingCommandsCalls_mACEC0B693CAD54B988E69B1165D57FA4D58C0594 (void);
// 0x00000278 System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_SendOutgoingCommandsCalls(System.Int32)
extern void TrafficStatsGameLevel_set_SendOutgoingCommandsCalls_m317DDA51ECB656F599A8AAFFBB121040216D1B7A (void);
// 0x00000279 System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_TotalMessageCount()
extern void TrafficStatsGameLevel_get_TotalMessageCount_m5FF0BAB29088E21FC4E7BABFA8FF0AC332C170FC (void);
// 0x0000027A System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_TotalIncomingMessageCount()
extern void TrafficStatsGameLevel_get_TotalIncomingMessageCount_m185188AEF7AFAF383F0D3EA889D318685D5011D7 (void);
// 0x0000027B System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_TotalOutgoingMessageCount()
extern void TrafficStatsGameLevel_get_TotalOutgoingMessageCount_m9FA83A501E753F7753AF6873CEA2A783EAA39E0A (void);
// 0x0000027C System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::CountOperation(System.Int32)
extern void TrafficStatsGameLevel_CountOperation_m334DA7808D71BC2A60C5B3589DAB4275CFC943CD (void);
// 0x0000027D System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::CountResult(System.Int32)
extern void TrafficStatsGameLevel_CountResult_m4B8F689C560DB680108119E069991A7AFF13C773 (void);
// 0x0000027E System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::CountEvent(System.Int32)
extern void TrafficStatsGameLevel_CountEvent_mFB5294614379B2E382FC543937B98B5E287A4A26 (void);
// 0x0000027F System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::TimeForResponseCallback(System.Byte,System.Int32)
extern void TrafficStatsGameLevel_TimeForResponseCallback_mD482EAE164E50DDC78BAEBE1F583648988097641 (void);
// 0x00000280 System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::TimeForEventCallback(System.Byte,System.Int32)
extern void TrafficStatsGameLevel_TimeForEventCallback_mA0287F67E9997DFD264012CF9ED9B7AD6BBB36A7 (void);
// 0x00000281 System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::TimeForMessageCallback(System.Int32)
extern void TrafficStatsGameLevel_TimeForMessageCallback_m4A38512E991BAC5D2A36BC7593754EF057EDC8F6 (void);
// 0x00000282 System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::TimeForRawMessageCallback(System.Int32)
extern void TrafficStatsGameLevel_TimeForRawMessageCallback_mC536643CA42341E390E7967FE45E6642E69B1006 (void);
// 0x00000283 System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::DispatchIncomingCommandsCalled()
extern void TrafficStatsGameLevel_DispatchIncomingCommandsCalled_mA26A91F3B6EB06CF7E4D9418F9AD4AF25B9D115D (void);
// 0x00000284 System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::SendOutgoingCommandsCalled()
extern void TrafficStatsGameLevel_SendOutgoingCommandsCalled_m4B8BBF7D68D579038264B3C920AD8C5E363B22E7 (void);
// 0x00000285 System.String ExitGames.Client.Photon.TrafficStatsGameLevel::ToString()
extern void TrafficStatsGameLevel_ToString_mB818F33F565AF3FB170F863254F769D0D49E59F5 (void);
// 0x00000286 System.String ExitGames.Client.Photon.TrafficStatsGameLevel::ToStringVitalStats()
extern void TrafficStatsGameLevel_ToStringVitalStats_m3543515813E6D397B38B6D161329AC93476D2362 (void);
// 0x00000287 System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::.ctor()
extern void TrafficStatsGameLevel__ctor_mE2C7C201AF77EE7E14623CD2DE84E8AB089D791A (void);
// 0x00000288 System.Int32 ExitGames.Client.Photon.TrafficStats::get_PackageHeaderSize()
extern void TrafficStats_get_PackageHeaderSize_m2904FE9BCAAB75D3B06327769E9E3679B6BCFEF1 (void);
// 0x00000289 System.Void ExitGames.Client.Photon.TrafficStats::set_PackageHeaderSize(System.Int32)
extern void TrafficStats_set_PackageHeaderSize_mD308E175248F5D539BF224F334E2822DE78D22AA (void);
// 0x0000028A System.Int32 ExitGames.Client.Photon.TrafficStats::get_ReliableCommandCount()
extern void TrafficStats_get_ReliableCommandCount_mCC25CD81366BFD75421B98CF37E98F66F274A708 (void);
// 0x0000028B System.Void ExitGames.Client.Photon.TrafficStats::set_ReliableCommandCount(System.Int32)
extern void TrafficStats_set_ReliableCommandCount_mD44C41FFC98CCDD17F1A84B3D55439E93211D594 (void);
// 0x0000028C System.Int32 ExitGames.Client.Photon.TrafficStats::get_UnreliableCommandCount()
extern void TrafficStats_get_UnreliableCommandCount_mC32A036F1E24D6D88598B9A00046C4CCB60F5A02 (void);
// 0x0000028D System.Void ExitGames.Client.Photon.TrafficStats::set_UnreliableCommandCount(System.Int32)
extern void TrafficStats_set_UnreliableCommandCount_m6E0057BF8B7A9CBD0E8644EDEBD9CA7C170AC033 (void);
// 0x0000028E System.Int32 ExitGames.Client.Photon.TrafficStats::get_FragmentCommandCount()
extern void TrafficStats_get_FragmentCommandCount_m4D109A149ED7A5665C127A5B3AE389E73F618195 (void);
// 0x0000028F System.Void ExitGames.Client.Photon.TrafficStats::set_FragmentCommandCount(System.Int32)
extern void TrafficStats_set_FragmentCommandCount_m60D391BC11A531E1326C65D17937FE1C648AE5E5 (void);
// 0x00000290 System.Int32 ExitGames.Client.Photon.TrafficStats::get_ControlCommandCount()
extern void TrafficStats_get_ControlCommandCount_m550CF80E96E08A8575C046871AA83005ED75DBF0 (void);
// 0x00000291 System.Void ExitGames.Client.Photon.TrafficStats::set_ControlCommandCount(System.Int32)
extern void TrafficStats_set_ControlCommandCount_mF0C61DA8296BF21916AE1706AAFBE08E796F08DA (void);
// 0x00000292 System.Int32 ExitGames.Client.Photon.TrafficStats::get_TotalPacketCount()
extern void TrafficStats_get_TotalPacketCount_m951DFC6DECA478141523582C0970C66D56D1AA04 (void);
// 0x00000293 System.Void ExitGames.Client.Photon.TrafficStats::set_TotalPacketCount(System.Int32)
extern void TrafficStats_set_TotalPacketCount_m65787539C6510BB636947E59D6C25834F719FBDF (void);
// 0x00000294 System.Int32 ExitGames.Client.Photon.TrafficStats::get_TotalCommandsInPackets()
extern void TrafficStats_get_TotalCommandsInPackets_m3EEB8F5479A923CFAD2B430B38A61CF9A1EFE51D (void);
// 0x00000295 System.Void ExitGames.Client.Photon.TrafficStats::set_TotalCommandsInPackets(System.Int32)
extern void TrafficStats_set_TotalCommandsInPackets_mCB4D586FED2D6B67161654765053D7FF092A5F98 (void);
// 0x00000296 System.Int32 ExitGames.Client.Photon.TrafficStats::get_ReliableCommandBytes()
extern void TrafficStats_get_ReliableCommandBytes_m97D380DE601854ECB562289F0A611F561B779A24 (void);
// 0x00000297 System.Void ExitGames.Client.Photon.TrafficStats::set_ReliableCommandBytes(System.Int32)
extern void TrafficStats_set_ReliableCommandBytes_m62E82B66FC499E6F7AA121D6F0D5224D1CF3CBA4 (void);
// 0x00000298 System.Int32 ExitGames.Client.Photon.TrafficStats::get_UnreliableCommandBytes()
extern void TrafficStats_get_UnreliableCommandBytes_mBF3066840BAF02EAB6FC4C413F16532128AF8189 (void);
// 0x00000299 System.Void ExitGames.Client.Photon.TrafficStats::set_UnreliableCommandBytes(System.Int32)
extern void TrafficStats_set_UnreliableCommandBytes_m2CC9814E62A542709541DA693B96035C657CF2AC (void);
// 0x0000029A System.Int32 ExitGames.Client.Photon.TrafficStats::get_FragmentCommandBytes()
extern void TrafficStats_get_FragmentCommandBytes_mFCF1476158DB9D054E5F9751B6C3151B1B098760 (void);
// 0x0000029B System.Void ExitGames.Client.Photon.TrafficStats::set_FragmentCommandBytes(System.Int32)
extern void TrafficStats_set_FragmentCommandBytes_m786B9BD978F6289B37EC34CB6D9F08CCA0CB3A73 (void);
// 0x0000029C System.Int32 ExitGames.Client.Photon.TrafficStats::get_ControlCommandBytes()
extern void TrafficStats_get_ControlCommandBytes_m55CD18E8C7771E41CBBE66335A083285CD33884B (void);
// 0x0000029D System.Void ExitGames.Client.Photon.TrafficStats::set_ControlCommandBytes(System.Int32)
extern void TrafficStats_set_ControlCommandBytes_m1E12AD93941B98D390902BC3A2308DE2B5B94D66 (void);
// 0x0000029E System.Void ExitGames.Client.Photon.TrafficStats::.ctor(System.Int32)
extern void TrafficStats__ctor_m3DDBAE04D292FD05AEE437A8ED3233988C2549CD (void);
// 0x0000029F System.Int32 ExitGames.Client.Photon.TrafficStats::get_TotalCommandBytes()
extern void TrafficStats_get_TotalCommandBytes_mDAF5285EA29C6FF71D7E446981F29D7A57A50159 (void);
// 0x000002A0 System.Int32 ExitGames.Client.Photon.TrafficStats::get_TotalPacketBytes()
extern void TrafficStats_get_TotalPacketBytes_m2FC95176E72723D1222DB4226EF33C2037D51FBA (void);
// 0x000002A1 System.Void ExitGames.Client.Photon.TrafficStats::set_TimestampOfLastAck(System.Int32)
extern void TrafficStats_set_TimestampOfLastAck_m547136B5AA1372A71A8E7E21972611E47225E984 (void);
// 0x000002A2 System.Void ExitGames.Client.Photon.TrafficStats::set_TimestampOfLastReliableCommand(System.Int32)
extern void TrafficStats_set_TimestampOfLastReliableCommand_mD3FA1EE8CB013F6AA66D0A47FB1649A7E0356097 (void);
// 0x000002A3 System.Void ExitGames.Client.Photon.TrafficStats::CountControlCommand(System.Int32)
extern void TrafficStats_CountControlCommand_mB6419EA4DD5C2B64DB2185E0E4ECD01550E9301A (void);
// 0x000002A4 System.Void ExitGames.Client.Photon.TrafficStats::CountReliableOpCommand(System.Int32)
extern void TrafficStats_CountReliableOpCommand_m9C16ED50D7BEC14B81B7ACC50A23368EF22F1296 (void);
// 0x000002A5 System.Void ExitGames.Client.Photon.TrafficStats::CountUnreliableOpCommand(System.Int32)
extern void TrafficStats_CountUnreliableOpCommand_mE684E7F3BFC63F8628CD568BD3797ABCD271D513 (void);
// 0x000002A6 System.Void ExitGames.Client.Photon.TrafficStats::CountFragmentOpCommand(System.Int32)
extern void TrafficStats_CountFragmentOpCommand_mD8F18787B770D60146236C24FB4EEDED1247D2FB (void);
// 0x000002A7 System.String ExitGames.Client.Photon.TrafficStats::ToString()
extern void TrafficStats_ToString_m5F6638AE5C823EA98AFD80A69FD8F20AA0956FAE (void);
// 0x000002A8 System.Void ExitGames.Client.Photon.EncryptorManaged.CryptoBase::Finalize()
extern void CryptoBase_Finalize_m2886DF6631789C681ABB0C1BE50ABE328CEE8252 (void);
// 0x000002A9 System.Void ExitGames.Client.Photon.EncryptorManaged.CryptoBase::Init(System.Byte[],System.Byte[])
extern void CryptoBase_Init_m00ECAC0A32BD218ADFE26E89615678838F33BBE5 (void);
// 0x000002AA System.Void ExitGames.Client.Photon.EncryptorManaged.CryptoBase::Dispose()
extern void CryptoBase_Dispose_m24E8825C1CA3A3D1B222AD1A4EDF9D7525A0DD02 (void);
// 0x000002AB System.Void ExitGames.Client.Photon.EncryptorManaged.CryptoBase::Dispose(System.Boolean)
extern void CryptoBase_Dispose_mE0DC43DBF4EC4DE744B754B7B70514B165117338 (void);
// 0x000002AC System.Void ExitGames.Client.Photon.EncryptorManaged.CryptoBase::.ctor()
extern void CryptoBase__ctor_mB1A24B5AF758B62B06D03E8103F535A3874AB2A1 (void);
// 0x000002AD System.Void ExitGames.Client.Photon.EncryptorManaged.Encryptor::Encrypt(System.Byte[],System.Int32,System.Byte[],System.Int32&)
extern void Encryptor_Encrypt_m2FAEBFC218959D447959BF7EC8C089BF9DFFC71B (void);
// 0x000002AE System.Byte[] ExitGames.Client.Photon.EncryptorManaged.Encryptor::FinishHMAC(System.Byte[],System.Int32,System.Int32)
extern void Encryptor_FinishHMAC_m3FABB4FADB652129D05213F36E3E20829132D6CC (void);
// 0x000002AF System.Void ExitGames.Client.Photon.EncryptorManaged.Encryptor::.ctor()
extern void Encryptor__ctor_m78FB7189B1CC3D520CCA2AA2FD65B1A5BF3A1CB5 (void);
// 0x000002B0 System.Void ExitGames.Client.Photon.EncryptorManaged.Encryptor::.cctor()
extern void Encryptor__cctor_m1B3DC25890306F5E12EA5493C12D3F5F8936A389 (void);
// 0x000002B1 System.Byte[] ExitGames.Client.Photon.EncryptorManaged.Decryptor::DecryptBufferWithIV(System.Byte[],System.Int32,System.Int32,System.Int32&)
extern void Decryptor_DecryptBufferWithIV_mAB0D42664EA4B598EB7E888C2200FD29B886BEA1 (void);
// 0x000002B2 System.Boolean ExitGames.Client.Photon.EncryptorManaged.Decryptor::CheckHMAC(System.Byte[],System.Int32)
extern void Decryptor_CheckHMAC_mB49176DFE57C07E3329385879BCA379EA5B2FA4A (void);
// 0x000002B3 System.Void ExitGames.Client.Photon.EncryptorManaged.Decryptor::.ctor()
extern void Decryptor__ctor_m41D95AB18982C2AEEC8CA44840127DA593253A2A (void);
static Il2CppMethodPointer s_methodPointers[691] = 
{
	OakleyGroups__cctor_mEE487C19564510344780DB28FCF08FAB66BD1349,
	NULL,
	NULL,
	NULL,
	NULL,
	DiffieHellmanCryptoProvider__ctor_mEED5CF4E94D4A37E43788595543E564166BBF03B,
	DiffieHellmanCryptoProvider__ctor_mA45E9BF1BECADA59F6EE5F2985F647C0A8D01798,
	DiffieHellmanCryptoProvider_get_PublicKey_mAF51FCC008349D1349720A851804320733B1EC70,
	DiffieHellmanCryptoProvider_DeriveSharedKey_mCEA2A1A47E14CC5783308E54AAEEADC062D43A07,
	DiffieHellmanCryptoProvider_Encrypt_mC705B8A8A95291E25AAE17E63C33296034436643,
	DiffieHellmanCryptoProvider_Decrypt_mD2AA88976027E9643AE10FFCA1B21872EEF69595,
	DiffieHellmanCryptoProvider_Dispose_m898DBFC6BB6B3CBB1CF867D78BBC230F02329183,
	DiffieHellmanCryptoProvider_Dispose_mFA5FAD398D484DA18926BE3D2F740660CD804082,
	DiffieHellmanCryptoProvider_CalculatePublicKey_m418F07D4B6CD32A81BC097A96D2F6AFFA588F322,
	DiffieHellmanCryptoProvider_CalculateSharedKey_mBD7C289D60270BF2020FC6960997D9861F37725D,
	DiffieHellmanCryptoProvider_GenerateRandomSecret_mE238E890E4BB6E476FB84637BB949FE61AAE0502,
	DiffieHellmanCryptoProvider__cctor_m34DA563DA4E57F1210EC7B62DD164A9B57EAFC88,
	BigInteger__ctor_m7654A4E955F4DBF085DB6D6D7CA2110303BC43AC,
	BigInteger__ctor_m20A30083A85B7F65F08565E68BCA041C63CA9260,
	BigInteger__ctor_m9D56B47AAEB7FBF25260773E6663A0EB85842597,
	BigInteger__ctor_mFB53F6C2CD5B373EC0B478D58156642DA9FBA61E,
	BigInteger__ctor_mDD9AE78E88E60EC3FF7C4C0178B5E2748BE00A99,
	BigInteger_op_Implicit_m038D734EA9CFEEFC334278161B8182005BFE2C4A,
	BigInteger_op_Implicit_mE7777641390753B556FF1FD504B3A7F2FEE43240,
	BigInteger_op_Addition_m959824F8E3F87297FB175D21FC404CD403798C4B,
	BigInteger_op_Subtraction_m871FBEDE8026A6E50B21157D741CF729DFDB6F8E,
	BigInteger_op_Multiply_m0717827BA7EB4732DCD2C82ADE1FCCDCE723C8F6,
	BigInteger_op_LeftShift_m36C249FFD4E33FB167D14EA1040B2A51A57136FC,
	BigInteger_shiftLeft_mC81AD034750BF1A10B3B73A5951A8F6602061BAE,
	BigInteger_shiftRight_mC0A7A08C847F07B5576F66F4AE693E8903D7C00F,
	BigInteger_op_UnaryNegation_m1C37D388C5FABA9880A3CCF9ACDAEA377A045882,
	BigInteger_op_Equality_m6474ADCED8364C13284E89AAC2DCD9A5DDBB1418,
	BigInteger_Equals_mFC4BD77E14958DC59F038CA4B57DDD936668D199,
	BigInteger_GetHashCode_m26DA903494055B79A9B4C3E2F1FC3D9C06E15C91,
	BigInteger_op_GreaterThan_m12DE07EBCD6C50D7463DCD2DCE516B0805CB6796,
	BigInteger_op_LessThan_m9A2B88AB5E5F6E5C8BB5F8192C5945357B9DEAF3,
	BigInteger_op_GreaterThanOrEqual_mD7CEA0FF6F33C684954E9ADDD0A6EC8CEFF0A033,
	BigInteger_multiByteDivide_m6B16471D0E3E03A6E1AFCB8D7A883E0606F9FAC1,
	BigInteger_singleByteDivide_mCC1B96C9A65800761B36F0C92BB5A8F1E925DF49,
	BigInteger_op_Division_m53B0EFEBFEF15410DCA583D08BE8315AF3BFBF51,
	BigInteger_op_Modulus_mDF8ADED75CF0AD36F0F27D817E8D55D8D966F3B1,
	BigInteger_ToString_mAA9CCDF4D2E91A19083BA449C7DE7E3F2B4A5F0C,
	BigInteger_ToString_m40EC68B17522B4790FE2A7A9444011C8864CCC31,
	BigInteger_ModPow_m1BCAF6A2734663F10C5D5C419014EB8272571990,
	BigInteger_BarrettReduction_mEFDCB07965CC0195F8F54F9E7396019F2C7F2693,
	BigInteger_GenerateRandom_mBA6450672822064BA5A03ABD43C88D479FBF3A0D,
	BigInteger_genRandomBits_mEC777F07AF82405BD5288EB485C22419C25270EC,
	BigInteger_bitCount_m4F5EBDC11660A1A189B57152873565AC776C822B,
	BigInteger_GetBytes_m992F27AD584544DE44102032C68E6FBC2905A079,
	BigInteger__cctor_m99653F6665C22C6B042412EA0220BA757D80A6BB,
	Version__cctor_m14E6C97EAB505D37977A7CD87D8C91204E5F8E26,
	Hashtable__ctor_mF9D32F7643B5F4BC034869DF7FED7FD9B856007F,
	Hashtable__ctor_m21C3D1A4D13C0E47F103016CE15919602D9B8CC2,
	Hashtable_get_Item_m9D94443F944580E7D54903CE54335A089D9F7122,
	Hashtable_set_Item_m814AA0E55EEF6ED7A59D56BDE4ACB784BBA20331,
	Hashtable_GetEnumerator_m32CC484CB5A95C9FD49C8A307FAB2915D296F6D0,
	Hashtable_ToString_mC77A49741434C7C3EC6BAE41C487E4E411D53BB0,
	DictionaryEntryEnumerator__ctor_mDE026E9865C77B8C210DDB5E5079C931D129E6BD,
	DictionaryEntryEnumerator_MoveNext_m6ED263E5E8A9F2D244F07DE5F7A8E51A0472D6FC,
	DictionaryEntryEnumerator_Reset_m54702804DD8E087499C67EF82BB126F9A52CAE40,
	DictionaryEntryEnumerator_System_Collections_IEnumerator_get_Current_mECCDD932F3824EF512FF7799A5D357C92073C458,
	DictionaryEntryEnumerator_get_Current_mE9A5C61240977483C92C60B017B018D755A3C127,
	DictionaryEntryEnumerator_Dispose_mF7375EAA50473DABBEE6DD2B65402298431E31BB,
	SupportClass_GetMethods_mA1DAB55A43B24B1DA454AFBB244B4D16241100C2,
	SupportClass_GetTickCount_m508AB440648DF77ECB5D775076706830756D0A6E,
	SupportClass_CallInBackground_mFC6750DF3A3DC7C9F679C697D47FF7E43C9898D0,
	SupportClass_StartBackgroundCalls_m83CE203DE0AC119B61A5120825CB3BEE7BBF4FC6,
	SupportClass_StopAllBackgroundCalls_m1A720D56F587D06B4A07EA23C85E9160776F67D2,
	SupportClass_WriteStackTrace_m9D1ED09B80B784F3AD3E84DC6087AC4AE3AB1C78,
	SupportClass_WriteStackTrace_m28141655ED044AE33F34B6CD49B2F85E17AA00ED,
	SupportClass_DictionaryToString_mE8C789DBC4CF65223825BDF90C948E1D9D992965,
	SupportClass_DictionaryToString_m055DA340910A84B9DAD8C7096617989787BADA7D,
	SupportClass_ByteArrayToString_mF5A56EBB889C26AE344E3FB35A2A290CD43F1793,
	SupportClass_InitializeTable_m0447919B22EAEE012D30BA17E7660343B1DA02C2,
	SupportClass_CalculateCrc_mEB9B95CB432299982740C4C12ABB5BAB6BD6D529,
	SupportClass__cctor_m42E427DE501E9D016179E517DEB09493B06ECA50,
	IntegerMillisecondsDelegate__ctor_m11666C49068D352A52644CEB6909BF64D69730D5,
	IntegerMillisecondsDelegate_Invoke_mF5B9C2456C857608EA138B56F337902DB635A4D6,
	IntegerMillisecondsDelegate_BeginInvoke_mF887B99A4EADA24B2E56D4FE9C43221C750F1E7C,
	IntegerMillisecondsDelegate_EndInvoke_m99985ADAC82D1A51135240ECA0A9A124CFB58B83,
	ThreadSafeRandom_Next_mE8EBD798219A6B348CF41939ACE7FC9A569930F5,
	ThreadSafeRandom__cctor_mD24A912B6C400AD675AA96B1466FDB046223FABD,
	U3CU3Ec__DisplayClass6_0__ctor_mA23F9BC40FC9C6D7A865E40733C979785B60A535,
	U3CU3Ec__DisplayClass6_0_U3CStartBackgroundCallsU3Eb__0_m983126A5A16C7AA2F1B52F9F4260990DEB7B7911,
	U3CU3Ec__cctor_m4D2300D9699E93CFAD0BD56AD065C1CF8AA00E7A,
	U3CU3Ec__ctor_m16CA5C3CEE7C1F73426D5A4B5BE6CF4F13BC59B2,
	U3CU3Ec_U3C_cctorU3Eb__20_0_m048C6973C52F6559EBFD6048D45EFCB5C01DB161,
	NULL,
	NULL,
	NULL,
	NULL,
	StreamBuffer__ctor_m7F0ED256385E47AF69692C5BD1028E0662D16965,
	StreamBuffer__ctor_m14F2D045ACCDD0608E0C846A5F2A18BFB5D8CF07,
	StreamBuffer_ToArray_m9C37D61D978A55F2DEC32E65D3D20964D8CF79AC,
	StreamBuffer_ToArrayFromPos_mB0B98B80F89BD8D884D5F79345A130265C8779FF,
	StreamBuffer_Compact_m584C429EAC58F98444FFADC47A469750D836398B,
	StreamBuffer_GetBuffer_mB2EB1A6F8252B7E3083CD445B73551131F651499,
	StreamBuffer_GetBufferAndAdvance_m440B5F2AD727185E0412AA60986BDE5DC5998ED4,
	StreamBuffer_get_Length_mF8F62F1856C0FB2E00A788695B56E7780948C34F,
	StreamBuffer_get_Position_mBDBC8E75BBD06EEB73505239E520BFF86794DDE9,
	StreamBuffer_set_Position_m6FF1D39C7F5C25CB5FEFB43169712739BCAF383F,
	StreamBuffer_Seek_m00D9849A38807DA28F4B6A5261F01871F650FE83,
	StreamBuffer_SetLength_m80BDB2D33060CE967AE7A1F541CE1FA750662488,
	StreamBuffer_SetCapacityMinimum_mBC8D7C80985E0C879CACEDEAF6CFEDBDA91F8186,
	StreamBuffer_Read_m2DCA3EFBD2350B51F4D19463C2D350188CAB750B,
	StreamBuffer_Write_m46BB33B27E747E8A6560F0C531511EB0315CFDFC,
	StreamBuffer_ReadByte_mD6CA8D84EA717C29B9E8EC480DFB6D1F395D2E52,
	StreamBuffer_WriteByte_mA31883A13FA3BCFB89EB6F8B151AD5CD90EE1E4C,
	StreamBuffer_WriteBytes_mCBFB947875BE288D2B3AD3C2D88B025140637B3F,
	StreamBuffer_CheckSize_m069E4A12B1100AC180202901241A27E4B304A51C,
	PhotonPeer_get_ClientSdkIdShifted_mBA20D7BCDEC3FC349FAB8CB03A101834D67A9E64,
	PhotonPeer_get_ClientVersion_mCE1217D603C03298DC56F405C5626D131F545C21,
	PhotonPeer_get_SerializationProtocolType_m241B6FD506A2ADFD73E919D7800C79E5FE0DEBB5,
	PhotonPeer_set_SerializationProtocolType_m3F1A6890FDCAB6240FE1617BFF7CCF4EAB80B4E3,
	PhotonPeer_get_SocketImplementation_m6820F1409E0067DE4B3B99676AAC7D10B39FBEAB,
	PhotonPeer_set_SocketImplementation_mB7D4C9883EFEA396B3DA9B1D45467D97B80EDB51,
	PhotonPeer_get_Listener_mE4DE6E89632E53F3DFC74FC70FEF50E8DBE72CBA,
	PhotonPeer_set_Listener_m6AE3033E47CDC53C687383BA8302ED398135CE03,
	PhotonPeer_get_ReuseEventInstance_m11BB0E904457B18F66E1A61997B2DE083832A8DD,
	PhotonPeer_get_EnableServerTracing_m79A85DCE8AB979C0C8029A1F58071AFDDE8B106B,
	PhotonPeer_get_QuickResendAttempts_m2BCB8DCCD0F8A4542AAD0C6EDFBBDCF2354F5941,
	PhotonPeer_set_QuickResendAttempts_m6DB0CD7664806B86AD9A4605D33260D84B428D9E,
	PhotonPeer_get_PeerState_m744647FF47CD8C6E8D82A0C272AD42CA8EAE70C8,
	PhotonPeer_get_PeerID_mF9E0C4EC264895D0C0AA28C84C075EF4DE7B0EAF,
	PhotonPeer_get_CrcEnabled_mE56C894F8F801F306E69015FBC682368DF9B4602,
	PhotonPeer_set_CrcEnabled_m5EFE4B945190C571F55C72290F39B6148898758A,
	PhotonPeer_get_PacketLossByCrc_m44CF3DB2B594358BCB73EC785CF117B6EAA57DC9,
	PhotonPeer_get_ResentReliableCommands_m593BA98ED969E97ADF337923C6D6D9407A961EA7,
	PhotonPeer_get_ServerTimeInMilliSeconds_m073DB9E850E0A15CFF62168FB14719B5332AA8E9,
	PhotonPeer_set_LocalMsTimestampDelegate_m976350E12892A2C87DEBC3BA436F556D010B37DE,
	PhotonPeer_get_ConnectionTime_mFCBBFD1B4B6E8A165F187BE45297591A9D3812AF,
	PhotonPeer_get_LastSendOutgoingTime_m1EF6C16FBE9C64E3831833D92482051DCA01B9C7,
	PhotonPeer_get_LongestSentCall_mD99C118B30F791AE0ED3D8A3E96A619C45EC7C59,
	PhotonPeer_get_RoundTripTime_m62272FBDA0559234256B59318B17958B356CC470,
	PhotonPeer_get_RoundTripTimeVariance_mDAA6158DACAB662BD837D8FA5C43C188DA3AC944,
	PhotonPeer_get_TimestampOfLastSocketReceive_m2636CC04F03168D4523E6ED6B54D23846CF2DE38,
	PhotonPeer_get_ServerAddress_m7FA216808061F2113B7A902B921B6B3BE3C49A52,
	PhotonPeer_get_ServerIpAddress_mCCDB432F5B35677DD1F2610F0BBEC6FCE140ADDF,
	PhotonPeer_get_UsedProtocol_m941EB6A3A8BD5D059352ACA7E6EF13845CCE68F2,
	PhotonPeer_get_TransportProtocol_mFEED042A2E83B87CBE556BA1252CBEFAC0207623,
	PhotonPeer_set_TransportProtocol_m9101DD605A90289E142A201B00A8A504AB83C119,
	PhotonPeer_get_IsSimulationEnabled_m43EF1F251D098CC443D78CED9EBE5263F432ED5A,
	PhotonPeer_set_IsSimulationEnabled_m9141CD17E738B46600AD4ED3E8926A1B55BF40E1,
	PhotonPeer_get_NetworkSimulationSettings_m97D23008F2713E2E3A62AF889CAD9AD2D00C2BAD,
	PhotonPeer_get_MaximumTransferUnit_mFF03F9C31DD8A110D705F322181623B8E4DC6BC7,
	PhotonPeer_get_IsEncryptionAvailable_mE014D64EEFD5D20BFC57367BC420CCFC68716439,
	PhotonPeer_get_IsSendingOnlyAcks_m33A8FFD5F0EC8D5C60AF70F1C3AB48C12844643F,
	PhotonPeer_set_IsSendingOnlyAcks_mB45BBA417D520682220A98BD088D161D062A6156,
	PhotonPeer_get_TrafficStatsIncoming_m2DE8612AD9C2442910F6470C783E8F3926C5DFCD,
	PhotonPeer_set_TrafficStatsIncoming_m3527030FD2D27CE57094A84ADED781871F63B751,
	PhotonPeer_get_TrafficStatsOutgoing_mD5AAF890C29FEED4293897A3EE59BF40FA24327D,
	PhotonPeer_set_TrafficStatsOutgoing_m4549C6D901C01786E112188A617D851011D03BA6,
	PhotonPeer_get_TrafficStatsGameLevel_m303B9889C33154A66D106DF084FE4307F41B7796,
	PhotonPeer_set_TrafficStatsGameLevel_m5ACC9C88363789E2F54E597663C243608B9D8104,
	PhotonPeer_get_TrafficStatsElapsedMs_mC5EC6DF9440E00F14867964EB87430C3C73F8651,
	PhotonPeer_get_TrafficStatsEnabled_mBF1DB3B0597DA43209A70CEC016184E6A79003B7,
	PhotonPeer_set_TrafficStatsEnabled_mD1BC18264BC8E32FF3BF002198066BC8717961B8,
	PhotonPeer_TrafficStatsReset_m02CC52F2C9BF5C3E1F4B9ADC07425401931178A0,
	PhotonPeer_InitializeTrafficStats_mCE9ECBFA897962B11CCB2E8130FADE5B71DAB9A6,
	PhotonPeer_VitalStatsToString_mBE4DB09E7BFDAC0C6C2143A5E4F1676C1638685F,
	PhotonPeer__ctor_mA24D4FC9F9550C68800F4D1CE652EDBD341B525A,
	PhotonPeer__ctor_m8662F89CCD4FF7AF4D98F68B638DBE818E0D543C,
	PhotonPeer_Connect_m55EBBF5DCAC6ADB2787F0DDEAADC46A3B9859BAD,
	PhotonPeer_Connect_mB27D3BA2C51FB0832F6114BA26DF66FD91FA3E53,
	PhotonPeer_CreatePeerBase_m5E7A92325DAC67A66AFF6EB4B20F3734B5814E93,
	PhotonPeer_Disconnect_m93DA50619C668C585E6FF76AC96243E71ADF3D55,
	PhotonPeer_StopThread_mF30F314511A11097BEA7E84A8DAB82CEC3AB721A,
	PhotonPeer_FetchServerTimestamp_mFED8B6680D0DC87B643CBB4A151DA31FC1C26CBB,
	PhotonPeer_EstablishEncryption_mBFDFC1E1BD18BE4EB9DFF8093497F0DD481470D2,
	PhotonPeer_InitDatagramEncryption_m957B1401835C4C14EE1B88D54105DC8ADFE95300,
	PhotonPeer_InitPayloadEncryption_m5818EE76FF876DAC4D2F2D8758DC9A764C6D4525,
	PhotonPeer_Service_m86FA834146E54C53B7AEA6DB957ADAFB8514A52A,
	PhotonPeer_SendOutgoingCommands_mC69CB3D8F7AB255651AF411377E2C4860F1D40B6,
	PhotonPeer_SendAcksOnly_m7AFFE361BB36F1D3E2A759029B1F7DE344DC92A5,
	PhotonPeer_DispatchIncomingCommands_mFD406C9E792960EFFF5BEF4B7D04CECAB41DCE99,
	PhotonPeer_SendOperation_m4D7D271BA8337D9006365956332B91ACA154A068,
	PhotonPeer_RegisterType_m1F8EDFA7E77F1D5F279103FA2D39C506364650EB,
	PhotonPeer__cctor_m19723A97D60766CD651A293E69962978B57BDD95,
	PhotonPeer_U3CEstablishEncryptionU3Eb__176_0_m87B77B44E4DE1FC6EE194C1E1C9117E51A89D1FC,
	PhotonCodes__cctor_m1D735D3E8ED81167A471CD98BA5592E979A1F0C8,
	PeerBase_get_SocketImplementation_m79A3101465FD7FA6060FC55A12A3D58F154A8505,
	PeerBase_get_ServerAddress_m71CA11FE813913DAA4F1F9554964E367EC1E8419,
	PeerBase_set_ServerAddress_m7C1A0AC51E2E110A24CB79B6C5CF061405B51877,
	PeerBase_get_Listener_m8A72A92EFEF8395D98F2C0538E3420E8A42A3E45,
	PeerBase_get_debugOut_m95153EB49C30BC059E23104B0DA1E3EC77080138,
	PeerBase_get_DisconnectTimeout_m141518E1E754C78C917DCCA42178DE749CC45C6C,
	PeerBase_get_timePingInterval_mFB4C5685DC0A19D076B2A1E7717B0262169623D1,
	PeerBase_get_ChannelCount_mACF054DC726A37FC6E0E39F62A6F75447920D8C4,
	PeerBase_get_PeerID_m754EF3CD8E17F2C497833AC6A2FEF5A30E9D09B0,
	PeerBase_get_timeInt_m94580582ABA72B8851C01EC4E598DCFD4D6A5024,
	PeerBase_get_IsSendingOnlyAcks_mC759A8A5D23A2327C5E1EED67A0C53BF07A50DC2,
	PeerBase_get_mtu_mEC86F0DD4AF2C40E596839235FE151C781F614DA,
	PeerBase_get_IsIpv6_m3EA33C3E36F90D24C2497113BAB6B0DDB4FA0F4F,
	PeerBase__ctor_mE31EA56DDBFCBE25590BC491DA8C70364F8EF2C7,
	PeerBase_MessageBufferPoolGet_mF43ADD7AADFD540877793EABA1489AC5B3B444A1,
	PeerBase_MessageBufferPoolPut_m7513BC7EAD750A9AC82B84BEF2BC2F409D26A903,
	PeerBase_InitPeerBase_m0E945EDBED31E3B81474FBB434DAD23695954ED1,
	NULL,
	PeerBase_GetHttpKeyValueString_mBD50A4BDADEC199C285F9BE694A6FA7402EA3FEC,
	PeerBase_PrepareConnectData_m1BF42E8D598315385C35B722328E2F65C11E493B,
	PeerBase_PepareWebSocketUrl_mE3F995C56A7F40515E3C8B264BB188531AA065E9,
	NULL,
	PeerBase_InitCallback_m384CDAE1DFE2844D384A9A043A97DD305CCB5622,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PeerBase_SendAcksOnly_m1305296D0CAEFCEF356E3555754A3B68F95AFB45,
	NULL,
	NULL,
	PeerBase_DeserializeMessageAndCallback_m956E8E078C23BE6F8AFAA4E80C3630AE25B97A80,
	PeerBase_UpdateRoundTripTimeAndVariance_mE35461EE543B563ED93948696D371D511E4D2A75,
	PeerBase_ExchangeKeysForEncryption_m79F8BDC9F5A8B79589EE8B09658AD813B705688A,
	PeerBase_DeriveSharedKey_mF302F8533114D8F3B5724562B246BBA55233AD5B,
	PeerBase_InitEncryption_m7E66B1132094454CF9A7D584D7D015D1DC2B5B7F,
	PeerBase_EnqueueActionForDispatch_m1BBE511B812834985E304E6D39520D6F82831F50,
	PeerBase_EnqueueDebugReturn_m43A0CAA0916A7FB988AB52C51E339FEAFEDB48F7,
	PeerBase_EnqueueStatusCallback_mD104ECB17930B820145CD8B870AE1B4DCCDF9D9D,
	PeerBase_get_NetworkSimulationSettings_m3A06EA5987C4E3AB2C28B6D3226CEF7993718227,
	PeerBase_SendNetworkSimulated_m7F1DB81F6EDFF65F01BF8E5DE6325CB4F2636684,
	PeerBase_ReceiveNetworkSimulated_mC3EA2A65918D87FFFC1D5BFD622B0F7FF06182AC,
	PeerBase_NetworkSimRun_mBB05EDFBE1B39E744CBF4907D5ED7C3F54B5A2FE,
	PeerBase_get_TrafficStatsEnabled_mC073D175183A6D1E9DF3444465DDA6053EA0DF2E,
	PeerBase_get_TrafficStatsIncoming_mEFDE4E31EB4C7D63F90EEE1722C7110F40F86369,
	PeerBase_get_TrafficStatsOutgoing_m39246E29BF90DF7DEECA31244F5C7B1B00C26114,
	PeerBase_get_TrafficStatsGameLevel_mAA3C1ADC90CBF4FADDDF5A7FAAAD3D7720A2FFC8,
	PeerBase_get_CommandLogSize_m0272BA0E68346EBED1381BC2E8F864B27C5F309C,
	PeerBase_CommandLogResize_m12247256EE0054174B7CA7004CB7730E9D6E7174,
	PeerBase_CommandLogInit_m86007D73282305CA5A55328FF9FCCE0CA3F544CC,
	PeerBase__cctor_mB75A698BBAEB86982256B91C22ED16BC78EEE186,
	MyAction__ctor_m2063CA7F96349400661145A2E272CB00F9325729,
	MyAction_Invoke_m146AB584524C6E17179A1E1A714C958BE9F11755,
	MyAction_BeginInvoke_m84D1393AB90A6A84616F44D815DCAB9808BE465F,
	MyAction_EndInvoke_mA444DF66DED1780E583646376BCFEDDC331958E1,
	U3CU3Ec__DisplayClass104_0__ctor_mC25CA6D2D449CF07A13AE17AC374CB0115584133,
	U3CU3Ec__DisplayClass104_0_U3CEnqueueDebugReturnU3Eb__0_mA924D04338C87495348EF2F28793191B8EF0683C,
	U3CU3Ec__DisplayClass105_0__ctor_m8933D54880906067B2193D0DCBAFFA41AB190A40,
	U3CU3Ec__DisplayClass105_0_U3CEnqueueStatusCallbackU3Eb__0_m97CC8A71DA1BEE3A9827F1601299F6D700B3AC85,
	CmdLogItem__ctor_m32026F769F8D0688E36B1270F3F37197CFE569B2,
	CmdLogItem__ctor_mEDA72F344CEE187C40DEA539F3BF34EAAB65AFDA,
	CmdLogItem_ToString_m945408FFC25E43A643E427C0DF401747160CAB0B,
	CmdLogReceivedReliable__ctor_m93B8CF226CC177A245237DDBDAAB9234CF1E67B2,
	CmdLogReceivedReliable_ToString_m0200DD37EFFC8759129B7381FD0085E855E767B1,
	CmdLogReceivedAck__ctor_m883C9D7509A995B25249C9BA442D41FB912B607F,
	CmdLogReceivedAck_ToString_mAF6FAF10603BD993308D43B45693AA817FEFE026,
	CmdLogSentReliable__ctor_m63E7C8618186D3988E4CCD0004C2712FF5DB5B3D,
	CmdLogSentReliable_ToString_m3D32AAD8165EBC71A527D7EDC6733FAB68AAB3C4,
	SendOptions_set_Reliability_mD444223906FA26A1589B9EECB772CA98CA66E164,
	SendOptions__cctor_mD73641D14A950B790656EC669F4DF1E198B7C527,
	EnetPeer__ctor_mC16BA5BE856937FD5013C2E858C9AEC81FD68396,
	EnetPeer_InitPeerBase_m6F4958EC3E0EF7AD6752151219B3E87DD77F4DA6,
	EnetPeer_ApplyRandomizedSequenceNumbers_m260809622D75FFB81C2D83374F624CBB9425A0D9,
	EnetPeer_Connect_mD4F2DD6412A5378DE8AF795BCCB4E272E5EBF5A4,
	EnetPeer_OnConnect_mD31CB14E4ADE8ACC99F12CB2C1BFD3D292B2D634,
	EnetPeer_Disconnect_mA93FC6CED1708D1CCFC653EF9C2E8D45AF4AD7BB,
	EnetPeer_StopConnection_mEEEC4D532A831B54D129D983D6B1C893F0C9113D,
	EnetPeer_FetchServerTimestamp_mB0A96A370674E7C082DC4598CBA69F0E8C69D2B4,
	EnetPeer_DispatchIncomingCommands_m6E81ED91B3A7920805A832B1F45E203CD86A52EA,
	EnetPeer_GetFragmentLength_m4DCA08CA85A208184FFD77EA01FEDAA38F17BB61,
	EnetPeer_CalculateBufferLen_m41DCC0ECCE15F5AFD2C775B24542D532D2ABFD5E,
	EnetPeer_CalculateInitialOffset_m4CB5BCA2852247CB0E1A7620D9E687DE3C146A02,
	EnetPeer_SendAcksOnly_m248A3B4E348AC6D0019E9528D5654A547FBC2C5F,
	EnetPeer_SendOutgoingCommands_mDA37A5DBB0AF3E816A33D7780CAA2AA34EA99D16,
	EnetPeer_AreReliableCommandsInTransit_mB81E257DF032D48AF5796A4E4B18185B30387139,
	EnetPeer_EnqueueOperation_m13141EB4BAE223899210464B0D58E433D10DE181,
	EnetPeer_GetChannel_m66F0E0B762B4FDF4A479D2F58D90A82FD21AD242,
	EnetPeer_CreateAndEnqueueCommand_m7B9E0742E16B337CB97766C61A484558BFBBE4D7,
	EnetPeer_SerializeOperationToMessage_m372A833A19081AAA150841CC7F1C758C2EAE4E37,
	EnetPeer_SerializeAckToBuffer_m8485E0F374E2A135DCDFE10EEB06327B7961DBD5,
	EnetPeer_SerializeToBuffer_m97F0967FE3F12E6F8D2D929A76A96E6C07433F4C,
	EnetPeer_SendData_m3BCF88DE2DD6A00B5E56352BF0866A59CF5A2403,
	EnetPeer_SendToSocket_m22FDBBE8E82DF6F00EFC7E0F536E9B05B718EC96,
	EnetPeer_SendDataEncrypted_m9C6D6FDFD58916CC6F9B670156D3A7DBF22AA730,
	EnetPeer_QueueSentCommand_m25E8E48FB3436E4498E0BF1208611C34FE9BC023,
	EnetPeer_QueueOutgoingReliableCommand_m18F7367F1F665391BB9DDD2EE00FF0FB47A5BEC7,
	EnetPeer_QueueOutgoingUnreliableCommand_mD46505878D1A89EE761F00ED96B45CDD11651F2E,
	EnetPeer_QueueOutgoingAcknowledgement_m3142B32F001108364EF6FAECC3E71306A1026196,
	EnetPeer_ReceiveIncomingCommands_m6934BA2C5ABA3A01676D29A655720410F1EED19E,
	EnetPeer_ExecuteCommand_mC3FCC0808F6BFFB23812DEE44D73EB035123CD00,
	EnetPeer_QueueIncomingCommand_mCCFB2017D73DED9676DD5D6F0682003A7587000C,
	EnetPeer_RemoveSentReliableCommand_mB3DD19635F42CD363CA3BCB27986E015A4DD7D84,
	EnetPeer__cctor_mADAB19D86CD5EBC58A2C38D9490E506EE8FFCC59,
	EnetPeer_U3CExecuteCommandU3Eb__71_0_m8541C1486162470F57850A932CFC89128B7EA8D8,
	EnetChannel__ctor_m27DE491AB4D607CD74B4B4C797AB8ECC71F55FFD,
	EnetChannel_ContainsUnreliableSequenceNumber_mC39737900F3B65BF0B8DA66EBCCF9981935110F0,
	EnetChannel_ContainsReliableSequenceNumber_mE6F01508E0A1861256CD459E5F9B952968AE9D84,
	EnetChannel_FetchReliableSequenceNumber_m6F24FB6BCFD362289856B49C4117E897E0D59B70,
	EnetChannel_TryGetFragment_mB8F6D45911EE25780D54E6DD1A13F712583E8EC5,
	EnetChannel_RemoveFragment_m35877E62DAF48FC2ED3C5710FFB77822CC4C579B,
	EnetChannel_clearAll_m42BE5930070E0C05832C200C59DB401050CE894D,
	EnetChannel_QueueIncomingReliableUnsequenced_mED56B16EAD0A232F440B0CFB33FC861893AEEB81,
	NCommand_get_SizeOfPayload_m3315BB34B3E705E0168E9F82A3A49A7433C84E36,
	NCommand_get_IsFlaggedUnsequenced_m6B9E6E8F62B45AF7D06AD99DEF402470EA83024E,
	NCommand_get_IsFlaggedReliable_mD64E895E47FAA56D11372DC725F27BFEC15D9618,
	NCommand__ctor_m75847131A07A0A65D2CBA53FBC0D3E011A282B5D,
	NCommand_CreateAck_m53FDB2FEA5BD52168164317092BC8CE652E50DA2,
	NCommand__ctor_mD368544E4ACE32DB42A8F0AC51A18DF89C2C9211,
	NCommand_SerializeHeader_mA2578944C419E5E9397E316BD7F9A4E10700E65B,
	NCommand_Serialize_mB1BE9852725338715B906B668BF2BF7F3DCCFB01,
	NCommand_FreePayload_m66C17A68D5E64FFB6D007463AB3DFD708AB36AFB,
	NCommand_CompareTo_m2BC91BD2FA385F8A622B6D83E1D0682C82C8B889,
	NCommand_ToString_mB42A0C647E0D464792B68E9753D1102C07D389EC,
	TPeer__ctor_m0AAD038F46524E7C33C194CBCB60C2EF4FE2B4A0,
	TPeer_InitPeerBase_m933F91DDF8D5C6E5D6453CE2021BD83081767D19,
	TPeer_Connect_mB5E19BE438AD5C6C98FAE73802202DB9607B7C60,
	TPeer_OnConnect_mD088FF892E5D3C59E690FDE0717D1970ABF2A65E,
	TPeer_Disconnect_mD15D80443A8401D03E9210AA1FC78F787F6D3446,
	TPeer_StopConnection_mBA5FC93C53B3BA1C22C49D66145E3F3AA53A77F7,
	TPeer_FetchServerTimestamp_mF05A52D2AF86BEB276123975E70C65C7D6A7F62E,
	TPeer_EnqueueInit_mDA521CF73AECDBD1E33EA52E891895CD5B0709C1,
	TPeer_DispatchIncomingCommands_m553C5D561EF7BFD1D6C0E0AEE9685BA5C1CF2E68,
	TPeer_SendOutgoingCommands_mAD8142D5186B9FE11694DD522B4CE5EF7995D904,
	TPeer_SendAcksOnly_m0F51C9848996F89C697407E0330A1497B9257AAF,
	TPeer_EnqueueOperation_m0FC01EF92EBB29BB78967379AD74F93E50C6E375,
	TPeer_SerializeOperationToMessage_m40F15E7EEABE1E7F44D95FB13E0D711991EBD754,
	TPeer_EnqueueMessageAsPayload_m7A0A4E1AAAED9F89038E8F2C20900AAE17144484,
	TPeer_SendPing_m700E8BEE15A1825E0C84DD69D25EAE8799461B11,
	TPeer_SendData_mE46B18DAFDC4FF4190E07EF8F160C58D41358638,
	TPeer_ReceiveIncomingCommands_m0260CF906CEB6D7A6F571E3F147CA4717AEDAA10,
	TPeer_ReadPingResult_m27549D3A36D87047F8BBE45202ADF109F3F929EE,
	TPeer_ReadPingResult_mE1983BB482F9F5B5628E6D88F89D3C37CC7C88FB,
	TPeer__cctor_mFBCEDEDC2A674F76E7F358B1D6BCE6EEB7E54F78,
	SerializationProtocolFactory_Create_m5F16DBF123F6C86DE1F7A88A829261A317B195E0,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	IProtocol_Serialize_mC230D51065735957460BFA29F0EC0BF6DD9A7278,
	IProtocol_DeserializeMessage_mC02A0E20435EDB669F67D0E0F865A83B91C1E0E5,
	IProtocol__ctor_m210F07501179E0D1583384E853319379CE615D29,
	OperationRequest__ctor_mAAE3BDF567044323DD790053485BA12285BB986A,
	OperationResponse_get_Item_mFB5F85A07DEEF51181E49C8F80C1FEC67BDFAAE9,
	OperationResponse_ToString_m1E6A5A078575A5956918F1C076F8A59DFEAE0886,
	OperationResponse_ToStringFull_m44C8BD752CF85BA4EE8091B55CD2B567F4493271,
	OperationResponse__ctor_m8DFAE336235BEDD612AAA6E756A132F8ECC1DF31,
	EventData_get_Item_m9F3B5A208B3320BE5703C900234045F531945D09,
	EventData_get_Sender_m2C4FD0BD7FB932831A4FE09DA5684BF47A097C7E,
	EventData_get_CustomData_mC2B19B362AD05F80FE3C0A3942298A274A7905B6,
	EventData_Reset_mA6F9E66B16D35D5F05413A673AEC4C271AE1B237,
	EventData_ToString_m9ACDEF9B8BBB9B08E9753D0581A9E4C5C016D015,
	EventData__ctor_mFEC6E52A01DA96CF8744498E59912E2463FDCE39,
	SerializeMethod__ctor_m53A0D155AEA1303B5C087D8AECCF5E2106BF01F8,
	SerializeMethod_Invoke_mF244EE2F86DE7D2BA965F242191800544C0C3200,
	SerializeMethod_BeginInvoke_mE1905D51F9776AA4670E6592459036E3735367FB,
	SerializeMethod_EndInvoke_m7933046D7C2F615CF4E93131E90B162EE98576F5,
	SerializeStreamMethod__ctor_m18AEEE1ACFECE593803F5BACB876161A9B90BAEC,
	SerializeStreamMethod_Invoke_mCCD2BE0E82AE7C7D55830163421FBF91C8A0FCBC,
	SerializeStreamMethod_BeginInvoke_m96AEBC54D3B30CF7BB8999A177CBCB22FF5AE907,
	SerializeStreamMethod_EndInvoke_m5ED21923CC063F16EF3717A906A1CA18148EC29A,
	DeserializeMethod__ctor_m3DF590D0362710894C97C458B50AF7C8E50C38FF,
	DeserializeMethod_Invoke_m70CBA7C1B5A21F7C6356AB4D0563E35C3B1DFA5C,
	DeserializeMethod_BeginInvoke_m8DF87D6B7302FF6D61ED3F57918181C500C95039,
	DeserializeMethod_EndInvoke_mEF00BEAF2019744BFD2F68C77F785038964EE120,
	DeserializeStreamMethod__ctor_m262F59ADCC2C3231C1669392405B0EEC17D0BCBF,
	DeserializeStreamMethod_Invoke_m6FD40B3E411B8CD37D8992B4DDCF74BF539B8193,
	DeserializeStreamMethod_BeginInvoke_mBFB52E941674D727215F5FA3D6577831BA678203,
	DeserializeStreamMethod_EndInvoke_m83B7093FE124B2300B51B912915D6C6A1C682577,
	CustomType__ctor_m9B5896E661D1A284E3F424D408F6C09AC9B132C9,
	Protocol_TryRegisterType_m1F42CEE04C1CB3AA95A4EFCF4E71BBEF1EF4941A,
	Protocol_Serialize_m51A1A0D9AA1D7ECF0451A85B1A2503BCBA159C44,
	Protocol_Serialize_m68E82AB3C30D00432FF336B2DA2F728CA841AFF9,
	Protocol_Serialize_mB0113005516D9F65E01D660036CA261988B4AD77,
	Protocol_Deserialize_mA419E340A605A24E5E58E167D7C8B491B1A97E5E,
	Protocol_Deserialize_mC51EDC50C443F02A9A10F662B9A6B90E33E35283,
	Protocol_Deserialize_m0C8F5B0748310559281378101B4D945823CD0406,
	Protocol__cctor_m4464D147D032ECA451D409E29FFE31B23FF8DC20,
	Protocol16_get_ProtocolType_m4117BFA067EA5B6D95CB543A7D3500B33D719A5F,
	Protocol16_get_VersionBytes_mFFC0E7D06CC68114C4824681AE461DE0DDFFFDCA,
	Protocol16_SerializeCustom_m51F02A854AB6D79AE3D2C31141A905F670C10D93,
	Protocol16_DeserializeCustom_mA66301817515061C364B96A654724B7587078715,
	Protocol16_GetTypeOfCode_mBB4406039CD2930C880AA7A344DFBEDA7A76A6A5,
	Protocol16_GetCodeOfType_m404353148323F3B7BAABED19054E00B8250C2795,
	Protocol16_CreateArrayByType_m4B7180AE6ABFAF8D027A8E4B7F55650369FAF8F0,
	Protocol16_SerializeOperationRequest_m4BBA5011C31AD542F763E9F67475441AB12C2575,
	Protocol16_SerializeOperationRequest_m60BE0F1CC22EFE1B0BCB66DE0883AAF32D25B74A,
	Protocol16_DeserializeOperationRequest_m7BF6E3B988F16DA080B43FABA4C9B1460014BD8D,
	Protocol16_SerializeOperationResponse_mEE7DF98645155131422FE54F45DE1F284F420F64,
	Protocol16_DeserializeOperationResponse_mE4931FC9875501D583E4CFEF4451DBAC1D6D196F,
	Protocol16_SerializeEventData_mFFDA044BAA2401B07AEFD64A6022169D2993E5D9,
	Protocol16_DeserializeEventData_m1360D20976EC991DA3B0846C887265E2A1692894,
	Protocol16_SerializeParameterTable_m477E86E43B9D749F3FC4F2CD44374EC4E37CCA5F,
	Protocol16_DeserializeParameterTable_m29ED70DB84DB714CB981AE64677B221F88212D7C,
	Protocol16_Serialize_mD67437DF9C379AB41269F636FF2A9B53F06B44D1,
	Protocol16_SerializeByte_mE7A2DE97766F460339C5AAAE8FD49EA71C7CCCB7,
	Protocol16_SerializeBoolean_m961D93A4C48B2F261666BF4A4AE32E7816B561CE,
	Protocol16_SerializeShort_mA017EE1ACF8E528E1F1110F0D02666319FF9AA0E,
	Protocol16_SerializeInteger_mAB82399E7917AF27CB112E4D7481E6671822CC8A,
	Protocol16_SerializeLong_mCBB1B4DE3B55AA49FD22A26A1483F59B10863729,
	Protocol16_SerializeFloat_mE4A5403413074E677BAAB7B00A4544EEA42AE9E3,
	Protocol16_SerializeDouble_m5E8D87283ACF0BDFB1ACF55E684129398DD62B05,
	Protocol16_SerializeString_m945B7BB907C8776CC66F14B51B403A037AADD527,
	Protocol16_SerializeArray_mD9970EF06A769EF994AD8FA64F064093C17A3269,
	Protocol16_SerializeByteArray_m345789FD0A865793B52636722E792A58835DDB44,
	Protocol16_SerializeByteArraySegment_mE994FE9493B4A87A319E876A7675718E4715D969,
	Protocol16_SerializeIntArrayOptimized_m2A965AC0B57EF717BEEAC2902E4652DA39610827,
	Protocol16_SerializeObjectArray_mE1543AF4B9C3DC00900F19BD8BDDCFFD769E15F2,
	Protocol16_SerializeHashTable_mD9A2245C98278F9B0666D526ECDF93CC7E92B7BE,
	Protocol16_SerializeDictionary_mAB6B3817D0F0DA74146E7C583D9BA80B5852A907,
	Protocol16_SerializeDictionaryHeader_mF01ABD689EB216F5439254691DF2BF6EE376081E,
	Protocol16_SerializeDictionaryHeader_mE3176475FB07D641CAA268C89FE3D32382BFD10A,
	Protocol16_SerializeDictionaryElements_m029994D35AB62954E2DD4F17B387EBFA4FAACEC3,
	Protocol16_Deserialize_m2D4E73FE116ABC6469C0275C21411E337BDF4F15,
	Protocol16_DeserializeByte_mCC85BCCD41C1EAEF3FD34BBA7A40F51CC7ED40DF,
	Protocol16_DeserializeBoolean_mC5E230DE51DA7262F631B11CB18431E3B9622405,
	Protocol16_DeserializeShort_m423E63D87D0E1AC87D581E1AD30FDAB4AA505969,
	Protocol16_DeserializeInteger_m9894A4B1BF5B4095D57EA34F443D0ED796A7B423,
	Protocol16_DeserializeLong_m0076276DA3AD3FF724F8C4494D1A460F142157D7,
	Protocol16_DeserializeFloat_mEBF6EBC72148213980984E7F1FFA154E4C649819,
	Protocol16_DeserializeDouble_m05FC8B4B58B0E26AF8C12E5C531AD720EB042BEA,
	Protocol16_DeserializeString_mE6BB13EBE18335BD2E38049C6733F50EA980B3B9,
	Protocol16_DeserializeArray_m79B5F660AAAD7A460AD3E8EF96088769FFAB5B78,
	Protocol16_DeserializeByteArray_mACB1269F8EA101451CC692E2379BCCFC077773E8,
	Protocol16_DeserializeIntArray_m3BFF6BBC4EEBE0F694949ABF0BF8F6E78EB0A6C9,
	Protocol16_DeserializeStringArray_mB4DF22E304CC4FFB1B06DAAACC3801E9BBFC4E80,
	Protocol16_DeserializeObjectArray_m27E7107257D3D0C4BB50581967336A9C30925A08,
	Protocol16_DeserializeHashTable_mFDDA9CD7ABC1330FDC66AF3328A0C3BC2398F441,
	Protocol16_DeserializeDictionary_m176EC6C735BCC77731D7A994D12F4876E8DF6B59,
	Protocol16_DeserializeDictionaryArray_m579130A4CA868EE9AF7DC6D9A8B59E43C326463E,
	Protocol16_DeserializeDictionaryType_m7CFD67ECB349092C3017269D75C71C0F61648418,
	Protocol16__ctor_m5DD0125F13E5A36EEA804792D8E22AE165EC7849,
	Protocol16__cctor_m5BF7B1F43A20AD8AFDB1D561D99A4DDC41A37265,
	InvalidDataException__ctor_m758F4FD47275F3EE043B409F144D4CA828F9134F,
	Protocol18_get_ProtocolType_m08E01A92C24E912CD2FD903877C80D7D759D37C2,
	Protocol18_get_VersionBytes_mAF0EB5AEB64ABD146A3286295709213C4D1872C7,
	Protocol18_Serialize_m7C6FB14B76FF91615FE6FD3CA83D1841A52055E7,
	Protocol18_SerializeShort_mBB0F0D7218C747B5907831D954C59BFE68F52389,
	Protocol18_SerializeString_mC4E096D4D5CE13B493E64B0C3D51441AE1AB7CBE,
	Protocol18_Deserialize_m1FE393106DF14380C910828D4D323CEB143461AB,
	Protocol18_DeserializeShort_m2432676BFF74D3B16DA7DBE7FB8C24CA339D3235,
	Protocol18_DeserializeByte_mCD9F2A1CDDD1C5FD2B11E87DA7E48B4C3515F7E9,
	Protocol18_GetAllowedDictionaryKeyTypes_m2BC01A796D123A0C27B309F825816BC90293BF6B,
	Protocol18_GetClrArrayType_mDD8DB56376D2E59F33317387A599985DCC7EF929,
	Protocol18_GetCodeOfType_m395B9C479ADF3DFB54D8497114680B4D77A7980A,
	Protocol18_GetCodeOfTypeCode_mA91B7B6663DBC8919048BC671B5EF8A3DD31AAF8,
	Protocol18_Read_mFB5CCF85CBEAA840D46BF84D2D559B245AE4149B,
	Protocol18_Read_m0C9C1B33AD94BE442B79C9E7F7847108AB21C5E5,
	Protocol18_ReadBoolean_mA741304E3977EA7C673BAD06AD347CE0BA0E46E1,
	Protocol18_ReadByte_m0130C5C2D77AA5B51B9D200A970A28803B63DCF2,
	Protocol18_ReadInt16_mCFE1065DD31950DEE0DE7011767CECEA8AD08310,
	Protocol18_ReadUShort_mC69C03B256F0671166941F2730109CC6077FCEE8,
	Protocol18_ReadSingle_m1A82A3E2FF37D73EB2D97F37825F94A91E6D4D35,
	Protocol18_ReadDouble_m72FD5E1AD19325D3CFC20192FFE13C81CC8E2988,
	Protocol18_ReadByteArray_m238D163A841FD8B36FDFA3902D5F1B1BC33F1822,
	Protocol18_ReadCustomType_m3541BB96FCC6D4DA0B5DAB480F4666B311C1B35C,
	Protocol18_DeserializeEventData_m11BE750DC8B6CAF17D1DDF637BDDCAEB103797BB,
	Protocol18_ReadParameterTable_mEF088E2A7264C72F23F6AA55FF927DEFD3022FD2,
	Protocol18_ReadHashtable_mC9C7A07179B4ED749E13618D8A18DB98991BA46B,
	Protocol18_DeserializeOperationRequest_m35D7A5186DC4D10D979CA3B29D299A1FCE372EC0,
	Protocol18_DeserializeOperationResponse_m40299B281C32BD65340D38A84BEEA6D2CF6804B6,
	Protocol18_ReadString_m7F0C36011DD5A3998323F631D9B59371F123C6D5,
	Protocol18_ReadCustomTypeArray_mE488D22819AF87A2AF128B195814BC1FD2AFD08D,
	Protocol18_ReadDictionaryType_mEEB530A532207371C9D429D1888A549F1E18B816,
	Protocol18_ReadDictionaryType_m825B743097E20ED52ED718F84600133AC3DA7A78,
	Protocol18_GetDictArrayType_m2EC2C1AA54111F9AE92918DB55AAA57A73ABB3FB,
	Protocol18_ReadDictionary_m7035D57890135D288F57A1EAA8B6778CF7D8366F,
	Protocol18_ReadDictionaryElements_mA3AA7CA6E4975D0D4F5475F6729522F29D50EC44,
	Protocol18_ReadObjectArray_m0FCE00465BD7902FEE21055092D85F9C01154CA6,
	Protocol18_ReadBooleanArray_m77C3BA7465BF53BE0EEC32001CD143D10E7DF214,
	Protocol18_ReadInt16Array_m72E13B2F035ECF8F24E6120B74ADE3885F383283,
	Protocol18_ReadSingleArray_mE74F022589D56D6EB408F956ADCA857B380EBDEC,
	Protocol18_ReadDoubleArray_m7C9B2FF1033B2AAED0106F46C9B7F02A1D4F76EF,
	Protocol18_ReadStringArray_mFF9000AEF1C9CE9CCF30199D4BA049442B7BC3CF,
	Protocol18_ReadHashtableArray_m72A20B1A359271EFBD9D908090CB37B251212B8F,
	Protocol18_ReadDictionaryArray_m8E126F56E19BB8F278CF867680B5BF081C420E60,
	Protocol18_ReadArrayInArray_mCE6FFC3C75C6E3DAD3B9569F8803B546D0729711,
	Protocol18_ReadInt1_m13981CEAC26C36F639A35B11EDDF4220F38D7CF9,
	Protocol18_ReadInt2_m52273E733DFD2EAC7E6AE0DCEEA13FA2E065D547,
	Protocol18_ReadCompressedInt32_mA2A4E657653C85751A7CD46645F361C8DD1C3702,
	Protocol18_ReadCompressedUInt32_m7EB98434941C130BEFB21776EAD9DF3308231A1E,
	Protocol18_ReadCompressedInt64_m6D036EAD500BB6565BF57A4163F15395BFFF64E9,
	Protocol18_ReadCompressedUInt64_mD033F7B9255D15C910954048D1B31134855FAAC3,
	Protocol18_ReadCompressedInt32Array_m2889BEF8151DEE6031A93366886B861FB7BF99B7,
	Protocol18_ReadCompressedInt64Array_m2BE6D697330767C0D15D548B77729535BB9A71E8,
	Protocol18_DecodeZigZag32_m44762B11F5C71392DCE0EEAEFFA44EA9DCF1AD3F,
	Protocol18_DecodeZigZag64_m7E74E517D455D04168977EB4AF6C728B90E9829C,
	Protocol18_Write_mAB8D7EDA5931D939D8BFC5BE64E7FB3FDF96D266,
	Protocol18_Write_m270019BD722DD2618EC628D088C51452F8E9C464,
	Protocol18_SerializeEventData_m1FA5B22295AAE71F96F35D7248C10E955779C211,
	Protocol18_WriteParameterTable_m0AD5BA217308208F76544843A720219B3AD2A094,
	Protocol18_SerializeOperationRequest_mB20D17F11A2E73BC747D5DD0D24D90CCD13B8D88,
	Protocol18_SerializeOperationRequest_mE5EB11A120A4ED311130D51FE9220EC9FAA07A46,
	Protocol18_SerializeOperationResponse_mE98959AF79C5261C772D1566958845247C1C2C6A,
	Protocol18_WriteByte_m6A9777EEA4FF2A02F2D2358663DD5EDDAF9384B7,
	Protocol18_WriteBoolean_mDB0B42AE178EB6025F114B3842BF578389C7692C,
	Protocol18_WriteUShort_mACEC4768D9301159010DF062BFB9FACCB4B4FF22,
	Protocol18_WriteInt16_m4DE8FA74E9395BCAA36AE229466CDB9D76AFCA2A,
	Protocol18_WriteDouble_m167E263BAAC5586A2531A02123B17522CA313D92,
	Protocol18_WriteSingle_mD086CEB523C5B9058183842AF1C23568BA0ACCFD,
	Protocol18_WriteString_m63E833F200FAB7E7433CAB78252E8856D2D170E6,
	Protocol18_WriteHashtable_mEC42D1BEB630C6322C08DA891D8415745AD7115E,
	Protocol18_WriteByteArray_mFFF2AE67AF76DF387CAD5386320CF91B72A1859E,
	Protocol18_WriteByteArraySegment_m9B4D564E12703B70D0DC03BD639C90D222FA8ABD,
	Protocol18_WriteInt32ArrayCompressed_mBB0FEE3782FC3E9EC7626D29CC108BF1E8583147,
	Protocol18_WriteInt64ArrayCompressed_m8E930EB5AB006314458230E301A27E50CBBFA5C4,
	Protocol18_WriteBoolArray_m29018D85EEDCE7418874E1A660F262D394689E08,
	Protocol18_WriteInt16Array_mB172D4EF1B33C7A56197658687A2259BA21E7D94,
	Protocol18_WriteSingleArray_m152F41A02966104DC368CA071E5D1F409212FF2A,
	Protocol18_WriteDoubleArray_mC31A8B9F35D97704AE34073A2CE773A977F37A46,
	Protocol18_WriteStringArray_mF667D9786577566F8CAD0C5A6761F94F6BF440E7,
	Protocol18_WriteObjectArray_mC540DC60245B1E5E3A226BC69F3344C3817F3642,
	Protocol18_WriteArrayInArray_mE24870B9DF61820A9763B99CDD6BD280B916F513,
	Protocol18_WriteCustomTypeBody_m02459C82D73BFC9737B3BE91C35D3A7E3970899A,
	Protocol18_WriteCustomType_m9AD4C5D501DD2A51B7CD8C0493B6B3E08A998223,
	Protocol18_WriteCustomTypeArray_mD438CE38C2B2243ECA5B124D3C670D74EACD09DC,
	Protocol18_WriteArrayHeader_m165E8EDAF662B62143C0BEEBED9B939CE626A735,
	Protocol18_WriteDictionaryElements_m756A188D80214363D40AABFEC623C9254981F406,
	Protocol18_WriteDictionary_m5D2C9A24E68406BC7CB202F6628D747E18C54376,
	Protocol18_WriteDictionaryHeader_mF5510616F59EA19A56EAB3BC001FF864779B46CF,
	Protocol18_WriteArrayType_mDB9DADAA2395178F22492CA1AE1B4F026A2B8232,
	Protocol18_WriteHashtableArray_m503694D98DED09E7308777A668EF88298D7F5B3B,
	Protocol18_WriteDictionaryArray_mE328367A3028BC358CE5AE1A479E13F55B4E7159,
	Protocol18_WriteIntLength_mDD200FB74D5ECD5A5F9B84E8A72D7D3DD130F6A8,
	Protocol18_WriteCompressedInt32_m925C6263E481768696441FD66B5FBD38E205D0D5,
	Protocol18_WriteCompressedInt64_m615C6B98496D9DB780A46E02EFD7254E57DCC48A,
	Protocol18_WriteCompressedUInt32_m4C6E2C103DB47B3F858AF1E3F9794347CE428DC8,
	Protocol18_WriteCompressedUInt32_m140A4F86F440339F65D60D63EE564606830832B8,
	Protocol18_WriteCompressedUInt64_m683653B6E1B78041AACDDC385CF7D61463A834FF,
	Protocol18_EncodeZigZag32_m367446287361EA0DEF7BF402A7A1B52DCA774002,
	Protocol18_EncodeZigZag64_m28BB0A4F75253DC03DC44737931AE4C9ED4462A2,
	Protocol18__ctor_m7B63A8E8A7BD6DB927516889B9B53E1283951D6E,
	Protocol18__cctor_m521D7C7CF85014632615B99CEB7CAD64542DE541,
	IPhotonSocket_get_Listener_m0551A21F4B0740E1F6AB78E323108FEF5EB64555,
	IPhotonSocket_get_MTU_mD1EE1B2381D26FAF4F77331FF7F0265CF849880D,
	IPhotonSocket_get_State_mE53E514522E11EB4EB724897E34BD331EC9AFC48,
	IPhotonSocket_set_State_mC8D290806513F5EA139B22D299985D8C3C472B0D,
	IPhotonSocket_get_Connected_m4D00A7E518BBFB6FBE49A2F0A26819246E686BE7,
	IPhotonSocket_get_ServerAddress_m507F4853E9D31EF130D9A6FA92D6E08FD31C1468,
	IPhotonSocket_set_ServerAddress_m54A239F3BCC438B9EA4D618C8EFCDD9E4D176A5B,
	IPhotonSocket_get_ServerIpAddress_m4211E8F1F2397F4A6A115D1CEDB7AF16932AA77B,
	IPhotonSocket_set_ServerIpAddress_mC2CDC34DF19E06B745CB7580E873717E9E367550,
	IPhotonSocket_get_ServerPort_m288E05494A4C23632A5F0709E904B9400151C969,
	IPhotonSocket_set_ServerPort_m52E503F3F6E34A75BE04D06917CBE99ABAA6AED4,
	IPhotonSocket_get_AddressResolvedAsIpv6_m95FE13CA7CEC68DCB7C0070B5551A5D647B5556C,
	IPhotonSocket_set_AddressResolvedAsIpv6_mD221497B6749904CE2FFD2B9F2A5E95289BE569F,
	IPhotonSocket_set_UrlProtocol_m0F7099863EB5593D791E901CE4C4B767708CC74C,
	IPhotonSocket_set_UrlPath_m87B9DD2CA3ABC7AA56AA42303F4195219BA03CD0,
	IPhotonSocket__ctor_m41CB988C577239E38F8D9B92D133858599D197CA,
	IPhotonSocket_Connect_m7A0B1483190F73A96078EB0CC21F4996B0DDE4A5,
	NULL,
	NULL,
	NULL,
	IPhotonSocket_HandleReceivedDatagram_m2E5FBFCD72BC93FF39AF51DB6BA5E47BBE37EA87,
	IPhotonSocket_ReportDebugOfLevel_m10DA4BA8075E690C233590B19905FB19840362DB,
	IPhotonSocket_EnqueueDebugReturn_m8E7061C7152912F0D38AFFCF341576C6E13D2D94,
	IPhotonSocket_HandleException_m596BF9AC251A53883D136C3DEBD55036C35EB894,
	IPhotonSocket_TryParseAddress_mE94333A1CD82A5303E69D4369639692FEE64ABA1,
	IPhotonSocket_IsIpv6SimpleCheck_mDEB4F524082496210C6E222B42254B9270A775E5,
	IPhotonSocket_GetIpAddress_m7BC0DFF03AEBA416A3E63942988FF58A0025DE89,
	IPhotonSocket_U3CHandleExceptionU3Eb__47_0_mE715582B365C7183682882E38E4A9D7FB0E7A098,
	SocketUdp__ctor_mC6E7F786B2D9168A3033674FFDA5B53DB507E242,
	SocketUdp_Dispose_mAB4C7332A466190BF0E52C7AD85156764097994E,
	SocketUdp_Connect_m0E22325D01F5D96DE8F314AAC4061DD9BAA9AF83,
	SocketUdp_Disconnect_m70104096544AD84A502C033CC1F68780E2298B12,
	SocketUdp_Send_mE44D3595EDFEFA6D5673C58A932820E0AAF8DD7F,
	SocketUdp_Receive_mE4ABCEDABFE0C3B530DE0A09C7E2A85A7619E4C8,
	SocketUdp_DnsAndConnect_m58AF55C58CDD073F97AC337FE9A52A219C397655,
	SocketUdp_ReceiveLoop_mBA78CA0669E02422696BD04DAC3753E0ABB8B2BE,
	SocketTcp__ctor_m072CB58C8C8A27CDEC64C9ADBC76A8EEB9D37B3C,
	SocketTcp_Dispose_m9F39A2738802D67590F659AED6DDE4BCE13ABB5A,
	SocketTcp_Connect_mDBB2AE2B3D07D8EACD3BA124873355B61ECDF1A9,
	SocketTcp_Disconnect_mA79E879DD7A4F97F035B7DF163AA28DAF0DE9D4D,
	SocketTcp_Send_m1025822705450801D5902A99A1DDD1E1685285C0,
	SocketTcp_Receive_m31272566160E8EC550BC44B49A6A83D7FEF93A26,
	SocketTcp_DnsAndConnect_m88FB14221EBA637B2CFD8FD7145DBD20BCD7DAAF,
	SocketTcp_ReceiveLoop_m4D6F93CD01DDEE04CC38FC4647C0E55827A09005,
	SimulationItem__ctor_m58E35B4B03E7BFA3529806737DBB2ABCA4B11811,
	SimulationItem_get_Delay_m59BC993F4707DE4FEE34ABE28078ED6D7D3485D0,
	SimulationItem_set_Delay_m20B0D990E14FF816B48DF43B0027276B6B57C462,
	NetworkSimulationSet_get_IsSimulationEnabled_m6C64C0079C4D249DA419DDDECC647DA1DCF58396,
	NetworkSimulationSet_set_IsSimulationEnabled_m0797432DE71AB180AD997EE6CF8837A3F8A97B75,
	NetworkSimulationSet_get_OutgoingLag_mEC03E58F5F36A5BD7E7EB97D96D7C9B8FB8B4C2B,
	NetworkSimulationSet_set_OutgoingLag_m74D50D732A6A3327B08E0BD15585EFEC3526D19B,
	NetworkSimulationSet_get_OutgoingJitter_m911D4DF9D50DC91A3564ABF71114911A159661AF,
	NetworkSimulationSet_set_OutgoingJitter_m7134006F8500D0CD82AFC7A2E19FB6279B956566,
	NetworkSimulationSet_get_OutgoingLossPercentage_m92D5286B157CEE9523AC43B5B9F14530B8E042CC,
	NetworkSimulationSet_set_OutgoingLossPercentage_m92B98F56F839CD7100586B8CCDF678FF48D3272D,
	NetworkSimulationSet_get_IncomingLag_m24A45732B49CC9274E5147B6D530C0FBE5DEAE08,
	NetworkSimulationSet_set_IncomingLag_m9FEF65615A8643342F7D49F4A28D7A93F1D7521A,
	NetworkSimulationSet_get_IncomingJitter_mFEBF3B0154008817357F79A0506298A47A83F3B5,
	NetworkSimulationSet_set_IncomingJitter_m83CFA57DE61E150697028FF81091578C4F678F72,
	NetworkSimulationSet_get_IncomingLossPercentage_mD934819F1A37C9F7C0C058209260C061881CB3A8,
	NetworkSimulationSet_set_IncomingLossPercentage_m14051A5B7A905F1950DF6634A8B4C17D88CFB0D8,
	NetworkSimulationSet_get_LostPackagesOut_mE3DDEBFC7048D96AA247E606796D7F72260020DB,
	NetworkSimulationSet_set_LostPackagesOut_m71EE8A4CDA0C6124FE4FD7CBF9DE365AAB5D2B3A,
	NetworkSimulationSet_get_LostPackagesIn_m0FAF896285859C4CAB2375154FC84AC0F093656D,
	NetworkSimulationSet_set_LostPackagesIn_m99DC1E0A32C93D557C89F82041F488912B26E5BA,
	NetworkSimulationSet_ToString_mE4B93C12D944E31ECA7F5C3BD58A12EB893C9840,
	NetworkSimulationSet__ctor_m7F0708A5CD215C458EB02B41FF06C1A279D4B31C,
	TrafficStatsGameLevel_get_OperationByteCount_m170EEE0C86D9F8D3077F345C3963B3B2D5463D9B,
	TrafficStatsGameLevel_set_OperationByteCount_mDCFE744B1963ECE8C13771187BDD0E59E8106577,
	TrafficStatsGameLevel_get_OperationCount_m111B512B8F9D37E86229E49D054D226B311BC98E,
	TrafficStatsGameLevel_set_OperationCount_mE2D4B6DC1E125189C8F76E11C833120D347BF61A,
	TrafficStatsGameLevel_get_ResultByteCount_m80BFDD85B5B5734BF4D8573CF648F94B11AE71D8,
	TrafficStatsGameLevel_set_ResultByteCount_m5446B07E0D0A379D95DCBE8B290372E0A463A5AA,
	TrafficStatsGameLevel_get_ResultCount_mD7E8060E13143225C362829967F71F8CC2174B61,
	TrafficStatsGameLevel_set_ResultCount_m3CC9E5DD3CE24616FEDA6A30288A4134347A097D,
	TrafficStatsGameLevel_get_EventByteCount_mA6C273E2399F50C2B4774C60603488AEAC8B3D2A,
	TrafficStatsGameLevel_set_EventByteCount_m54ABC33D5E6485A3B70B04FF513DEB3817A84808,
	TrafficStatsGameLevel_get_EventCount_mB0CF01EF31EE5D769C581CA7DC48C03CEF0EF494,
	TrafficStatsGameLevel_set_EventCount_mCC6CDE66951D8A7B6AD979E3314563C16ADF3EA0,
	TrafficStatsGameLevel_get_LongestOpResponseCallback_mFD7D9B3E7CCB375132612A07FC2FDE9CD30CFCE8,
	TrafficStatsGameLevel_set_LongestOpResponseCallback_m135EAFC22AE1FA5C08082C511F2CD820E737B911,
	TrafficStatsGameLevel_get_LongestOpResponseCallbackOpCode_m6992DB8B32A62EF34E79A89F918E0B631A9C371C,
	TrafficStatsGameLevel_set_LongestOpResponseCallbackOpCode_m3392D9077C18542A7957840E6D060A5DAF89DE81,
	TrafficStatsGameLevel_get_LongestEventCallback_m99722D0FC45EA8F948FF7ECAC0BA50B463B1649F,
	TrafficStatsGameLevel_set_LongestEventCallback_mADA5C8191F23F0E0753FAAC39193AF09C1E95A4E,
	TrafficStatsGameLevel_get_LongestMessageCallback_m9BFFA5257FDE0AAFFD08258FE67B064F2B57AA31,
	TrafficStatsGameLevel_set_LongestMessageCallback_mDCB4AE22FFF20BA7BF5FB33C8DF6EFC02DD03C3D,
	TrafficStatsGameLevel_get_LongestRawMessageCallback_m82F479078957DF54089D3A92AAC1E755F2267098,
	TrafficStatsGameLevel_set_LongestRawMessageCallback_m6DD456707E367DEEB5D3435335DF7FEDEF860C2A,
	TrafficStatsGameLevel_get_LongestEventCallbackCode_mB640866FE8237D2FFA6AAE2A833299D6A84AF5AB,
	TrafficStatsGameLevel_set_LongestEventCallbackCode_mAB95B63CD0F46ABECACD2B7923FBEC51E5C7F6A5,
	TrafficStatsGameLevel_get_LongestDeltaBetweenDispatching_m8E4CFD18DDF757847919E02CB719EE276D7F574B,
	TrafficStatsGameLevel_set_LongestDeltaBetweenDispatching_m25E2F5C2EE0664C0C27FC5FBB490F39DF7115CC6,
	TrafficStatsGameLevel_get_LongestDeltaBetweenSending_m0D602DB49980F5D7781AC41C4649DD53EF19CB5D,
	TrafficStatsGameLevel_set_LongestDeltaBetweenSending_m6725D7C5370BE640674DA8C302E17E268C30CC69,
	TrafficStatsGameLevel_get_DispatchIncomingCommandsCalls_m237506964895E1336089714DEE5525929A3DD193,
	TrafficStatsGameLevel_set_DispatchIncomingCommandsCalls_mBB41143F95D0719346B7502DE3211EA49C64156F,
	TrafficStatsGameLevel_get_SendOutgoingCommandsCalls_mACEC0B693CAD54B988E69B1165D57FA4D58C0594,
	TrafficStatsGameLevel_set_SendOutgoingCommandsCalls_m317DDA51ECB656F599A8AAFFBB121040216D1B7A,
	TrafficStatsGameLevel_get_TotalMessageCount_m5FF0BAB29088E21FC4E7BABFA8FF0AC332C170FC,
	TrafficStatsGameLevel_get_TotalIncomingMessageCount_m185188AEF7AFAF383F0D3EA889D318685D5011D7,
	TrafficStatsGameLevel_get_TotalOutgoingMessageCount_m9FA83A501E753F7753AF6873CEA2A783EAA39E0A,
	TrafficStatsGameLevel_CountOperation_m334DA7808D71BC2A60C5B3589DAB4275CFC943CD,
	TrafficStatsGameLevel_CountResult_m4B8F689C560DB680108119E069991A7AFF13C773,
	TrafficStatsGameLevel_CountEvent_mFB5294614379B2E382FC543937B98B5E287A4A26,
	TrafficStatsGameLevel_TimeForResponseCallback_mD482EAE164E50DDC78BAEBE1F583648988097641,
	TrafficStatsGameLevel_TimeForEventCallback_mA0287F67E9997DFD264012CF9ED9B7AD6BBB36A7,
	TrafficStatsGameLevel_TimeForMessageCallback_m4A38512E991BAC5D2A36BC7593754EF057EDC8F6,
	TrafficStatsGameLevel_TimeForRawMessageCallback_mC536643CA42341E390E7967FE45E6642E69B1006,
	TrafficStatsGameLevel_DispatchIncomingCommandsCalled_mA26A91F3B6EB06CF7E4D9418F9AD4AF25B9D115D,
	TrafficStatsGameLevel_SendOutgoingCommandsCalled_m4B8BBF7D68D579038264B3C920AD8C5E363B22E7,
	TrafficStatsGameLevel_ToString_mB818F33F565AF3FB170F863254F769D0D49E59F5,
	TrafficStatsGameLevel_ToStringVitalStats_m3543515813E6D397B38B6D161329AC93476D2362,
	TrafficStatsGameLevel__ctor_mE2C7C201AF77EE7E14623CD2DE84E8AB089D791A,
	TrafficStats_get_PackageHeaderSize_m2904FE9BCAAB75D3B06327769E9E3679B6BCFEF1,
	TrafficStats_set_PackageHeaderSize_mD308E175248F5D539BF224F334E2822DE78D22AA,
	TrafficStats_get_ReliableCommandCount_mCC25CD81366BFD75421B98CF37E98F66F274A708,
	TrafficStats_set_ReliableCommandCount_mD44C41FFC98CCDD17F1A84B3D55439E93211D594,
	TrafficStats_get_UnreliableCommandCount_mC32A036F1E24D6D88598B9A00046C4CCB60F5A02,
	TrafficStats_set_UnreliableCommandCount_m6E0057BF8B7A9CBD0E8644EDEBD9CA7C170AC033,
	TrafficStats_get_FragmentCommandCount_m4D109A149ED7A5665C127A5B3AE389E73F618195,
	TrafficStats_set_FragmentCommandCount_m60D391BC11A531E1326C65D17937FE1C648AE5E5,
	TrafficStats_get_ControlCommandCount_m550CF80E96E08A8575C046871AA83005ED75DBF0,
	TrafficStats_set_ControlCommandCount_mF0C61DA8296BF21916AE1706AAFBE08E796F08DA,
	TrafficStats_get_TotalPacketCount_m951DFC6DECA478141523582C0970C66D56D1AA04,
	TrafficStats_set_TotalPacketCount_m65787539C6510BB636947E59D6C25834F719FBDF,
	TrafficStats_get_TotalCommandsInPackets_m3EEB8F5479A923CFAD2B430B38A61CF9A1EFE51D,
	TrafficStats_set_TotalCommandsInPackets_mCB4D586FED2D6B67161654765053D7FF092A5F98,
	TrafficStats_get_ReliableCommandBytes_m97D380DE601854ECB562289F0A611F561B779A24,
	TrafficStats_set_ReliableCommandBytes_m62E82B66FC499E6F7AA121D6F0D5224D1CF3CBA4,
	TrafficStats_get_UnreliableCommandBytes_mBF3066840BAF02EAB6FC4C413F16532128AF8189,
	TrafficStats_set_UnreliableCommandBytes_m2CC9814E62A542709541DA693B96035C657CF2AC,
	TrafficStats_get_FragmentCommandBytes_mFCF1476158DB9D054E5F9751B6C3151B1B098760,
	TrafficStats_set_FragmentCommandBytes_m786B9BD978F6289B37EC34CB6D9F08CCA0CB3A73,
	TrafficStats_get_ControlCommandBytes_m55CD18E8C7771E41CBBE66335A083285CD33884B,
	TrafficStats_set_ControlCommandBytes_m1E12AD93941B98D390902BC3A2308DE2B5B94D66,
	TrafficStats__ctor_m3DDBAE04D292FD05AEE437A8ED3233988C2549CD,
	TrafficStats_get_TotalCommandBytes_mDAF5285EA29C6FF71D7E446981F29D7A57A50159,
	TrafficStats_get_TotalPacketBytes_m2FC95176E72723D1222DB4226EF33C2037D51FBA,
	TrafficStats_set_TimestampOfLastAck_m547136B5AA1372A71A8E7E21972611E47225E984,
	TrafficStats_set_TimestampOfLastReliableCommand_mD3FA1EE8CB013F6AA66D0A47FB1649A7E0356097,
	TrafficStats_CountControlCommand_mB6419EA4DD5C2B64DB2185E0E4ECD01550E9301A,
	TrafficStats_CountReliableOpCommand_m9C16ED50D7BEC14B81B7ACC50A23368EF22F1296,
	TrafficStats_CountUnreliableOpCommand_mE684E7F3BFC63F8628CD568BD3797ABCD271D513,
	TrafficStats_CountFragmentOpCommand_mD8F18787B770D60146236C24FB4EEDED1247D2FB,
	TrafficStats_ToString_m5F6638AE5C823EA98AFD80A69FD8F20AA0956FAE,
	CryptoBase_Finalize_m2886DF6631789C681ABB0C1BE50ABE328CEE8252,
	CryptoBase_Init_m00ECAC0A32BD218ADFE26E89615678838F33BBE5,
	CryptoBase_Dispose_m24E8825C1CA3A3D1B222AD1A4EDF9D7525A0DD02,
	CryptoBase_Dispose_mE0DC43DBF4EC4DE744B754B7B70514B165117338,
	CryptoBase__ctor_mB1A24B5AF758B62B06D03E8103F535A3874AB2A1,
	Encryptor_Encrypt_m2FAEBFC218959D447959BF7EC8C089BF9DFFC71B,
	Encryptor_FinishHMAC_m3FABB4FADB652129D05213F36E3E20829132D6CC,
	Encryptor__ctor_m78FB7189B1CC3D520CCA2AA2FD65B1A5BF3A1CB5,
	Encryptor__cctor_m1B3DC25890306F5E12EA5493C12D3F5F8936A389,
	Decryptor_DecryptBufferWithIV_mAB0D42664EA4B598EB7E888C2200FD29B886BEA1,
	Decryptor_CheckHMAC_mB49176DFE57C07E3329385879BCA379EA5B2FA4A,
	Decryptor__ctor_m41D95AB18982C2AEEC8CA44840127DA593253A2A,
};
extern void SendOptions_set_Reliability_mD444223906FA26A1589B9EECB772CA98CA66E164_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[1] = 
{
	{ 0x060000FA, SendOptions_set_Reliability_mD444223906FA26A1589B9EECB772CA98CA66E164_AdjustorThunk },
};
static const int32_t s_InvokerIndices[691] = 
{
	2828,
	1758,
	1522,
	523,
	523,
	1791,
	1522,
	1758,
	1522,
	523,
	523,
	1791,
	1541,
	1758,
	1238,
	1235,
	2828,
	1791,
	1514,
	1522,
	1522,
	1522,
	2713,
	2712,
	2481,
	2481,
	2481,
	2477,
	2439,
	2439,
	2715,
	2533,
	1361,
	1747,
	2533,
	2533,
	2533,
	2197,
	2197,
	2481,
	2481,
	1758,
	1235,
	758,
	527,
	2712,
	918,
	1747,
	1758,
	2828,
	2828,
	1791,
	1513,
	1238,
	972,
	1758,
	1758,
	1522,
	1778,
	1791,
	1758,
	1739,
	1791,
	2481,
	2808,
	2321,
	2321,
	2821,
	2605,
	2776,
	2715,
	2482,
	2715,
	2712,
	2439,
	2828,
	971,
	1747,
	758,
	1175,
	2808,
	2828,
	1791,
	1791,
	2828,
	1791,
	1747,
	984,
	1522,
	1513,
	1522,
	1513,
	1522,
	1758,
	1758,
	1791,
	1758,
	743,
	1747,
	1747,
	1513,
	732,
	1514,
	1513,
	489,
	642,
	1778,
	1541,
	985,
	1350,
	1778,
	1758,
	1747,
	1513,
	1758,
	1522,
	1758,
	1522,
	1778,
	1778,
	1778,
	1541,
	1778,
	1758,
	1778,
	1541,
	1747,
	1747,
	1747,
	1522,
	1747,
	1747,
	1747,
	1747,
	1747,
	1747,
	1758,
	1758,
	1778,
	1778,
	1541,
	1778,
	1541,
	1758,
	1747,
	1778,
	1778,
	1541,
	1758,
	1522,
	1758,
	1522,
	1758,
	1522,
	1748,
	1778,
	1541,
	1791,
	1791,
	1239,
	1541,
	975,
	800,
	587,
	1791,
	1791,
	1791,
	1791,
	1778,
	588,
	1522,
	1791,
	1778,
	1778,
	1778,
	597,
	2147,
	2828,
	1778,
	2828,
	1758,
	1758,
	1522,
	1758,
	1778,
	1747,
	1747,
	1778,
	1758,
	1747,
	1778,
	1747,
	1778,
	1791,
	2813,
	2776,
	1791,
	587,
	1238,
	527,
	527,
	1791,
	1791,
	1791,
	1791,
	1791,
	377,
	341,
	1778,
	1778,
	969,
	1778,
	1361,
	1513,
	1361,
	1522,
	1522,
	1522,
	984,
	1513,
	1758,
	1522,
	1522,
	1791,
	1778,
	1758,
	1758,
	1758,
	1747,
	1791,
	1791,
	2828,
	971,
	1791,
	758,
	1522,
	1791,
	1791,
	1791,
	1791,
	1791,
	413,
	1758,
	134,
	1758,
	413,
	1758,
	224,
	1758,
	1541,
	2828,
	1791,
	1791,
	1791,
	587,
	1791,
	1791,
	1791,
	1791,
	1778,
	1747,
	1747,
	1747,
	1778,
	1778,
	1778,
	377,
	1239,
	596,
	341,
	1747,
	1175,
	969,
	969,
	969,
	1522,
	1522,
	1522,
	969,
	969,
	1361,
	1361,
	511,
	2828,
	1791,
	983,
	1350,
	1350,
	1235,
	574,
	928,
	1791,
	1361,
	1747,
	1778,
	1778,
	443,
	2188,
	650,
	963,
	1758,
	1791,
	1175,
	1758,
	1791,
	1791,
	587,
	1791,
	1791,
	1791,
	1791,
	1522,
	1778,
	1778,
	1778,
	377,
	341,
	572,
	1791,
	969,
	969,
	1522,
	1522,
	2828,
	2712,
	1758,
	1758,
	654,
	640,
	654,
	654,
	443,
	654,
	759,
	1104,
	1361,
	758,
	1238,
	1238,
	1238,
	1238,
	1791,
	1791,
	1239,
	1758,
	1758,
	1791,
	1239,
	1747,
	1758,
	1791,
	1758,
	1791,
	971,
	1238,
	527,
	1238,
	971,
	698,
	327,
	1104,
	971,
	1238,
	527,
	1238,
	971,
	756,
	313,
	1238,
	442,
	2147,
	2363,
	2368,
	2398,
	2360,
	2360,
	2360,
	2828,
	1758,
	1758,
	800,
	759,
	1239,
	1361,
	761,
	654,
	443,
	1238,
	654,
	1238,
	654,
	758,
	972,
	1238,
	654,
	662,
	662,
	640,
	645,
	648,
	664,
	638,
	654,
	654,
	654,
	232,
	654,
	654,
	654,
	654,
	972,
	424,
	439,
	759,
	1361,
	1361,
	1104,
	1175,
	1213,
	1415,
	1097,
	1238,
	1238,
	757,
	757,
	1238,
	1238,
	1238,
	1238,
	579,
	517,
	1791,
	2828,
	1522,
	1758,
	1758,
	654,
	640,
	654,
	759,
	1104,
	1361,
	2720,
	2720,
	1361,
	1350,
	1238,
	759,
	1361,
	1361,
	1104,
	1104,
	1415,
	1097,
	1238,
	759,
	758,
	758,
	1238,
	1238,
	1238,
	1238,
	1238,
	517,
	1238,
	1238,
	1238,
	376,
	1238,
	1238,
	1238,
	1238,
	1238,
	1238,
	1238,
	1238,
	1238,
	714,
	714,
	1175,
	1175,
	1213,
	1213,
	1238,
	1238,
	1167,
	1212,
	654,
	439,
	654,
	972,
	654,
	443,
	654,
	662,
	662,
	968,
	640,
	638,
	664,
	654,
	654,
	654,
	232,
	654,
	654,
	654,
	654,
	654,
	654,
	654,
	654,
	654,
	652,
	654,
	654,
	800,
	439,
	654,
	424,
	584,
	654,
	654,
	969,
	645,
	648,
	969,
	712,
	970,
	1167,
	1212,
	1791,
	2828,
	1758,
	1747,
	1747,
	1513,
	1778,
	1758,
	1522,
	2813,
	2776,
	1747,
	1513,
	1778,
	1541,
	1522,
	1522,
	1522,
	1778,
	1778,
	712,
	1147,
	645,
	1379,
	984,
	1513,
	199,
	1361,
	2715,
	1791,
	1522,
	1791,
	1778,
	1778,
	712,
	1147,
	1791,
	1791,
	1522,
	1791,
	1778,
	1778,
	712,
	1147,
	1791,
	1791,
	1791,
	1747,
	1513,
	1778,
	1541,
	1747,
	1513,
	1747,
	1513,
	1747,
	1513,
	1747,
	1513,
	1747,
	1513,
	1747,
	1513,
	1747,
	1513,
	1747,
	1513,
	1758,
	1791,
	1747,
	1513,
	1747,
	1513,
	1747,
	1513,
	1747,
	1513,
	1747,
	1513,
	1747,
	1513,
	1747,
	1513,
	1778,
	1541,
	1747,
	1513,
	1747,
	1513,
	1747,
	1513,
	1778,
	1541,
	1747,
	1513,
	1747,
	1513,
	1747,
	1513,
	1747,
	1513,
	1747,
	1747,
	1747,
	1513,
	1513,
	1513,
	983,
	983,
	1513,
	1513,
	1791,
	1791,
	1758,
	1758,
	1791,
	1747,
	1513,
	1747,
	1513,
	1747,
	1513,
	1747,
	1513,
	1747,
	1513,
	1747,
	1513,
	1747,
	1513,
	1747,
	1513,
	1747,
	1513,
	1747,
	1513,
	1747,
	1513,
	1513,
	1747,
	1747,
	1513,
	1513,
	1513,
	1513,
	1513,
	1513,
	1758,
	1791,
	972,
	1791,
	1541,
	1791,
	416,
	523,
	1791,
	2828,
	314,
	798,
	1791,
};
extern const CustomAttributesCacheGenerator g_Photon3Unity3D_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Photon3Unity3D_CodeGenModule;
const Il2CppCodeGenModule g_Photon3Unity3D_CodeGenModule = 
{
	"Photon3Unity3D.dll",
	691,
	s_methodPointers,
	1,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_Photon3Unity3D_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
