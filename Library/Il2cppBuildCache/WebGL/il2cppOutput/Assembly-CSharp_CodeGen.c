﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void ChannelSelector::SetChannel(System.String)
extern void ChannelSelector_SetChannel_m73C936B325A97928B7A90D7C263923DB1608AD3E (void);
// 0x00000002 System.Void ChannelSelector::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void ChannelSelector_OnPointerClick_mF2CE1256B4C1587300BC65F68C57871E9D04EF5B (void);
// 0x00000003 System.Void ChannelSelector::.ctor()
extern void ChannelSelector__ctor_m955A69F0C09110A7184264FC4A400625C9C3712B (void);
// 0x00000004 System.Void ChatAppIdCheckerUI::Update()
extern void ChatAppIdCheckerUI_Update_m3E0F22639A6DC1534C8E37925CAD3F7F0C169A69 (void);
// 0x00000005 System.Void ChatAppIdCheckerUI::.ctor()
extern void ChatAppIdCheckerUI__ctor_m810602E0BA6ACAF276B84807C7531EDF0AA7895A (void);
// 0x00000006 System.String ChatGui::get_UserName()
extern void ChatGui_get_UserName_mFC72D99B3857A2A850E610182954EF21BFDD656B (void);
// 0x00000007 System.Void ChatGui::set_UserName(System.String)
extern void ChatGui_set_UserName_m2A5D15C5A2C57BF815E69DEBD12771BBF5F28F0E (void);
// 0x00000008 System.Void ChatGui::Start()
extern void ChatGui_Start_m39521BDC549E6122372E4BD4057244FE97C85F54 (void);
// 0x00000009 System.Void ChatGui::Connect()
extern void ChatGui_Connect_m085418B018B9DC3121B4B4F763086FE181F7CCF0 (void);
// 0x0000000A System.Void ChatGui::OnDestroy()
extern void ChatGui_OnDestroy_m374C72D03D08B9C92F382B4AC9B5C7ECFCB0E33E (void);
// 0x0000000B System.Void ChatGui::OnApplicationQuit()
extern void ChatGui_OnApplicationQuit_mF07E706B4A1A3DD845DDD336D2FD8D67556BDC66 (void);
// 0x0000000C System.Void ChatGui::Update()
extern void ChatGui_Update_mEE5D2E3175984AC39518A58153C7961BB30F90BD (void);
// 0x0000000D System.Void ChatGui::OnEnterSend()
extern void ChatGui_OnEnterSend_mC29FE6FC78C35108FD2E3D44A5FE158744F09EFB (void);
// 0x0000000E System.Void ChatGui::OnClickSend()
extern void ChatGui_OnClickSend_m2FA72879D41DD1187AA3ED5084D818ABFA2B6E49 (void);
// 0x0000000F System.Void ChatGui::SendChatMessage(System.String)
extern void ChatGui_SendChatMessage_m5CEA1D17144CE9652A8B65E8860D0DA2E369594A (void);
// 0x00000010 System.Void ChatGui::PostHelpToCurrentChannel()
extern void ChatGui_PostHelpToCurrentChannel_mFFA856F30EAABC8F6CC153BB4BB0B42760EB102D (void);
// 0x00000011 System.Void ChatGui::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String)
extern void ChatGui_DebugReturn_mCFBB07C217B3858CFC14884DF3AC55A8B022154A (void);
// 0x00000012 System.Void ChatGui::OnConnected()
extern void ChatGui_OnConnected_m3EAFCA54E75B6AB28C363E920AE048EF5DCB4521 (void);
// 0x00000013 System.Void ChatGui::OnDisconnected()
extern void ChatGui_OnDisconnected_m3003FC2C479F5537B1AA6F75747DA866D02B5532 (void);
// 0x00000014 System.Void ChatGui::OnChatStateChange(Photon.Chat.ChatState)
extern void ChatGui_OnChatStateChange_mBC049CFBD2000061938736E7CCE03C1C91C0CB63 (void);
// 0x00000015 System.Void ChatGui::OnSubscribed(System.String[],System.Boolean[])
extern void ChatGui_OnSubscribed_m20D1F15C70C822E6AB5560B8E5140F9BD88596F1 (void);
// 0x00000016 System.Void ChatGui::InstantiateChannelButton(System.String)
extern void ChatGui_InstantiateChannelButton_m3848B8D380461CD3C4DDF9A90FE3EDD2E332ACDB (void);
// 0x00000017 System.Void ChatGui::InstantiateFriendButton(System.String)
extern void ChatGui_InstantiateFriendButton_m7F07EDAD1DA6F0F76A44AFF743B7C4F459D9A849 (void);
// 0x00000018 System.Void ChatGui::OnUnsubscribed(System.String[])
extern void ChatGui_OnUnsubscribed_mCCCE787B3F97594074B62E14C601AFCAB61A47A2 (void);
// 0x00000019 System.Void ChatGui::OnGetMessages(System.String,System.String[],System.Object[])
extern void ChatGui_OnGetMessages_m399FFB8E1300C33A9EB374F9F2787374FEC80D1F (void);
// 0x0000001A System.Void ChatGui::OnPrivateMessage(System.String,System.Object,System.String)
extern void ChatGui_OnPrivateMessage_m7E046C5EE238E0B998565EB88D5EC7CB0B62D76F (void);
// 0x0000001B System.Void ChatGui::OnStatusUpdate(System.String,System.Int32,System.Boolean,System.Object)
extern void ChatGui_OnStatusUpdate_mD19AEAB3025B776571DC5DA3AE50FFFB1C383AF1 (void);
// 0x0000001C System.Void ChatGui::OnUserSubscribed(System.String,System.String)
extern void ChatGui_OnUserSubscribed_mE99340D2BD23C7D636899E650E54C37D0507DAD6 (void);
// 0x0000001D System.Void ChatGui::OnUserUnsubscribed(System.String,System.String)
extern void ChatGui_OnUserUnsubscribed_m2E16B304BDD24E9D2C8D1A7914AEED529FF6EE7F (void);
// 0x0000001E System.Void ChatGui::AddMessageToSelectedChannel(System.String)
extern void ChatGui_AddMessageToSelectedChannel_m171FB1F09B4D53912B39728479F91056E18323E7 (void);
// 0x0000001F System.Void ChatGui::ShowChannel(System.String)
extern void ChatGui_ShowChannel_m48BBE9EA44EC7992C42B584E7CD2E05352789551 (void);
// 0x00000020 System.Void ChatGui::OpenDashboard()
extern void ChatGui_OpenDashboard_m27812C12469AD5A0D2CA52D2B7E11593BCF9EC90 (void);
// 0x00000021 System.Void ChatGui::.ctor()
extern void ChatGui__ctor_mE9C452C98CEA2FA3D5007EAB2B295C57B4DCE205 (void);
// 0x00000022 System.Void ChatGui::.cctor()
extern void ChatGui__cctor_m7D892708067CB07DB58081A631DA9DFBAC7B3083 (void);
// 0x00000023 System.Void FriendItem::set_FriendId(System.String)
extern void FriendItem_set_FriendId_m8E9861AA8C28705ED7855791487BB3C012F60CBA (void);
// 0x00000024 System.String FriendItem::get_FriendId()
extern void FriendItem_get_FriendId_mEF088D10E35110124CC84308164FA4F5E5096DA3 (void);
// 0x00000025 System.Void FriendItem::Awake()
extern void FriendItem_Awake_m6CC1DB18190F7F5A79029BE4AB4F6F29F519BDA0 (void);
// 0x00000026 System.Void FriendItem::OnFriendStatusUpdate(System.Int32,System.Boolean,System.Object)
extern void FriendItem_OnFriendStatusUpdate_mCC056D85F3DD38A7C9869E7919F64729ED0551B3 (void);
// 0x00000027 System.Void FriendItem::.ctor()
extern void FriendItem__ctor_mB9DE36BE9496080F797F23BCC813AC66857692B0 (void);
// 0x00000028 System.Boolean IgnoreUiRaycastWhenInactive::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera)
extern void IgnoreUiRaycastWhenInactive_IsRaycastLocationValid_mE22106037E41083C28AE240A56C2571C1A98D531 (void);
// 0x00000029 System.Void IgnoreUiRaycastWhenInactive::.ctor()
extern void IgnoreUiRaycastWhenInactive__ctor_m23CB38907F86355773CE8F77CC5AA54CC5854BD7 (void);
// 0x0000002A System.Void NamePickGui::Start()
extern void NamePickGui_Start_m51D755E15E2717751111AB4747BE1116B6493729 (void);
// 0x0000002B System.Void NamePickGui::EndEditOnEnter()
extern void NamePickGui_EndEditOnEnter_m5FB829C5A26086E594AC805A82CD29B8F7C2D89E (void);
// 0x0000002C System.Void NamePickGui::StartChat()
extern void NamePickGui_StartChat_mE4FE987367755FCE2168B2883873504901E3319C (void);
// 0x0000002D System.Void NamePickGui::.ctor()
extern void NamePickGui__ctor_m40BA9A6C50F204FB90838B67DE6C01D345A5FAD6 (void);
// 0x0000002E System.Void Skidmarks::Start()
extern void Skidmarks_Start_m630AD543C6831297B563C7E0FCCF6E593B1E66E4 (void);
// 0x0000002F System.Void Skidmarks::LateUpdate()
extern void Skidmarks_LateUpdate_m243E9DE089D3D2FFEF63A8BDFD41CA21330B16FC (void);
// 0x00000030 System.Int32 Skidmarks::AddSkidMark(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern void Skidmarks_AddSkidMark_mA1475B4E1C10D429155C166434BF9D33B1B8C0A1 (void);
// 0x00000031 System.Void Skidmarks::UpdateSkidmarksMesh()
extern void Skidmarks_UpdateSkidmarksMesh_mC10BD9781B76400B84588F5F1E4878B6F8866CC8 (void);
// 0x00000032 System.Void Skidmarks::.ctor()
extern void Skidmarks__ctor_mCF8400BC9314E7EAD88F56AC072EB921C0DAB2DA (void);
// 0x00000033 System.Void Skidmarks/MarkSection::.ctor()
extern void MarkSection__ctor_mEA1F249CE4D680B005BD82AEEB94B031ECFA6F6A (void);
// 0x00000034 System.Void WheelSkid::Awake()
extern void WheelSkid_Awake_m954ABF30FE9A499C675621A338A62DB212F2829A (void);
// 0x00000035 System.Void WheelSkid::Start()
extern void WheelSkid_Start_m4CB4A41755192A3C08F381AF0E4DB7F1EC5F2254 (void);
// 0x00000036 System.Void WheelSkid::FixedUpdate()
extern void WheelSkid_FixedUpdate_mA87613D301950479E08C6B790B4CFD94C7130302 (void);
// 0x00000037 System.Void WheelSkid::LateUpdate()
extern void WheelSkid_LateUpdate_m20C03A54816EDFAB7E8747BF9ACDA92945D489C8 (void);
// 0x00000038 System.Void WheelSkid::.ctor()
extern void WheelSkid__ctor_m7C3F4F3C5F0F283C7BDEB4C956B68EBBFF588FD6 (void);
// 0x00000039 System.Void NetworkedPlayer::Awake()
extern void NetworkedPlayer_Awake_m1414180636F0B7DC12167279B4A57B5F2136A4F1 (void);
// 0x0000003A System.Void NetworkedPlayer::.ctor()
extern void NetworkedPlayer__ctor_m582B2562027C1619FEF73CA7FA69FEECF58B27D1 (void);
// 0x0000003B System.Void RightBtn::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void RightBtn_OnPointerDown_m6AC878546D248F9361160C2B865DAE5C297F9B66 (void);
// 0x0000003C System.Void RightBtn::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void RightBtn_OnPointerUp_mC9861A081B815E6E1CF4AD0215CF4B3CF6096BF1 (void);
// 0x0000003D System.Void RightBtn::.ctor()
extern void RightBtn__ctor_m672B36D6C85CB0278E67A1330E61F92F5388C6A0 (void);
// 0x0000003E System.Void RightBtn::.cctor()
extern void RightBtn__cctor_mD55EC86AB2BFAC9917A7A4C0D86DDE3C6F6EAF14 (void);
// 0x0000003F System.Void AIController::Start()
extern void AIController_Start_m4C6AC8E7E19CF8263AC914947D25676DA491B832 (void);
// 0x00000040 System.Void AIController::ProgressTracker()
extern void AIController_ProgressTracker_m77F0A24A605087102A956949FD63662E6EEE94BE (void);
// 0x00000041 System.Void AIController::ResetLayer()
extern void AIController_ResetLayer_m13254D2EC569FA1ABA0FA5308DF0489E11FB7679 (void);
// 0x00000042 System.Void AIController::Update()
extern void AIController_Update_mC1B4E8DE676F4EDFD8E3296F5AE6EB8C7D6E73F7 (void);
// 0x00000043 System.Void AIController::.ctor()
extern void AIController__ctor_m28A6370E8C2F1F7BB0C5D75702AE025E843F28A5 (void);
// 0x00000044 System.Void AltAIController::Start()
extern void AltAIController_Start_m94EED304C0D89DECCAB2C4599CA5CBD8107D23FA (void);
// 0x00000045 System.Void AltAIController::Update()
extern void AltAIController_Update_m7DE56AB527F4FA0ED29461988B05DBFE047D6F76 (void);
// 0x00000046 System.Void AltAIController::.ctor()
extern void AltAIController__ctor_mE1176095003853F4B9400976B9B02E906CB2A757 (void);
// 0x00000047 System.Void AntiRolllBar::Start()
extern void AntiRolllBar_Start_mA5FF49C82425F3EE568125D1128874A20ED83514 (void);
// 0x00000048 System.Void AntiRolllBar::GroundWheels(UnityEngine.WheelCollider,UnityEngine.WheelCollider)
extern void AntiRolllBar_GroundWheels_m6A37DAB8017DA3B204FCB633AE3F5FCB2D89FCE3 (void);
// 0x00000049 System.Void AntiRolllBar::FixedUpdate()
extern void AntiRolllBar_FixedUpdate_mEB6D89EC34459019AC90F624F587F65CD0FDE8B5 (void);
// 0x0000004A System.Void AntiRolllBar::.ctor()
extern void AntiRolllBar__ctor_m5D2FF8CEF6C2C891A9A12870BC8ADE061B9841F2 (void);
// 0x0000004B System.Void AvoidDetector::OnCollisionExit(UnityEngine.Collision)
extern void AvoidDetector_OnCollisionExit_m7C453B7E42B0A2337887E716133E1382214184B4 (void);
// 0x0000004C System.Void AvoidDetector::OnCollisionStay(UnityEngine.Collision)
extern void AvoidDetector_OnCollisionStay_m4EC78296C373AA603E976F205D45AAF2317663C6 (void);
// 0x0000004D System.Void AvoidDetector::.ctor()
extern void AvoidDetector__ctor_m9DF63910A415FFB8A2B0F3B16918E47D7AC14EDC (void);
// 0x0000004E System.Void CameraTrack::Update()
extern void CameraTrack_Update_m66A257B7FF4E3F3FC96EE1F8C733F3F795F75CDC (void);
// 0x0000004F System.Void CameraTrack::.ctor()
extern void CameraTrack__ctor_mF77D9D06785445AD0DCFA3522D23DC85F00D13BA (void);
// 0x00000050 System.Void CheckPointCreator::Start()
extern void CheckPointCreator_Start_m0F5629A0157636B65EFE4F47B2884BBB6AC34CAB (void);
// 0x00000051 System.Void CheckPointCreator::CreateCheckpoints()
extern void CheckPointCreator_CreateCheckpoints_mB2A5AA69625956620D97842B6ADDE4D109F51BB2 (void);
// 0x00000052 System.Void CheckPointCreator::PlaceCheckPoint()
extern void CheckPointCreator_PlaceCheckPoint_m0CCE80450810B726CE8C05B833FBC44E3FE36256 (void);
// 0x00000053 System.Void CheckPointCreator::Update()
extern void CheckPointCreator_Update_mEB8480AC814D854E4BB002A5BD7E05A6E46038CC (void);
// 0x00000054 System.Void CheckPointCreator::.ctor()
extern void CheckPointCreator__ctor_mAD6A603105022A2B97A5B127BFCFB36DFC28613C (void);
// 0x00000055 System.Void CheckpointManager::Start()
extern void CheckpointManager_Start_mDC1F849F88DF9D1B14E0EB8E1F76EA9C3AA7A698 (void);
// 0x00000056 System.Void CheckpointManager::OnTriggerEnter(UnityEngine.Collider)
extern void CheckpointManager_OnTriggerEnter_m858AE07B2E5AE9EA876290C92F1DF5CC356B58AF (void);
// 0x00000057 System.Void CheckpointManager::.ctor()
extern void CheckpointManager__ctor_mF83BD8095E2174E9980F7FA8037E76A8889AE671 (void);
// 0x00000058 System.Void Circuit::OnDrawGizmos()
extern void Circuit_OnDrawGizmos_mD54705909BE7C302CCD0027D98F1EFF212674422 (void);
// 0x00000059 System.Void Circuit::OnDrawGizmosSelected()
extern void Circuit_OnDrawGizmosSelected_mB698FC2BE58C72CDCBFEEE18A4DCA4036A2F5009 (void);
// 0x0000005A System.Void Circuit::DrawGizmos(System.Boolean)
extern void Circuit_DrawGizmos_mBABEF9A9A778C9FFDAF1AA181A01E39206C55805 (void);
// 0x0000005B System.Void Circuit::.ctor()
extern void Circuit__ctor_m7E4580990ACC700B000E9A3431CC84573AACDF08 (void);
// 0x0000005C System.Void DisplayLeaderBoard::Start()
extern void DisplayLeaderBoard_Start_m7723DA395E9A2A283A99E1BF491F858A976D13E1 (void);
// 0x0000005D System.Void DisplayLeaderBoard::LateUpdate()
extern void DisplayLeaderBoard_LateUpdate_mD8F864D90F208C534DD39DFE346040B4025C93FD (void);
// 0x0000005E System.Void DisplayLeaderBoard::.ctor()
extern void DisplayLeaderBoard__ctor_mDC58C4F6FD9569FE4129B6597B404B014A5B4D3B (void);
// 0x0000005F System.Single Drive::get_currentSpeed()
extern void Drive_get_currentSpeed_mC834CB89A8F6B5EDB6F897E72EA8E11E04A50C3A (void);
// 0x00000060 System.Void Drive::StartSkidTrail(System.Int32)
extern void Drive_StartSkidTrail_m7EF968E10F3FDE728A205EB5DDA087CD51A00869 (void);
// 0x00000061 System.Void Drive::EndSkidTrail(System.Int32)
extern void Drive_EndSkidTrail_m1DF3360DB30C93753A184ED91293B9DDD3B8E13E (void);
// 0x00000062 System.Void Drive::Start()
extern void Drive_Start_m00DE622CD2DFD9A230CA50E48B9C9665AB1C431A (void);
// 0x00000063 System.Void Drive::CalculateEngineSound()
extern void Drive_CalculateEngineSound_mF29404E1C3081601D5D388178D25480EEC25213C (void);
// 0x00000064 System.Void Drive::CheckForSkid()
extern void Drive_CheckForSkid_m226470DD839D5989B936927632F7C09E479A204A (void);
// 0x00000065 System.Void Drive::Go(System.Single,System.Single,System.Single)
extern void Drive_Go_m1B0DD606EB8051157D766E645BB3A03A81DC6EA1 (void);
// 0x00000066 System.Void Drive::.ctor()
extern void Drive__ctor_m82E5A07230C621543B43D4757090899336704F57 (void);
// 0x00000067 System.Void FlipCar::Start()
extern void FlipCar_Start_m1FB2119858EA5E5973B0E740B651379DB61CE344 (void);
// 0x00000068 System.Void FlipCar::RightCar()
extern void FlipCar_RightCar_m7495B5F03C591399C7908103D1790958B9A86A23 (void);
// 0x00000069 System.Void FlipCar::Update()
extern void FlipCar_Update_m7F7203B80EA92D22309342E617D4CA0E64DD2987 (void);
// 0x0000006A System.Void FlipCar::.ctor()
extern void FlipCar__ctor_m8B901A09C4B0E3477E7CE5304A761DDD678ACCAF (void);
// 0x0000006B System.Void Ghost::Start()
extern void Ghost_Start_m4D58708DF0B0990AD546DD02C95396D5C3BE6D11 (void);
// 0x0000006C System.Void Ghost::OnDisable()
extern void Ghost_OnDisable_m4CEB32BD759CB70D34A548FC15962377651135BC (void);
// 0x0000006D System.Void Ghost::Update()
extern void Ghost_Update_m3339B7394A7239AED2A4216B27A5F6E1D430BEB5 (void);
// 0x0000006E System.Void Ghost::.ctor()
extern void Ghost__ctor_mD85306F4823FAF1C651A2FEBE432C090D2F34948 (void);
// 0x0000006F System.Void HUDController::Start()
extern void HUDController_Start_mAF284C5BAF9E4F1E227F1D145DF32335D93018F3 (void);
// 0x00000070 System.Void HUDController::Update()
extern void HUDController_Update_mA7D955F9057166D42112EE91A4ED934A4D9372DB (void);
// 0x00000071 System.Void HUDController::.ctor()
extern void HUDController__ctor_mB896F630035C0CAC01533CF6DDF50FF7EB580FBA (void);
// 0x00000072 System.Void LaunchManager::Awake()
extern void LaunchManager_Awake_mD3FF92EFBB7CAEE9B9C427AAB4060C8304020EE9 (void);
// 0x00000073 System.Void LaunchManager::ConnectNetwork()
extern void LaunchManager_ConnectNetwork_mEF1779F80654B7939A8310E220BCECB39AE23B8B (void);
// 0x00000074 System.Void LaunchManager::SetName(System.String)
extern void LaunchManager_SetName_m5C7C959062313707B8A143046B78C741AD90A32C (void);
// 0x00000075 System.Void LaunchManager::ConnectSingle()
extern void LaunchManager_ConnectSingle_m45690DE35F745D56C7D300F26A0AC1431C4B8237 (void);
// 0x00000076 System.Void LaunchManager::OnConnectedToMaster()
extern void LaunchManager_OnConnectedToMaster_mD65A22F97FC1159B3E475458F38D07AE25F7D4C9 (void);
// 0x00000077 System.Void LaunchManager::OnJoinRandomFailed(System.Int16,System.String)
extern void LaunchManager_OnJoinRandomFailed_mA8F474004CFC59A0E7898295B717D1175FE18008 (void);
// 0x00000078 System.Void LaunchManager::OnDisconnected(Photon.Realtime.DisconnectCause)
extern void LaunchManager_OnDisconnected_mD90D190269F86304603A4E10C9A6BC37A5373428 (void);
// 0x00000079 System.Void LaunchManager::OnJoinedRoom()
extern void LaunchManager_OnJoinedRoom_m1608E650254D6AAA392AD8DA0D1BD1D1E4D5C2EB (void);
// 0x0000007A System.Void LaunchManager::.ctor()
extern void LaunchManager__ctor_mCA209563BF16905DA4E0F76775C721858A39D40B (void);
// 0x0000007B System.Void PlayerStats::.ctor(System.String,System.Int32,System.Single)
extern void PlayerStats__ctor_mE4D57CC30DBFA8D2A0EAB7313A8BF03362B8BCA8 (void);
// 0x0000007C System.Void Leaderboard::Reset()
extern void Leaderboard_Reset_mA05B8F85D2A61BFD6786133E514BA0E376549BF0 (void);
// 0x0000007D System.Int32 Leaderboard::RegisterCar(System.String)
extern void Leaderboard_RegisterCar_m3315874D619AAD14A75869626820418ADC425A67 (void);
// 0x0000007E System.Void Leaderboard::SetPosition(System.Int32,System.Int32,System.Int32,System.Single)
extern void Leaderboard_SetPosition_mE63840E6641646B39BFB1F327991708563B87D88 (void);
// 0x0000007F System.String Leaderboard::GetPosition(System.Int32)
extern void Leaderboard_GetPosition_mC10ED76915B6752A6ACB549A7070F437DEEEA68E (void);
// 0x00000080 System.Collections.Generic.List`1<System.String> Leaderboard::GetPlaces()
extern void Leaderboard_GetPlaces_m50CB53BC1633A3F0992B567D3F9E78CAA1E08799 (void);
// 0x00000081 System.Void Leaderboard::.ctor()
extern void Leaderboard__ctor_mC72FE37B9E2BDBA75DDE02AFC34BC5BC06312835 (void);
// 0x00000082 System.Void Leaderboard::.cctor()
extern void Leaderboard__cctor_mA598F200896D5C4ABB0520F7080B57E1EE04FE8C (void);
// 0x00000083 System.Void Leaderboard/<>c::.cctor()
extern void U3CU3Ec__cctor_mBC82474B30E70C08AE70F5F08AFB90D32E8432B5 (void);
// 0x00000084 System.Void Leaderboard/<>c::.ctor()
extern void U3CU3Ec__ctor_m5BCF54269757977342F977D81BE710097ABF3835 (void);
// 0x00000085 System.Int32 Leaderboard/<>c::<GetPosition>b__5_0(System.Collections.Generic.KeyValuePair`2<System.Int32,PlayerStats>)
extern void U3CU3Ec_U3CGetPositionU3Eb__5_0_m885B09827C509AA6F2A5F4F5A8225CA3E6C56987 (void);
// 0x00000086 System.Single Leaderboard/<>c::<GetPosition>b__5_1(System.Collections.Generic.KeyValuePair`2<System.Int32,PlayerStats>)
extern void U3CU3Ec_U3CGetPositionU3Eb__5_1_m1D2EC7F20A928448902867DE1AA2331B48B3615C (void);
// 0x00000087 System.Int32 Leaderboard/<>c::<GetPlaces>b__6_0(System.Collections.Generic.KeyValuePair`2<System.Int32,PlayerStats>)
extern void U3CU3Ec_U3CGetPlacesU3Eb__6_0_m77D5A1241B7C0E3A9E2373E3DA2B6269C5A371E6 (void);
// 0x00000088 System.Single Leaderboard/<>c::<GetPlaces>b__6_1(System.Collections.Generic.KeyValuePair`2<System.Int32,PlayerStats>)
extern void U3CU3Ec_U3CGetPlacesU3Eb__6_1_mEA8DA102CF84C2CD98E728F884E858B732AC973F (void);
// 0x00000089 System.Void NameUIController::Start()
extern void NameUIController_Start_mC1FC7B1192B676436FFE114540E26E074E51B60D (void);
// 0x0000008A System.Void NameUIController::LateUpdate()
extern void NameUIController_LateUpdate_m5B7DEAFCBC5FB32D8015A50F23096523C0F02BCD (void);
// 0x0000008B System.Void NameUIController::.ctor()
extern void NameUIController__ctor_m920B8F4101D9B7B2A410B937FD718C1AF2C7EE17 (void);
// 0x0000008C System.Void NumberCheckPoints::Awake()
extern void NumberCheckPoints_Awake_mA4A31AC76B20B3F1A9259D778C27D36215C2C551 (void);
// 0x0000008D System.Void NumberCheckPoints::.ctor()
extern void NumberCheckPoints__ctor_m56C958658CF7AE24C629CADB172F9B6911C2BA07 (void);
// 0x0000008E System.Void PathFollower::Start()
extern void PathFollower_Start_m9F40130677E165EE4EECC7B346BDC4B4C248A2B5 (void);
// 0x0000008F System.Void PathFollower::Update()
extern void PathFollower_Update_m95CE5C1C214D396145A853C5C44B9BF5CB46C311 (void);
// 0x00000090 System.Void PathFollower::.ctor()
extern void PathFollower__ctor_m595E7462BB252CC5DFA5DC06E4B49BC0F86501B9 (void);
// 0x00000091 System.Void PlayerController::ResetLayer()
extern void PlayerController_ResetLayer_mBA91C18C1AE4B73ED7B8BDF30B6A6478869D5176 (void);
// 0x00000092 System.Void PlayerController::Start()
extern void PlayerController_Start_m9531F30EC892BDD1758A2EEC724E86EFBDA150A3 (void);
// 0x00000093 System.Void PlayerController::Update()
extern void PlayerController_Update_mB31159CAD7DD2329859472554BC9154A83D8E794 (void);
// 0x00000094 System.Void PlayerController::.ctor()
extern void PlayerController__ctor_mF30385729DAFDFCB895C4939F6051DCE6C0327FB (void);
// 0x00000095 System.Void RaceMonitor::Start()
extern void RaceMonitor_Start_m7E1C7622E72452A13F7793A109B7CA23D176214A (void);
// 0x00000096 System.Void RaceMonitor::BeginGame()
extern void RaceMonitor_BeginGame_mB9D740A6163921DF095FA12AF0BF0B4ADF15C144 (void);
// 0x00000097 System.Void RaceMonitor::StartGame()
extern void RaceMonitor_StartGame_m16C77E7E37D6E63401B69B451E07778C366CFB40 (void);
// 0x00000098 System.Collections.IEnumerator RaceMonitor::PlayCountDown()
extern void RaceMonitor_PlayCountDown_mF04A2256EC663E556B5B874D8BDAAEE6DFAFE16F (void);
// 0x00000099 System.Void RaceMonitor::RestartGame()
extern void RaceMonitor_RestartGame_m759A2996773157F0D112BE71514420DF0DEDAF87 (void);
// 0x0000009A System.Void RaceMonitor::RestartLevel()
extern void RaceMonitor_RestartLevel_mF299E67480FAE141D7A28BD18199C64C870EA47B (void);
// 0x0000009B System.Void RaceMonitor::LateUpdate()
extern void RaceMonitor_LateUpdate_mAC51480C46413A26B8E5860F6F64294364F6F54B (void);
// 0x0000009C System.Void RaceMonitor::.ctor()
extern void RaceMonitor__ctor_m929B74C1DCD9F3B8847462D9DA387C6FAF11343B (void);
// 0x0000009D System.Void RaceMonitor::.cctor()
extern void RaceMonitor__cctor_m3D2324D8E560F49DFB58631D7340251ADF304756 (void);
// 0x0000009E System.Void RaceMonitor/<PlayCountDown>d__14::.ctor(System.Int32)
extern void U3CPlayCountDownU3Ed__14__ctor_mCE5993B521B484149B9994A40E5E57C120C1AC8B (void);
// 0x0000009F System.Void RaceMonitor/<PlayCountDown>d__14::System.IDisposable.Dispose()
extern void U3CPlayCountDownU3Ed__14_System_IDisposable_Dispose_m9D1ECE478D960C672A5EF96F325A4FD0637C53F3 (void);
// 0x000000A0 System.Boolean RaceMonitor/<PlayCountDown>d__14::MoveNext()
extern void U3CPlayCountDownU3Ed__14_MoveNext_m0EC06AC86AFDCB046C3E932C8C85C2412EE8913F (void);
// 0x000000A1 System.Object RaceMonitor/<PlayCountDown>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPlayCountDownU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0C3B670A55B4FF61E0AE97872E84AB529F3A2750 (void);
// 0x000000A2 System.Void RaceMonitor/<PlayCountDown>d__14::System.Collections.IEnumerator.Reset()
extern void U3CPlayCountDownU3Ed__14_System_Collections_IEnumerator_Reset_m63A44294F71CB5EEDD94657C04858037A53438B0 (void);
// 0x000000A3 System.Object RaceMonitor/<PlayCountDown>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CPlayCountDownU3Ed__14_System_Collections_IEnumerator_get_Current_mD829CEFFE479AFAA019DAD7BF917065881768CA7 (void);
// 0x000000A4 System.Void RotatePlatforms::Start()
extern void RotatePlatforms_Start_mBFE1D83E83642807F90B47DDA0877B4B71858BB2 (void);
// 0x000000A5 System.Void RotatePlatforms::Update()
extern void RotatePlatforms_Update_m121F72E7F42A10B29B5B987C5155E899D48EB94A (void);
// 0x000000A6 System.Void RotatePlatforms::.ctor()
extern void RotatePlatforms__ctor_mB1A54DED186750E92B5A6A9C277B38902368C94A (void);
// 0x000000A7 System.Void SelectCar::Start()
extern void SelectCar_Start_m1AA5D4AA1FA23D7D37EB4263D0A7812AFC146150 (void);
// 0x000000A8 System.Void SelectCar::Update()
extern void SelectCar_Update_mE7D12194112670C831536B35271A2061D6273662 (void);
// 0x000000A9 System.Void SelectCar::.ctor()
extern void SelectCar__ctor_mD4AAD929ECFCF12BE8E6E3EBE80E5F2B267BCBDE (void);
// 0x000000AA System.Void SmoothFollow::Start()
extern void SmoothFollow_Start_mA18519E2373B9E301AAD2E303727CF7389ACEED5 (void);
// 0x000000AB System.Void SmoothFollow::LateUpdate()
extern void SmoothFollow_LateUpdate_mBCD77B85B99EA85E30687BE48FA3DDAE8544EE97 (void);
// 0x000000AC System.Void SmoothFollow::Update()
extern void SmoothFollow_Update_m77E28DF8A3F5CDA015FA08D613B6DF60C483936E (void);
// 0x000000AD System.Void SmoothFollow::.ctor()
extern void SmoothFollow__ctor_m6BA4AC412B8D5D63B87CCDAB25EDF2C446DD410B (void);
// 0x000000AE System.Void SmoothFollow::.cctor()
extern void SmoothFollow__cctor_m3D8971592714ABFD2F9D0B7F501C10F1B40E9125 (void);
// 0x000000AF System.Void accelbtn::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void accelbtn_OnPointerDown_m106A700BAB0D0F6E4A92D9ED5BCDEEE90E521E1E (void);
// 0x000000B0 System.Void accelbtn::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void accelbtn_OnPointerUp_m5078EA940FFEF1F05516B1D6771B8489B72316C5 (void);
// 0x000000B1 System.Void accelbtn::.ctor()
extern void accelbtn__ctor_m63F8561450AD97BE9681F1CF7AA9B9FB550A0DAE (void);
// 0x000000B2 System.Void accelbtn::.cctor()
extern void accelbtn__cctor_mB8C9A1100771E31C53A62411BF5C44E206690FAB (void);
// 0x000000B3 System.Void breakbtn::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void breakbtn_OnPointerDown_mA2B3EA725D8746791960B00A1824DB3727DB3C45 (void);
// 0x000000B4 System.Void breakbtn::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void breakbtn_OnPointerUp_m97ECBC44FB64EE79A1F703A11038BA2FF9A7C4C5 (void);
// 0x000000B5 System.Void breakbtn::.ctor()
extern void breakbtn__ctor_m4F75F6CCC9ADDD8006A2CABA7CEE81F7A69DD6B2 (void);
// 0x000000B6 System.Void breakbtn::.cctor()
extern void breakbtn__cctor_m62F4B5578FA358FAD60104E5E85EE23ECAF6DBB8 (void);
// 0x000000B7 System.Void chnageConltoll::ChangeControll()
extern void chnageConltoll_ChangeControll_m26C3B509FD1D504A49A56ADC76EB00619F510C5A (void);
// 0x000000B8 System.Void chnageConltoll::.ctor()
extern void chnageConltoll__ctor_mDF1C8FD9AF4BEE6E8C8EFE6CF954E41705A12A61 (void);
// 0x000000B9 System.Void chnageConltoll::.cctor()
extern void chnageConltoll__cctor_m5914520D07F5F3F1ED8267A50DCE0DC9D65EC7DF (void);
// 0x000000BA System.Void controllsbutton::.ctor()
extern void controllsbutton__ctor_mBC8277D2354AB5D2BCBBB80B82273053453308A4 (void);
// 0x000000BB System.Void leftBtn::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void leftBtn_OnPointerDown_m58A465770C31E9510AF75F9EB92241D39D7BBB29 (void);
// 0x000000BC System.Void leftBtn::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void leftBtn_OnPointerUp_mE838154EFB0BEC262E76992E337E57F368DE15B5 (void);
// 0x000000BD System.Void leftBtn::.ctor()
extern void leftBtn__ctor_mB508BF15A4EDB66CB595289E0056A46A5D08CD2B (void);
// 0x000000BE System.Void leftBtn::.cctor()
extern void leftBtn__cctor_m95BF5F2FFF6CC23B470DF6420C629BF4B96BBFA6 (void);
// 0x000000BF System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::Start()
extern void ConnectAndJoinRandomLb_Start_m8784F9A942632995E527C9AAC79D40049C1B4421 (void);
// 0x000000C0 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::Update()
extern void ConnectAndJoinRandomLb_Update_m27F506BAFEF2C3264B9030218D0ADB86478750E2 (void);
// 0x000000C1 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnConnected()
extern void ConnectAndJoinRandomLb_OnConnected_m5879B131543322FA6CC2D9FB2B4C196698680114 (void);
// 0x000000C2 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnConnectedToMaster()
extern void ConnectAndJoinRandomLb_OnConnectedToMaster_mCCF5E7C0A7A1A8486F3571F05B2E0662C7C939F2 (void);
// 0x000000C3 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnDisconnected(Photon.Realtime.DisconnectCause)
extern void ConnectAndJoinRandomLb_OnDisconnected_m2576DA0537A899046151D7FA51E25AC367F800A7 (void);
// 0x000000C4 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnCustomAuthenticationResponse(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void ConnectAndJoinRandomLb_OnCustomAuthenticationResponse_m8F6E456A2C572923AD8E720088C22588A1DB1459 (void);
// 0x000000C5 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnCustomAuthenticationFailed(System.String)
extern void ConnectAndJoinRandomLb_OnCustomAuthenticationFailed_m447EA30CA679C12865C767F01D060A3A2EF74C5B (void);
// 0x000000C6 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnRegionListReceived(Photon.Realtime.RegionHandler)
extern void ConnectAndJoinRandomLb_OnRegionListReceived_m6F54D1CDC5C51EF9ED8C44E439E83210851CDA8E (void);
// 0x000000C7 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnRoomListUpdate(System.Collections.Generic.List`1<Photon.Realtime.RoomInfo>)
extern void ConnectAndJoinRandomLb_OnRoomListUpdate_m91AEF2D4CB5CC06810FED71B26B736DDC2938DA0 (void);
// 0x000000C8 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnLobbyStatisticsUpdate(System.Collections.Generic.List`1<Photon.Realtime.TypedLobbyInfo>)
extern void ConnectAndJoinRandomLb_OnLobbyStatisticsUpdate_m02972ED26C4B5FCECE1E7A0B482A32E53363F2F7 (void);
// 0x000000C9 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnJoinedLobby()
extern void ConnectAndJoinRandomLb_OnJoinedLobby_m171BA1094BCA0FC7A1D13EA07C858CD2D2F423EF (void);
// 0x000000CA System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnLeftLobby()
extern void ConnectAndJoinRandomLb_OnLeftLobby_m1BCA515302514D0EF88687D2216D67A9D724B0F4 (void);
// 0x000000CB System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnFriendListUpdate(System.Collections.Generic.List`1<Photon.Realtime.FriendInfo>)
extern void ConnectAndJoinRandomLb_OnFriendListUpdate_m6E1EBEFEC84BA9FFA5296B547431BDB692E053DB (void);
// 0x000000CC System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnCreatedRoom()
extern void ConnectAndJoinRandomLb_OnCreatedRoom_mB1906ADF16C593829B0DD191F319716A97B10583 (void);
// 0x000000CD System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnCreateRoomFailed(System.Int16,System.String)
extern void ConnectAndJoinRandomLb_OnCreateRoomFailed_mD11765F8AA8A45B8AC5555207281A43ABA829D24 (void);
// 0x000000CE System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnJoinedRoom()
extern void ConnectAndJoinRandomLb_OnJoinedRoom_mCE9BDE69A13EA84E9ACA73E064A695B9D3754E8B (void);
// 0x000000CF System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnJoinRoomFailed(System.Int16,System.String)
extern void ConnectAndJoinRandomLb_OnJoinRoomFailed_m5622896774D36D4960735E32F353869D9D6DF94C (void);
// 0x000000D0 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnJoinRandomFailed(System.Int16,System.String)
extern void ConnectAndJoinRandomLb_OnJoinRandomFailed_m4BCA35CBE444C5AD28D7D713E92E893198F91B69 (void);
// 0x000000D1 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnLeftRoom()
extern void ConnectAndJoinRandomLb_OnLeftRoom_m488901B35215F9B70BACC7A08ABB123F3B605137 (void);
// 0x000000D2 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnRegionPingCompleted(Photon.Realtime.RegionHandler)
extern void ConnectAndJoinRandomLb_OnRegionPingCompleted_mD76EE8F39A7AE8EF7E071B9E1C5D1A52452FEF17 (void);
// 0x000000D3 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::.ctor()
extern void ConnectAndJoinRandomLb__ctor_mD06217DD59635BD5DC39E8E14274D6604FDF85C1 (void);
// 0x000000D4 System.Void Photon.Chat.UtilityScripts.EventSystemSpawner::Start()
extern void EventSystemSpawner_Start_mFA0E27C8C114D0849B4CD49887D46A5951A7A285 (void);
// 0x000000D5 System.Void Photon.Chat.UtilityScripts.EventSystemSpawner::.ctor()
extern void EventSystemSpawner__ctor_m6FCC0DAF89825B22BEDC803B5E02B42FB86CF331 (void);
// 0x000000D6 System.Void Photon.Chat.UtilityScripts.OnStartDelete::Start()
extern void OnStartDelete_Start_m033082A59BA5941F5740FAD3CDD2D4501430DD33 (void);
// 0x000000D7 System.Void Photon.Chat.UtilityScripts.OnStartDelete::.ctor()
extern void OnStartDelete__ctor_m0B816BC930476E7580303D8E9E8E07444F0C5270 (void);
// 0x000000D8 System.Void Photon.Chat.UtilityScripts.TextButtonTransition::Awake()
extern void TextButtonTransition_Awake_m5402B2656DCEF5F78D8BA1D3D7D77EBD7F07AC9E (void);
// 0x000000D9 System.Void Photon.Chat.UtilityScripts.TextButtonTransition::OnEnable()
extern void TextButtonTransition_OnEnable_mD3229078DB78111EDC0B823872D1E381F3C15A5F (void);
// 0x000000DA System.Void Photon.Chat.UtilityScripts.TextButtonTransition::OnDisable()
extern void TextButtonTransition_OnDisable_mAB2CCC24F8B8F1D664154DC1D0D7E47A925A3B72 (void);
// 0x000000DB System.Void Photon.Chat.UtilityScripts.TextButtonTransition::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TextButtonTransition_OnPointerEnter_m28E7599E058554E4C13D7EA152521BCF4C104B07 (void);
// 0x000000DC System.Void Photon.Chat.UtilityScripts.TextButtonTransition::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TextButtonTransition_OnPointerExit_m25F271B30D439D9AD93483E1CC08FC198A3E9A02 (void);
// 0x000000DD System.Void Photon.Chat.UtilityScripts.TextButtonTransition::.ctor()
extern void TextButtonTransition__ctor_m9E3AFEA1BEFD79F8F0D16DA749243B7D43B78570 (void);
// 0x000000DE System.Void Photon.Chat.UtilityScripts.TextToggleIsOnTransition::OnEnable()
extern void TextToggleIsOnTransition_OnEnable_mA240AFB3178800D5275FB5BD05D54449EC49BAF2 (void);
// 0x000000DF System.Void Photon.Chat.UtilityScripts.TextToggleIsOnTransition::OnDisable()
extern void TextToggleIsOnTransition_OnDisable_m824A3816D8873B0A5F86F4F401FF080163560404 (void);
// 0x000000E0 System.Void Photon.Chat.UtilityScripts.TextToggleIsOnTransition::OnValueChanged(System.Boolean)
extern void TextToggleIsOnTransition_OnValueChanged_m2C3DAB9655DD596BFC184D0A5B4F83D36C8B2FFB (void);
// 0x000000E1 System.Void Photon.Chat.UtilityScripts.TextToggleIsOnTransition::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TextToggleIsOnTransition_OnPointerEnter_m01E7951E3FE9C9C9A1FC1D5E26EFF4041799E0E5 (void);
// 0x000000E2 System.Void Photon.Chat.UtilityScripts.TextToggleIsOnTransition::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TextToggleIsOnTransition_OnPointerExit_mBF6DDD1410475F0C6118F9C85F572E669FBB4C20 (void);
// 0x000000E3 System.Void Photon.Chat.UtilityScripts.TextToggleIsOnTransition::.ctor()
extern void TextToggleIsOnTransition__ctor_mA401C98E0CC8F9B8728B46138CCF941C6E471F04 (void);
static Il2CppMethodPointer s_methodPointers[227] = 
{
	ChannelSelector_SetChannel_m73C936B325A97928B7A90D7C263923DB1608AD3E,
	ChannelSelector_OnPointerClick_mF2CE1256B4C1587300BC65F68C57871E9D04EF5B,
	ChannelSelector__ctor_m955A69F0C09110A7184264FC4A400625C9C3712B,
	ChatAppIdCheckerUI_Update_m3E0F22639A6DC1534C8E37925CAD3F7F0C169A69,
	ChatAppIdCheckerUI__ctor_m810602E0BA6ACAF276B84807C7531EDF0AA7895A,
	ChatGui_get_UserName_mFC72D99B3857A2A850E610182954EF21BFDD656B,
	ChatGui_set_UserName_m2A5D15C5A2C57BF815E69DEBD12771BBF5F28F0E,
	ChatGui_Start_m39521BDC549E6122372E4BD4057244FE97C85F54,
	ChatGui_Connect_m085418B018B9DC3121B4B4F763086FE181F7CCF0,
	ChatGui_OnDestroy_m374C72D03D08B9C92F382B4AC9B5C7ECFCB0E33E,
	ChatGui_OnApplicationQuit_mF07E706B4A1A3DD845DDD336D2FD8D67556BDC66,
	ChatGui_Update_mEE5D2E3175984AC39518A58153C7961BB30F90BD,
	ChatGui_OnEnterSend_mC29FE6FC78C35108FD2E3D44A5FE158744F09EFB,
	ChatGui_OnClickSend_m2FA72879D41DD1187AA3ED5084D818ABFA2B6E49,
	ChatGui_SendChatMessage_m5CEA1D17144CE9652A8B65E8860D0DA2E369594A,
	ChatGui_PostHelpToCurrentChannel_mFFA856F30EAABC8F6CC153BB4BB0B42760EB102D,
	ChatGui_DebugReturn_mCFBB07C217B3858CFC14884DF3AC55A8B022154A,
	ChatGui_OnConnected_m3EAFCA54E75B6AB28C363E920AE048EF5DCB4521,
	ChatGui_OnDisconnected_m3003FC2C479F5537B1AA6F75747DA866D02B5532,
	ChatGui_OnChatStateChange_mBC049CFBD2000061938736E7CCE03C1C91C0CB63,
	ChatGui_OnSubscribed_m20D1F15C70C822E6AB5560B8E5140F9BD88596F1,
	ChatGui_InstantiateChannelButton_m3848B8D380461CD3C4DDF9A90FE3EDD2E332ACDB,
	ChatGui_InstantiateFriendButton_m7F07EDAD1DA6F0F76A44AFF743B7C4F459D9A849,
	ChatGui_OnUnsubscribed_mCCCE787B3F97594074B62E14C601AFCAB61A47A2,
	ChatGui_OnGetMessages_m399FFB8E1300C33A9EB374F9F2787374FEC80D1F,
	ChatGui_OnPrivateMessage_m7E046C5EE238E0B998565EB88D5EC7CB0B62D76F,
	ChatGui_OnStatusUpdate_mD19AEAB3025B776571DC5DA3AE50FFFB1C383AF1,
	ChatGui_OnUserSubscribed_mE99340D2BD23C7D636899E650E54C37D0507DAD6,
	ChatGui_OnUserUnsubscribed_m2E16B304BDD24E9D2C8D1A7914AEED529FF6EE7F,
	ChatGui_AddMessageToSelectedChannel_m171FB1F09B4D53912B39728479F91056E18323E7,
	ChatGui_ShowChannel_m48BBE9EA44EC7992C42B584E7CD2E05352789551,
	ChatGui_OpenDashboard_m27812C12469AD5A0D2CA52D2B7E11593BCF9EC90,
	ChatGui__ctor_mE9C452C98CEA2FA3D5007EAB2B295C57B4DCE205,
	ChatGui__cctor_m7D892708067CB07DB58081A631DA9DFBAC7B3083,
	FriendItem_set_FriendId_m8E9861AA8C28705ED7855791487BB3C012F60CBA,
	FriendItem_get_FriendId_mEF088D10E35110124CC84308164FA4F5E5096DA3,
	FriendItem_Awake_m6CC1DB18190F7F5A79029BE4AB4F6F29F519BDA0,
	FriendItem_OnFriendStatusUpdate_mCC056D85F3DD38A7C9869E7919F64729ED0551B3,
	FriendItem__ctor_mB9DE36BE9496080F797F23BCC813AC66857692B0,
	IgnoreUiRaycastWhenInactive_IsRaycastLocationValid_mE22106037E41083C28AE240A56C2571C1A98D531,
	IgnoreUiRaycastWhenInactive__ctor_m23CB38907F86355773CE8F77CC5AA54CC5854BD7,
	NamePickGui_Start_m51D755E15E2717751111AB4747BE1116B6493729,
	NamePickGui_EndEditOnEnter_m5FB829C5A26086E594AC805A82CD29B8F7C2D89E,
	NamePickGui_StartChat_mE4FE987367755FCE2168B2883873504901E3319C,
	NamePickGui__ctor_m40BA9A6C50F204FB90838B67DE6C01D345A5FAD6,
	Skidmarks_Start_m630AD543C6831297B563C7E0FCCF6E593B1E66E4,
	Skidmarks_LateUpdate_m243E9DE089D3D2FFEF63A8BDFD41CA21330B16FC,
	Skidmarks_AddSkidMark_mA1475B4E1C10D429155C166434BF9D33B1B8C0A1,
	Skidmarks_UpdateSkidmarksMesh_mC10BD9781B76400B84588F5F1E4878B6F8866CC8,
	Skidmarks__ctor_mCF8400BC9314E7EAD88F56AC072EB921C0DAB2DA,
	MarkSection__ctor_mEA1F249CE4D680B005BD82AEEB94B031ECFA6F6A,
	WheelSkid_Awake_m954ABF30FE9A499C675621A338A62DB212F2829A,
	WheelSkid_Start_m4CB4A41755192A3C08F381AF0E4DB7F1EC5F2254,
	WheelSkid_FixedUpdate_mA87613D301950479E08C6B790B4CFD94C7130302,
	WheelSkid_LateUpdate_m20C03A54816EDFAB7E8747BF9ACDA92945D489C8,
	WheelSkid__ctor_m7C3F4F3C5F0F283C7BDEB4C956B68EBBFF588FD6,
	NetworkedPlayer_Awake_m1414180636F0B7DC12167279B4A57B5F2136A4F1,
	NetworkedPlayer__ctor_m582B2562027C1619FEF73CA7FA69FEECF58B27D1,
	RightBtn_OnPointerDown_m6AC878546D248F9361160C2B865DAE5C297F9B66,
	RightBtn_OnPointerUp_mC9861A081B815E6E1CF4AD0215CF4B3CF6096BF1,
	RightBtn__ctor_m672B36D6C85CB0278E67A1330E61F92F5388C6A0,
	RightBtn__cctor_mD55EC86AB2BFAC9917A7A4C0D86DDE3C6F6EAF14,
	AIController_Start_m4C6AC8E7E19CF8263AC914947D25676DA491B832,
	AIController_ProgressTracker_m77F0A24A605087102A956949FD63662E6EEE94BE,
	AIController_ResetLayer_m13254D2EC569FA1ABA0FA5308DF0489E11FB7679,
	AIController_Update_mC1B4E8DE676F4EDFD8E3296F5AE6EB8C7D6E73F7,
	AIController__ctor_m28A6370E8C2F1F7BB0C5D75702AE025E843F28A5,
	AltAIController_Start_m94EED304C0D89DECCAB2C4599CA5CBD8107D23FA,
	AltAIController_Update_m7DE56AB527F4FA0ED29461988B05DBFE047D6F76,
	AltAIController__ctor_mE1176095003853F4B9400976B9B02E906CB2A757,
	AntiRolllBar_Start_mA5FF49C82425F3EE568125D1128874A20ED83514,
	AntiRolllBar_GroundWheels_m6A37DAB8017DA3B204FCB633AE3F5FCB2D89FCE3,
	AntiRolllBar_FixedUpdate_mEB6D89EC34459019AC90F624F587F65CD0FDE8B5,
	AntiRolllBar__ctor_m5D2FF8CEF6C2C891A9A12870BC8ADE061B9841F2,
	AvoidDetector_OnCollisionExit_m7C453B7E42B0A2337887E716133E1382214184B4,
	AvoidDetector_OnCollisionStay_m4EC78296C373AA603E976F205D45AAF2317663C6,
	AvoidDetector__ctor_m9DF63910A415FFB8A2B0F3B16918E47D7AC14EDC,
	CameraTrack_Update_m66A257B7FF4E3F3FC96EE1F8C733F3F795F75CDC,
	CameraTrack__ctor_mF77D9D06785445AD0DCFA3522D23DC85F00D13BA,
	CheckPointCreator_Start_m0F5629A0157636B65EFE4F47B2884BBB6AC34CAB,
	CheckPointCreator_CreateCheckpoints_mB2A5AA69625956620D97842B6ADDE4D109F51BB2,
	CheckPointCreator_PlaceCheckPoint_m0CCE80450810B726CE8C05B833FBC44E3FE36256,
	CheckPointCreator_Update_mEB8480AC814D854E4BB002A5BD7E05A6E46038CC,
	CheckPointCreator__ctor_mAD6A603105022A2B97A5B127BFCFB36DFC28613C,
	CheckpointManager_Start_mDC1F849F88DF9D1B14E0EB8E1F76EA9C3AA7A698,
	CheckpointManager_OnTriggerEnter_m858AE07B2E5AE9EA876290C92F1DF5CC356B58AF,
	CheckpointManager__ctor_mF83BD8095E2174E9980F7FA8037E76A8889AE671,
	Circuit_OnDrawGizmos_mD54705909BE7C302CCD0027D98F1EFF212674422,
	Circuit_OnDrawGizmosSelected_mB698FC2BE58C72CDCBFEEE18A4DCA4036A2F5009,
	Circuit_DrawGizmos_mBABEF9A9A778C9FFDAF1AA181A01E39206C55805,
	Circuit__ctor_m7E4580990ACC700B000E9A3431CC84573AACDF08,
	DisplayLeaderBoard_Start_m7723DA395E9A2A283A99E1BF491F858A976D13E1,
	DisplayLeaderBoard_LateUpdate_mD8F864D90F208C534DD39DFE346040B4025C93FD,
	DisplayLeaderBoard__ctor_mDC58C4F6FD9569FE4129B6597B404B014A5B4D3B,
	Drive_get_currentSpeed_mC834CB89A8F6B5EDB6F897E72EA8E11E04A50C3A,
	Drive_StartSkidTrail_m7EF968E10F3FDE728A205EB5DDA087CD51A00869,
	Drive_EndSkidTrail_m1DF3360DB30C93753A184ED91293B9DDD3B8E13E,
	Drive_Start_m00DE622CD2DFD9A230CA50E48B9C9665AB1C431A,
	Drive_CalculateEngineSound_mF29404E1C3081601D5D388178D25480EEC25213C,
	Drive_CheckForSkid_m226470DD839D5989B936927632F7C09E479A204A,
	Drive_Go_m1B0DD606EB8051157D766E645BB3A03A81DC6EA1,
	Drive__ctor_m82E5A07230C621543B43D4757090899336704F57,
	FlipCar_Start_m1FB2119858EA5E5973B0E740B651379DB61CE344,
	FlipCar_RightCar_m7495B5F03C591399C7908103D1790958B9A86A23,
	FlipCar_Update_m7F7203B80EA92D22309342E617D4CA0E64DD2987,
	FlipCar__ctor_m8B901A09C4B0E3477E7CE5304A761DDD678ACCAF,
	Ghost_Start_m4D58708DF0B0990AD546DD02C95396D5C3BE6D11,
	Ghost_OnDisable_m4CEB32BD759CB70D34A548FC15962377651135BC,
	Ghost_Update_m3339B7394A7239AED2A4216B27A5F6E1D430BEB5,
	Ghost__ctor_mD85306F4823FAF1C651A2FEBE432C090D2F34948,
	HUDController_Start_mAF284C5BAF9E4F1E227F1D145DF32335D93018F3,
	HUDController_Update_mA7D955F9057166D42112EE91A4ED934A4D9372DB,
	HUDController__ctor_mB896F630035C0CAC01533CF6DDF50FF7EB580FBA,
	LaunchManager_Awake_mD3FF92EFBB7CAEE9B9C427AAB4060C8304020EE9,
	LaunchManager_ConnectNetwork_mEF1779F80654B7939A8310E220BCECB39AE23B8B,
	LaunchManager_SetName_m5C7C959062313707B8A143046B78C741AD90A32C,
	LaunchManager_ConnectSingle_m45690DE35F745D56C7D300F26A0AC1431C4B8237,
	LaunchManager_OnConnectedToMaster_mD65A22F97FC1159B3E475458F38D07AE25F7D4C9,
	LaunchManager_OnJoinRandomFailed_mA8F474004CFC59A0E7898295B717D1175FE18008,
	LaunchManager_OnDisconnected_mD90D190269F86304603A4E10C9A6BC37A5373428,
	LaunchManager_OnJoinedRoom_m1608E650254D6AAA392AD8DA0D1BD1D1E4D5C2EB,
	LaunchManager__ctor_mCA209563BF16905DA4E0F76775C721858A39D40B,
	PlayerStats__ctor_mE4D57CC30DBFA8D2A0EAB7313A8BF03362B8BCA8,
	Leaderboard_Reset_mA05B8F85D2A61BFD6786133E514BA0E376549BF0,
	Leaderboard_RegisterCar_m3315874D619AAD14A75869626820418ADC425A67,
	Leaderboard_SetPosition_mE63840E6641646B39BFB1F327991708563B87D88,
	Leaderboard_GetPosition_mC10ED76915B6752A6ACB549A7070F437DEEEA68E,
	Leaderboard_GetPlaces_m50CB53BC1633A3F0992B567D3F9E78CAA1E08799,
	Leaderboard__ctor_mC72FE37B9E2BDBA75DDE02AFC34BC5BC06312835,
	Leaderboard__cctor_mA598F200896D5C4ABB0520F7080B57E1EE04FE8C,
	U3CU3Ec__cctor_mBC82474B30E70C08AE70F5F08AFB90D32E8432B5,
	U3CU3Ec__ctor_m5BCF54269757977342F977D81BE710097ABF3835,
	U3CU3Ec_U3CGetPositionU3Eb__5_0_m885B09827C509AA6F2A5F4F5A8225CA3E6C56987,
	U3CU3Ec_U3CGetPositionU3Eb__5_1_m1D2EC7F20A928448902867DE1AA2331B48B3615C,
	U3CU3Ec_U3CGetPlacesU3Eb__6_0_m77D5A1241B7C0E3A9E2373E3DA2B6269C5A371E6,
	U3CU3Ec_U3CGetPlacesU3Eb__6_1_mEA8DA102CF84C2CD98E728F884E858B732AC973F,
	NameUIController_Start_mC1FC7B1192B676436FFE114540E26E074E51B60D,
	NameUIController_LateUpdate_m5B7DEAFCBC5FB32D8015A50F23096523C0F02BCD,
	NameUIController__ctor_m920B8F4101D9B7B2A410B937FD718C1AF2C7EE17,
	NumberCheckPoints_Awake_mA4A31AC76B20B3F1A9259D778C27D36215C2C551,
	NumberCheckPoints__ctor_m56C958658CF7AE24C629CADB172F9B6911C2BA07,
	PathFollower_Start_m9F40130677E165EE4EECC7B346BDC4B4C248A2B5,
	PathFollower_Update_m95CE5C1C214D396145A853C5C44B9BF5CB46C311,
	PathFollower__ctor_m595E7462BB252CC5DFA5DC06E4B49BC0F86501B9,
	PlayerController_ResetLayer_mBA91C18C1AE4B73ED7B8BDF30B6A6478869D5176,
	PlayerController_Start_m9531F30EC892BDD1758A2EEC724E86EFBDA150A3,
	PlayerController_Update_mB31159CAD7DD2329859472554BC9154A83D8E794,
	PlayerController__ctor_mF30385729DAFDFCB895C4939F6051DCE6C0327FB,
	RaceMonitor_Start_m7E1C7622E72452A13F7793A109B7CA23D176214A,
	RaceMonitor_BeginGame_mB9D740A6163921DF095FA12AF0BF0B4ADF15C144,
	RaceMonitor_StartGame_m16C77E7E37D6E63401B69B451E07778C366CFB40,
	RaceMonitor_PlayCountDown_mF04A2256EC663E556B5B874D8BDAAEE6DFAFE16F,
	RaceMonitor_RestartGame_m759A2996773157F0D112BE71514420DF0DEDAF87,
	RaceMonitor_RestartLevel_mF299E67480FAE141D7A28BD18199C64C870EA47B,
	RaceMonitor_LateUpdate_mAC51480C46413A26B8E5860F6F64294364F6F54B,
	RaceMonitor__ctor_m929B74C1DCD9F3B8847462D9DA387C6FAF11343B,
	RaceMonitor__cctor_m3D2324D8E560F49DFB58631D7340251ADF304756,
	U3CPlayCountDownU3Ed__14__ctor_mCE5993B521B484149B9994A40E5E57C120C1AC8B,
	U3CPlayCountDownU3Ed__14_System_IDisposable_Dispose_m9D1ECE478D960C672A5EF96F325A4FD0637C53F3,
	U3CPlayCountDownU3Ed__14_MoveNext_m0EC06AC86AFDCB046C3E932C8C85C2412EE8913F,
	U3CPlayCountDownU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0C3B670A55B4FF61E0AE97872E84AB529F3A2750,
	U3CPlayCountDownU3Ed__14_System_Collections_IEnumerator_Reset_m63A44294F71CB5EEDD94657C04858037A53438B0,
	U3CPlayCountDownU3Ed__14_System_Collections_IEnumerator_get_Current_mD829CEFFE479AFAA019DAD7BF917065881768CA7,
	RotatePlatforms_Start_mBFE1D83E83642807F90B47DDA0877B4B71858BB2,
	RotatePlatforms_Update_m121F72E7F42A10B29B5B987C5155E899D48EB94A,
	RotatePlatforms__ctor_mB1A54DED186750E92B5A6A9C277B38902368C94A,
	SelectCar_Start_m1AA5D4AA1FA23D7D37EB4263D0A7812AFC146150,
	SelectCar_Update_mE7D12194112670C831536B35271A2061D6273662,
	SelectCar__ctor_mD4AAD929ECFCF12BE8E6E3EBE80E5F2B267BCBDE,
	SmoothFollow_Start_mA18519E2373B9E301AAD2E303727CF7389ACEED5,
	SmoothFollow_LateUpdate_mBCD77B85B99EA85E30687BE48FA3DDAE8544EE97,
	SmoothFollow_Update_m77E28DF8A3F5CDA015FA08D613B6DF60C483936E,
	SmoothFollow__ctor_m6BA4AC412B8D5D63B87CCDAB25EDF2C446DD410B,
	SmoothFollow__cctor_m3D8971592714ABFD2F9D0B7F501C10F1B40E9125,
	accelbtn_OnPointerDown_m106A700BAB0D0F6E4A92D9ED5BCDEEE90E521E1E,
	accelbtn_OnPointerUp_m5078EA940FFEF1F05516B1D6771B8489B72316C5,
	accelbtn__ctor_m63F8561450AD97BE9681F1CF7AA9B9FB550A0DAE,
	accelbtn__cctor_mB8C9A1100771E31C53A62411BF5C44E206690FAB,
	breakbtn_OnPointerDown_mA2B3EA725D8746791960B00A1824DB3727DB3C45,
	breakbtn_OnPointerUp_m97ECBC44FB64EE79A1F703A11038BA2FF9A7C4C5,
	breakbtn__ctor_m4F75F6CCC9ADDD8006A2CABA7CEE81F7A69DD6B2,
	breakbtn__cctor_m62F4B5578FA358FAD60104E5E85EE23ECAF6DBB8,
	chnageConltoll_ChangeControll_m26C3B509FD1D504A49A56ADC76EB00619F510C5A,
	chnageConltoll__ctor_mDF1C8FD9AF4BEE6E8C8EFE6CF954E41705A12A61,
	chnageConltoll__cctor_m5914520D07F5F3F1ED8267A50DCE0DC9D65EC7DF,
	controllsbutton__ctor_mBC8277D2354AB5D2BCBBB80B82273053453308A4,
	leftBtn_OnPointerDown_m58A465770C31E9510AF75F9EB92241D39D7BBB29,
	leftBtn_OnPointerUp_mE838154EFB0BEC262E76992E337E57F368DE15B5,
	leftBtn__ctor_mB508BF15A4EDB66CB595289E0056A46A5D08CD2B,
	leftBtn__cctor_m95BF5F2FFF6CC23B470DF6420C629BF4B96BBFA6,
	ConnectAndJoinRandomLb_Start_m8784F9A942632995E527C9AAC79D40049C1B4421,
	ConnectAndJoinRandomLb_Update_m27F506BAFEF2C3264B9030218D0ADB86478750E2,
	ConnectAndJoinRandomLb_OnConnected_m5879B131543322FA6CC2D9FB2B4C196698680114,
	ConnectAndJoinRandomLb_OnConnectedToMaster_mCCF5E7C0A7A1A8486F3571F05B2E0662C7C939F2,
	ConnectAndJoinRandomLb_OnDisconnected_m2576DA0537A899046151D7FA51E25AC367F800A7,
	ConnectAndJoinRandomLb_OnCustomAuthenticationResponse_m8F6E456A2C572923AD8E720088C22588A1DB1459,
	ConnectAndJoinRandomLb_OnCustomAuthenticationFailed_m447EA30CA679C12865C767F01D060A3A2EF74C5B,
	ConnectAndJoinRandomLb_OnRegionListReceived_m6F54D1CDC5C51EF9ED8C44E439E83210851CDA8E,
	ConnectAndJoinRandomLb_OnRoomListUpdate_m91AEF2D4CB5CC06810FED71B26B736DDC2938DA0,
	ConnectAndJoinRandomLb_OnLobbyStatisticsUpdate_m02972ED26C4B5FCECE1E7A0B482A32E53363F2F7,
	ConnectAndJoinRandomLb_OnJoinedLobby_m171BA1094BCA0FC7A1D13EA07C858CD2D2F423EF,
	ConnectAndJoinRandomLb_OnLeftLobby_m1BCA515302514D0EF88687D2216D67A9D724B0F4,
	ConnectAndJoinRandomLb_OnFriendListUpdate_m6E1EBEFEC84BA9FFA5296B547431BDB692E053DB,
	ConnectAndJoinRandomLb_OnCreatedRoom_mB1906ADF16C593829B0DD191F319716A97B10583,
	ConnectAndJoinRandomLb_OnCreateRoomFailed_mD11765F8AA8A45B8AC5555207281A43ABA829D24,
	ConnectAndJoinRandomLb_OnJoinedRoom_mCE9BDE69A13EA84E9ACA73E064A695B9D3754E8B,
	ConnectAndJoinRandomLb_OnJoinRoomFailed_m5622896774D36D4960735E32F353869D9D6DF94C,
	ConnectAndJoinRandomLb_OnJoinRandomFailed_m4BCA35CBE444C5AD28D7D713E92E893198F91B69,
	ConnectAndJoinRandomLb_OnLeftRoom_m488901B35215F9B70BACC7A08ABB123F3B605137,
	ConnectAndJoinRandomLb_OnRegionPingCompleted_mD76EE8F39A7AE8EF7E071B9E1C5D1A52452FEF17,
	ConnectAndJoinRandomLb__ctor_mD06217DD59635BD5DC39E8E14274D6604FDF85C1,
	EventSystemSpawner_Start_mFA0E27C8C114D0849B4CD49887D46A5951A7A285,
	EventSystemSpawner__ctor_m6FCC0DAF89825B22BEDC803B5E02B42FB86CF331,
	OnStartDelete_Start_m033082A59BA5941F5740FAD3CDD2D4501430DD33,
	OnStartDelete__ctor_m0B816BC930476E7580303D8E9E8E07444F0C5270,
	TextButtonTransition_Awake_m5402B2656DCEF5F78D8BA1D3D7D77EBD7F07AC9E,
	TextButtonTransition_OnEnable_mD3229078DB78111EDC0B823872D1E381F3C15A5F,
	TextButtonTransition_OnDisable_mAB2CCC24F8B8F1D664154DC1D0D7E47A925A3B72,
	TextButtonTransition_OnPointerEnter_m28E7599E058554E4C13D7EA152521BCF4C104B07,
	TextButtonTransition_OnPointerExit_m25F271B30D439D9AD93483E1CC08FC198A3E9A02,
	TextButtonTransition__ctor_m9E3AFEA1BEFD79F8F0D16DA749243B7D43B78570,
	TextToggleIsOnTransition_OnEnable_mA240AFB3178800D5275FB5BD05D54449EC49BAF2,
	TextToggleIsOnTransition_OnDisable_m824A3816D8873B0A5F86F4F401FF080163560404,
	TextToggleIsOnTransition_OnValueChanged_m2C3DAB9655DD596BFC184D0A5B4F83D36C8B2FFB,
	TextToggleIsOnTransition_OnPointerEnter_m01E7951E3FE9C9C9A1FC1D5E26EFF4041799E0E5,
	TextToggleIsOnTransition_OnPointerExit_mBF6DDD1410475F0C6118F9C85F572E669FBB4C20,
	TextToggleIsOnTransition__ctor_mA401C98E0CC8F9B8728B46138CCF941C6E471F04,
};
extern void PlayerStats__ctor_mE4D57CC30DBFA8D2A0EAB7313A8BF03362B8BCA8_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[1] = 
{
	{ 0x0600007B, PlayerStats__ctor_mE4D57CC30DBFA8D2A0EAB7313A8BF03362B8BCA8_AdjustorThunk },
};
static const int32_t s_InvokerIndices[227] = 
{
	1522,
	1522,
	1791,
	1791,
	1791,
	1758,
	1522,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1522,
	1791,
	984,
	1791,
	1791,
	1513,
	972,
	1522,
	1522,
	1522,
	652,
	652,
	419,
	972,
	972,
	1522,
	1522,
	1791,
	1791,
	2828,
	1522,
	1758,
	1791,
	624,
	1791,
	819,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	295,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1522,
	1522,
	1791,
	2828,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	972,
	1791,
	1791,
	1522,
	1522,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1522,
	1791,
	1791,
	1791,
	1541,
	1791,
	1791,
	1791,
	1791,
	1780,
	1513,
	1513,
	1791,
	1791,
	1791,
	684,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1522,
	1791,
	1791,
	855,
	1513,
	1791,
	1791,
	646,
	2828,
	2672,
	2174,
	2712,
	2813,
	1791,
	2828,
	2828,
	1791,
	1125,
	1413,
	1125,
	1413,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1758,
	1791,
	1791,
	1791,
	1791,
	2828,
	1513,
	1791,
	1778,
	1758,
	1791,
	1758,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	2828,
	1522,
	1522,
	1791,
	2828,
	1522,
	1522,
	1791,
	2828,
	1791,
	1791,
	2828,
	1791,
	1522,
	1522,
	1791,
	2828,
	1791,
	1791,
	1791,
	1791,
	1513,
	1522,
	1522,
	1522,
	1522,
	1522,
	1791,
	1791,
	1522,
	1791,
	855,
	1791,
	855,
	855,
	1791,
	1522,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1522,
	1522,
	1791,
	1791,
	1791,
	1541,
	1522,
	1522,
	1791,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	227,
	s_methodPointers,
	1,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
