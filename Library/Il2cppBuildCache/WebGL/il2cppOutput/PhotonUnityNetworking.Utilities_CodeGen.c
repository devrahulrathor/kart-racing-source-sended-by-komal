﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Int32 Photon.Pun.UtilityScripts.CullArea::get_CellCount()
extern void CullArea_get_CellCount_mC896B34CC2398FF0D8C88C6FDF2D0B4765668542 (void);
// 0x00000002 System.Void Photon.Pun.UtilityScripts.CullArea::set_CellCount(System.Int32)
extern void CullArea_set_CellCount_mBE8FBA6BA28C8EA2729EDBB484778096AB68A9AC (void);
// 0x00000003 Photon.Pun.UtilityScripts.CellTree Photon.Pun.UtilityScripts.CullArea::get_CellTree()
extern void CullArea_get_CellTree_m0D313911F55073D69FF4D498A1D08F24D06B239A (void);
// 0x00000004 System.Void Photon.Pun.UtilityScripts.CullArea::set_CellTree(Photon.Pun.UtilityScripts.CellTree)
extern void CullArea_set_CellTree_m20F8FBD48F628DBFF56FE8E2D1227188E1AE3E9B (void);
// 0x00000005 System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GameObject> Photon.Pun.UtilityScripts.CullArea::get_Map()
extern void CullArea_get_Map_m41602F663C80A56CA416A620AA432990F05DABB9 (void);
// 0x00000006 System.Void Photon.Pun.UtilityScripts.CullArea::set_Map(System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GameObject>)
extern void CullArea_set_Map_m75450BDB6E944F4BE133FFC68FE4960895A94EBD (void);
// 0x00000007 System.Void Photon.Pun.UtilityScripts.CullArea::Awake()
extern void CullArea_Awake_m7A700AA5B6897B5FFE6C52887E3A9A2A1B7E5F7C (void);
// 0x00000008 System.Void Photon.Pun.UtilityScripts.CullArea::OnDrawGizmos()
extern void CullArea_OnDrawGizmos_m420A22206D92DDFA3904D2C821FD4F6180DBA9AF (void);
// 0x00000009 System.Void Photon.Pun.UtilityScripts.CullArea::CreateCellHierarchy()
extern void CullArea_CreateCellHierarchy_mD8B6DF94B76E244C57E7E4CC7646ADDCAF48B6D5 (void);
// 0x0000000A System.Void Photon.Pun.UtilityScripts.CullArea::CreateChildCells(Photon.Pun.UtilityScripts.CellTreeNode,System.Int32)
extern void CullArea_CreateChildCells_m2FF74828DC2932F644F768AEC9399CE42102E89C (void);
// 0x0000000B System.Void Photon.Pun.UtilityScripts.CullArea::DrawCells()
extern void CullArea_DrawCells_m72711D17BE5D133E7598E9FAF526FB99652618E5 (void);
// 0x0000000C System.Boolean Photon.Pun.UtilityScripts.CullArea::IsCellCountAllowed()
extern void CullArea_IsCellCountAllowed_mC528A1E69232765CADC34D5296A89C2CF7CE18C9 (void);
// 0x0000000D System.Collections.Generic.List`1<System.Byte> Photon.Pun.UtilityScripts.CullArea::GetActiveCells(UnityEngine.Vector3)
extern void CullArea_GetActiveCells_m102F965E9B8EA1E68B7834AFDA2519E03C5B7FE7 (void);
// 0x0000000E System.Void Photon.Pun.UtilityScripts.CullArea::.ctor()
extern void CullArea__ctor_m0526DAFA7A4D1B353BE853551ADFB36A708F64C4 (void);
// 0x0000000F Photon.Pun.UtilityScripts.CellTreeNode Photon.Pun.UtilityScripts.CellTree::get_RootNode()
extern void CellTree_get_RootNode_mF64DBAF7D06C2DE7C846436B60D1672E825B6D57 (void);
// 0x00000010 System.Void Photon.Pun.UtilityScripts.CellTree::set_RootNode(Photon.Pun.UtilityScripts.CellTreeNode)
extern void CellTree_set_RootNode_mDA1EFE7FE3230A2E22E3E103647A4EC8F997F949 (void);
// 0x00000011 System.Void Photon.Pun.UtilityScripts.CellTree::.ctor()
extern void CellTree__ctor_m6D84F2C85758D4159E7CD9B95F64D5FEE2636536 (void);
// 0x00000012 System.Void Photon.Pun.UtilityScripts.CellTree::.ctor(Photon.Pun.UtilityScripts.CellTreeNode)
extern void CellTree__ctor_m5D145514F48F42159E03D17C954BD68E4AEC460F (void);
// 0x00000013 System.Void Photon.Pun.UtilityScripts.CellTreeNode::.ctor()
extern void CellTreeNode__ctor_m8DD2569D820820C7000B2CA1A68291FF69544797 (void);
// 0x00000014 System.Void Photon.Pun.UtilityScripts.CellTreeNode::.ctor(System.Byte,Photon.Pun.UtilityScripts.CellTreeNode/ENodeType,Photon.Pun.UtilityScripts.CellTreeNode)
extern void CellTreeNode__ctor_m1848F7BA51B29DF544EF709E26474A9E2FB14B35 (void);
// 0x00000015 System.Void Photon.Pun.UtilityScripts.CellTreeNode::AddChild(Photon.Pun.UtilityScripts.CellTreeNode)
extern void CellTreeNode_AddChild_m1E36EA9A58B23D0B7FAF6F382A29534D2A860590 (void);
// 0x00000016 System.Void Photon.Pun.UtilityScripts.CellTreeNode::Draw()
extern void CellTreeNode_Draw_mF1C88940B7BF41A505E79A2A0DEFAC1D9AE163FC (void);
// 0x00000017 System.Void Photon.Pun.UtilityScripts.CellTreeNode::GetActiveCells(System.Collections.Generic.List`1<System.Byte>,System.Boolean,UnityEngine.Vector3)
extern void CellTreeNode_GetActiveCells_mE59FC6E4917ACC44670437C93935BB4F41FEB53F (void);
// 0x00000018 System.Boolean Photon.Pun.UtilityScripts.CellTreeNode::IsPointInsideCell(System.Boolean,UnityEngine.Vector3)
extern void CellTreeNode_IsPointInsideCell_m6311D164BD6B6A8AC0C17D1FC2A31EE0AF96CBF4 (void);
// 0x00000019 System.Boolean Photon.Pun.UtilityScripts.CellTreeNode::IsPointNearCell(System.Boolean,UnityEngine.Vector3)
extern void CellTreeNode_IsPointNearCell_m1D70557A65B17CE5076244C94BD7A20E27EBF5CC (void);
// 0x0000001A System.Void Photon.Pun.UtilityScripts.CullingHandler::OnEnable()
extern void CullingHandler_OnEnable_mF1C97B6775F53992D554CDE33B1AB9F7480F812F (void);
// 0x0000001B System.Void Photon.Pun.UtilityScripts.CullingHandler::Start()
extern void CullingHandler_Start_m278F0FB85F7F677483F777D4C6060DE92030BAF1 (void);
// 0x0000001C System.Void Photon.Pun.UtilityScripts.CullingHandler::Update()
extern void CullingHandler_Update_m3CC460C74D1904A3E570F59FA0C7A654BFF841C9 (void);
// 0x0000001D System.Void Photon.Pun.UtilityScripts.CullingHandler::OnGUI()
extern void CullingHandler_OnGUI_m412388F0BF2D00894D4B87704BA29726DD4690FE (void);
// 0x0000001E System.Boolean Photon.Pun.UtilityScripts.CullingHandler::HaveActiveCellsChanged()
extern void CullingHandler_HaveActiveCellsChanged_mE5723D2871CE3D7099132AD75E648EE64ED2B95E (void);
// 0x0000001F System.Void Photon.Pun.UtilityScripts.CullingHandler::UpdateInterestGroups()
extern void CullingHandler_UpdateInterestGroups_m463B58F2AE762180F8020F8B8C785E5999C04C0F (void);
// 0x00000020 System.Void Photon.Pun.UtilityScripts.CullingHandler::OnPhotonSerializeView(Photon.Pun.PhotonStream,Photon.Pun.PhotonMessageInfo)
extern void CullingHandler_OnPhotonSerializeView_mDF45D42CB6F558FB6A89218BA9A5D7242026C5E5 (void);
// 0x00000021 System.Void Photon.Pun.UtilityScripts.CullingHandler::.ctor()
extern void CullingHandler__ctor_m21AF64968030CFFB3827198A7FFBCC5E7C147C36 (void);
// 0x00000022 ExitGames.Client.Photon.PhotonPeer Photon.Pun.UtilityScripts.PhotonLagSimulationGui::get_Peer()
extern void PhotonLagSimulationGui_get_Peer_mEA43D9BFEBBBC06D5BE4073B475F7F6762F013B5 (void);
// 0x00000023 System.Void Photon.Pun.UtilityScripts.PhotonLagSimulationGui::set_Peer(ExitGames.Client.Photon.PhotonPeer)
extern void PhotonLagSimulationGui_set_Peer_m14579466FABF5BAEDD2703D57F4CABB641BDD4D9 (void);
// 0x00000024 System.Void Photon.Pun.UtilityScripts.PhotonLagSimulationGui::Start()
extern void PhotonLagSimulationGui_Start_m0361078A955DEB2D880CED2B5536811E110D44DB (void);
// 0x00000025 System.Void Photon.Pun.UtilityScripts.PhotonLagSimulationGui::OnGUI()
extern void PhotonLagSimulationGui_OnGUI_m9AAC26AB96368BEE82599A4E713B2A6FF414776C (void);
// 0x00000026 System.Void Photon.Pun.UtilityScripts.PhotonLagSimulationGui::NetSimHasNoPeerWindow(System.Int32)
extern void PhotonLagSimulationGui_NetSimHasNoPeerWindow_m32165956DF907526D65F802352CF279F7075E0FD (void);
// 0x00000027 System.Void Photon.Pun.UtilityScripts.PhotonLagSimulationGui::NetSimWindow(System.Int32)
extern void PhotonLagSimulationGui_NetSimWindow_m83D2FB5CA3265991D0A766B16AD4C5A48EF61DB4 (void);
// 0x00000028 System.Void Photon.Pun.UtilityScripts.PhotonLagSimulationGui::.ctor()
extern void PhotonLagSimulationGui__ctor_m83F35AB7D40ED0FFA37A780873704BA7FC5E825A (void);
// 0x00000029 System.Void Photon.Pun.UtilityScripts.PhotonStatsGui::Start()
extern void PhotonStatsGui_Start_mE6EBA95416A254A5111165D528245689DD6DACD1 (void);
// 0x0000002A System.Void Photon.Pun.UtilityScripts.PhotonStatsGui::Update()
extern void PhotonStatsGui_Update_mD22063BD8B39911118995797758105995934EDA4 (void);
// 0x0000002B System.Void Photon.Pun.UtilityScripts.PhotonStatsGui::OnGUI()
extern void PhotonStatsGui_OnGUI_m9C400586F470FAA747F5F09368F1F72B3C28EEEF (void);
// 0x0000002C System.Void Photon.Pun.UtilityScripts.PhotonStatsGui::TrafficStatsWindow(System.Int32)
extern void PhotonStatsGui_TrafficStatsWindow_mB7D9830C83D6DA18457EF8D5F31EADA84C468040 (void);
// 0x0000002D System.Void Photon.Pun.UtilityScripts.PhotonStatsGui::.ctor()
extern void PhotonStatsGui__ctor_m88E1C95E343B4F0A115031F12D06241F930A4477 (void);
// 0x0000002E System.Void Photon.Pun.UtilityScripts.PointedAtGameObjectInfo::Start()
extern void PointedAtGameObjectInfo_Start_m9EF8695C852B3A37AF50A3818255468805383BEE (void);
// 0x0000002F System.Void Photon.Pun.UtilityScripts.PointedAtGameObjectInfo::SetFocus(Photon.Pun.PhotonView)
extern void PointedAtGameObjectInfo_SetFocus_mD8D3995AA506573F2E343D6AB623B27DB50B0627 (void);
// 0x00000030 System.Void Photon.Pun.UtilityScripts.PointedAtGameObjectInfo::RemoveFocus(Photon.Pun.PhotonView)
extern void PointedAtGameObjectInfo_RemoveFocus_m1BCE38D70D190D9B41640FB6DE9BCBBCA7576416 (void);
// 0x00000031 System.Void Photon.Pun.UtilityScripts.PointedAtGameObjectInfo::LateUpdate()
extern void PointedAtGameObjectInfo_LateUpdate_mE54B9566308674A1F14151994E452F59C208204D (void);
// 0x00000032 System.Void Photon.Pun.UtilityScripts.PointedAtGameObjectInfo::.ctor()
extern void PointedAtGameObjectInfo__ctor_mE40EC9EE75EF11314F21D9038969DAE5E9D083B3 (void);
// 0x00000033 System.Void Photon.Pun.UtilityScripts.StatesGui::Awake()
extern void StatesGui_Awake_mD149D187CC6C6ECE0751BC3C371A0566207D9EBE (void);
// 0x00000034 System.Void Photon.Pun.UtilityScripts.StatesGui::OnDisable()
extern void StatesGui_OnDisable_mD55DB381981CA14B6D03012432C0BDAEB885252C (void);
// 0x00000035 System.Void Photon.Pun.UtilityScripts.StatesGui::OnGUI()
extern void StatesGui_OnGUI_m772D24DD07DBF828407F4BE7D2F2171B7CC7906B (void);
// 0x00000036 System.String Photon.Pun.UtilityScripts.StatesGui::PlayerToString(Photon.Realtime.Player)
extern void StatesGui_PlayerToString_mEF430A7FEB15745730C024488F9BE069291801B1 (void);
// 0x00000037 System.Void Photon.Pun.UtilityScripts.StatesGui::.ctor()
extern void StatesGui__ctor_m85D2C6EB2C4BCB5E03F233B886DB98C0A87BB450 (void);
// 0x00000038 System.Void Photon.Pun.UtilityScripts.PlayerNumbering::add_OnPlayerNumberingChanged(Photon.Pun.UtilityScripts.PlayerNumbering/PlayerNumberingChanged)
extern void PlayerNumbering_add_OnPlayerNumberingChanged_mBB7EA60386DB8526A85C32733F78EBC8D9921BBF (void);
// 0x00000039 System.Void Photon.Pun.UtilityScripts.PlayerNumbering::remove_OnPlayerNumberingChanged(Photon.Pun.UtilityScripts.PlayerNumbering/PlayerNumberingChanged)
extern void PlayerNumbering_remove_OnPlayerNumberingChanged_mE3D19EDDE7AFC3178AFE7012BB4247C6347831C6 (void);
// 0x0000003A System.Void Photon.Pun.UtilityScripts.PlayerNumbering::Awake()
extern void PlayerNumbering_Awake_m7065EAE003AADA1A7AD748AED0D3393E21FCE717 (void);
// 0x0000003B System.Void Photon.Pun.UtilityScripts.PlayerNumbering::OnJoinedRoom()
extern void PlayerNumbering_OnJoinedRoom_mD8DE8FBB3722E8E468FB10BA20F7F86DC30537A0 (void);
// 0x0000003C System.Void Photon.Pun.UtilityScripts.PlayerNumbering::OnLeftRoom()
extern void PlayerNumbering_OnLeftRoom_m24F374499F413A9F83EE173989F94811745A738E (void);
// 0x0000003D System.Void Photon.Pun.UtilityScripts.PlayerNumbering::OnPlayerEnteredRoom(Photon.Realtime.Player)
extern void PlayerNumbering_OnPlayerEnteredRoom_mCB67FE8F1C8FFE14B2BBBE3FAC3024DE8069F1A5 (void);
// 0x0000003E System.Void Photon.Pun.UtilityScripts.PlayerNumbering::OnPlayerLeftRoom(Photon.Realtime.Player)
extern void PlayerNumbering_OnPlayerLeftRoom_m6532C5EA49C90C8D90D8F8BAEE82BAE698DB83B1 (void);
// 0x0000003F System.Void Photon.Pun.UtilityScripts.PlayerNumbering::OnPlayerPropertiesUpdate(Photon.Realtime.Player,ExitGames.Client.Photon.Hashtable)
extern void PlayerNumbering_OnPlayerPropertiesUpdate_m3107FB59A20928AACE643852332416235271D3D2 (void);
// 0x00000040 System.Void Photon.Pun.UtilityScripts.PlayerNumbering::RefreshData()
extern void PlayerNumbering_RefreshData_m575ECDFE79979ACE71F5C167D575291D7975C7A9 (void);
// 0x00000041 System.Void Photon.Pun.UtilityScripts.PlayerNumbering::.ctor()
extern void PlayerNumbering__ctor_mF93A1B29D2AFCD0B203F3085D55F9FB8F680F49C (void);
// 0x00000042 System.Void Photon.Pun.UtilityScripts.PlayerNumbering/PlayerNumberingChanged::.ctor(System.Object,System.IntPtr)
extern void PlayerNumberingChanged__ctor_mFFC3495BE543768BFC627729FCB76439A63036AA (void);
// 0x00000043 System.Void Photon.Pun.UtilityScripts.PlayerNumbering/PlayerNumberingChanged::Invoke()
extern void PlayerNumberingChanged_Invoke_mA3FA5A2037749852ED0FB05E4497A23A4FC48334 (void);
// 0x00000044 System.IAsyncResult Photon.Pun.UtilityScripts.PlayerNumbering/PlayerNumberingChanged::BeginInvoke(System.AsyncCallback,System.Object)
extern void PlayerNumberingChanged_BeginInvoke_m46DCFE2333973B1F7A8BCB3E3909AD3D64B5E0CA (void);
// 0x00000045 System.Void Photon.Pun.UtilityScripts.PlayerNumbering/PlayerNumberingChanged::EndInvoke(System.IAsyncResult)
extern void PlayerNumberingChanged_EndInvoke_m8C16A15C08AF4F9D04D096513BCD5DEEDDFA393C (void);
// 0x00000046 System.Void Photon.Pun.UtilityScripts.PlayerNumbering/<>c::.cctor()
extern void U3CU3Ec__cctor_m293375C60F48F8EE702D464C16D130CEECA21549 (void);
// 0x00000047 System.Void Photon.Pun.UtilityScripts.PlayerNumbering/<>c::.ctor()
extern void U3CU3Ec__ctor_m610B526629D5D6AFBFE7A36F0670B2656C66817A (void);
// 0x00000048 System.Int32 Photon.Pun.UtilityScripts.PlayerNumbering/<>c::<RefreshData>b__14_0(Photon.Realtime.Player)
extern void U3CU3Ec_U3CRefreshDataU3Eb__14_0_mFCD93BC13C2C3A03DACA09A38F5CEEEB9DE1587F (void);
// 0x00000049 System.Int32 Photon.Pun.UtilityScripts.PlayerNumbering/<>c::<RefreshData>b__14_1(Photon.Realtime.Player)
extern void U3CU3Ec_U3CRefreshDataU3Eb__14_1_mF9141A84EAF61C21CE7241C6FE7D452CB23779F2 (void);
// 0x0000004A System.Int32 Photon.Pun.UtilityScripts.PlayerNumbering/<>c::<RefreshData>b__14_2(Photon.Realtime.Player)
extern void U3CU3Ec_U3CRefreshDataU3Eb__14_2_m37048553001F7BE6EE2C158742A95FFC0DAB696C (void);
// 0x0000004B System.Int32 Photon.Pun.UtilityScripts.PlayerNumberingExtensions::GetPlayerNumber(Photon.Realtime.Player)
extern void PlayerNumberingExtensions_GetPlayerNumber_m0A03E46AD4CA342C7681117EF0B636461D9FD47E (void);
// 0x0000004C System.Void Photon.Pun.UtilityScripts.PlayerNumberingExtensions::SetPlayerNumber(Photon.Realtime.Player,System.Int32)
extern void PlayerNumberingExtensions_SetPlayerNumber_m150612B8CE9CB30A4564B0DA79D865851CDD9026 (void);
// 0x0000004D System.Void Photon.Pun.UtilityScripts.PunPlayerScores::.ctor()
extern void PunPlayerScores__ctor_m331D8AE116668F796CADBF6280922E26CAE6EAAB (void);
// 0x0000004E System.Void Photon.Pun.UtilityScripts.ScoreExtensions::SetScore(Photon.Realtime.Player,System.Int32)
extern void ScoreExtensions_SetScore_m0F31F48E615433EDFA3673419F62BEED5215B240 (void);
// 0x0000004F System.Void Photon.Pun.UtilityScripts.ScoreExtensions::AddScore(Photon.Realtime.Player,System.Int32)
extern void ScoreExtensions_AddScore_m6D8E5D41F1FD0887665A2F75CC543E1104793448 (void);
// 0x00000050 System.Int32 Photon.Pun.UtilityScripts.ScoreExtensions::GetScore(Photon.Realtime.Player)
extern void ScoreExtensions_GetScore_m60C4E4920BF40136BEE7F90FD007FA696036ED99 (void);
// 0x00000051 System.Void Photon.Pun.UtilityScripts.PunTeams::Start()
extern void PunTeams_Start_m322E02290FA706333633F25171E2A0BAEF8938BC (void);
// 0x00000052 System.Void Photon.Pun.UtilityScripts.PunTeams::OnDisable()
extern void PunTeams_OnDisable_m96950151F04F8063D82B37ECA82FA63E7DA4103B (void);
// 0x00000053 System.Void Photon.Pun.UtilityScripts.PunTeams::OnJoinedRoom()
extern void PunTeams_OnJoinedRoom_m5C8D6B504AC4B77268E22EC10284C4DF5A25088F (void);
// 0x00000054 System.Void Photon.Pun.UtilityScripts.PunTeams::OnLeftRoom()
extern void PunTeams_OnLeftRoom_m5FCBFBB5B656565AA6B613313144263283374239 (void);
// 0x00000055 System.Void Photon.Pun.UtilityScripts.PunTeams::OnPlayerPropertiesUpdate(Photon.Realtime.Player,ExitGames.Client.Photon.Hashtable)
extern void PunTeams_OnPlayerPropertiesUpdate_m328CBA4BC5C5CE9AA388D817076829D3E822E112 (void);
// 0x00000056 System.Void Photon.Pun.UtilityScripts.PunTeams::OnPlayerLeftRoom(Photon.Realtime.Player)
extern void PunTeams_OnPlayerLeftRoom_m5805D79FED7C9E04116F8505DB7963247FC6A16F (void);
// 0x00000057 System.Void Photon.Pun.UtilityScripts.PunTeams::OnPlayerEnteredRoom(Photon.Realtime.Player)
extern void PunTeams_OnPlayerEnteredRoom_mFC57EBF6C836D9E9AEEC50B0F7ECBEF5009290D4 (void);
// 0x00000058 System.Void Photon.Pun.UtilityScripts.PunTeams::UpdateTeams()
extern void PunTeams_UpdateTeams_m90430BF45EF62AE670C29F643878A8C2FF4C2D45 (void);
// 0x00000059 System.Void Photon.Pun.UtilityScripts.PunTeams::.ctor()
extern void PunTeams__ctor_m1587202531D427931E50682EDF01DC81F2494459 (void);
// 0x0000005A Photon.Pun.UtilityScripts.PunTeams/Team Photon.Pun.UtilityScripts.TeamExtensions::GetTeam(Photon.Realtime.Player)
extern void TeamExtensions_GetTeam_m0623E10A26876105B719FE5BA3E85FD0F0934899 (void);
// 0x0000005B System.Void Photon.Pun.UtilityScripts.TeamExtensions::SetTeam(Photon.Realtime.Player,Photon.Pun.UtilityScripts.PunTeams/Team)
extern void TeamExtensions_SetTeam_mFDFF1A7047108153D81CA1ECE7C151F869BB9F48 (void);
// 0x0000005C System.Void Photon.Pun.UtilityScripts.SmoothSyncMovement::Awake()
extern void SmoothSyncMovement_Awake_m83BFD699FBA423D41B6FCC3C93E64571D254D235 (void);
// 0x0000005D System.Void Photon.Pun.UtilityScripts.SmoothSyncMovement::OnPhotonSerializeView(Photon.Pun.PhotonStream,Photon.Pun.PhotonMessageInfo)
extern void SmoothSyncMovement_OnPhotonSerializeView_mEC74D31532288A4692E9DB2EB00D1D10FA083EE6 (void);
// 0x0000005E System.Void Photon.Pun.UtilityScripts.SmoothSyncMovement::Update()
extern void SmoothSyncMovement_Update_m7F239108ABF0F150F8A4176DBCE42859F60BA9A2 (void);
// 0x0000005F System.Void Photon.Pun.UtilityScripts.SmoothSyncMovement::.ctor()
extern void SmoothSyncMovement__ctor_mCDFB1CEDE12D01589A10C5EFB3A87F696BEBFE26 (void);
// 0x00000060 System.Void Photon.Pun.UtilityScripts.ConnectAndJoinRandom::Start()
extern void ConnectAndJoinRandom_Start_m4A56759A88980B826FC0BEF46BD2A7B9E9F85C08 (void);
// 0x00000061 System.Void Photon.Pun.UtilityScripts.ConnectAndJoinRandom::ConnectNow()
extern void ConnectAndJoinRandom_ConnectNow_mBBE27554A21F79BF8A479784037E6E0CB11D443A (void);
// 0x00000062 System.Void Photon.Pun.UtilityScripts.ConnectAndJoinRandom::OnConnectedToMaster()
extern void ConnectAndJoinRandom_OnConnectedToMaster_m8E8D6401382B397A127B6D79B145F27B05196324 (void);
// 0x00000063 System.Void Photon.Pun.UtilityScripts.ConnectAndJoinRandom::OnJoinedLobby()
extern void ConnectAndJoinRandom_OnJoinedLobby_mD609A960BD57F04E478EC0F74CD413689DD3010F (void);
// 0x00000064 System.Void Photon.Pun.UtilityScripts.ConnectAndJoinRandom::OnJoinRandomFailed(System.Int16,System.String)
extern void ConnectAndJoinRandom_OnJoinRandomFailed_m544DF7DA0A6D37EE5B29AED67435382E1CFAFEC1 (void);
// 0x00000065 System.Void Photon.Pun.UtilityScripts.ConnectAndJoinRandom::OnDisconnected(Photon.Realtime.DisconnectCause)
extern void ConnectAndJoinRandom_OnDisconnected_mEF1F9716B0234D6669D776F795648154F14531B9 (void);
// 0x00000066 System.Void Photon.Pun.UtilityScripts.ConnectAndJoinRandom::OnJoinedRoom()
extern void ConnectAndJoinRandom_OnJoinedRoom_mACD2B57642AD66BFCB6DED0C2149B114A5958756 (void);
// 0x00000067 System.Void Photon.Pun.UtilityScripts.ConnectAndJoinRandom::.ctor()
extern void ConnectAndJoinRandom__ctor_m2E6133129FEAB84291168E24EF1752A040D65AD0 (void);
// 0x00000068 System.Void Photon.Pun.UtilityScripts.MoveByKeys::Start()
extern void MoveByKeys_Start_mBA857B269EB1CC4ECCB8A6B47EDE4C0645C65063 (void);
// 0x00000069 System.Void Photon.Pun.UtilityScripts.MoveByKeys::FixedUpdate()
extern void MoveByKeys_FixedUpdate_m5B1B39A4F22E3D59E1D49F6A98CECE6F7B0858C3 (void);
// 0x0000006A System.Void Photon.Pun.UtilityScripts.MoveByKeys::.ctor()
extern void MoveByKeys__ctor_m3F10B2047381BCFC3C22B2319A74A3CC76A362BC (void);
// 0x0000006B System.Void Photon.Pun.UtilityScripts.OnClickDestroy::UnityEngine.EventSystems.IPointerClickHandler.OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void OnClickDestroy_UnityEngine_EventSystems_IPointerClickHandler_OnPointerClick_m832D149EF3C6D7EE5388A733A3F22DE791ABDF51 (void);
// 0x0000006C System.Collections.IEnumerator Photon.Pun.UtilityScripts.OnClickDestroy::DestroyRpc()
extern void OnClickDestroy_DestroyRpc_m857F90BFDEE9D447362D71EC2875B7668D7814B9 (void);
// 0x0000006D System.Void Photon.Pun.UtilityScripts.OnClickDestroy::.ctor()
extern void OnClickDestroy__ctor_m95E15ED7BCD9B35607A3CDCC77CB48CE8E4ED176 (void);
// 0x0000006E System.Void Photon.Pun.UtilityScripts.OnClickDestroy/<DestroyRpc>d__4::.ctor(System.Int32)
extern void U3CDestroyRpcU3Ed__4__ctor_m621B9190F6FE020A6C624A2B74963F09BE7EE457 (void);
// 0x0000006F System.Void Photon.Pun.UtilityScripts.OnClickDestroy/<DestroyRpc>d__4::System.IDisposable.Dispose()
extern void U3CDestroyRpcU3Ed__4_System_IDisposable_Dispose_m2331D4395BBC0230414B7195785C314F4813E174 (void);
// 0x00000070 System.Boolean Photon.Pun.UtilityScripts.OnClickDestroy/<DestroyRpc>d__4::MoveNext()
extern void U3CDestroyRpcU3Ed__4_MoveNext_mC0C89CB2D97AF92CFE532A7B7999594682D7DDB9 (void);
// 0x00000071 System.Object Photon.Pun.UtilityScripts.OnClickDestroy/<DestroyRpc>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDestroyRpcU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0A15F547DED847B3415F25FEEDFC02942275E4AC (void);
// 0x00000072 System.Void Photon.Pun.UtilityScripts.OnClickDestroy/<DestroyRpc>d__4::System.Collections.IEnumerator.Reset()
extern void U3CDestroyRpcU3Ed__4_System_Collections_IEnumerator_Reset_m6BF2FE10CF8171BBA7DDB8D2D4497A77E3D25F39 (void);
// 0x00000073 System.Object Photon.Pun.UtilityScripts.OnClickDestroy/<DestroyRpc>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CDestroyRpcU3Ed__4_System_Collections_IEnumerator_get_Current_mA30CE51561261565206BAB8B90923C29459A67B9 (void);
// 0x00000074 System.Void Photon.Pun.UtilityScripts.OnClickInstantiate::UnityEngine.EventSystems.IPointerClickHandler.OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void OnClickInstantiate_UnityEngine_EventSystems_IPointerClickHandler_OnPointerClick_mD50C462E8BDBBAC654C0A1F565C21F44D4714095 (void);
// 0x00000075 System.Void Photon.Pun.UtilityScripts.OnClickInstantiate::.ctor()
extern void OnClickInstantiate__ctor_m0B49CC01530FAE3098EE264B5D4E6F148FC783B7 (void);
// 0x00000076 System.Void Photon.Pun.UtilityScripts.OnClickRpc::UnityEngine.EventSystems.IPointerClickHandler.OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void OnClickRpc_UnityEngine_EventSystems_IPointerClickHandler_OnPointerClick_m7443A2B85611870FDF1D2C14428D8C6CB5D0ECB7 (void);
// 0x00000077 System.Void Photon.Pun.UtilityScripts.OnClickRpc::ClickRpc()
extern void OnClickRpc_ClickRpc_m89E074536BC1C02E15F9360D2B1873DB905772CD (void);
// 0x00000078 System.Collections.IEnumerator Photon.Pun.UtilityScripts.OnClickRpc::ClickFlash()
extern void OnClickRpc_ClickFlash_m7087F3D9C3F6048E41D1FDF03CCFB64F9ACE5CFB (void);
// 0x00000079 System.Void Photon.Pun.UtilityScripts.OnClickRpc::.ctor()
extern void OnClickRpc__ctor_mE6A964D86B5B162C95BC83C7803FA812594449D1 (void);
// 0x0000007A System.Void Photon.Pun.UtilityScripts.OnClickRpc/<ClickFlash>d__8::.ctor(System.Int32)
extern void U3CClickFlashU3Ed__8__ctor_m2A93E4DACCC264BA2BA525E718E45B4F01C27525 (void);
// 0x0000007B System.Void Photon.Pun.UtilityScripts.OnClickRpc/<ClickFlash>d__8::System.IDisposable.Dispose()
extern void U3CClickFlashU3Ed__8_System_IDisposable_Dispose_mD050F87E615A7E03EE0EF3B947E9E7B5D6DC0E3E (void);
// 0x0000007C System.Boolean Photon.Pun.UtilityScripts.OnClickRpc/<ClickFlash>d__8::MoveNext()
extern void U3CClickFlashU3Ed__8_MoveNext_mDE5F7B9C67C8BE74D129C8A3297EBD35E7707BA3 (void);
// 0x0000007D System.Object Photon.Pun.UtilityScripts.OnClickRpc/<ClickFlash>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CClickFlashU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAC1ABADE4341BD6EAB1D3D93723567E3BA61A0E3 (void);
// 0x0000007E System.Void Photon.Pun.UtilityScripts.OnClickRpc/<ClickFlash>d__8::System.Collections.IEnumerator.Reset()
extern void U3CClickFlashU3Ed__8_System_Collections_IEnumerator_Reset_m62838FA6374A3EB45BA08ED9BECE91F3B73B3A43 (void);
// 0x0000007F System.Object Photon.Pun.UtilityScripts.OnClickRpc/<ClickFlash>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CClickFlashU3Ed__8_System_Collections_IEnumerator_get_Current_mD694E610E2C416ACF79BEB73DA3723C1F6D5D1E0 (void);
// 0x00000080 System.Void Photon.Pun.UtilityScripts.OnEscapeQuit::Update()
extern void OnEscapeQuit_Update_mC2365FFC05E56349E2072921FD143944ECD72080 (void);
// 0x00000081 System.Void Photon.Pun.UtilityScripts.OnEscapeQuit::.ctor()
extern void OnEscapeQuit__ctor_m054775A6FA431DC5BB15B2CB5D036749BC7DE27A (void);
// 0x00000082 System.Void Photon.Pun.UtilityScripts.OnJoinedInstantiate::OnEnable()
extern void OnJoinedInstantiate_OnEnable_m18E2FA692A52B5382CB32975692FBFE12450FEA1 (void);
// 0x00000083 System.Void Photon.Pun.UtilityScripts.OnJoinedInstantiate::OnDisable()
extern void OnJoinedInstantiate_OnDisable_m867DFC9561B8AA365F9682E26D92042A66BDC0BE (void);
// 0x00000084 System.Void Photon.Pun.UtilityScripts.OnJoinedInstantiate::OnJoinedRoom()
extern void OnJoinedInstantiate_OnJoinedRoom_mF5A3064BB84ACE67139C6D38560EC94253155711 (void);
// 0x00000085 System.Void Photon.Pun.UtilityScripts.OnJoinedInstantiate::OnConnected()
extern void OnJoinedInstantiate_OnConnected_m59250D1C6D4621C8B3BEB26796C792CD7896847C (void);
// 0x00000086 System.Void Photon.Pun.UtilityScripts.OnJoinedInstantiate::OnCustomAuthenticationResponse(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void OnJoinedInstantiate_OnCustomAuthenticationResponse_mF644274CCC937BF2693BE0521C80C01D79F7332E (void);
// 0x00000087 System.Void Photon.Pun.UtilityScripts.OnJoinedInstantiate::OnCustomAuthenticationFailed(System.String)
extern void OnJoinedInstantiate_OnCustomAuthenticationFailed_m3497ACB0B57FB589448799632013427BCBDADB79 (void);
// 0x00000088 System.Void Photon.Pun.UtilityScripts.OnJoinedInstantiate::OnConnectedToMaster()
extern void OnJoinedInstantiate_OnConnectedToMaster_m9BF6E404BD5565B513AB632884791E7D29473E23 (void);
// 0x00000089 System.Void Photon.Pun.UtilityScripts.OnJoinedInstantiate::OnDisconnected(Photon.Realtime.DisconnectCause)
extern void OnJoinedInstantiate_OnDisconnected_m6BC6D9CD5FFE068C723E0310AB190829C3F33EDC (void);
// 0x0000008A System.Void Photon.Pun.UtilityScripts.OnJoinedInstantiate::OnRegionListReceived(Photon.Realtime.RegionHandler)
extern void OnJoinedInstantiate_OnRegionListReceived_m2551386FEC329FFCC926F15E09E4BE7ADD628E14 (void);
// 0x0000008B System.Void Photon.Pun.UtilityScripts.OnJoinedInstantiate::OnRoomListUpdate(System.Collections.Generic.List`1<Photon.Realtime.RoomInfo>)
extern void OnJoinedInstantiate_OnRoomListUpdate_m409A10D62950B4E8146E431CD805DF8CD7DA5179 (void);
// 0x0000008C System.Void Photon.Pun.UtilityScripts.OnJoinedInstantiate::OnFriendListUpdate(System.Collections.Generic.List`1<Photon.Realtime.FriendInfo>)
extern void OnJoinedInstantiate_OnFriendListUpdate_m21B9A98A96A70A7DE9D48D1D01B9016AFF8FD6B6 (void);
// 0x0000008D System.Void Photon.Pun.UtilityScripts.OnJoinedInstantiate::OnJoinedLobby()
extern void OnJoinedInstantiate_OnJoinedLobby_m6261701DF1364A787884DF3C786F357987126963 (void);
// 0x0000008E System.Void Photon.Pun.UtilityScripts.OnJoinedInstantiate::OnLeftLobby()
extern void OnJoinedInstantiate_OnLeftLobby_mA6D379302CD454E3481B7761F2F1599E54A25CB6 (void);
// 0x0000008F System.Void Photon.Pun.UtilityScripts.OnJoinedInstantiate::OnLobbyStatisticsUpdate(System.Collections.Generic.List`1<Photon.Realtime.TypedLobbyInfo>)
extern void OnJoinedInstantiate_OnLobbyStatisticsUpdate_m0CF4E02032C3D881DA917CE23F517A4FF1C2ACDB (void);
// 0x00000090 System.Void Photon.Pun.UtilityScripts.OnJoinedInstantiate::OnCreatedRoom()
extern void OnJoinedInstantiate_OnCreatedRoom_m691D7527DA1C584ED25AD9115319D1BE9C8A442E (void);
// 0x00000091 System.Void Photon.Pun.UtilityScripts.OnJoinedInstantiate::OnCreateRoomFailed(System.Int16,System.String)
extern void OnJoinedInstantiate_OnCreateRoomFailed_m47344AA6EF45CC54400059B2C1C45C66C0541960 (void);
// 0x00000092 System.Void Photon.Pun.UtilityScripts.OnJoinedInstantiate::OnJoinRoomFailed(System.Int16,System.String)
extern void OnJoinedInstantiate_OnJoinRoomFailed_mACC5A9F8E97CCE343D481572E7D81CFE722CB610 (void);
// 0x00000093 System.Void Photon.Pun.UtilityScripts.OnJoinedInstantiate::OnJoinRandomFailed(System.Int16,System.String)
extern void OnJoinedInstantiate_OnJoinRandomFailed_mA83F1B9375B6AA3007F16FA5DCCB8B119770EBB3 (void);
// 0x00000094 System.Void Photon.Pun.UtilityScripts.OnJoinedInstantiate::OnLeftRoom()
extern void OnJoinedInstantiate_OnLeftRoom_mC4FFC6FAC4BAF1303E2F9C60F9EF1AF5A94B05A2 (void);
// 0x00000095 System.Void Photon.Pun.UtilityScripts.OnJoinedInstantiate::.ctor()
extern void OnJoinedInstantiate__ctor_m64DCA56A5530A370271563A22566198012092129 (void);
// 0x00000096 System.Void Photon.Pun.UtilityScripts.OnStartDelete::Start()
extern void OnStartDelete_Start_m4D4A3F37EDAA5AF862BB1A6915065C4DA43E06E8 (void);
// 0x00000097 System.Void Photon.Pun.UtilityScripts.OnStartDelete::.ctor()
extern void OnStartDelete__ctor_m858C26A4F9A077DD08B79D1A49337A4BCFDE1098 (void);
// 0x00000098 System.Void Photon.Pun.UtilityScripts.CountdownTimer::add_OnCountdownTimerHasExpired(Photon.Pun.UtilityScripts.CountdownTimer/CountdownTimerHasExpired)
extern void CountdownTimer_add_OnCountdownTimerHasExpired_mF6580A850B0E01A5ADE2F16FB53D17153040054F (void);
// 0x00000099 System.Void Photon.Pun.UtilityScripts.CountdownTimer::remove_OnCountdownTimerHasExpired(Photon.Pun.UtilityScripts.CountdownTimer/CountdownTimerHasExpired)
extern void CountdownTimer_remove_OnCountdownTimerHasExpired_m151E3DC7B8ACFE1D6E1CCAE513B7EDB8D5892E9A (void);
// 0x0000009A System.Void Photon.Pun.UtilityScripts.CountdownTimer::Start()
extern void CountdownTimer_Start_mD6BA05D6038F4BBD03AA753B722FD123581E12A1 (void);
// 0x0000009B System.Void Photon.Pun.UtilityScripts.CountdownTimer::Update()
extern void CountdownTimer_Update_m609D6AF08FFDC6BD0E78E33F86D051B8BE5E8E50 (void);
// 0x0000009C System.Void Photon.Pun.UtilityScripts.CountdownTimer::OnRoomPropertiesUpdate(ExitGames.Client.Photon.Hashtable)
extern void CountdownTimer_OnRoomPropertiesUpdate_m373B1B365413351650694B4AF2355E345F39C087 (void);
// 0x0000009D System.Void Photon.Pun.UtilityScripts.CountdownTimer::.ctor()
extern void CountdownTimer__ctor_m0F8939C4FA24EF1C2FCCEA485ACD05D7F7FD453D (void);
// 0x0000009E System.Void Photon.Pun.UtilityScripts.CountdownTimer/CountdownTimerHasExpired::.ctor(System.Object,System.IntPtr)
extern void CountdownTimerHasExpired__ctor_m08C1521F106E76342333DAD320A3E7F1EB9FAE9F (void);
// 0x0000009F System.Void Photon.Pun.UtilityScripts.CountdownTimer/CountdownTimerHasExpired::Invoke()
extern void CountdownTimerHasExpired_Invoke_m8D5690A759DD53CD0DB1923D7DC33B22776B9279 (void);
// 0x000000A0 System.IAsyncResult Photon.Pun.UtilityScripts.CountdownTimer/CountdownTimerHasExpired::BeginInvoke(System.AsyncCallback,System.Object)
extern void CountdownTimerHasExpired_BeginInvoke_mFC0BF6CB1A65B012EEF8BDE00E6B86FE3FE25707 (void);
// 0x000000A1 System.Void Photon.Pun.UtilityScripts.CountdownTimer/CountdownTimerHasExpired::EndInvoke(System.IAsyncResult)
extern void CountdownTimerHasExpired_EndInvoke_m09ED2AAA732CC70C0096F081A3629230631561EF (void);
// 0x000000A2 System.Int32 Photon.Pun.UtilityScripts.PunTurnManager::get_Turn()
extern void PunTurnManager_get_Turn_m1293DE05C5440CA33F5BD293E50E487F70EAC966 (void);
// 0x000000A3 System.Void Photon.Pun.UtilityScripts.PunTurnManager::set_Turn(System.Int32)
extern void PunTurnManager_set_Turn_m2635EFCF67DDD0B8781A097F56AEC0C172016099 (void);
// 0x000000A4 System.Single Photon.Pun.UtilityScripts.PunTurnManager::get_ElapsedTimeInTurn()
extern void PunTurnManager_get_ElapsedTimeInTurn_m8D2F2D914C6E4603A09AC02FF20B34C6E071A84E (void);
// 0x000000A5 System.Single Photon.Pun.UtilityScripts.PunTurnManager::get_RemainingSecondsInTurn()
extern void PunTurnManager_get_RemainingSecondsInTurn_mD8AD1D72E248AB9AC2F7B73BCF4640FA6E4F0C91 (void);
// 0x000000A6 System.Boolean Photon.Pun.UtilityScripts.PunTurnManager::get_IsCompletedByAll()
extern void PunTurnManager_get_IsCompletedByAll_m388FC57B015D8A9D46553CCA3EC4CB6269DAA79E (void);
// 0x000000A7 System.Boolean Photon.Pun.UtilityScripts.PunTurnManager::get_IsFinishedByMe()
extern void PunTurnManager_get_IsFinishedByMe_mE6268289C028688F573E40CE692B382872F49A88 (void);
// 0x000000A8 System.Boolean Photon.Pun.UtilityScripts.PunTurnManager::get_IsOver()
extern void PunTurnManager_get_IsOver_m7059AC986F6BC718F4EC928A7CEDD9C3A6CC60BE (void);
// 0x000000A9 System.Void Photon.Pun.UtilityScripts.PunTurnManager::Start()
extern void PunTurnManager_Start_mB44B031AA0154F4F7BC850945A034F4EACF8A02E (void);
// 0x000000AA System.Void Photon.Pun.UtilityScripts.PunTurnManager::Update()
extern void PunTurnManager_Update_mF0E1FB736401D4CEDF00205DE58E4C9AE0EB322B (void);
// 0x000000AB System.Void Photon.Pun.UtilityScripts.PunTurnManager::BeginTurn()
extern void PunTurnManager_BeginTurn_m65B48FC9B2EDD1CE017A64EE680DA0DC57501E5C (void);
// 0x000000AC System.Void Photon.Pun.UtilityScripts.PunTurnManager::SendMove(System.Object,System.Boolean)
extern void PunTurnManager_SendMove_m55E17CBC71FEFE845AE01DE31897B85316679276 (void);
// 0x000000AD System.Boolean Photon.Pun.UtilityScripts.PunTurnManager::GetPlayerFinishedTurn(Photon.Realtime.Player)
extern void PunTurnManager_GetPlayerFinishedTurn_mEEEB696A63EB5C397D26C8C58586873C4B697A24 (void);
// 0x000000AE System.Void Photon.Pun.UtilityScripts.PunTurnManager::ProcessOnEvent(System.Byte,System.Object,System.Int32)
extern void PunTurnManager_ProcessOnEvent_mBE02B41E074D418910EAEAAC391849CE13EA08A9 (void);
// 0x000000AF System.Void Photon.Pun.UtilityScripts.PunTurnManager::OnEvent(ExitGames.Client.Photon.EventData)
extern void PunTurnManager_OnEvent_mF999A8F6CB506BAA1F3CAE4D3715C8A700DFC313 (void);
// 0x000000B0 System.Void Photon.Pun.UtilityScripts.PunTurnManager::OnRoomPropertiesUpdate(ExitGames.Client.Photon.Hashtable)
extern void PunTurnManager_OnRoomPropertiesUpdate_mCF2E80B4C1C3A10F2E507C08953447D474D14D1E (void);
// 0x000000B1 System.Void Photon.Pun.UtilityScripts.PunTurnManager::.ctor()
extern void PunTurnManager__ctor_m7777470A5AC0A0BFF5445408C9359EDEBEDB7A8E (void);
// 0x000000B2 System.Void Photon.Pun.UtilityScripts.IPunTurnManagerCallbacks::OnTurnBegins(System.Int32)
// 0x000000B3 System.Void Photon.Pun.UtilityScripts.IPunTurnManagerCallbacks::OnTurnCompleted(System.Int32)
// 0x000000B4 System.Void Photon.Pun.UtilityScripts.IPunTurnManagerCallbacks::OnPlayerMove(Photon.Realtime.Player,System.Int32,System.Object)
// 0x000000B5 System.Void Photon.Pun.UtilityScripts.IPunTurnManagerCallbacks::OnPlayerFinished(Photon.Realtime.Player,System.Int32,System.Object)
// 0x000000B6 System.Void Photon.Pun.UtilityScripts.IPunTurnManagerCallbacks::OnTurnTimeEnds(System.Int32)
// 0x000000B7 System.Void Photon.Pun.UtilityScripts.TurnExtensions::SetTurn(Photon.Realtime.Room,System.Int32,System.Boolean)
extern void TurnExtensions_SetTurn_m45E586C445EFFDBCAD5776A3B04B2180A5E529DA (void);
// 0x000000B8 System.Int32 Photon.Pun.UtilityScripts.TurnExtensions::GetTurn(Photon.Realtime.RoomInfo)
extern void TurnExtensions_GetTurn_mFB9AE18C0A2CC0ED3737A0E5FB3C10BAE936DA7E (void);
// 0x000000B9 System.Int32 Photon.Pun.UtilityScripts.TurnExtensions::GetTurnStart(Photon.Realtime.RoomInfo)
extern void TurnExtensions_GetTurnStart_m69B98B4B7B10F319364C1ABA866C3F92659CF2AA (void);
// 0x000000BA System.Int32 Photon.Pun.UtilityScripts.TurnExtensions::GetFinishedTurn(Photon.Realtime.Player)
extern void TurnExtensions_GetFinishedTurn_mDC6FB3EAB8098F19C13B823E44FA2F263731B03B (void);
// 0x000000BB System.Void Photon.Pun.UtilityScripts.TurnExtensions::SetFinishedTurn(Photon.Realtime.Player,System.Int32)
extern void TurnExtensions_SetFinishedTurn_mF11DDA1AC3EE81D3F7F48E327522945DBE2EC197 (void);
// 0x000000BC System.Void Photon.Pun.UtilityScripts.TurnExtensions::.cctor()
extern void TurnExtensions__cctor_m3EE89F8E0A15684D0526D3DB40F0413422CEFC4B (void);
// 0x000000BD System.Void Photon.Pun.UtilityScripts.ButtonInsideScrollList::Start()
extern void ButtonInsideScrollList_Start_mB6A36575850C3D148474C18CB5F698C66CC32D1E (void);
// 0x000000BE System.Void Photon.Pun.UtilityScripts.ButtonInsideScrollList::UnityEngine.EventSystems.IPointerDownHandler.OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void ButtonInsideScrollList_UnityEngine_EventSystems_IPointerDownHandler_OnPointerDown_mEF76B47CABC7A2DCFE2CC15082D078870FF5C763 (void);
// 0x000000BF System.Void Photon.Pun.UtilityScripts.ButtonInsideScrollList::UnityEngine.EventSystems.IPointerUpHandler.OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void ButtonInsideScrollList_UnityEngine_EventSystems_IPointerUpHandler_OnPointerUp_m7A8F9AFF81E97366BAE4B369B6413C5AE045D18F (void);
// 0x000000C0 System.Void Photon.Pun.UtilityScripts.ButtonInsideScrollList::.ctor()
extern void ButtonInsideScrollList__ctor_mAEADBDBAA78C39BEA2AADB373460E76F8A227F2F (void);
// 0x000000C1 System.Void Photon.Pun.UtilityScripts.EventSystemSpawner::OnEnable()
extern void EventSystemSpawner_OnEnable_m4CE18E16F6FA0066C219B1EF4AA40016CE87CCC6 (void);
// 0x000000C2 System.Void Photon.Pun.UtilityScripts.EventSystemSpawner::.ctor()
extern void EventSystemSpawner__ctor_m3CA059DCD101FEA607ABA6B75D26A21030305A0F (void);
// 0x000000C3 System.Void Photon.Pun.UtilityScripts.GraphicToggleIsOnTransition::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void GraphicToggleIsOnTransition_OnPointerEnter_mCF385DB2983AFE7A5B8FF7B9694AC706AEFB3F0A (void);
// 0x000000C4 System.Void Photon.Pun.UtilityScripts.GraphicToggleIsOnTransition::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void GraphicToggleIsOnTransition_OnPointerExit_m72A25EF71036CB9250918A31260C3040E5350C74 (void);
// 0x000000C5 System.Void Photon.Pun.UtilityScripts.GraphicToggleIsOnTransition::OnEnable()
extern void GraphicToggleIsOnTransition_OnEnable_mD6156FF4490D8FB49414C553F651E4735C6AC9A2 (void);
// 0x000000C6 System.Void Photon.Pun.UtilityScripts.GraphicToggleIsOnTransition::OnDisable()
extern void GraphicToggleIsOnTransition_OnDisable_m84B1000D5A61334FC178F61AA1336E9B48700B76 (void);
// 0x000000C7 System.Void Photon.Pun.UtilityScripts.GraphicToggleIsOnTransition::OnValueChanged(System.Boolean)
extern void GraphicToggleIsOnTransition_OnValueChanged_m4D0B9067515692B515B12D92889FC8ABE085836B (void);
// 0x000000C8 System.Void Photon.Pun.UtilityScripts.GraphicToggleIsOnTransition::.ctor()
extern void GraphicToggleIsOnTransition__ctor_m653CE3A04A15A63CD29EBBDEFC561A96D02FA6DD (void);
// 0x000000C9 System.Void Photon.Pun.UtilityScripts.OnPointerOverTooltip::OnDestroy()
extern void OnPointerOverTooltip_OnDestroy_m9C054E052D7BC96FC40BE152BB2238C2C4423618 (void);
// 0x000000CA System.Void Photon.Pun.UtilityScripts.OnPointerOverTooltip::UnityEngine.EventSystems.IPointerExitHandler.OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void OnPointerOverTooltip_UnityEngine_EventSystems_IPointerExitHandler_OnPointerExit_m37892FBEA7AE19256A2D3C4E10DCF1F1F0A8A190 (void);
// 0x000000CB System.Void Photon.Pun.UtilityScripts.OnPointerOverTooltip::UnityEngine.EventSystems.IPointerEnterHandler.OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void OnPointerOverTooltip_UnityEngine_EventSystems_IPointerEnterHandler_OnPointerEnter_m860A8F9148CD2C2CEB073EE83A22629016C7B77F (void);
// 0x000000CC System.Void Photon.Pun.UtilityScripts.OnPointerOverTooltip::.ctor()
extern void OnPointerOverTooltip__ctor_m2F8BDDF376E906A3892231F01C387D79ABF81EC6 (void);
// 0x000000CD System.Void Photon.Pun.UtilityScripts.TabViewManager::Start()
extern void TabViewManager_Start_m93550E29343F633EAFBDF70CECE9BDEF04F25E1F (void);
// 0x000000CE System.Void Photon.Pun.UtilityScripts.TabViewManager::SelectTab(System.String)
extern void TabViewManager_SelectTab_m5F1432DE5D617604F752A1978A1435A716C53327 (void);
// 0x000000CF System.Void Photon.Pun.UtilityScripts.TabViewManager::OnTabSelected(Photon.Pun.UtilityScripts.TabViewManager/Tab)
extern void TabViewManager_OnTabSelected_mA0F4C87748374796E0840B2ED247FF9053F599CC (void);
// 0x000000D0 System.Void Photon.Pun.UtilityScripts.TabViewManager::.ctor()
extern void TabViewManager__ctor_m5E53C78C7C73378D8165A0A87536D40FD9A690EA (void);
// 0x000000D1 System.Void Photon.Pun.UtilityScripts.TabViewManager/TabChangeEvent::.ctor()
extern void TabChangeEvent__ctor_mF5D2514040F08D21F8BD0D9FFF4D76DD105C0F54 (void);
// 0x000000D2 System.Void Photon.Pun.UtilityScripts.TabViewManager/Tab::.ctor()
extern void Tab__ctor_m0CC974E46CCEF4FAEF193DF561D56E688C75BF8A (void);
// 0x000000D3 System.Void Photon.Pun.UtilityScripts.TabViewManager/<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_mB82177BE04F3D8C45C27D81CD5B9C1B12CE90C88 (void);
// 0x000000D4 System.Void Photon.Pun.UtilityScripts.TabViewManager/<>c__DisplayClass7_0::<Start>b__0(System.Boolean)
extern void U3CU3Ec__DisplayClass7_0_U3CStartU3Eb__0_mF05E43041298C5CCB46F7CDDA3A83A30473C4EF1 (void);
// 0x000000D5 System.Void Photon.Pun.UtilityScripts.TextButtonTransition::Awake()
extern void TextButtonTransition_Awake_m362275D8918F4EE1B982E6012D005BE4DA11E855 (void);
// 0x000000D6 System.Void Photon.Pun.UtilityScripts.TextButtonTransition::OnEnable()
extern void TextButtonTransition_OnEnable_mBE1BDE6585782A7F6BE52759EAA61C98EB668747 (void);
// 0x000000D7 System.Void Photon.Pun.UtilityScripts.TextButtonTransition::OnDisable()
extern void TextButtonTransition_OnDisable_m5CF60C6D9D9CEA833E887105DADA5569025477E3 (void);
// 0x000000D8 System.Void Photon.Pun.UtilityScripts.TextButtonTransition::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TextButtonTransition_OnPointerEnter_mF449C5E0DA26BA7F7D7E63D0F922D8E3F4AE45CD (void);
// 0x000000D9 System.Void Photon.Pun.UtilityScripts.TextButtonTransition::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TextButtonTransition_OnPointerExit_m88774276D2AE33582766A4E75C132A3AEC6D9C3F (void);
// 0x000000DA System.Void Photon.Pun.UtilityScripts.TextButtonTransition::.ctor()
extern void TextButtonTransition__ctor_mA0D9DEA43F6641AFF3C78430D1F1952516DF598B (void);
// 0x000000DB System.Void Photon.Pun.UtilityScripts.TextToggleIsOnTransition::OnEnable()
extern void TextToggleIsOnTransition_OnEnable_m842347A141A9077E3C8CF47F2BF44F713CF333BA (void);
// 0x000000DC System.Void Photon.Pun.UtilityScripts.TextToggleIsOnTransition::OnDisable()
extern void TextToggleIsOnTransition_OnDisable_m2896AB0E606D5BA4E0B7E39FC95B134C56E3FBC4 (void);
// 0x000000DD System.Void Photon.Pun.UtilityScripts.TextToggleIsOnTransition::OnValueChanged(System.Boolean)
extern void TextToggleIsOnTransition_OnValueChanged_mDE35A8F753E88D4170597BDE5DE24829A227D4E6 (void);
// 0x000000DE System.Void Photon.Pun.UtilityScripts.TextToggleIsOnTransition::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TextToggleIsOnTransition_OnPointerEnter_m2E49A8821B5ED7449541203F24C111DABBC6C65E (void);
// 0x000000DF System.Void Photon.Pun.UtilityScripts.TextToggleIsOnTransition::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TextToggleIsOnTransition_OnPointerExit_m9B65A2FA6D68C99EAB3A9A4466933448E3B93DF5 (void);
// 0x000000E0 System.Void Photon.Pun.UtilityScripts.TextToggleIsOnTransition::.ctor()
extern void TextToggleIsOnTransition__ctor_m70DD438D857F85A254926B12EB4110FB43085B62 (void);
static Il2CppMethodPointer s_methodPointers[224] = 
{
	CullArea_get_CellCount_mC896B34CC2398FF0D8C88C6FDF2D0B4765668542,
	CullArea_set_CellCount_mBE8FBA6BA28C8EA2729EDBB484778096AB68A9AC,
	CullArea_get_CellTree_m0D313911F55073D69FF4D498A1D08F24D06B239A,
	CullArea_set_CellTree_m20F8FBD48F628DBFF56FE8E2D1227188E1AE3E9B,
	CullArea_get_Map_m41602F663C80A56CA416A620AA432990F05DABB9,
	CullArea_set_Map_m75450BDB6E944F4BE133FFC68FE4960895A94EBD,
	CullArea_Awake_m7A700AA5B6897B5FFE6C52887E3A9A2A1B7E5F7C,
	CullArea_OnDrawGizmos_m420A22206D92DDFA3904D2C821FD4F6180DBA9AF,
	CullArea_CreateCellHierarchy_mD8B6DF94B76E244C57E7E4CC7646ADDCAF48B6D5,
	CullArea_CreateChildCells_m2FF74828DC2932F644F768AEC9399CE42102E89C,
	CullArea_DrawCells_m72711D17BE5D133E7598E9FAF526FB99652618E5,
	CullArea_IsCellCountAllowed_mC528A1E69232765CADC34D5296A89C2CF7CE18C9,
	CullArea_GetActiveCells_m102F965E9B8EA1E68B7834AFDA2519E03C5B7FE7,
	CullArea__ctor_m0526DAFA7A4D1B353BE853551ADFB36A708F64C4,
	CellTree_get_RootNode_mF64DBAF7D06C2DE7C846436B60D1672E825B6D57,
	CellTree_set_RootNode_mDA1EFE7FE3230A2E22E3E103647A4EC8F997F949,
	CellTree__ctor_m6D84F2C85758D4159E7CD9B95F64D5FEE2636536,
	CellTree__ctor_m5D145514F48F42159E03D17C954BD68E4AEC460F,
	CellTreeNode__ctor_m8DD2569D820820C7000B2CA1A68291FF69544797,
	CellTreeNode__ctor_m1848F7BA51B29DF544EF709E26474A9E2FB14B35,
	CellTreeNode_AddChild_m1E36EA9A58B23D0B7FAF6F382A29534D2A860590,
	CellTreeNode_Draw_mF1C88940B7BF41A505E79A2A0DEFAC1D9AE163FC,
	CellTreeNode_GetActiveCells_mE59FC6E4917ACC44670437C93935BB4F41FEB53F,
	CellTreeNode_IsPointInsideCell_m6311D164BD6B6A8AC0C17D1FC2A31EE0AF96CBF4,
	CellTreeNode_IsPointNearCell_m1D70557A65B17CE5076244C94BD7A20E27EBF5CC,
	CullingHandler_OnEnable_mF1C97B6775F53992D554CDE33B1AB9F7480F812F,
	CullingHandler_Start_m278F0FB85F7F677483F777D4C6060DE92030BAF1,
	CullingHandler_Update_m3CC460C74D1904A3E570F59FA0C7A654BFF841C9,
	CullingHandler_OnGUI_m412388F0BF2D00894D4B87704BA29726DD4690FE,
	CullingHandler_HaveActiveCellsChanged_mE5723D2871CE3D7099132AD75E648EE64ED2B95E,
	CullingHandler_UpdateInterestGroups_m463B58F2AE762180F8020F8B8C785E5999C04C0F,
	CullingHandler_OnPhotonSerializeView_mDF45D42CB6F558FB6A89218BA9A5D7242026C5E5,
	CullingHandler__ctor_m21AF64968030CFFB3827198A7FFBCC5E7C147C36,
	PhotonLagSimulationGui_get_Peer_mEA43D9BFEBBBC06D5BE4073B475F7F6762F013B5,
	PhotonLagSimulationGui_set_Peer_m14579466FABF5BAEDD2703D57F4CABB641BDD4D9,
	PhotonLagSimulationGui_Start_m0361078A955DEB2D880CED2B5536811E110D44DB,
	PhotonLagSimulationGui_OnGUI_m9AAC26AB96368BEE82599A4E713B2A6FF414776C,
	PhotonLagSimulationGui_NetSimHasNoPeerWindow_m32165956DF907526D65F802352CF279F7075E0FD,
	PhotonLagSimulationGui_NetSimWindow_m83D2FB5CA3265991D0A766B16AD4C5A48EF61DB4,
	PhotonLagSimulationGui__ctor_m83F35AB7D40ED0FFA37A780873704BA7FC5E825A,
	PhotonStatsGui_Start_mE6EBA95416A254A5111165D528245689DD6DACD1,
	PhotonStatsGui_Update_mD22063BD8B39911118995797758105995934EDA4,
	PhotonStatsGui_OnGUI_m9C400586F470FAA747F5F09368F1F72B3C28EEEF,
	PhotonStatsGui_TrafficStatsWindow_mB7D9830C83D6DA18457EF8D5F31EADA84C468040,
	PhotonStatsGui__ctor_m88E1C95E343B4F0A115031F12D06241F930A4477,
	PointedAtGameObjectInfo_Start_m9EF8695C852B3A37AF50A3818255468805383BEE,
	PointedAtGameObjectInfo_SetFocus_mD8D3995AA506573F2E343D6AB623B27DB50B0627,
	PointedAtGameObjectInfo_RemoveFocus_m1BCE38D70D190D9B41640FB6DE9BCBBCA7576416,
	PointedAtGameObjectInfo_LateUpdate_mE54B9566308674A1F14151994E452F59C208204D,
	PointedAtGameObjectInfo__ctor_mE40EC9EE75EF11314F21D9038969DAE5E9D083B3,
	StatesGui_Awake_mD149D187CC6C6ECE0751BC3C371A0566207D9EBE,
	StatesGui_OnDisable_mD55DB381981CA14B6D03012432C0BDAEB885252C,
	StatesGui_OnGUI_m772D24DD07DBF828407F4BE7D2F2171B7CC7906B,
	StatesGui_PlayerToString_mEF430A7FEB15745730C024488F9BE069291801B1,
	StatesGui__ctor_m85D2C6EB2C4BCB5E03F233B886DB98C0A87BB450,
	PlayerNumbering_add_OnPlayerNumberingChanged_mBB7EA60386DB8526A85C32733F78EBC8D9921BBF,
	PlayerNumbering_remove_OnPlayerNumberingChanged_mE3D19EDDE7AFC3178AFE7012BB4247C6347831C6,
	PlayerNumbering_Awake_m7065EAE003AADA1A7AD748AED0D3393E21FCE717,
	PlayerNumbering_OnJoinedRoom_mD8DE8FBB3722E8E468FB10BA20F7F86DC30537A0,
	PlayerNumbering_OnLeftRoom_m24F374499F413A9F83EE173989F94811745A738E,
	PlayerNumbering_OnPlayerEnteredRoom_mCB67FE8F1C8FFE14B2BBBE3FAC3024DE8069F1A5,
	PlayerNumbering_OnPlayerLeftRoom_m6532C5EA49C90C8D90D8F8BAEE82BAE698DB83B1,
	PlayerNumbering_OnPlayerPropertiesUpdate_m3107FB59A20928AACE643852332416235271D3D2,
	PlayerNumbering_RefreshData_m575ECDFE79979ACE71F5C167D575291D7975C7A9,
	PlayerNumbering__ctor_mF93A1B29D2AFCD0B203F3085D55F9FB8F680F49C,
	PlayerNumberingChanged__ctor_mFFC3495BE543768BFC627729FCB76439A63036AA,
	PlayerNumberingChanged_Invoke_mA3FA5A2037749852ED0FB05E4497A23A4FC48334,
	PlayerNumberingChanged_BeginInvoke_m46DCFE2333973B1F7A8BCB3E3909AD3D64B5E0CA,
	PlayerNumberingChanged_EndInvoke_m8C16A15C08AF4F9D04D096513BCD5DEEDDFA393C,
	U3CU3Ec__cctor_m293375C60F48F8EE702D464C16D130CEECA21549,
	U3CU3Ec__ctor_m610B526629D5D6AFBFE7A36F0670B2656C66817A,
	U3CU3Ec_U3CRefreshDataU3Eb__14_0_mFCD93BC13C2C3A03DACA09A38F5CEEEB9DE1587F,
	U3CU3Ec_U3CRefreshDataU3Eb__14_1_mF9141A84EAF61C21CE7241C6FE7D452CB23779F2,
	U3CU3Ec_U3CRefreshDataU3Eb__14_2_m37048553001F7BE6EE2C158742A95FFC0DAB696C,
	PlayerNumberingExtensions_GetPlayerNumber_m0A03E46AD4CA342C7681117EF0B636461D9FD47E,
	PlayerNumberingExtensions_SetPlayerNumber_m150612B8CE9CB30A4564B0DA79D865851CDD9026,
	PunPlayerScores__ctor_m331D8AE116668F796CADBF6280922E26CAE6EAAB,
	ScoreExtensions_SetScore_m0F31F48E615433EDFA3673419F62BEED5215B240,
	ScoreExtensions_AddScore_m6D8E5D41F1FD0887665A2F75CC543E1104793448,
	ScoreExtensions_GetScore_m60C4E4920BF40136BEE7F90FD007FA696036ED99,
	PunTeams_Start_m322E02290FA706333633F25171E2A0BAEF8938BC,
	PunTeams_OnDisable_m96950151F04F8063D82B37ECA82FA63E7DA4103B,
	PunTeams_OnJoinedRoom_m5C8D6B504AC4B77268E22EC10284C4DF5A25088F,
	PunTeams_OnLeftRoom_m5FCBFBB5B656565AA6B613313144263283374239,
	PunTeams_OnPlayerPropertiesUpdate_m328CBA4BC5C5CE9AA388D817076829D3E822E112,
	PunTeams_OnPlayerLeftRoom_m5805D79FED7C9E04116F8505DB7963247FC6A16F,
	PunTeams_OnPlayerEnteredRoom_mFC57EBF6C836D9E9AEEC50B0F7ECBEF5009290D4,
	PunTeams_UpdateTeams_m90430BF45EF62AE670C29F643878A8C2FF4C2D45,
	PunTeams__ctor_m1587202531D427931E50682EDF01DC81F2494459,
	TeamExtensions_GetTeam_m0623E10A26876105B719FE5BA3E85FD0F0934899,
	TeamExtensions_SetTeam_mFDFF1A7047108153D81CA1ECE7C151F869BB9F48,
	SmoothSyncMovement_Awake_m83BFD699FBA423D41B6FCC3C93E64571D254D235,
	SmoothSyncMovement_OnPhotonSerializeView_mEC74D31532288A4692E9DB2EB00D1D10FA083EE6,
	SmoothSyncMovement_Update_m7F239108ABF0F150F8A4176DBCE42859F60BA9A2,
	SmoothSyncMovement__ctor_mCDFB1CEDE12D01589A10C5EFB3A87F696BEBFE26,
	ConnectAndJoinRandom_Start_m4A56759A88980B826FC0BEF46BD2A7B9E9F85C08,
	ConnectAndJoinRandom_ConnectNow_mBBE27554A21F79BF8A479784037E6E0CB11D443A,
	ConnectAndJoinRandom_OnConnectedToMaster_m8E8D6401382B397A127B6D79B145F27B05196324,
	ConnectAndJoinRandom_OnJoinedLobby_mD609A960BD57F04E478EC0F74CD413689DD3010F,
	ConnectAndJoinRandom_OnJoinRandomFailed_m544DF7DA0A6D37EE5B29AED67435382E1CFAFEC1,
	ConnectAndJoinRandom_OnDisconnected_mEF1F9716B0234D6669D776F795648154F14531B9,
	ConnectAndJoinRandom_OnJoinedRoom_mACD2B57642AD66BFCB6DED0C2149B114A5958756,
	ConnectAndJoinRandom__ctor_m2E6133129FEAB84291168E24EF1752A040D65AD0,
	MoveByKeys_Start_mBA857B269EB1CC4ECCB8A6B47EDE4C0645C65063,
	MoveByKeys_FixedUpdate_m5B1B39A4F22E3D59E1D49F6A98CECE6F7B0858C3,
	MoveByKeys__ctor_m3F10B2047381BCFC3C22B2319A74A3CC76A362BC,
	OnClickDestroy_UnityEngine_EventSystems_IPointerClickHandler_OnPointerClick_m832D149EF3C6D7EE5388A733A3F22DE791ABDF51,
	OnClickDestroy_DestroyRpc_m857F90BFDEE9D447362D71EC2875B7668D7814B9,
	OnClickDestroy__ctor_m95E15ED7BCD9B35607A3CDCC77CB48CE8E4ED176,
	U3CDestroyRpcU3Ed__4__ctor_m621B9190F6FE020A6C624A2B74963F09BE7EE457,
	U3CDestroyRpcU3Ed__4_System_IDisposable_Dispose_m2331D4395BBC0230414B7195785C314F4813E174,
	U3CDestroyRpcU3Ed__4_MoveNext_mC0C89CB2D97AF92CFE532A7B7999594682D7DDB9,
	U3CDestroyRpcU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0A15F547DED847B3415F25FEEDFC02942275E4AC,
	U3CDestroyRpcU3Ed__4_System_Collections_IEnumerator_Reset_m6BF2FE10CF8171BBA7DDB8D2D4497A77E3D25F39,
	U3CDestroyRpcU3Ed__4_System_Collections_IEnumerator_get_Current_mA30CE51561261565206BAB8B90923C29459A67B9,
	OnClickInstantiate_UnityEngine_EventSystems_IPointerClickHandler_OnPointerClick_mD50C462E8BDBBAC654C0A1F565C21F44D4714095,
	OnClickInstantiate__ctor_m0B49CC01530FAE3098EE264B5D4E6F148FC783B7,
	OnClickRpc_UnityEngine_EventSystems_IPointerClickHandler_OnPointerClick_m7443A2B85611870FDF1D2C14428D8C6CB5D0ECB7,
	OnClickRpc_ClickRpc_m89E074536BC1C02E15F9360D2B1873DB905772CD,
	OnClickRpc_ClickFlash_m7087F3D9C3F6048E41D1FDF03CCFB64F9ACE5CFB,
	OnClickRpc__ctor_mE6A964D86B5B162C95BC83C7803FA812594449D1,
	U3CClickFlashU3Ed__8__ctor_m2A93E4DACCC264BA2BA525E718E45B4F01C27525,
	U3CClickFlashU3Ed__8_System_IDisposable_Dispose_mD050F87E615A7E03EE0EF3B947E9E7B5D6DC0E3E,
	U3CClickFlashU3Ed__8_MoveNext_mDE5F7B9C67C8BE74D129C8A3297EBD35E7707BA3,
	U3CClickFlashU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAC1ABADE4341BD6EAB1D3D93723567E3BA61A0E3,
	U3CClickFlashU3Ed__8_System_Collections_IEnumerator_Reset_m62838FA6374A3EB45BA08ED9BECE91F3B73B3A43,
	U3CClickFlashU3Ed__8_System_Collections_IEnumerator_get_Current_mD694E610E2C416ACF79BEB73DA3723C1F6D5D1E0,
	OnEscapeQuit_Update_mC2365FFC05E56349E2072921FD143944ECD72080,
	OnEscapeQuit__ctor_m054775A6FA431DC5BB15B2CB5D036749BC7DE27A,
	OnJoinedInstantiate_OnEnable_m18E2FA692A52B5382CB32975692FBFE12450FEA1,
	OnJoinedInstantiate_OnDisable_m867DFC9561B8AA365F9682E26D92042A66BDC0BE,
	OnJoinedInstantiate_OnJoinedRoom_mF5A3064BB84ACE67139C6D38560EC94253155711,
	OnJoinedInstantiate_OnConnected_m59250D1C6D4621C8B3BEB26796C792CD7896847C,
	OnJoinedInstantiate_OnCustomAuthenticationResponse_mF644274CCC937BF2693BE0521C80C01D79F7332E,
	OnJoinedInstantiate_OnCustomAuthenticationFailed_m3497ACB0B57FB589448799632013427BCBDADB79,
	OnJoinedInstantiate_OnConnectedToMaster_m9BF6E404BD5565B513AB632884791E7D29473E23,
	OnJoinedInstantiate_OnDisconnected_m6BC6D9CD5FFE068C723E0310AB190829C3F33EDC,
	OnJoinedInstantiate_OnRegionListReceived_m2551386FEC329FFCC926F15E09E4BE7ADD628E14,
	OnJoinedInstantiate_OnRoomListUpdate_m409A10D62950B4E8146E431CD805DF8CD7DA5179,
	OnJoinedInstantiate_OnFriendListUpdate_m21B9A98A96A70A7DE9D48D1D01B9016AFF8FD6B6,
	OnJoinedInstantiate_OnJoinedLobby_m6261701DF1364A787884DF3C786F357987126963,
	OnJoinedInstantiate_OnLeftLobby_mA6D379302CD454E3481B7761F2F1599E54A25CB6,
	OnJoinedInstantiate_OnLobbyStatisticsUpdate_m0CF4E02032C3D881DA917CE23F517A4FF1C2ACDB,
	OnJoinedInstantiate_OnCreatedRoom_m691D7527DA1C584ED25AD9115319D1BE9C8A442E,
	OnJoinedInstantiate_OnCreateRoomFailed_m47344AA6EF45CC54400059B2C1C45C66C0541960,
	OnJoinedInstantiate_OnJoinRoomFailed_mACC5A9F8E97CCE343D481572E7D81CFE722CB610,
	OnJoinedInstantiate_OnJoinRandomFailed_mA83F1B9375B6AA3007F16FA5DCCB8B119770EBB3,
	OnJoinedInstantiate_OnLeftRoom_mC4FFC6FAC4BAF1303E2F9C60F9EF1AF5A94B05A2,
	OnJoinedInstantiate__ctor_m64DCA56A5530A370271563A22566198012092129,
	OnStartDelete_Start_m4D4A3F37EDAA5AF862BB1A6915065C4DA43E06E8,
	OnStartDelete__ctor_m858C26A4F9A077DD08B79D1A49337A4BCFDE1098,
	CountdownTimer_add_OnCountdownTimerHasExpired_mF6580A850B0E01A5ADE2F16FB53D17153040054F,
	CountdownTimer_remove_OnCountdownTimerHasExpired_m151E3DC7B8ACFE1D6E1CCAE513B7EDB8D5892E9A,
	CountdownTimer_Start_mD6BA05D6038F4BBD03AA753B722FD123581E12A1,
	CountdownTimer_Update_m609D6AF08FFDC6BD0E78E33F86D051B8BE5E8E50,
	CountdownTimer_OnRoomPropertiesUpdate_m373B1B365413351650694B4AF2355E345F39C087,
	CountdownTimer__ctor_m0F8939C4FA24EF1C2FCCEA485ACD05D7F7FD453D,
	CountdownTimerHasExpired__ctor_m08C1521F106E76342333DAD320A3E7F1EB9FAE9F,
	CountdownTimerHasExpired_Invoke_m8D5690A759DD53CD0DB1923D7DC33B22776B9279,
	CountdownTimerHasExpired_BeginInvoke_mFC0BF6CB1A65B012EEF8BDE00E6B86FE3FE25707,
	CountdownTimerHasExpired_EndInvoke_m09ED2AAA732CC70C0096F081A3629230631561EF,
	PunTurnManager_get_Turn_m1293DE05C5440CA33F5BD293E50E487F70EAC966,
	PunTurnManager_set_Turn_m2635EFCF67DDD0B8781A097F56AEC0C172016099,
	PunTurnManager_get_ElapsedTimeInTurn_m8D2F2D914C6E4603A09AC02FF20B34C6E071A84E,
	PunTurnManager_get_RemainingSecondsInTurn_mD8AD1D72E248AB9AC2F7B73BCF4640FA6E4F0C91,
	PunTurnManager_get_IsCompletedByAll_m388FC57B015D8A9D46553CCA3EC4CB6269DAA79E,
	PunTurnManager_get_IsFinishedByMe_mE6268289C028688F573E40CE692B382872F49A88,
	PunTurnManager_get_IsOver_m7059AC986F6BC718F4EC928A7CEDD9C3A6CC60BE,
	PunTurnManager_Start_mB44B031AA0154F4F7BC850945A034F4EACF8A02E,
	PunTurnManager_Update_mF0E1FB736401D4CEDF00205DE58E4C9AE0EB322B,
	PunTurnManager_BeginTurn_m65B48FC9B2EDD1CE017A64EE680DA0DC57501E5C,
	PunTurnManager_SendMove_m55E17CBC71FEFE845AE01DE31897B85316679276,
	PunTurnManager_GetPlayerFinishedTurn_mEEEB696A63EB5C397D26C8C58586873C4B697A24,
	PunTurnManager_ProcessOnEvent_mBE02B41E074D418910EAEAAC391849CE13EA08A9,
	PunTurnManager_OnEvent_mF999A8F6CB506BAA1F3CAE4D3715C8A700DFC313,
	PunTurnManager_OnRoomPropertiesUpdate_mCF2E80B4C1C3A10F2E507C08953447D474D14D1E,
	PunTurnManager__ctor_m7777470A5AC0A0BFF5445408C9359EDEBEDB7A8E,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	TurnExtensions_SetTurn_m45E586C445EFFDBCAD5776A3B04B2180A5E529DA,
	TurnExtensions_GetTurn_mFB9AE18C0A2CC0ED3737A0E5FB3C10BAE936DA7E,
	TurnExtensions_GetTurnStart_m69B98B4B7B10F319364C1ABA866C3F92659CF2AA,
	TurnExtensions_GetFinishedTurn_mDC6FB3EAB8098F19C13B823E44FA2F263731B03B,
	TurnExtensions_SetFinishedTurn_mF11DDA1AC3EE81D3F7F48E327522945DBE2EC197,
	TurnExtensions__cctor_m3EE89F8E0A15684D0526D3DB40F0413422CEFC4B,
	ButtonInsideScrollList_Start_mB6A36575850C3D148474C18CB5F698C66CC32D1E,
	ButtonInsideScrollList_UnityEngine_EventSystems_IPointerDownHandler_OnPointerDown_mEF76B47CABC7A2DCFE2CC15082D078870FF5C763,
	ButtonInsideScrollList_UnityEngine_EventSystems_IPointerUpHandler_OnPointerUp_m7A8F9AFF81E97366BAE4B369B6413C5AE045D18F,
	ButtonInsideScrollList__ctor_mAEADBDBAA78C39BEA2AADB373460E76F8A227F2F,
	EventSystemSpawner_OnEnable_m4CE18E16F6FA0066C219B1EF4AA40016CE87CCC6,
	EventSystemSpawner__ctor_m3CA059DCD101FEA607ABA6B75D26A21030305A0F,
	GraphicToggleIsOnTransition_OnPointerEnter_mCF385DB2983AFE7A5B8FF7B9694AC706AEFB3F0A,
	GraphicToggleIsOnTransition_OnPointerExit_m72A25EF71036CB9250918A31260C3040E5350C74,
	GraphicToggleIsOnTransition_OnEnable_mD6156FF4490D8FB49414C553F651E4735C6AC9A2,
	GraphicToggleIsOnTransition_OnDisable_m84B1000D5A61334FC178F61AA1336E9B48700B76,
	GraphicToggleIsOnTransition_OnValueChanged_m4D0B9067515692B515B12D92889FC8ABE085836B,
	GraphicToggleIsOnTransition__ctor_m653CE3A04A15A63CD29EBBDEFC561A96D02FA6DD,
	OnPointerOverTooltip_OnDestroy_m9C054E052D7BC96FC40BE152BB2238C2C4423618,
	OnPointerOverTooltip_UnityEngine_EventSystems_IPointerExitHandler_OnPointerExit_m37892FBEA7AE19256A2D3C4E10DCF1F1F0A8A190,
	OnPointerOverTooltip_UnityEngine_EventSystems_IPointerEnterHandler_OnPointerEnter_m860A8F9148CD2C2CEB073EE83A22629016C7B77F,
	OnPointerOverTooltip__ctor_m2F8BDDF376E906A3892231F01C387D79ABF81EC6,
	TabViewManager_Start_m93550E29343F633EAFBDF70CECE9BDEF04F25E1F,
	TabViewManager_SelectTab_m5F1432DE5D617604F752A1978A1435A716C53327,
	TabViewManager_OnTabSelected_mA0F4C87748374796E0840B2ED247FF9053F599CC,
	TabViewManager__ctor_m5E53C78C7C73378D8165A0A87536D40FD9A690EA,
	TabChangeEvent__ctor_mF5D2514040F08D21F8BD0D9FFF4D76DD105C0F54,
	Tab__ctor_m0CC974E46CCEF4FAEF193DF561D56E688C75BF8A,
	U3CU3Ec__DisplayClass7_0__ctor_mB82177BE04F3D8C45C27D81CD5B9C1B12CE90C88,
	U3CU3Ec__DisplayClass7_0_U3CStartU3Eb__0_mF05E43041298C5CCB46F7CDDA3A83A30473C4EF1,
	TextButtonTransition_Awake_m362275D8918F4EE1B982E6012D005BE4DA11E855,
	TextButtonTransition_OnEnable_mBE1BDE6585782A7F6BE52759EAA61C98EB668747,
	TextButtonTransition_OnDisable_m5CF60C6D9D9CEA833E887105DADA5569025477E3,
	TextButtonTransition_OnPointerEnter_mF449C5E0DA26BA7F7D7E63D0F922D8E3F4AE45CD,
	TextButtonTransition_OnPointerExit_m88774276D2AE33582766A4E75C132A3AEC6D9C3F,
	TextButtonTransition__ctor_mA0D9DEA43F6641AFF3C78430D1F1952516DF598B,
	TextToggleIsOnTransition_OnEnable_m842347A141A9077E3C8CF47F2BF44F713CF333BA,
	TextToggleIsOnTransition_OnDisable_m2896AB0E606D5BA4E0B7E39FC95B134C56E3FBC4,
	TextToggleIsOnTransition_OnValueChanged_mDE35A8F753E88D4170597BDE5DE24829A227D4E6,
	TextToggleIsOnTransition_OnPointerEnter_m2E49A8821B5ED7449541203F24C111DABBC6C65E,
	TextToggleIsOnTransition_OnPointerExit_m9B65A2FA6D68C99EAB3A9A4466933448E3B93DF5,
	TextToggleIsOnTransition__ctor_m70DD438D857F85A254926B12EB4110FB43085B62,
};
static const int32_t s_InvokerIndices[224] = 
{
	1747,
	1513,
	1758,
	1522,
	1758,
	1522,
	1791,
	1791,
	1791,
	969,
	1791,
	1778,
	1242,
	1791,
	1758,
	1522,
	1791,
	1522,
	1791,
	676,
	1522,
	1791,
	663,
	812,
	812,
	1791,
	1791,
	1791,
	1791,
	1778,
	1791,
	973,
	1791,
	1758,
	1522,
	1791,
	1791,
	1513,
	1513,
	1791,
	1791,
	1791,
	1791,
	1513,
	1791,
	1791,
	1522,
	1522,
	1791,
	1791,
	1791,
	1791,
	1791,
	1238,
	1791,
	2776,
	2776,
	1791,
	1791,
	1791,
	1522,
	1522,
	972,
	1791,
	1791,
	971,
	1791,
	758,
	1522,
	2828,
	1791,
	1175,
	1175,
	1175,
	2672,
	2601,
	1791,
	2601,
	2601,
	2672,
	1791,
	1791,
	1791,
	1791,
	972,
	1522,
	1522,
	1791,
	1791,
	2738,
	2608,
	1791,
	973,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	855,
	1513,
	1791,
	1791,
	1791,
	1791,
	1791,
	1522,
	1758,
	1791,
	1513,
	1791,
	1778,
	1758,
	1791,
	1758,
	1522,
	1791,
	1522,
	1791,
	1758,
	1791,
	1513,
	1791,
	1778,
	1758,
	1791,
	1758,
	1791,
	1791,
	1791,
	1791,
	1791,
	1791,
	1522,
	1522,
	1791,
	1513,
	1522,
	1522,
	1522,
	1791,
	1791,
	1522,
	1791,
	855,
	855,
	855,
	1791,
	1791,
	1791,
	1791,
	2776,
	2776,
	1791,
	1791,
	1522,
	1791,
	971,
	1791,
	758,
	1522,
	1747,
	1513,
	1780,
	1780,
	1778,
	1778,
	1778,
	1791,
	1791,
	1791,
	975,
	1361,
	677,
	1522,
	1522,
	1791,
	1513,
	1513,
	644,
	644,
	1513,
	2382,
	2672,
	2672,
	2672,
	2601,
	2828,
	1791,
	1522,
	1522,
	1791,
	1791,
	1791,
	1522,
	1522,
	1791,
	1791,
	1541,
	1791,
	1791,
	1522,
	1522,
	1791,
	1791,
	1522,
	1522,
	1791,
	1791,
	1791,
	1791,
	1541,
	1791,
	1791,
	1791,
	1522,
	1522,
	1791,
	1791,
	1791,
	1541,
	1522,
	1522,
	1791,
};
extern const CustomAttributesCacheGenerator g_PhotonUnityNetworking_Utilities_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_PhotonUnityNetworking_Utilities_CodeGenModule;
const Il2CppCodeGenModule g_PhotonUnityNetworking_Utilities_CodeGenModule = 
{
	"PhotonUnityNetworking.Utilities.dll",
	224,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_PhotonUnityNetworking_Utilities_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
