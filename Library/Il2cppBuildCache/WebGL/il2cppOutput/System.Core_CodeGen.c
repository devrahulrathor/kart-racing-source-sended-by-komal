﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.String SR::GetString(System.String)
extern void SR_GetString_mD7FC73A3473F4F165E55F8B4A7088F2E9F9CC412 (void);
// 0x00000002 System.Void System.Security.Cryptography.AesManaged::.ctor()
extern void AesManaged__ctor_m79644F6BCD0E8C2D8BAF1B1E22E90D3C364F5C57 (void);
// 0x00000003 System.Int32 System.Security.Cryptography.AesManaged::get_FeedbackSize()
extern void AesManaged_get_FeedbackSize_mCFE4C56DFF81F5E616CE535AB7D9E37DC1B7A937 (void);
// 0x00000004 System.Void System.Security.Cryptography.AesManaged::set_FeedbackSize(System.Int32)
extern void AesManaged_set_FeedbackSize_mD898DD8078EA74C68D8DA8D461E79ABE7BF0BDE2 (void);
// 0x00000005 System.Byte[] System.Security.Cryptography.AesManaged::get_IV()
extern void AesManaged_get_IV_mB1D7896A5F5E71B8B7938A5DF3A743FC2E444018 (void);
// 0x00000006 System.Void System.Security.Cryptography.AesManaged::set_IV(System.Byte[])
extern void AesManaged_set_IV_m1DBDC4FDAE66A5F2FA99AA4A4E76769BB8897D1E (void);
// 0x00000007 System.Byte[] System.Security.Cryptography.AesManaged::get_Key()
extern void AesManaged_get_Key_m4CC3B2D28A918B935AD42F3F8D54E93A6CB2FA31 (void);
// 0x00000008 System.Void System.Security.Cryptography.AesManaged::set_Key(System.Byte[])
extern void AesManaged_set_Key_m35D61E5FD8942054840B1F24E685E91E3E6CA6E1 (void);
// 0x00000009 System.Int32 System.Security.Cryptography.AesManaged::get_KeySize()
extern void AesManaged_get_KeySize_mBE6EA533BD5978099974A74FF3DE3ECB8B173CD6 (void);
// 0x0000000A System.Void System.Security.Cryptography.AesManaged::set_KeySize(System.Int32)
extern void AesManaged_set_KeySize_m2003A2B9200003C23B544F56E949A0630AA87F93 (void);
// 0x0000000B System.Security.Cryptography.CipherMode System.Security.Cryptography.AesManaged::get_Mode()
extern void AesManaged_get_Mode_mF9D7222B2AB685AC46F4564B6F2247114244AEF6 (void);
// 0x0000000C System.Void System.Security.Cryptography.AesManaged::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesManaged_set_Mode_mA5CF4C1F3B41503C6E09373ADB0B8983A6F61460 (void);
// 0x0000000D System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesManaged::get_Padding()
extern void AesManaged_get_Padding_mD81B3F96D3421F6CD2189A01D65736A9098ACD45 (void);
// 0x0000000E System.Void System.Security.Cryptography.AesManaged::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesManaged_set_Padding_m6B07EC4A0F1F451417DC0AC64E9D637D7916866B (void);
// 0x0000000F System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor()
extern void AesManaged_CreateDecryptor_m41AE4428FE60C9FD485640F3A09F1BF345452A3C (void);
// 0x00000010 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateDecryptor_m7240F8C38B99CE73159DE7455046E951C4900268 (void);
// 0x00000011 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor()
extern void AesManaged_CreateEncryptor_mB2BBCAB8753A59FFB572091D2EF80F287CD951BF (void);
// 0x00000012 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateEncryptor_m1E4EB80DE75FCF9E940228E1D7664C0EA1378153 (void);
// 0x00000013 System.Void System.Security.Cryptography.AesManaged::Dispose(System.Boolean)
extern void AesManaged_Dispose_mB0D969841D51825F37095A93E73A50C15C1A1477 (void);
// 0x00000014 System.Void System.Security.Cryptography.AesManaged::GenerateIV()
extern void AesManaged_GenerateIV_mBB19651CC37782273A882055D4E63370268F2D91 (void);
// 0x00000015 System.Void System.Security.Cryptography.AesManaged::GenerateKey()
extern void AesManaged_GenerateKey_mF6673B955AE82377595277C6B78C7DA8A16F480E (void);
// 0x00000016 System.Void System.Security.Cryptography.AesCryptoServiceProvider::.ctor()
extern void AesCryptoServiceProvider__ctor_mA9857852BC34D8AB0F463C1AF1837CBBD9102265 (void);
// 0x00000017 System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateIV()
extern void AesCryptoServiceProvider_GenerateIV_m18539D5136BA9A2FC71F439150D16E35AD3BF5C4 (void);
// 0x00000018 System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateKey()
extern void AesCryptoServiceProvider_GenerateKey_m574F877FD23D1F07033FC035E89BE232303F3502 (void);
// 0x00000019 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateDecryptor_mAB5FB857F549A86D986461C8665BE6B2393305D1 (void);
// 0x0000001A System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateEncryptor_m6BF20D5D8424DB627CD3010D9E4C8555C6BD0465 (void);
// 0x0000001B System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_IV()
extern void AesCryptoServiceProvider_get_IV_m6A46F1C255ABE41F98BEE8C0C37D6AFBB9F29D34 (void);
// 0x0000001C System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_IV(System.Byte[])
extern void AesCryptoServiceProvider_set_IV_mCB88C0F651B17F3EC7575F16E14C9E3BD2DB24DB (void);
// 0x0000001D System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_Key()
extern void AesCryptoServiceProvider_get_Key_mAC979BC922E8F1F15B36220E77972AC9CE5D5252 (void);
// 0x0000001E System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Key(System.Byte[])
extern void AesCryptoServiceProvider_set_Key_m65785032C270005BC120157A0C9D019F6F6BC96F (void);
// 0x0000001F System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_KeySize()
extern void AesCryptoServiceProvider_get_KeySize_m3081171DF6C11CA55ECEBA29B9559D18E78D8058 (void);
// 0x00000020 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_KeySize(System.Int32)
extern void AesCryptoServiceProvider_set_KeySize_mA994D2D3098216C0B8C4F02C0F0A0F63D4256218 (void);
// 0x00000021 System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_FeedbackSize()
extern void AesCryptoServiceProvider_get_FeedbackSize_m9DC2E1C3E84CC674ADB2D7E6B06066F333BEC89D (void);
// 0x00000022 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_FeedbackSize(System.Int32)
extern void AesCryptoServiceProvider_set_FeedbackSize_m5B367A05D9F985C7C83425637637B840858C255D (void);
// 0x00000023 System.Security.Cryptography.CipherMode System.Security.Cryptography.AesCryptoServiceProvider::get_Mode()
extern void AesCryptoServiceProvider_get_Mode_m3E1CBFD4D7CE748F3AB615EB88DE1A5D7238285D (void);
// 0x00000024 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesCryptoServiceProvider_set_Mode_mFE7044929761BABE312D1146B0ED51B331E35D63 (void);
// 0x00000025 System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesCryptoServiceProvider::get_Padding()
extern void AesCryptoServiceProvider_get_Padding_m89D49B05949BA2C6C557EFA5211B4934D279C7AD (void);
// 0x00000026 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesCryptoServiceProvider_set_Padding_mD3353CD8F4B931AA00203000140520775643F96E (void);
// 0x00000027 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor()
extern void AesCryptoServiceProvider_CreateDecryptor_mB1F90A7339DA65542795E17DF9C37810BD088DDF (void);
// 0x00000028 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor()
extern void AesCryptoServiceProvider_CreateEncryptor_m9555DFFCA344DF06C8B88DDE2EB987B3958EC6BB (void);
// 0x00000029 System.Void System.Security.Cryptography.AesCryptoServiceProvider::Dispose(System.Boolean)
extern void AesCryptoServiceProvider_Dispose_m7123198904819E2BF2B1398E20047B316C3D7D1E (void);
// 0x0000002A System.Void System.Security.Cryptography.AesTransform::.ctor(System.Security.Cryptography.Aes,System.Boolean,System.Byte[],System.Byte[])
extern void AesTransform__ctor_m3903A599E8B2C3F7AB3B70E1258980151D639598 (void);
// 0x0000002B System.Void System.Security.Cryptography.AesTransform::ECB(System.Byte[],System.Byte[])
extern void AesTransform_ECB_m2E2F4E2B307B0D34FEADF38684007E622FCEDFD1 (void);
// 0x0000002C System.UInt32 System.Security.Cryptography.AesTransform::SubByte(System.UInt32)
extern void AesTransform_SubByte_m2D77D545ABD3D84C04741B80ABB74BEFE8C55679 (void);
// 0x0000002D System.Void System.Security.Cryptography.AesTransform::Encrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Encrypt128_m57DA74A7E05818DFD92F2614F8F65B0D1E696129 (void);
// 0x0000002E System.Void System.Security.Cryptography.AesTransform::Decrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Decrypt128_m075F7BA40A4CFECA6F6A379065B731586EDDB23A (void);
// 0x0000002F System.Void System.Security.Cryptography.AesTransform::.cctor()
extern void AesTransform__cctor_mAC6D46ED54345C2D23DFCA026C69029757222CFD (void);
// 0x00000030 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_m0EDA0D46D72CA692518E3E2EB75B48044D8FD41E (void);
// 0x00000031 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m4C4756AF34A76EF12F3B2B6D8C78DE547F0FBCF8 (void);
// 0x00000032 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_mB89E91246572F009281D79730950808F17C3F353 (void);
// 0x00000033 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000034 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000035 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000036 System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x00000037 System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x00000038 System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderByDescending(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x00000039 System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::ThenBy(System.Linq.IOrderedEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000003A System.Boolean System.Linq.Enumerable::SequenceEqual(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003B System.Boolean System.Linq.Enumerable::SequenceEqual(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x0000003C TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003D System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003E TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003F TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000040 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000041 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000042 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000043 System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000044 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x00000045 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000046 System.Void System.Linq.Enumerable/Iterator`1::.ctor()
// 0x00000047 TSource System.Linq.Enumerable/Iterator`1::get_Current()
// 0x00000048 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/Iterator`1::Clone()
// 0x00000049 System.Void System.Linq.Enumerable/Iterator`1::Dispose()
// 0x0000004A System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/Iterator`1::GetEnumerator()
// 0x0000004B System.Boolean System.Linq.Enumerable/Iterator`1::MoveNext()
// 0x0000004C System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000004D System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000004E System.Object System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x0000004F System.Collections.IEnumerator System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000050 System.Void System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerator.Reset()
// 0x00000051 System.Void System.Linq.Enumerable/WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000052 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::Clone()
// 0x00000053 System.Void System.Linq.Enumerable/WhereEnumerableIterator`1::Dispose()
// 0x00000054 System.Boolean System.Linq.Enumerable/WhereEnumerableIterator`1::MoveNext()
// 0x00000055 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000056 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000057 System.Void System.Linq.Enumerable/WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000058 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereArrayIterator`1::Clone()
// 0x00000059 System.Boolean System.Linq.Enumerable/WhereArrayIterator`1::MoveNext()
// 0x0000005A System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000005B System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000005C System.Void System.Linq.Enumerable/WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000005D System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereListIterator`1::Clone()
// 0x0000005E System.Boolean System.Linq.Enumerable/WhereListIterator`1::MoveNext()
// 0x0000005F System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000060 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000061 System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000062 System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Clone()
// 0x00000063 System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Dispose()
// 0x00000064 System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2::MoveNext()
// 0x00000065 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000066 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000067 System.Void System.Linq.Enumerable/WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000068 System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::Clone()
// 0x00000069 System.Boolean System.Linq.Enumerable/WhereSelectArrayIterator`2::MoveNext()
// 0x0000006A System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000006B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000006C System.Void System.Linq.Enumerable/WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000006D System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2::Clone()
// 0x0000006E System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2::MoveNext()
// 0x0000006F System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000070 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000071 System.Void System.Linq.Enumerable/<>c__DisplayClass6_0`1::.ctor()
// 0x00000072 System.Boolean System.Linq.Enumerable/<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x00000073 System.Void System.Linq.Enumerable/<>c__DisplayClass7_0`3::.ctor()
// 0x00000074 TResult System.Linq.Enumerable/<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x00000075 System.Linq.IOrderedEnumerable`1<TElement> System.Linq.IOrderedEnumerable`1::CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x00000076 System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x00000077 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x00000078 System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000079 System.Linq.IOrderedEnumerable`1<TElement> System.Linq.OrderedEnumerable`1::System.Linq.IOrderedEnumerable<TElement>.CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x0000007A System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x0000007B System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::.ctor(System.Int32)
// 0x0000007C System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x0000007D System.Boolean System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::MoveNext()
// 0x0000007E TElement System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x0000007F System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x00000080 System.Object System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x00000081 System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x00000082 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x00000083 System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x00000084 System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x00000085 System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x00000086 System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x00000087 System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x00000088 System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x00000089 System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x0000008A System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x0000008B System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x0000008C TElement[] System.Linq.Buffer`1::ToArray()
// 0x0000008D System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x0000008E System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x0000008F System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000090 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x00000091 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x00000092 System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x00000093 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x00000094 System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x00000095 System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x00000096 System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x00000097 System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x00000098 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000099 System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000009A System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x0000009B System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x0000009C System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x0000009D System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x0000009E System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x0000009F System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x000000A0 System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x000000A1 System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x000000A2 System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x000000A3 System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x000000A4 System.Void System.Collections.Generic.HashSet`1/Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x000000A5 System.Void System.Collections.Generic.HashSet`1/Enumerator::Dispose()
// 0x000000A6 System.Boolean System.Collections.Generic.HashSet`1/Enumerator::MoveNext()
// 0x000000A7 T System.Collections.Generic.HashSet`1/Enumerator::get_Current()
// 0x000000A8 System.Object System.Collections.Generic.HashSet`1/Enumerator::System.Collections.IEnumerator.get_Current()
// 0x000000A9 System.Void System.Collections.Generic.HashSet`1/Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[169] = 
{
	SR_GetString_mD7FC73A3473F4F165E55F8B4A7088F2E9F9CC412,
	AesManaged__ctor_m79644F6BCD0E8C2D8BAF1B1E22E90D3C364F5C57,
	AesManaged_get_FeedbackSize_mCFE4C56DFF81F5E616CE535AB7D9E37DC1B7A937,
	AesManaged_set_FeedbackSize_mD898DD8078EA74C68D8DA8D461E79ABE7BF0BDE2,
	AesManaged_get_IV_mB1D7896A5F5E71B8B7938A5DF3A743FC2E444018,
	AesManaged_set_IV_m1DBDC4FDAE66A5F2FA99AA4A4E76769BB8897D1E,
	AesManaged_get_Key_m4CC3B2D28A918B935AD42F3F8D54E93A6CB2FA31,
	AesManaged_set_Key_m35D61E5FD8942054840B1F24E685E91E3E6CA6E1,
	AesManaged_get_KeySize_mBE6EA533BD5978099974A74FF3DE3ECB8B173CD6,
	AesManaged_set_KeySize_m2003A2B9200003C23B544F56E949A0630AA87F93,
	AesManaged_get_Mode_mF9D7222B2AB685AC46F4564B6F2247114244AEF6,
	AesManaged_set_Mode_mA5CF4C1F3B41503C6E09373ADB0B8983A6F61460,
	AesManaged_get_Padding_mD81B3F96D3421F6CD2189A01D65736A9098ACD45,
	AesManaged_set_Padding_m6B07EC4A0F1F451417DC0AC64E9D637D7916866B,
	AesManaged_CreateDecryptor_m41AE4428FE60C9FD485640F3A09F1BF345452A3C,
	AesManaged_CreateDecryptor_m7240F8C38B99CE73159DE7455046E951C4900268,
	AesManaged_CreateEncryptor_mB2BBCAB8753A59FFB572091D2EF80F287CD951BF,
	AesManaged_CreateEncryptor_m1E4EB80DE75FCF9E940228E1D7664C0EA1378153,
	AesManaged_Dispose_mB0D969841D51825F37095A93E73A50C15C1A1477,
	AesManaged_GenerateIV_mBB19651CC37782273A882055D4E63370268F2D91,
	AesManaged_GenerateKey_mF6673B955AE82377595277C6B78C7DA8A16F480E,
	AesCryptoServiceProvider__ctor_mA9857852BC34D8AB0F463C1AF1837CBBD9102265,
	AesCryptoServiceProvider_GenerateIV_m18539D5136BA9A2FC71F439150D16E35AD3BF5C4,
	AesCryptoServiceProvider_GenerateKey_m574F877FD23D1F07033FC035E89BE232303F3502,
	AesCryptoServiceProvider_CreateDecryptor_mAB5FB857F549A86D986461C8665BE6B2393305D1,
	AesCryptoServiceProvider_CreateEncryptor_m6BF20D5D8424DB627CD3010D9E4C8555C6BD0465,
	AesCryptoServiceProvider_get_IV_m6A46F1C255ABE41F98BEE8C0C37D6AFBB9F29D34,
	AesCryptoServiceProvider_set_IV_mCB88C0F651B17F3EC7575F16E14C9E3BD2DB24DB,
	AesCryptoServiceProvider_get_Key_mAC979BC922E8F1F15B36220E77972AC9CE5D5252,
	AesCryptoServiceProvider_set_Key_m65785032C270005BC120157A0C9D019F6F6BC96F,
	AesCryptoServiceProvider_get_KeySize_m3081171DF6C11CA55ECEBA29B9559D18E78D8058,
	AesCryptoServiceProvider_set_KeySize_mA994D2D3098216C0B8C4F02C0F0A0F63D4256218,
	AesCryptoServiceProvider_get_FeedbackSize_m9DC2E1C3E84CC674ADB2D7E6B06066F333BEC89D,
	AesCryptoServiceProvider_set_FeedbackSize_m5B367A05D9F985C7C83425637637B840858C255D,
	AesCryptoServiceProvider_get_Mode_m3E1CBFD4D7CE748F3AB615EB88DE1A5D7238285D,
	AesCryptoServiceProvider_set_Mode_mFE7044929761BABE312D1146B0ED51B331E35D63,
	AesCryptoServiceProvider_get_Padding_m89D49B05949BA2C6C557EFA5211B4934D279C7AD,
	AesCryptoServiceProvider_set_Padding_mD3353CD8F4B931AA00203000140520775643F96E,
	AesCryptoServiceProvider_CreateDecryptor_mB1F90A7339DA65542795E17DF9C37810BD088DDF,
	AesCryptoServiceProvider_CreateEncryptor_m9555DFFCA344DF06C8B88DDE2EB987B3958EC6BB,
	AesCryptoServiceProvider_Dispose_m7123198904819E2BF2B1398E20047B316C3D7D1E,
	AesTransform__ctor_m3903A599E8B2C3F7AB3B70E1258980151D639598,
	AesTransform_ECB_m2E2F4E2B307B0D34FEADF38684007E622FCEDFD1,
	AesTransform_SubByte_m2D77D545ABD3D84C04741B80ABB74BEFE8C55679,
	AesTransform_Encrypt128_m57DA74A7E05818DFD92F2614F8F65B0D1E696129,
	AesTransform_Decrypt128_m075F7BA40A4CFECA6F6A379065B731586EDDB23A,
	AesTransform__cctor_mAC6D46ED54345C2D23DFCA026C69029757222CFD,
	Error_ArgumentNull_m0EDA0D46D72CA692518E3E2EB75B48044D8FD41E,
	Error_MoreThanOneMatch_m4C4756AF34A76EF12F3B2B6D8C78DE547F0FBCF8,
	Error_NoElements_mB89E91246572F009281D79730950808F17C3F353,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[169] = 
{
	2715,
	1791,
	1747,
	1513,
	1758,
	1522,
	1758,
	1522,
	1747,
	1513,
	1747,
	1513,
	1747,
	1513,
	1758,
	758,
	1758,
	758,
	1541,
	1791,
	1791,
	1791,
	1791,
	1791,
	758,
	758,
	1758,
	1522,
	1758,
	1522,
	1747,
	1513,
	1747,
	1513,
	1747,
	1513,
	1747,
	1513,
	1758,
	1758,
	1541,
	442,
	972,
	1167,
	652,
	652,
	2828,
	2715,
	2813,
	2813,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[43] = 
{
	{ 0x02000008, { 70, 4 } },
	{ 0x02000009, { 74, 9 } },
	{ 0x0200000A, { 85, 7 } },
	{ 0x0200000B, { 94, 10 } },
	{ 0x0200000C, { 106, 11 } },
	{ 0x0200000D, { 120, 9 } },
	{ 0x0200000E, { 132, 12 } },
	{ 0x0200000F, { 147, 1 } },
	{ 0x02000010, { 148, 2 } },
	{ 0x02000012, { 150, 3 } },
	{ 0x02000013, { 155, 5 } },
	{ 0x02000014, { 160, 7 } },
	{ 0x02000015, { 167, 3 } },
	{ 0x02000016, { 170, 7 } },
	{ 0x02000017, { 177, 4 } },
	{ 0x02000018, { 181, 21 } },
	{ 0x0200001A, { 202, 2 } },
	{ 0x06000033, { 0, 10 } },
	{ 0x06000034, { 10, 10 } },
	{ 0x06000035, { 20, 5 } },
	{ 0x06000036, { 25, 5 } },
	{ 0x06000037, { 30, 2 } },
	{ 0x06000038, { 32, 2 } },
	{ 0x06000039, { 34, 1 } },
	{ 0x0600003A, { 35, 1 } },
	{ 0x0600003B, { 36, 5 } },
	{ 0x0600003C, { 41, 3 } },
	{ 0x0600003D, { 44, 2 } },
	{ 0x0600003E, { 46, 4 } },
	{ 0x0600003F, { 50, 4 } },
	{ 0x06000040, { 54, 3 } },
	{ 0x06000041, { 57, 1 } },
	{ 0x06000042, { 58, 3 } },
	{ 0x06000043, { 61, 2 } },
	{ 0x06000044, { 63, 2 } },
	{ 0x06000045, { 65, 5 } },
	{ 0x06000055, { 83, 2 } },
	{ 0x0600005A, { 92, 2 } },
	{ 0x0600005F, { 104, 2 } },
	{ 0x06000065, { 117, 3 } },
	{ 0x0600006A, { 129, 3 } },
	{ 0x0600006F, { 144, 3 } },
	{ 0x06000079, { 153, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[204] = 
{
	{ (Il2CppRGCTXDataType)2, 1615 },
	{ (Il2CppRGCTXDataType)3, 5359 },
	{ (Il2CppRGCTXDataType)2, 2576 },
	{ (Il2CppRGCTXDataType)2, 2224 },
	{ (Il2CppRGCTXDataType)3, 9334 },
	{ (Il2CppRGCTXDataType)2, 1703 },
	{ (Il2CppRGCTXDataType)2, 2231 },
	{ (Il2CppRGCTXDataType)3, 9352 },
	{ (Il2CppRGCTXDataType)2, 2226 },
	{ (Il2CppRGCTXDataType)3, 9341 },
	{ (Il2CppRGCTXDataType)2, 1616 },
	{ (Il2CppRGCTXDataType)3, 5360 },
	{ (Il2CppRGCTXDataType)2, 2592 },
	{ (Il2CppRGCTXDataType)2, 2233 },
	{ (Il2CppRGCTXDataType)3, 9359 },
	{ (Il2CppRGCTXDataType)2, 1719 },
	{ (Il2CppRGCTXDataType)2, 2241 },
	{ (Il2CppRGCTXDataType)3, 9378 },
	{ (Il2CppRGCTXDataType)2, 2237 },
	{ (Il2CppRGCTXDataType)3, 9368 },
	{ (Il2CppRGCTXDataType)2, 519 },
	{ (Il2CppRGCTXDataType)3, 94 },
	{ (Il2CppRGCTXDataType)3, 95 },
	{ (Il2CppRGCTXDataType)2, 1027 },
	{ (Il2CppRGCTXDataType)3, 4214 },
	{ (Il2CppRGCTXDataType)2, 520 },
	{ (Il2CppRGCTXDataType)3, 98 },
	{ (Il2CppRGCTXDataType)3, 99 },
	{ (Il2CppRGCTXDataType)2, 1034 },
	{ (Il2CppRGCTXDataType)3, 4216 },
	{ (Il2CppRGCTXDataType)2, 1950 },
	{ (Il2CppRGCTXDataType)3, 7836 },
	{ (Il2CppRGCTXDataType)2, 1951 },
	{ (Il2CppRGCTXDataType)3, 7837 },
	{ (Il2CppRGCTXDataType)3, 4621 },
	{ (Il2CppRGCTXDataType)3, 10836 },
	{ (Il2CppRGCTXDataType)3, 3801 },
	{ (Il2CppRGCTXDataType)2, 923 },
	{ (Il2CppRGCTXDataType)2, 1274 },
	{ (Il2CppRGCTXDataType)2, 1358 },
	{ (Il2CppRGCTXDataType)2, 1431 },
	{ (Il2CppRGCTXDataType)2, 621 },
	{ (Il2CppRGCTXDataType)3, 879 },
	{ (Il2CppRGCTXDataType)3, 880 },
	{ (Il2CppRGCTXDataType)2, 1704 },
	{ (Il2CppRGCTXDataType)3, 5997 },
	{ (Il2CppRGCTXDataType)2, 1523 },
	{ (Il2CppRGCTXDataType)2, 1169 },
	{ (Il2CppRGCTXDataType)2, 1278 },
	{ (Il2CppRGCTXDataType)2, 1359 },
	{ (Il2CppRGCTXDataType)2, 1524 },
	{ (Il2CppRGCTXDataType)2, 1170 },
	{ (Il2CppRGCTXDataType)2, 1279 },
	{ (Il2CppRGCTXDataType)2, 1360 },
	{ (Il2CppRGCTXDataType)2, 1280 },
	{ (Il2CppRGCTXDataType)2, 1361 },
	{ (Il2CppRGCTXDataType)3, 4215 },
	{ (Il2CppRGCTXDataType)2, 1269 },
	{ (Il2CppRGCTXDataType)2, 1270 },
	{ (Il2CppRGCTXDataType)2, 1356 },
	{ (Il2CppRGCTXDataType)3, 4213 },
	{ (Il2CppRGCTXDataType)2, 1168 },
	{ (Il2CppRGCTXDataType)2, 1277 },
	{ (Il2CppRGCTXDataType)2, 1167 },
	{ (Il2CppRGCTXDataType)3, 10816 },
	{ (Il2CppRGCTXDataType)3, 3800 },
	{ (Il2CppRGCTXDataType)2, 922 },
	{ (Il2CppRGCTXDataType)2, 1272 },
	{ (Il2CppRGCTXDataType)2, 1357 },
	{ (Il2CppRGCTXDataType)2, 1430 },
	{ (Il2CppRGCTXDataType)3, 5361 },
	{ (Il2CppRGCTXDataType)3, 5363 },
	{ (Il2CppRGCTXDataType)2, 328 },
	{ (Il2CppRGCTXDataType)3, 5362 },
	{ (Il2CppRGCTXDataType)3, 5371 },
	{ (Il2CppRGCTXDataType)2, 1619 },
	{ (Il2CppRGCTXDataType)2, 2227 },
	{ (Il2CppRGCTXDataType)3, 9342 },
	{ (Il2CppRGCTXDataType)3, 5372 },
	{ (Il2CppRGCTXDataType)2, 1317 },
	{ (Il2CppRGCTXDataType)2, 1384 },
	{ (Il2CppRGCTXDataType)3, 4222 },
	{ (Il2CppRGCTXDataType)3, 10807 },
	{ (Il2CppRGCTXDataType)2, 2238 },
	{ (Il2CppRGCTXDataType)3, 9369 },
	{ (Il2CppRGCTXDataType)3, 5364 },
	{ (Il2CppRGCTXDataType)2, 1618 },
	{ (Il2CppRGCTXDataType)2, 2225 },
	{ (Il2CppRGCTXDataType)3, 9335 },
	{ (Il2CppRGCTXDataType)3, 4221 },
	{ (Il2CppRGCTXDataType)3, 5365 },
	{ (Il2CppRGCTXDataType)3, 10806 },
	{ (Il2CppRGCTXDataType)2, 2234 },
	{ (Il2CppRGCTXDataType)3, 9360 },
	{ (Il2CppRGCTXDataType)3, 5378 },
	{ (Il2CppRGCTXDataType)2, 1620 },
	{ (Il2CppRGCTXDataType)2, 2232 },
	{ (Il2CppRGCTXDataType)3, 9353 },
	{ (Il2CppRGCTXDataType)3, 6039 },
	{ (Il2CppRGCTXDataType)3, 3119 },
	{ (Il2CppRGCTXDataType)3, 4223 },
	{ (Il2CppRGCTXDataType)3, 3118 },
	{ (Il2CppRGCTXDataType)3, 5379 },
	{ (Il2CppRGCTXDataType)3, 10808 },
	{ (Il2CppRGCTXDataType)2, 2242 },
	{ (Il2CppRGCTXDataType)3, 9379 },
	{ (Il2CppRGCTXDataType)3, 5392 },
	{ (Il2CppRGCTXDataType)2, 1622 },
	{ (Il2CppRGCTXDataType)2, 2240 },
	{ (Il2CppRGCTXDataType)3, 9371 },
	{ (Il2CppRGCTXDataType)3, 5393 },
	{ (Il2CppRGCTXDataType)2, 1320 },
	{ (Il2CppRGCTXDataType)2, 1387 },
	{ (Il2CppRGCTXDataType)3, 4227 },
	{ (Il2CppRGCTXDataType)3, 4226 },
	{ (Il2CppRGCTXDataType)2, 2229 },
	{ (Il2CppRGCTXDataType)3, 9344 },
	{ (Il2CppRGCTXDataType)3, 10811 },
	{ (Il2CppRGCTXDataType)2, 2239 },
	{ (Il2CppRGCTXDataType)3, 9370 },
	{ (Il2CppRGCTXDataType)3, 5385 },
	{ (Il2CppRGCTXDataType)2, 1621 },
	{ (Il2CppRGCTXDataType)2, 2236 },
	{ (Il2CppRGCTXDataType)3, 9362 },
	{ (Il2CppRGCTXDataType)3, 4225 },
	{ (Il2CppRGCTXDataType)3, 4224 },
	{ (Il2CppRGCTXDataType)3, 5386 },
	{ (Il2CppRGCTXDataType)2, 2228 },
	{ (Il2CppRGCTXDataType)3, 9343 },
	{ (Il2CppRGCTXDataType)3, 10810 },
	{ (Il2CppRGCTXDataType)2, 2235 },
	{ (Il2CppRGCTXDataType)3, 9361 },
	{ (Il2CppRGCTXDataType)3, 5399 },
	{ (Il2CppRGCTXDataType)2, 1623 },
	{ (Il2CppRGCTXDataType)2, 2244 },
	{ (Il2CppRGCTXDataType)3, 9381 },
	{ (Il2CppRGCTXDataType)3, 6040 },
	{ (Il2CppRGCTXDataType)3, 3121 },
	{ (Il2CppRGCTXDataType)3, 4229 },
	{ (Il2CppRGCTXDataType)3, 4228 },
	{ (Il2CppRGCTXDataType)3, 3120 },
	{ (Il2CppRGCTXDataType)3, 5400 },
	{ (Il2CppRGCTXDataType)2, 2230 },
	{ (Il2CppRGCTXDataType)3, 9345 },
	{ (Il2CppRGCTXDataType)3, 10812 },
	{ (Il2CppRGCTXDataType)2, 2243 },
	{ (Il2CppRGCTXDataType)3, 9380 },
	{ (Il2CppRGCTXDataType)3, 4219 },
	{ (Il2CppRGCTXDataType)3, 4220 },
	{ (Il2CppRGCTXDataType)3, 4230 },
	{ (Il2CppRGCTXDataType)2, 521 },
	{ (Il2CppRGCTXDataType)3, 102 },
	{ (Il2CppRGCTXDataType)3, 7820 },
	{ (Il2CppRGCTXDataType)2, 1952 },
	{ (Il2CppRGCTXDataType)3, 7838 },
	{ (Il2CppRGCTXDataType)2, 622 },
	{ (Il2CppRGCTXDataType)3, 881 },
	{ (Il2CppRGCTXDataType)3, 7826 },
	{ (Il2CppRGCTXDataType)3, 3090 },
	{ (Il2CppRGCTXDataType)2, 349 },
	{ (Il2CppRGCTXDataType)3, 7821 },
	{ (Il2CppRGCTXDataType)2, 1947 },
	{ (Il2CppRGCTXDataType)3, 998 },
	{ (Il2CppRGCTXDataType)2, 643 },
	{ (Il2CppRGCTXDataType)2, 904 },
	{ (Il2CppRGCTXDataType)3, 3099 },
	{ (Il2CppRGCTXDataType)3, 7822 },
	{ (Il2CppRGCTXDataType)3, 3085 },
	{ (Il2CppRGCTXDataType)3, 3086 },
	{ (Il2CppRGCTXDataType)3, 3084 },
	{ (Il2CppRGCTXDataType)3, 3087 },
	{ (Il2CppRGCTXDataType)2, 900 },
	{ (Il2CppRGCTXDataType)2, 2639 },
	{ (Il2CppRGCTXDataType)3, 4218 },
	{ (Il2CppRGCTXDataType)3, 3089 },
	{ (Il2CppRGCTXDataType)2, 1254 },
	{ (Il2CppRGCTXDataType)3, 3088 },
	{ (Il2CppRGCTXDataType)2, 1173 },
	{ (Il2CppRGCTXDataType)2, 2597 },
	{ (Il2CppRGCTXDataType)2, 1294 },
	{ (Il2CppRGCTXDataType)2, 1364 },
	{ (Il2CppRGCTXDataType)3, 3817 },
	{ (Il2CppRGCTXDataType)2, 931 },
	{ (Il2CppRGCTXDataType)3, 4490 },
	{ (Il2CppRGCTXDataType)3, 4491 },
	{ (Il2CppRGCTXDataType)3, 4496 },
	{ (Il2CppRGCTXDataType)2, 1439 },
	{ (Il2CppRGCTXDataType)3, 4493 },
	{ (Il2CppRGCTXDataType)3, 11103 },
	{ (Il2CppRGCTXDataType)2, 905 },
	{ (Il2CppRGCTXDataType)3, 3112 },
	{ (Il2CppRGCTXDataType)1, 1249 },
	{ (Il2CppRGCTXDataType)2, 2606 },
	{ (Il2CppRGCTXDataType)3, 4492 },
	{ (Il2CppRGCTXDataType)1, 2606 },
	{ (Il2CppRGCTXDataType)1, 1439 },
	{ (Il2CppRGCTXDataType)2, 2657 },
	{ (Il2CppRGCTXDataType)2, 2606 },
	{ (Il2CppRGCTXDataType)3, 4497 },
	{ (Il2CppRGCTXDataType)3, 4495 },
	{ (Il2CppRGCTXDataType)3, 4494 },
	{ (Il2CppRGCTXDataType)2, 231 },
	{ (Il2CppRGCTXDataType)3, 3122 },
	{ (Il2CppRGCTXDataType)2, 337 },
};
extern const CustomAttributesCacheGenerator g_System_Core_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_System_Core_CodeGenModule;
const Il2CppCodeGenModule g_System_Core_CodeGenModule = 
{
	"System.Core.dll",
	169,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	43,
	s_rgctxIndices,
	204,
	s_rgctxValues,
	NULL,
	g_System_Core_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
