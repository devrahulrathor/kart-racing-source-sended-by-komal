﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void WebSocket::.ctor(System.Uri,System.String)
extern void WebSocket__ctor_m05BADE388D0A7198CEE6097CB434A3CB4A3D627E (void);
// 0x00000002 System.Void WebSocket::SendString(System.String)
extern void WebSocket_SendString_m0DECEC3E21537D68C1E6FEB61D9B0D0EF230B0E3 (void);
// 0x00000003 System.String WebSocket::RecvString()
extern void WebSocket_RecvString_m1295B63CFF616D99B53FE5741D5DCECDB1BED943 (void);
// 0x00000004 System.Int32 WebSocket::SocketCreate(System.String,System.String)
extern void WebSocket_SocketCreate_m5A5FF318631802AEF0C1BA821DE3A0B3CB27C2D9 (void);
// 0x00000005 System.Int32 WebSocket::SocketState(System.Int32)
extern void WebSocket_SocketState_m76D79D73432B2C40AF3EE9848A090DE8AE77F0D6 (void);
// 0x00000006 System.Void WebSocket::SocketSend(System.Int32,System.Byte[],System.Int32)
extern void WebSocket_SocketSend_mD75D3688865C507B2D3C9DFD57FA46002BFAE70D (void);
// 0x00000007 System.Void WebSocket::SocketRecv(System.Int32,System.Byte[],System.Int32)
extern void WebSocket_SocketRecv_m1449A24F435FB7DFF61B20DA2018CC2110802FF3 (void);
// 0x00000008 System.Int32 WebSocket::SocketRecvLength(System.Int32)
extern void WebSocket_SocketRecvLength_m093296413425F357AD9DF189C79F6D9919DF5085 (void);
// 0x00000009 System.Void WebSocket::SocketClose(System.Int32)
extern void WebSocket_SocketClose_m50A2AF74741E8AB8D0649C8A4674FE034D4AEBB6 (void);
// 0x0000000A System.Int32 WebSocket::SocketError(System.Int32,System.Byte[],System.Int32)
extern void WebSocket_SocketError_mFB5CF493364D4CAA8ED6B6CB43AEFD1FA5861D8B (void);
// 0x0000000B System.Void WebSocket::Send(System.Byte[])
extern void WebSocket_Send_m8107DD1FCD7C5EB882B0BCF26B23503579D58105 (void);
// 0x0000000C System.Byte[] WebSocket::Recv()
extern void WebSocket_Recv_mF846FE0646A81AA27C6486FFA30DD1DF68B9CB2D (void);
// 0x0000000D System.Void WebSocket::Connect()
extern void WebSocket_Connect_m88A244A8C13AFA2C0BC8211EA2FE26D2220A6D1C (void);
// 0x0000000E System.Void WebSocket::Close()
extern void WebSocket_Close_m1547F56D350FD212CEBDBD2D3CCE2EFF998D4A6C (void);
// 0x0000000F System.Boolean WebSocket::get_Connected()
extern void WebSocket_get_Connected_m96B07C37B39646DD9DDDD43C4F408D04C7F9C9A4 (void);
// 0x00000010 System.String WebSocket::get_Error()
extern void WebSocket_get_Error_mDC0B5839A6888F5FACC303A7D04AB9E8FC7C4F6D (void);
// 0x00000011 System.Boolean ExitGames.Client.Photon.WaitForRealSeconds::get_keepWaiting()
extern void WaitForRealSeconds_get_keepWaiting_mAE34CA5AA8EE7368021C25DEAF73E0D5B98F6271 (void);
// 0x00000012 System.Void ExitGames.Client.Photon.WaitForRealSeconds::.ctor(System.Single)
extern void WaitForRealSeconds__ctor_mE061BAD3F4554FE37FA9A262E418271176D130D8 (void);
// 0x00000013 System.Void ExitGames.Client.Photon.SocketWebTcp::.ctor(ExitGames.Client.Photon.PeerBase)
extern void SocketWebTcp__ctor_m23280872DDFD1FBA4B45B86446BFA9E832030B2C (void);
// 0x00000014 System.Void ExitGames.Client.Photon.SocketWebTcp::Dispose()
extern void SocketWebTcp_Dispose_mFF67BAD2FCEB792F4FE0AC403178E0B73AF87364 (void);
// 0x00000015 System.Boolean ExitGames.Client.Photon.SocketWebTcp::Connect()
extern void SocketWebTcp_Connect_m427C9760B88579087F024DF9DD1CA44C49E59720 (void);
// 0x00000016 System.Boolean ExitGames.Client.Photon.SocketWebTcp::Disconnect()
extern void SocketWebTcp_Disconnect_mBA913FF125EB470247A77A3AF3ACA973544FADE1 (void);
// 0x00000017 ExitGames.Client.Photon.PhotonSocketError ExitGames.Client.Photon.SocketWebTcp::Send(System.Byte[],System.Int32)
extern void SocketWebTcp_Send_mA61435C035826BF7DEC25789BEA4129B0C8E53E8 (void);
// 0x00000018 ExitGames.Client.Photon.PhotonSocketError ExitGames.Client.Photon.SocketWebTcp::Receive(System.Byte[]&)
extern void SocketWebTcp_Receive_mDE5A8B9E2F6AD880879660E209685A3C43E72187 (void);
// 0x00000019 System.Collections.IEnumerator ExitGames.Client.Photon.SocketWebTcp::ReceiveLoop()
extern void SocketWebTcp_ReceiveLoop_m4BBE1E00B12066024C6EB467A8C6FA8BE29F01C6 (void);
// 0x0000001A System.Void ExitGames.Client.Photon.SocketWebTcp::.cctor()
extern void SocketWebTcp__cctor_m6B06D719BA1554C65ACCE86863BC055F5D467D48 (void);
// 0x0000001B System.Void ExitGames.Client.Photon.SocketWebTcp/MonoBehaviourExt::.ctor()
extern void MonoBehaviourExt__ctor_m13ECD342BD4BBC81797F66E32B569FA792C252CB (void);
// 0x0000001C System.Void ExitGames.Client.Photon.SocketWebTcp/<ReceiveLoop>d__13::.ctor(System.Int32)
extern void U3CReceiveLoopU3Ed__13__ctor_m0EF8F69A80E082865CAB6D2B4BF4D0A8A606CA3D (void);
// 0x0000001D System.Void ExitGames.Client.Photon.SocketWebTcp/<ReceiveLoop>d__13::System.IDisposable.Dispose()
extern void U3CReceiveLoopU3Ed__13_System_IDisposable_Dispose_mAC59C6201F6C37010150E982C8BB120C43E7A748 (void);
// 0x0000001E System.Boolean ExitGames.Client.Photon.SocketWebTcp/<ReceiveLoop>d__13::MoveNext()
extern void U3CReceiveLoopU3Ed__13_MoveNext_mF3CECCFC538BD33808CE2E287621607DCE083366 (void);
// 0x0000001F System.Object ExitGames.Client.Photon.SocketWebTcp/<ReceiveLoop>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CReceiveLoopU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m74753CEFB2989A1CCF7602AE3542FEBDB49E8496 (void);
// 0x00000020 System.Void ExitGames.Client.Photon.SocketWebTcp/<ReceiveLoop>d__13::System.Collections.IEnumerator.Reset()
extern void U3CReceiveLoopU3Ed__13_System_Collections_IEnumerator_Reset_mEA84328CEFFE5632EDE8A28A3D7943960B16314C (void);
// 0x00000021 System.Object ExitGames.Client.Photon.SocketWebTcp/<ReceiveLoop>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CReceiveLoopU3Ed__13_System_Collections_IEnumerator_get_Current_m1EDB654BC21E51AFF3A9A0A52738DF3DD8377F0D (void);
static Il2CppMethodPointer s_methodPointers[33] = 
{
	WebSocket__ctor_m05BADE388D0A7198CEE6097CB434A3CB4A3D627E,
	WebSocket_SendString_m0DECEC3E21537D68C1E6FEB61D9B0D0EF230B0E3,
	WebSocket_RecvString_m1295B63CFF616D99B53FE5741D5DCECDB1BED943,
	WebSocket_SocketCreate_m5A5FF318631802AEF0C1BA821DE3A0B3CB27C2D9,
	WebSocket_SocketState_m76D79D73432B2C40AF3EE9848A090DE8AE77F0D6,
	WebSocket_SocketSend_mD75D3688865C507B2D3C9DFD57FA46002BFAE70D,
	WebSocket_SocketRecv_m1449A24F435FB7DFF61B20DA2018CC2110802FF3,
	WebSocket_SocketRecvLength_m093296413425F357AD9DF189C79F6D9919DF5085,
	WebSocket_SocketClose_m50A2AF74741E8AB8D0649C8A4674FE034D4AEBB6,
	WebSocket_SocketError_mFB5CF493364D4CAA8ED6B6CB43AEFD1FA5861D8B,
	WebSocket_Send_m8107DD1FCD7C5EB882B0BCF26B23503579D58105,
	WebSocket_Recv_mF846FE0646A81AA27C6486FFA30DD1DF68B9CB2D,
	WebSocket_Connect_m88A244A8C13AFA2C0BC8211EA2FE26D2220A6D1C,
	WebSocket_Close_m1547F56D350FD212CEBDBD2D3CCE2EFF998D4A6C,
	WebSocket_get_Connected_m96B07C37B39646DD9DDDD43C4F408D04C7F9C9A4,
	WebSocket_get_Error_mDC0B5839A6888F5FACC303A7D04AB9E8FC7C4F6D,
	WaitForRealSeconds_get_keepWaiting_mAE34CA5AA8EE7368021C25DEAF73E0D5B98F6271,
	WaitForRealSeconds__ctor_mE061BAD3F4554FE37FA9A262E418271176D130D8,
	SocketWebTcp__ctor_m23280872DDFD1FBA4B45B86446BFA9E832030B2C,
	SocketWebTcp_Dispose_mFF67BAD2FCEB792F4FE0AC403178E0B73AF87364,
	SocketWebTcp_Connect_m427C9760B88579087F024DF9DD1CA44C49E59720,
	SocketWebTcp_Disconnect_mBA913FF125EB470247A77A3AF3ACA973544FADE1,
	SocketWebTcp_Send_mA61435C035826BF7DEC25789BEA4129B0C8E53E8,
	SocketWebTcp_Receive_mDE5A8B9E2F6AD880879660E209685A3C43E72187,
	SocketWebTcp_ReceiveLoop_m4BBE1E00B12066024C6EB467A8C6FA8BE29F01C6,
	SocketWebTcp__cctor_m6B06D719BA1554C65ACCE86863BC055F5D467D48,
	MonoBehaviourExt__ctor_m13ECD342BD4BBC81797F66E32B569FA792C252CB,
	U3CReceiveLoopU3Ed__13__ctor_m0EF8F69A80E082865CAB6D2B4BF4D0A8A606CA3D,
	U3CReceiveLoopU3Ed__13_System_IDisposable_Dispose_mAC59C6201F6C37010150E982C8BB120C43E7A748,
	U3CReceiveLoopU3Ed__13_MoveNext_mF3CECCFC538BD33808CE2E287621607DCE083366,
	U3CReceiveLoopU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m74753CEFB2989A1CCF7602AE3542FEBDB49E8496,
	U3CReceiveLoopU3Ed__13_System_Collections_IEnumerator_Reset_mEA84328CEFFE5632EDE8A28A3D7943960B16314C,
	U3CReceiveLoopU3Ed__13_System_Collections_IEnumerator_get_Current_m1EDB654BC21E51AFF3A9A0A52738DF3DD8377F0D,
};
static const int32_t s_InvokerIndices[33] = 
{
	972,
	1522,
	1758,
	2441,
	2668,
	2369,
	2369,
	2668,
	2774,
	2232,
	1522,
	1758,
	1791,
	1791,
	1778,
	1758,
	1778,
	1543,
	1522,
	1791,
	1778,
	1778,
	712,
	1147,
	1758,
	2828,
	1791,
	1513,
	1791,
	1778,
	1758,
	1791,
	1758,
};
extern const CustomAttributesCacheGenerator g_PhotonWebSocket_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_PhotonWebSocket_CodeGenModule;
const Il2CppCodeGenModule g_PhotonWebSocket_CodeGenModule = 
{
	"PhotonWebSocket.dll",
	33,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_PhotonWebSocket_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
