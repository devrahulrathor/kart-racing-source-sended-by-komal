﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LaunchManager : MonoBehaviourPunCallbacks {

    byte maxPlayersPerRoom = 2;
    bool isConnecting;

    public InputField playerName;
    public Text feedbackText;
    string gameVersion = "0.1";

    private void Awake() {

        PhotonNetwork.AutomaticallySyncScene = true;

        if (PlayerPrefs.HasKey("PlayerName")) {
            playerName.text = PlayerPrefs.GetString("PlayerName");
        }
    }
    private void Update()
    {
        if(PhotonNetwork.CurrentRoom != null)
        if (PhotonNetwork.CurrentRoom.PlayerCount > 1)
        {
            Debug.LogError("load level ");
            PhotonNetwork.LoadLevel("Track01");
        }
    }
    public void ConnectNetwork() {

        feedbackText.text = "";
        isConnecting = true;

        PhotonNetwork.NickName = playerName.text;
        if (PhotonNetwork.IsConnected) {

            feedbackText.text += "\nJoining Room...";
            PhotonNetwork.JoinRandomRoom();
        } else {

            feedbackText.text += "\nConnecting...";
            PhotonNetwork.GameVersion = gameVersion;
            //PhotonNetwork.
            
            var connect=PhotonNetwork.ConnectUsingSettings();
            Debug.Log(connect);
        }
    }

    public void SetName(string name) {

        PlayerPrefs.SetString("PlayerName", name);
    }

    public void ConnectSingle() {

        SceneManager.LoadScene("Track01");
    }

    //////////////////// NETWORK CALLBACKS ////////////////////
    public override void OnConnectedToMaster() {

        if (isConnecting) {

            feedbackText.text += "\nOnConnectedToMaster...";
            PhotonNetwork.JoinRandomRoom();
        }
    }

    public override void OnJoinRandomFailed(short returnCode, string message) {

        feedbackText.text += "\nFailed to join random room.";
        Debug.Log("failed("+returnCode+") to joiin random room = " + message);
        PhotonNetwork.CreateRoom(null, new RoomOptions { MaxPlayers = this.maxPlayersPerRoom });
    }

    public override void OnDisconnected(DisconnectCause cause) {

        feedbackText.text += "\nDisconnected because " + cause;
        isConnecting = false;
    }
    public override void OnCreatedRoom()
    {
        Debug.LogError("created room = " + PhotonNetwork.CurrentRoom.Name);
        base.OnCreatedRoom();
    }
    public override void OnJoinedRoom() {

        feedbackText.text += "\nJoined Room with " + PhotonNetwork.CurrentRoom.PlayerCount + " players.";
        Debug.LogError("Joined room = "+PhotonNetwork.CurrentRoom.Name);
        //if (PhotonNetwork.CurrentRoom.PlayerCount > 1)
        //{
            PhotonNetwork.LoadLevel("Track01");
        //}
        //else
        //{
        //    StartCoroutine(loadscene());
        //}
    }
    IEnumerator loadscene()
    {
        yield return new WaitForSeconds(70);
        PhotonNetwork.LoadLevel("Track01");
    }
}
