﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class RightBtn : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public static bool Right_pressed = false;
    // Start is called before the first frame update
    //bool ispressed = false;
    public void OnPointerDown(PointerEventData eventData)
    {
        Right_pressed = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        Right_pressed = false;
    }
}
