﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class breakbtn : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public static bool Break_pressed=false;
    // Start is called before the first frame update
    //bool ispressed = false;
    public void OnPointerDown(PointerEventData eventData)
    {
        Break_pressed = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        Break_pressed = false;
    }
}
